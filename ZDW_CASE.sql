-- ZDW_CASE:    Case extract for reporting
-- Summary:     Provides a view of all cases and their status, assigned to, last action etc.
SELECT to_char(CURRENT_TIMESTAMP, 'dd/mm/yyyy hh24:mi:ss') AS "extract_timestamp",
    STAT.currentStatusTypeName AS "workflow",
    STAT.currentstatusstatusname AS "workflow_status",
    STAT.currentStatusArea AS "workflow_status_business_area",
    CASE
        WHEN ALLOC.allocatedtousername = '<None>' THEN ''
        ELSE ALLOC.allocatedtousername
    END AS "allocated_to_user",
    CASE
        WHEN DEPT.description = '<None>' THEN ''
        ELSE DEPT.description
    END AS "allocated_to_user_dept",
    CAS.caseidsequencenumber AS "case_id",
    CAS.status AS "status",
    CASE
        WHEN AGR.agreementnumber = '<None>' THEN ''
        ELSE AGR.agreementnumber
    END AS "agreement_number",
    CASE
        WHEN BILL.caseBillingUniqueRef = '<None>' THEN ''
        ELSE BILL.caseBillingUniqueRef
    END AS "third_party",
    to_char(CAS.openeddate, 'DD/MM/YYYY') AS "opened_date",
    to_char(CAS.openedtime, '00:00:00') AS "opened_time",
    to_char(CAS.lastactiondate, 'DD/MM/YYYY') AS "last_action_date",
    to_char(CAS.lastactiontime, '00:00:00') AS "last_action_time",
    CAS.etlLogId AS "etlLogId"
FROM OdsCase CAS
    LEFT JOIN OdsAgreement AGR ON AGR.id = CAS.agreementid
    JOIN OdsCurrentCaseStatus STAT ON STAT.id = CAS.currentstatusid
    JOIN OdsAllocatedToUser ALLOC ON ALLOC.id = CAS.allocatedtouserid
    JOIN OdsDepartment DEPT ON DEPT.id = ALLOC.allocatedToUserDepartmentId
    JOIN OdsCaseBilling BILL ON BILL.id = CAS.caseBillingId
    JOIN OdsSystemDate ON 1 = 1
WHERE STAT.currentstatustypecode IN ('ORG', 'UWR')
    AND CAS.lastactiondate >= systemdate - 1