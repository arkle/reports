-- CASHREC:     Cash Reconciliation Report v2.0
-- Title:       Cash Rec
-- Summary:     Extract of cash allocation transactions to support bank account reconciliation
SELECT DISTINCT
	INVCO.invCompanyName        AS "Invoice_company",
    BIL.uniqueRef    	 	 	AS "TP_Reference",
	AGR.agreementNumber      	AS "Agreement",
    INVCUS.invcusname           AS "Invoicing_Customer",
    CTX.inputdate               AS "Effective_Date",
    CTX.effectivedate           AS "Receipt_Date",
    cshAllcPaymentTypeCode      AS "Payment_Type_Code",
    cshAllcPaymentTypeName      AS "Payment_Type",
	CUR.cshAllcCurrencyCode		AS "Currency",
	CSH.amount             AS "Amount", 
    ORIGT.origCashTxnAlfaIdentifier AS "ID"
FROM OdsCashAllocation CAL
	JOIN OdsCashAllocationTxn CTX
		ON CAL.cashAllocationTransactionId = CTX.id
	JOIN OdsCashAllocationCurrency CUR
		ON CAL.allocationCurrencyId = CUR.id
	JOIN OdsAgreement AGR
		ON CAL.agreementId = AGR.id
	JOIN OdsBillingAddress BIL
		ON CAL.billingAddressId = BIL.id
    JOIN OdsInvoicingCompany INVCO 
        ON INVCO.id = AGR.invCompanyId
    JOIN OdsInvoicingCustomer INVCUS
        ON INVCUS.id = AGR.invcusid
	JOIN OdsCashInOut CSH
		ON CTX.cashId = CSH.id
	JOIN OdsCashAllocationPaymentType PYT
		ON CSH.paymentTypeId = PYT.id
         JOIN odsoriginatingtransaction ORIGT ON CSH.id = ORIGT.origCashTxnCashId 
	LEFT OUTER JOIN 
		(	SELECT 
				IVL.id AS invoiceLineId,
				CTP.code AS chargeCode,
				CTP.name AS chargeName
			FROM OdsInvoiceLine IVL
				JOIN OdsChargeType CTP
					ON IVL.chargeTypeId = CTP.id ) AS B
		ON CAL.invoiceLineId = B.invoicelineid
	LEFT OUTER JOIN 
		(	SELECT 
				SIL.id AS SupplierInvoiceLineId,
				CTP2.payableChargeTypeCode AS chargeCode,
				CTP2.payableChargeTypeName AS chargeName
			FROM OdsSupplierInvoiceLine SIL
				JOIN OdsPayableChargeType CTP2
					ON SIL.payableChargeTypeId= CTP2.id ) AS C
		ON CAL.supplierInvoiceLineId= C.SupplierInvoiceLineId
WHERE CAL.transactionStatus = 'Applied'
AND CSH.amount <> '0.00'
    AND (CTX.inputdate >= to_date(:Date_from, 'YYYY-MM-DD') 
        AND CTX.inputdate <= to_date(:Date_to, 'YYYY-MM-DD')) 
