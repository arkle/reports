-- ZDW_INCOME: Aggregates funding summary information relating to ALFA schedules.
SELECT AGR.agreementnumber AS "AGREEMENT",
    to_char(INCOME.periodStartDate, 'DD/MM/YYYY') AS "income_period_start",
    to_char(INCOME.periodEndDate, 'DD/MM/YYYY') AS "income_period_end",
    STD.accountingStandardCode AS "accounting_standard_code",
    STD.accountingStandardName AS "accounting_standard_name",
    INCOME.rentalAccrual AS "rental_accrual",
    INCOME.depreciation AS "depreciation",
    INCOME.impairmentAccrual AS "impairment_accrual",
    INCOME.preEarningsExpenses AS "pre_earnings_expenses",
    INCOME.earnings AS "earnings",
    INCOME.postEarningsExpenses AS "post_earnings_expenses",
    INCOME.postEarningsExpenses - DOCFEE.expenseIncomeAccrued AS "post_earnings_expense_excl_docfee",
    INCOME.profitBeforeTax AS "profit_before_tax",
    INCOME.netBookValue AS "net_book_value",
    INCOME.isRecognised AS "is_income_recognised",
    INCOME.isRentalRecognised AS "is_rental_recognised",
    INCOME.isInterestRecognised AS "is_interest_recognised",
    INCOME.isInterestRcgnsdAsSuspended AS "is_interest_recognised_as_suspended",
    INCOME.isDepreciationRecognised AS "is_depreciation_recognised",
    INCOME.isSuspended AS "is_suspended",
    INCOME.isTerminated AS "is_terminated",
    INCOME.isAccrued AS "is_accrued",
    INCOME.capitalOutstanding AS "capital_OS",
    INCOME.weightedAverageCapitalOS AS "weighted_avg_capital_OS"
FROM eodScheduleIncome INCOME
    LEFT JOIN (
        SELECT scheduleIncomeId,
            expenseIncomeAccrued
        FROM eodScheduleExpenseIncome A
            JOIN eodExpenseIncomeChargeType B ON A.expenseIncomeChargeTypeId = B.odsid
            AND B.expIncChargeTypeCode = '103'
    ) DOCFEE ON DOCFEE.scheduleIncomeId = INCOME.odsid
    JOIN eodAccountingStandard STD ON STD.odsid = INCOME.accountingStandardId
    JOIN eodScheduleMain SCHEDM ON SCHEDM.odsid = INCOME.scheduleId
    JOIN eodAgreement AGR ON AGR.odsid = SCHEDM.agreementid
    JOIN (
        SELECT max(etllogid) AS "from_etllogid"
        FROM eodPostingHeader
            JOIN eodsystemdate on 1 = 1
        WHERE postingDate = systemdate - 5
    ) ETLLOG ON 1 = 1
WHERE SCHEDM.etllogid > from_etllogid
ORDER BY AGR.agreementnumber, INCOME.periodStartDate