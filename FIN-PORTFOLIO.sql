-- FIN-PORTFOLIO:   Portfolio Report (Finance Version)
-- Title:           Portfolio Report (Finance Version)
-- Summary:         Provides details of the portfolio covering all proposals and agreements in the following statuses - Live_Primary, Live_Secondary, Terminated, Matured
-- IMPORTANT:       Changes made to this report should also be considered for the non-Finance equivalent
SELECT --  Company info  
    AGR.agreementNumber AS "Agreement",
    CASE
        WHEN PRACTICAL.agreementid IS NOT NULL THEN 'Practical DAC (Paid_Out)'
        WHEN SCHED.schedulestatus = 'Proposal'
        AND NTU.agreementid IS Not Null THEN 'Proposal (NTU Holding)'
        WHEN SCHED.schedulestatus = 'Proposal'
        AND NTUALERT.ntuflag = 'Y' THEN 'Proposal (NTU)'
        WHEN SCHED.schedulestatus = 'Proposal'
        AND ORG.agreementid IS Null THEN 'Proposal (Cancelled)'
        WHEN SCHED.schedulestatus LIKE 'Live%'
        AND PARTIAL.agreementid IS NOT Null THEN CONCAT(SCHED.schedulestatus, ' (Partially Terminated)')
        ELSE SCHED.scheduleStatus
    END AS "Schedule_Status",
    AGR.agreementType AS "Agreement_type",
    AGR.product AS "Product",
    CASE
        WHEN AGRCO.agrcompanycode IN ('2', '4') THEN 'Evington'
        ELSE 'Arkle'
    END AS "Company",
    CASE
        WHEN AGRCO.agrcompanycode = '1' THEN 'Core'
        WHEN AGRCO.agrcompanycode IN ('2', '4') THEN 'Consumer'
        WHEN AGRCO.agrcompanycode = '3' THEN 'Prime'
    END AS "Pricing",
    --  Broker info
    CASE
        WHEN DLRBILALT.dealerBillingName IS NOT NULL THEN DLRBILALT.dealerBillingName
        ELSE DLR.dealerName
    END AS "Broker",
    CASE
        WHEN BILSALPER.salesperson > '' THEN BILSALPER.salesperson
        ELSE SALPER.salesperson
    END AS "Broker_Manager",
    --  Customer info
    INVCUS.invCusThirdPartyNumber AS "Invoice_cust_id",
    INVCUS.invCusName AS "Invoice_cust_name",
    EXTREF.reference AS "Actor_Code",
    INVCUS.invCusCaisLegalEntityType AS "Invoice_cust_legal_type",
    INVCUS.invCusCompanyRegNo AS "Company_reg",
    --  Flag if the customer undisclosed
    CASE
        WHEN EXPCUS.expCusThirdPartyNumber = INVCUS.invCusThirdPartyNumber THEN ''
        ELSE 'Y'
    END AS "Undisclosed",
    EXPCUS.expCusName AS "Exposure_Customer",
    --  Flag if the customer is vulnerable or has an active complaint (based on the Invoicing customer)  
    CASE
        WHEN VULNERABLE.thirdpartyid > 0 THEN 'Y'
        ELSE ''
    END AS "Vulnerable",
    CASE
        WHEN COMPLAINT.thirdpartyid > 0 THEN 'Y'
        ELSE ''
    END AS "Complaint_open",
    --  Agreement information                                           
    CASE
        WHEN MISC.agrid = '<None>' THEN ''
        ELSE MISC.agrid
    END AS "Alternative_Agreement",
    MIGALERT.migflag AS "Migrated",
    SCHED.scheduleRef AS "Schedule_ref",
    SCHED.scheduleDescription AS "Schedule_description",
    CASE
        WHEN AGR.isCcaRegulated = '1' THEN 'Y'
        ELSE ''
    END AS "Regulated",
    CASE
        WHEN AGR.hasGuarantee = '1' THEN 'Y'
        ELSE ''
    END AS "Guarantee",
    --  Asset information
    ASSET.assetTypeName AS "Asset_type",
    ASSET.assetDescription AS "Asset_description",
    EXPCUS.expCusIndustryCode AS "Sic_Code",
    EXPCUS.expCusIndustryName AS "Sic_Code_Description",
    CASE
        WHEN MISC.oaf IS NOT NULL THEN MISC.oaf
        ELSE SCHED.totalAssetCostFinanced
    END AS "Amount_financed",
    CASE
        WHEN AGR.product = 'STG' THEN DRAWDOWN.drawdown_amount + DRAWDOWN.drawdown_vat - DRAWDOWN.drawdown_deposit
        WHEN MISC.oaf IS NOT NULL THEN MISC.oaf
        ELSE SCHED.totalAssetCostFinanced
    END AS "Draw_down_to_date",
    SCHED.totalResidualValue AS "RV",
    SCHED.balloonAmount AS "Balloon",
    SCHED.balloondate AS "Balloon_Date",
    BALLPMT.balloonPaymentTypeCode AS "Balloon_Payment_Type",
    DOCFEE.documentation_fee AS "Document_fee",
    DOCFEESPLIT.documentation_fee_share AS "Document_fee_share",
    OTPFEE.option_to_purchase_fee AS "OTP_fee",
    --  Agreement details
    SCHED.termInMonths AS "Term",
    CASE
        WHEN PRACTICAL.agreementid IS NOT NULL THEN SCHED.startDate
        WHEN MISC.ors IS NOT NULL THEN MISC.ors
        ELSE SCHED.activationDate
    END AS "Activated_date",
    CASE
        WHEN MISC.ors IS NOT NULL THEN MISC.ors
        ELSE SCHED.startDate
    END AS "Start_date",
    SCHED.expectedEndDate AS "Expected_end_date",
    SCHED.maturityDate AS "Maturity_date",
    SCHED.secondaryPeriodExpiryDate AS "Secondary_expiry",
    ACTUSER.activatedByUserName AS "Activated_by",
    CASE
        WHEN SCHED.isTerminated = '1' THEN 'Y'
        ELSE ''
    END AS "Terminated",
    --  Termination details
    SCHED.terminationDate AS "Terminated_date",
    SCHED.terminationNumber AS "Termination_number",
    SCHED.terminationType AS "Termination_type",
    SCHED.terminationReason AS "Termination_reason",
    SCHED.terminationAmount AS "Amount_Terminated",
    SCHED.disposalAmount AS "Asset_disposal_proceeds",
    SCHED.totalWriteOffs AS "Total_Write_Offs",
    --  Rates
    ROUND(
        (
            COMM.commission / SCHED.originalAssetCostFinanced
        ) * 100,
        2
    ) AS "Commission_pct",
    COMM.commission AS "Commission",
    CASE
        WHEN SCHED.scheduleStatus IN ('Terminated', 'Matured') THEN 0
        ELSE COMMOS.oscomm
    END AS "Commission_OS",
    SCHED.currentFundingRate AS "Current_funding_rate",
    CASE
        WHEN MISC.ory IS NOT NULL THEN MISC.ory / 100
        ELSE SCHED.combinedLessorPtoRate / 100
    END AS "Yield",
    --  Current balances
    SCHED.totalPrimaryRentals AS "Total_Primary_Rentals",
    SCHED.totalSecondaryRentals AS "Total_Secondary_Rentals",
    SCHED.totalExposure AS "Total_Exposure",
    SCHED.grossBalanceRemaining AS "Balance_OS",
    CASE
        WHEN SCHED.scheduleStatus IN ('Terminated', 'Matured') THEN 0
        ELSE SCHED.capitalOutstanding
    END AS "Capital_OS_Lessee",
    CASE
        WHEN SCHED.scheduleStatus IN ('Terminated', 'Matured') THEN 0
        ELSE INTEREST_LESSEE.interest_os_lessee * -1
    END AS "Interest_OS_Lessee",
    CASE
        WHEN SCHED.scheduleStatus IN ('Terminated', 'Matured') THEN 0
        ELSE CAPOS_LESSOR.capitalOutstanding * -1
    END AS "Capital_OS_Lessor",
    CASE
        WHEN SCHED.scheduleStatus IN ('Terminated', 'Matured') THEN 0
        ELSE INTEREST_LESSOR.interest_os_lessor * -1
    END AS "Interest_OS_Lessor",
    AGR.agreementSuspense AS "Suspense",
    SCHED.totalReceipts AS "Total_receipts",
    CASE
        WHEN SCHED.scheduleStatus IN ('Terminated', 'Matured') THEN 0
        ELSE SCHED.futureReceivables
    END AS "Future_receivables",
    CASE
        WHEN SCHED.scheduleStatus IN ('Terminated', 'Matured') THEN 0
        ELSE coalesce(URINC.UnearnedIncome, 0.00) - coalesce(DepositUnearned, 0.00) - coalesce(NotDueDeposit, 0.00)
    END AS "Unearned Income",
    SCHED.totalScheduleArrears AS "Schedule_arrears",
    SCHED.inArrearsSinceDate AS "In_Arrears_since",
    zerothirty AS "0-30",
    thirtysixty AS "31-60",
    sixtyninety AS "61-90",
    ninetyplus AS "91P",
    SCHED.totalCustomerArrears as "Cust_Arrears",
    pczerothirty AS "PC_0-30",
    pcthirtysixty AS "PC_31-60",
    pcsixtyninety AS "PC_61-90",
    pcninetyplus AS "PC_91P",
    SCHED.monthsInExtension AS "Months_in_extension",
    --  Last Payment for the schedule (for the Invoicing Customer)
    LASTPMT.transactionEffectiveDate AS "Last_customer_payment_date",
    LASTPMT.lastpayment AS "Last_customer_payment_amount",
    --  Next rental info
    CASE
        WHEN SCHED.scheduleStatus IN ('Terminated', 'Matured') THEN 0
        ELSE SCHED.numberOfFutureRentals
    END AS "Future_rentals",
    SCHED.nextRentalDate AS "Next_rental_date",
    sched.nextRentalFrequency AS "Next_rental_freq",
    SCHED.nextRentalAmount AS "Next_rental_amount",
    NEXTRENTAL.nextRentalPaymentTypeName AS "Next_rental_pmt_type",
    --  Insurance status
    ACQUIS.insuranceStatus AS "Insurance_status",
    CASE WHEN GGS.ggsflag = 'Y' THEN 'GGS'
         WHEN RLS.rlsflag = 'Y' THEN 'RLS'
         WHEN SEC.securityTypeName = 'British Business Bank' THEN 'CBIL'
        ELSE ''
    END AS "BBB"
    --RLS.rlsflag AS "RLS"
FROM eodSchedule SCHED
    JOIN eodAgreement AGR ON AGR.odsid = SCHED.agreementid
    JOIN eodBalloonPaymentType BALLPMT ON BALLPMT.odsid = SCHED.balloonPaymentTypeId
    JOIN eodInvoicingCustomer INVCUS ON INVCUS.odsid = SCHED.invCusId
    JOIN eodExposureCustomer EXPCUS ON EXPCUS.odsid = SCHED.expCusId
    JOIN eodDealer DLR ON DLR.odsid = SCHED.dealerId
    JOIN eodDealerBilling DLRBIL ON DLRBIL.odsid = SCHED.dealerBillingId
    JOIN eodAgreementMiscellaneous MISC ON MISC.agreementid = AGR.odsid
    LEFT JOIN eodDealerBilling DLRBILALT ON DLRBILALT.dealerBillingUniqueRef = MISC.bro
    JOIN eodAgreementCompany AGRCO ON AGRCO.odsid = SCHED.agrCompanyId
    JOIN eodActivatedByUser ACTUSER ON ACTUSER.odsid = SCHED.activatedByUserId
    LEFT JOIN eodScheduleLlisl ACQUIS ON ACQUIS.scheduleId = SCHED.odsid
    LEFT JOIN eodNextRentalPaymentType NEXTRENTAL ON NEXTRENTAL.odsid = SCHED.nextRentalPaymentTypeId -- Identify Agreements with a Practical Paid Out Alert
    LEFT JOIN (
        SELECT agreementId,
            'Y' AS practical_paid_out
        FROM eodAgreementAlert
        WHERE ppo = '1'
    ) PRACTICAL ON PRACTICAL.agreementId = AGR.odsid -- Identify Third Parties with a Complaint Alert
    LEFT JOIN (
        SELECT thirdpartyid
        FROM eodThirdPartyAlert
            JOIN eodSystemDate ON 1 = 1
        WHERE com_code = '1'
            AND (
                com_code_start <= systemdate
                AND com_code_exp >= systemdate
            )
    ) COMPLAINT ON COMPLAINT.thirdpartyid = INVCUS.odsid -- Identify Third Parties with a Vulnerable Customer Alert
    LEFT JOIN (
        SELECT thirdpartyid
        FROM eodThirdPartyAlert
            JOIN eodSystemDate ON 1 = 1
        WHERE tex_code = '1'
            AND (
                tex_code_start <= systemdate
                AND tex_code_exp >= systemdate
            )
    ) VULNERABLE ON VULNERABLE.thirdpartyid = INVCUS.odsid -- Identify Total Drawdown to date
    LEFT JOIN (
        SELECT scheduleid,
            SUM(assetDrawdownAmountAGR) AS drawdown_amount,
            SUM(assetDrawdownVatAGR) AS drawdown_vat,
            SUM(assetDepositAGR) AS drawdown_deposit
        FROM eodAsset
            JOIN eodSystemDate ON 1 = 1
        WHERE assetDrawdownDate <= systemdate
        GROUP BY scheduleid
    ) DRAWDOWN ON DRAWDOWN.scheduleid = SCHED.odsid -- Identify Asset of most value
    LEFT JOIN (
        SELECT scheduleid,
            supplierId,
            assetTypeName,
            assetDescription
        FROM eodAsset
        WHERE isMainAsset = '1'
    ) ASSET ON ASSET.scheduleid = SCHED.odsid -- Identify the Broker Manager - select just one Salesperson associated with the third party in case there are more than one
    LEFT JOIN (
        SELECT DLRSAL.dealerid,
            SAL.name AS "salesperson"
        FROM eodSalesPerson SAL
            JOIN (
                SELECT dealerid,
                    MIN(salespersonid) AS "salesperson_id"
                FROM eodDealerSalesperson
                GROUP BY dealerid
            ) DLRSAL ON SAL.odsid = DLRSAL.salesperson_id
    ) SALPER ON SALPER.dealerid = DLR.odsid -- Identify the Broker Manager at the Billing Address Level - select just one Salesperson associated with the third party in case there are more than one
    LEFT JOIN (
        SELECT DLRBILSAL.dealerBillingId,
            SAL.name AS "salesperson"
        FROM eodSalesPerson SAL
            JOIN (
                SELECT dealerBillingId,
                    MIN(salespersonid) AS "salesperson_id"
                FROM eodDealerBillingSalesperson
                GROUP BY dealerBillingId
            ) DLRBILSAL ON SAL.odsid = DLRBILSAL.salesperson_id
    ) BILSALPER ON BILSALPER.dealerBillingId = DLRBIL.odsid -- Identify commission
    LEFT JOIN (
        SELECT PAYABLE.scheduleid,
            SUM(PAYABLE.amount) AS "commission"
        FROM eodPayable PAYABLE
            JOIN eodPayableChargeType PCT ON PCT.odsid = PAYABLE.payableChargeTypeId
            AND PCT.payableChargeTypeCode IN (605, 630)
        GROUP BY PAYABLE.scheduleid
    ) COMM ON COMM.scheduleid = SCHED.odsid -- Outstanding commission
    LEFT JOIN (
        SELECT INC.scheduleId,
            SUM(INC.expenseIncomeAccrued) AS "oscomm"
        FROM eodScheduleExpenseIncome INC
            JOIN eodExpenseIncomeChargeType INCTYP ON INCTYP.odsid = INC.expenseIncomeChargeTypeId
            AND INCTYP.expIncChargeTypeCode IN (605, 630)
        WHERE INC.isRecognised = false
        GROUP BY INC.scheduleid
    ) COMMOS ON COMMOS.scheduleid = SCHED.odsid -- Identify document fee
    LEFT JOIN (
        SELECT RECEIVABLE.scheduleid,
            SUM(RECEIVABLE.amount) AS "documentation_fee"
        FROM eodReceivable RECEIVABLE
            JOIN eodReceivableChargeType RCT ON RCT.odsid = RECEIVABLE.recvchargetypeid
            AND RCT.recvchargetypecode = 103
        GROUP BY RECEIVABLE.scheduleid
    ) DOCFEE ON DOCFEE.scheduleid = SCHED.odsid -- Identify document fee split
    LEFT JOIN (
        SELECT RECEIVABLE.scheduleid,
            SUM(RECEIVABLE.amount) AS "option_to_purchase_fee"
        FROM eodReceivable RECEIVABLE
            JOIN eodReceivableChargeType RCT ON RCT.odsid = RECEIVABLE.recvchargetypeid
            AND RCT.recvchargetypecode = 13
        GROUP BY RECEIVABLE.scheduleid
    ) OTPFEE ON OTPFEE.scheduleid = SCHED.odsid
    LEFT JOIN (
        SELECT RECEIVABLE.scheduleid,
            SUM(RECEIVABLE.amount) AS "future_receivable"
        FROM eodReceivable RECEIVABLE
            JOIN eodReceivableChargeType RCT ON RCT.odsid = RECEIVABLE.recvchargetypeid
            JOIN eodSystemDate ON 1 = 1
        WHERE RECEIVABLE.dueDate > systemdate
            AND RCT.recvchargetypeisrental IS true
        GROUP BY RECEIVABLE.scheduleid
    ) FUTRCV ON FUTRCV.scheduleid = SCHED.odsid
    LEFT JOIN (
        SELECT PAYABLE.scheduleid,
            SUM(PAYABLE.amount) AS "documentation_fee_share"
        FROM eodPayable PAYABLE
            JOIN eodPayableChargeType PCT ON PCT.odsid = PAYABLE.payableChargeTypeId
            AND PCT.payableChargeTypeCode = 614
        GROUP BY PAYABLE.scheduleid
    ) DOCFEESPLIT ON DOCFEESPLIT.scheduleid = SCHED.odsid -- Arrears - Unpaid and overdue invoices 0-30
    LEFT JOIN (
        SELECT INV.scheduleid,
            SUM(unpaidAmount) AS "zerothirty"
        FROM eodInvoiceLine INV
            JOIN eodSystemDate on 1 = 1
        WHERE unpaidAmount > 0
            AND isDue = '1'
            AND isPaid = '0'
            AND systemdate - invoiceReferenceDate >= 0
            AND systemdate - invoiceReferenceDate < 31
        GROUP BY scheduleid
    ) ARR30 ON ARR30.scheduleid = SCHED.odsid
    AND zerothirty > 0 -- Arrears - Unpaid and overdue invoices 31-60
    LEFT JOIN (
        SELECT INV.scheduleid,
            SUM(unpaidAmount) AS "thirtysixty"
        FROM eodInvoiceLine INV
            JOIN eodSystemDate on 1 = 1
        WHERE unpaidAmount > 0
            AND isDue = '1'
            AND isPaid = '0'
            AND systemdate - invoiceReferenceDate > 30
            AND systemdate - invoiceReferenceDate < 61
        GROUP BY scheduleid
    ) ARR60 ON ARR60.scheduleid = SCHED.odsid
    AND thirtysixty > 0 -- Arrears - Unpaid and overdue invoices 61-90
    LEFT JOIN (
        SELECT INV.scheduleid,
            SUM(unpaidAmount) AS "sixtyninety"
        FROM eodInvoiceLine INV
            JOIN eodSystemDate on 1 = 1
        WHERE unpaidAmount > 0
            AND isDue = '1'
            AND isPaid = '0'
            AND systemdate - invoiceReferenceDate > 60
            AND systemdate - invoiceReferenceDate < 91
        GROUP BY scheduleid
    ) ARR90 ON ARR90.scheduleid = SCHED.odsid
    AND sixtyninety > 0 -- Arrears - Unpaid and overdue invoices 90+
    LEFT JOIN (
        SELECT INV.scheduleid,
            SUM(unpaidAmount) AS "ninetyplus"
        FROM eodInvoiceLine INV
            JOIN eodSystemDate on 1 = 1
        WHERE unpaidAmount > 0
            AND isDue = '1'
            AND isPaid = '0'
            AND systemdate - invoiceReferenceDate > 90
        GROUP BY scheduleid
    ) ninetyplus ON ninetyplus.scheduleid = SCHED.odsid
    AND ninetyplus > 0 -- Last Cash Allocation date and amount - based on the scheduleid and linked to the invoicing customer
    LEFT JOIN (
        SELECT C.scheduleid,
            C.transactionEffectiveDate,
            SUM(C.totalamount) AS lastpayment
        FROM eodCashAllocation C
            JOIN eodInvoiceLine INV ON INV.odsid = C.invoiceLineId
            JOIN eodInvoicingCustomer INVC ON INVC.odsid = INV.invCusId
            JOIN (
                SELECT scheduleid,
                    max(transactionEffectiveDate) AS lasttxndate
                FROM eodCashAllocation
                WHERE transactionStatus != 'Expired'
                GROUP BY scheduleid
            ) LASTTXN ON LASTTXN.scheduleid = C.scheduleid
            AND LASTTXN.lasttxndate = C.transactionEffectiveDate
        GROUP BY C.scheduleid,
            C.transactionEffectiveDate
    ) LASTPMT ON LASTPMT.scheduleid = SCHED.odsid -- Outstanding interest
    LEFT JOIN (
        SELECT CASHFLOW.scheduleid,
            SUM(CASHFLOW.profit) AS "interest_os_lessee"
        FROM eodScheduleCashflow CASHFLOW
            JOIN eodSystemDate ON 1 = 1
        WHERE CASHFLOW.isPrimaryAccountingStandard = false
            AND CASHFLOW.cashFlowEventDate > systemdate
        GROUP BY CASHFLOW.scheduleId
    ) INTEREST_LESSEE ON INTEREST_LESSEE.scheduleid = SCHED.odsid
    LEFT JOIN (
        SELECT CASHFLOW.scheduleid,
            SUM(CASHFLOW.profit) AS "interest_os_lessor"
        FROM eodScheduleCashflow CASHFLOW
            JOIN eodSystemDate ON 1 = 1
        WHERE CASHFLOW.isPrimaryAccountingStandard = true
            AND CASHFLOW.cashFlowEventDate > systemdate
        GROUP BY CASHFLOW.scheduleId
    ) INTEREST_LESSOR ON INTEREST_LESSOR.scheduleid = SCHED.odsid
    LEFT JOIN (
        SELECT CASHFLOW.scheduleid,
            CASHFLOW.capitaloutstanding
        FROM eodScheduleCashflow CASHFLOW
        WHERE CASHFLOW.odsid IN (
                SELECT MAX(odsid)
                FROM eodScheduleCashflow
                    JOIN eodSystemDate ON 1 = 1
                WHERE cashFlowEventDate <= systemdate
                    AND isPrimaryAccountingStandard = true
                GROUP BY scheduleid
            )
    ) CAPOS_LESSOR ON CAPOS_LESSOR.scheduleid = SCHED.odsid
    LEFT JOIN (
        SELECT thirdpartyid,
            reference
        FROM eodExternalSystemReference
        WHERE systemcode = 'ACR'
    ) EXTREF ON EXTREF.thirdpartyid = SCHED.invCusId
    LEFT JOIN (
        SELECT CAS.agreementid
        FROM eodCase CAS
            JOIN eodCurrentCaseStatus STAT ON STAT.odsid = CAS.currentStatusId
        WHERE CAS.status = 'Open'
            AND STAT.currentStatusTypeCode = 'NTU'
            AND STAT.currentStatusStatusCode = '004'
        GROUP BY CAS.agreementid
    ) NTU ON NTU.agreementid = AGR.odsid
    LEFT JOIN (
        SELECT CAS.agreementid
        FROM eodCase CAS
            JOIN eodCurrentCaseStatus STAT ON STAT.odsid = CAS.currentStatusId
        WHERE CAS.status = 'Open'
            AND STAT.currentStatusTypeCode = 'ORG'
        GROUP BY CAS.agreementid
    ) ORG ON ORG.agreementid = AGR.odsid
    LEFT JOIN (
        SELECT CAS.agreementid
        FROM OdsCase CAS
            JOIN eodCurrentCaseStatus STAT ON STAT.odsid = CAS.currentStatusId
        WHERE STAT.currentStatusTypeCode = 'MIG'
        GROUP BY CAS.agreementid
    ) MIG ON MIG.agreementid = AGR.odsid
    LEFT JOIN (
        SELECT agreementId,
            'Y' AS ntuflag
        FROM eodAgreementAlert
        WHERE ntu = '1'
    ) NTUALERT ON NTUALERT.agreementId = AGR.odsid
    LEFT JOIN (
        SELECT agreementId,
            'Y' AS ggsflag
        FROM odsAgreementAlert
        WHERE ggs = '1'
    ) GGS ON GGS.agreementId = AGR.odsid
    LEFT JOIN (
        SELECT agreementId,
            'Y' AS rlsflag
        FROM eodAgreementAlert
        WHERE rls = '1'
    ) RLS ON RLS.agreementId = AGR.odsid
    LEFT JOIN (
        SELECT scheduleid,
            SUM(A.rentalaccrual - A.depreciation) as UnearnedIncome
        FROM eodScheduleIncome A
        WHERE isPrimaryAccountingStandard = true
            and isRecognised = false
        GROUP BY scheduleid
    ) URINC ON SCHED.odsid = URINC.scheduleId
    LEFT JOIN (
        SELECT scheduleid,
            SUM(expenseIncomeAccrued) as DepositUnearned
        FROM eodScheduleExpenseIncome A
            JOIN eodExpenseIncomeChargeType B ON A.expenseIncomeChargeTypeId = B.odsid
        WHERE isRecognised = false
            AND expIncChargeTypeCode in (101, 102, 602)
        GROUP BY scheduleid
    ) URDEP ON SCHED.odsid = URDEP.scheduleId
    LEFT JOIN (
        SELECT scheduleid,
            SUM(amount) as NotDueDeposit
        FROM eodReceivable A
            JOIN eodReceivableChargeType B ON A.recvChargeTypeId = B.odsid
        WHERE isdue = false
            AND recvChargeTypeCode in (101, 102, 602)
        GROUP BY scheduleid
    ) NDDEP ON SCHED.odsid = NDDEP.scheduleId
    LEFT JOIN (
        SELECT agreementId,
            'Y' AS migflag
        FROM odsAgreementAlert
        WHERE mig = '1'
    ) MIGALERT ON MIGALERT.agreementId = AGR.odsid
    LEFT JOIN (
        SELECT agreementid
        FROM OdsSchedule
        WHERE terminationNumber > 1
        GROUP BY agreementid
    ) PARTIAL ON PARTIAL.agreementid = SCHED.agreementid
    LEFT OUTER JOIN (
        SELECT agreementId,
                securityTypeName 
        FROM odsSecurity
         WHERE securityTypeName = 'British Business Bank') SEC on AGR.odsid = SEC.agreementId
LEFT JOIN (
        SELECT DISTINCT INV.scheduleid,
               INVH.inv3pyid,
            SUM(unpaidAmount) AS "pczerothirty"
        FROM eodInvoiceLine INV
            JOIN eodSystemDate on 1 = 1
            JOIN eodInvoiceHeader INVH on INV.invoiceheaderid = INVH.odsid
        WHERE unpaidAmount > 0
            AND isDue = '1'
            AND isPaid = '0'
            AND systemdate - invoiceReferenceDate >= 0
            AND systemdate - invoiceReferenceDate < 31
        GROUP BY scheduleid, inv3pyid
    ) PCARR30 ON PCARR30.inv3pyid = SCHED.invCusId AND PCARR30.scheduleId = SCHED.odsid
    AND pczerothirty > 0 -- Arrears - Unpaid and overdue invoices 31-60
    LEFT JOIN (
        SELECT DISTINCT INV.scheduleid,
               INVH.inv3pyid,
            SUM(unpaidAmount) AS "pcthirtysixty"
        FROM eodInvoiceLine INV
            JOIN eodSystemDate on 1 = 1
            JOIN eodInvoiceHeader INVH on INV.invoiceheaderid = INVH.odsid
        WHERE unpaidAmount > 0
            AND isDue = '1'
            AND isPaid = '0'
            AND systemdate - invoiceReferenceDate > 30
            AND systemdate - invoiceReferenceDate < 61
        GROUP BY scheduleid, inv3pyid
    ) PCARR60 ON PCARR60.inv3pyid = SCHED.invCusId AND PCARR60.scheduleId = SCHED.odsid
    AND pcthirtysixty > 0 -- Arrears - Unpaid and overdue invoices 61-90
    LEFT JOIN (
        SELECT DISTINCT INV.scheduleid,
               INVH.inv3pyid,
            SUM(unpaidAmount) AS "pcsixtyninety"
        FROM eodInvoiceLine INV
            JOIN eodSystemDate on 1 = 1
            JOIN eodInvoiceHeader INVH on INV.invoiceheaderid = INVH.odsid
        WHERE unpaidAmount > 0
            AND isDue = '1'
            AND isPaid = '0'
            AND systemdate - invoiceReferenceDate > 60
            AND systemdate - invoiceReferenceDate < 91
        GROUP BY scheduleid, inv3pyid
    ) PCARR90 ON PCARR90.inv3pyid = SCHED.invCusId AND PCARR90.scheduleId = SCHED.odsid
    AND pcsixtyninety > 0 -- Arrears - Unpaid and overdue invoices 90+
    LEFT JOIN (
        SELECT DISTINCT INV.scheduleid,
               INVH.inv3pyid,
            SUM(unpaidAmount) AS "pcninetyplus"
        FROM eodInvoiceLine INV
            JOIN eodSystemDate on 1 = 1
            JOIN eodInvoiceHeader INVH on INV.invoiceheaderid = INVH.odsid
        WHERE unpaidAmount > 0
            AND isDue = '1'
            AND isPaid = '0'
            AND systemdate - invoiceReferenceDate > 90
        GROUP BY scheduleid, inv3pyid
    ) pcninetyplus ON pcninetyplus.inv3pyid = SCHED.invCusId AND pcninetyplus.scheduleId = SCHED.odsid
    AND pcninetyplus > 0 
     LEFT OUTER JOIN eodSupplier SUP on ASSET.supplierid = SUP.odsid
WHERE SCHED.scheduleStatus IN (
        'Live (Primary)',
        'Live (Secondary)',
        'Terminated',
        'Matured'
    ) 
--AND agreementnumber IN ('A000020331','A000022024','A000022516','A000022557')