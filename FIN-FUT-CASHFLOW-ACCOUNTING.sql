--FIN-FUT-CASHFLOW-ACCOUNTING
select AGR.agreementNumber as "Agreement",
       AGR.agreementType as "Type",
       CASE
        WHEN MISC.pcompcode = 'PRIM' THEN 'Prime'
        WHEN MISC.pcompcode = 'CORE' THEN 'Core'
        WHEN MISC.pcompcode = 'EVIN' THEN 'Consumer'
        WHEN AGRCO.agrcompanycode = '1' THEN 'Core'
        WHEN AGRCO.agrcompanycode IN ('2', '4') THEN 'Consumer'
        WHEN AGRCO.agrcompanycode = '3' THEN 'Prime'
        END AS "Pricing",
       to_char(SCHEDINC.periodstartdate,'Mon-YY') as "Period",
       SCHEDINC.rentalaccrual as "Rentals",
       SCHEDINC.depreciation as "Base depreciation",
       SCHEDEXPINC.expenseIncomeAccrued as "Subsidy",
       SCHEDINC.earnings as "Earnings",
       SCHEDEXPINCCOM."Commission" as "Commission",
       SCHEDEXPINCDOC.expenseIncomeAccrued as "DOC FEE",
       SCHEDEXPINCASS.expenseIncomeAccrued as "Asset Protection",
       SCHEDINC.netbookvalue as "NBV",
       SCHED.schedulestatus as "Status"
from odsAgreement AGR
left outer join odsSchedule SCHED on AGR.id = SCHED.agreementId
left outer join odsScheduleIncome SCHEDINC on SCHED.id = SCHEDINC.scheduleId
left outer join ( SELECT * FROM OdsScheduleExpenseIncome 
                       WHERE expenseincomechargetypeid = 1109) SCHEDEXPINC
 ON SCHEDINC.id = SCHEDEXPINC.scheduleIncomeId
left outer join ( SELECT * FROM OdsScheduleExpenseIncome 
                       WHERE expenseincomechargetypeid = 1108) SCHEDEXPINCDOC
ON SCHEDINC.id = SCHEDEXPINCDOC.scheduleIncomeId
left outer join ( SELECT scheduleIncomeId, SUM(expenseIncomeAccrued) as "Commission" FROM OdsScheduleExpenseIncome 
                       WHERE expenseincomechargetypeid IN (1610,1635) GROUP BY scheduleIncomeId) SCHEDEXPINCCOM
ON SCHEDINC.id = SCHEDEXPINCCOM.scheduleIncomeId
left outer join ( SELECT * FROM OdsScheduleExpenseIncome 
                       WHERE expenseincomechargetypeid = 1110) SCHEDEXPINCASS
 ON SCHEDINC.id = SCHEDEXPINCASS.scheduleIncomeId
left outer join odsAgreementMiscellaneous MISC ON MISC.agreementid = AGR.id
left outer join odsAgreementCompany AGRCO ON AGRCO.id = SCHED.agrCompanyId
WHERE SCHED.scheduleStatus NOT IN ('<None>','<Unknown>','Proposal')
AND SCHEDINC.periodStartDate >= current_date - interval '2 month'
--AND AGR.agreementNumber = 'A000006348'
--LIMIT 10
ORDER BY 1, SCHEDINC.periodstartdate ASC