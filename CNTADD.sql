-- CNTADD:      Contract Additions
-- Summary:     Provides details contracts started within a date range, and for a specific broker - used for commission calculations
-- Parameter:   Broker_name -- free entry due to limit on 100 returns from sql used for a parameter - report will do a like match
--              Drawdown_from, Drawdown_to
-- IMPORTANT:   Changes made to this report should also be considered for the Finance equivalent
SELECT --  Broker info
    DLR.dealerThirdPartyNumber AS "Broker_TP_id",
    DLR.dealerName AS "Broker_name",
    CASE
        WHEN BILSALPER.salesperson > '' THEN BILSALPER.salesperson
        ELSE SALPER.salesperson
    END AS "Broker_Manager",
    --  Company & Agreement info
    CASE
        WHEN AGRCO.agrcompanycode IN ('2', '4') THEN 'Evington'
        ELSE 'Arkle'
    END AS "Company",
    CASE
        WHEN AGRCO.agrcompanycode = '1' THEN 'Core'
        WHEN AGRCO.agrcompanycode IN ('2', '4') THEN 'Consumer'
        WHEN AGRCO.agrcompanycode = '3' THEN 'Prime'
    END AS "Pricing",
    AGR.product AS "Product",
    SCHED.termInMonths AS "Term",
    CASE
        WHEN AGR.isCcaRegulated = '1' THEN 'Y'
        ELSE ''
    END AS "Regulated",
    --  Customer info
    INVCUS.invCusThirdPartyNumber AS "Invoice_cust_id",
    INVCUS.invCusName AS "Invoice_cust_name",
    --  Agreement information                                           
    AGR.agreementNumber AS "Agreement",
    ASSETDET.assetDescription AS "Asset",
    CASE
        WHEN PRACTICAL.agreementid IS NULL THEN SCHED.activationDate
        ELSE SCHED.startDate
    END AS "Activated_date",
    DRAWDOWN.drawdown_date AS "Drawdown_Date",
    SCHED.startDate AS "Start_date",
    SCHED.totalAssetCost AS "Total_asset_cost",
    SCHED.totalAssetCostFinanced AS "Total_asset_cost_financed",
    CASE
        WHEN AGR.product = 'Staged Payment Loan' THEN DRAWDOWN.drawdown_amount
        WHEN AGRMISC.stpl = true THEN SCHED.totalAssetCostFinanced - coalesce(DRAWDOWNTD.drawdown_amount_to_date, 0)
        ELSE SCHED.totalAssetCostFinanced
    END AS "Drawdown_amount",
    CASE
        WHEN AGRMISC.stpl = true
        OR AGR.product = 'Staged Payment Loan' THEN 'Y'
        ELSE ''
    END AS "StgPmt",
    DOCFEE.documentation_fee AS "Document_fee",
    FEESPLIT.documentation_fee_share AS "Document_fee_share",
    DEP.deposit AS "Deposit",
    BALLOONRENTAL.balloonrental AS "Balloon",
    SCHED.numberOfFutureRentals AS "No_of_future_rentals",
    ALLINT.fixedrateinterest + ALLINT.floatingrateinterest AS "Total_interest",
    --  Rates
    COMM.commission AS "Commission",
    ROUND(
        (
            COMM.commission / SCHED.originalAssetCostFinanced
        ) * 100,
        2
    ) AS "Commission_pct",
    SCHED.combinedLessorPtoRate AS "Yield",
    -- Status
    CASE
        WHEN PRACTICAL.agreementid IS NULL THEN SCHED.scheduleStatus
        ELSE 'Practical DAC (Paid_Out)'
    END AS "Status"
FROM OdsSchedule SCHED
    JOIN OdsAgreement AGR ON AGR.id = SCHED.agreementid
    LEFT JOIN (
        SELECT scheduleId,
            assetDrawdownDate AS "drawdown_date",
            SUM(assetDrawdownAmount) AS "drawdown_amount",
            SUM(assetDrawdownVat) AS "drawdown_vat"
        FROM OdsAsset
        GROUP BY scheduleId,
            assetDrawdownDate
    ) DRAWDOWN ON DRAWDOWN.scheduleid = SCHED.id
    LEFT JOIN (
        SELECT scheduleId,
            SUM(assetDrawdownAmount) AS "drawdown_amount_to_date"
        FROM OdsAsset
            JOIN OdsSystemDate ON 1 = 1
        WHERE assetDrawdownDate <= to_date(:Drawdown_to, 'YYYY-MM-DD')
        GROUP BY scheduleId
    ) DRAWDOWNTD ON DRAWDOWNTD.scheduleid = SCHED.id
    LEFT JOIN (
        SELECT agreementid,
            stpl
        FROM OdsAgreementMiscellaneous
        WHERE stpl = true
    ) AGRMISC ON AGRMISC.agreementid = AGR.id
    LEFT JOIN (
        SELECT agreementId,
            'Y' AS practical_paid_out
        FROM OdsAgreementAlert
        WHERE ppo = '1'
    ) PRACTICAL ON PRACTICAL.agreementId = AGR.id
    JOIN OdsInvoicingCustomer INVCUS ON INVCUS.id = SCHED.invCusId
    JOIN OdsExposureCustomer EXPCUS ON EXPCUS.id = SCHED.expCusId
    JOIN OdsDealer DLR ON DLR.id = SCHED.dealerId
    AND LOWER(DLR.dealername) LIKE LOWER(CONCAT('%', :Broker_name, '%'))
    JOIN OdsDealerBilling DLRBIL ON DLRBIL.id = SCHED.dealerBillingId
    LEFT JOIN (
        SELECT DLRSAL.dealerid,
            SAL.name AS "salesperson"
        FROM OdsSalesPerson SAL
            JOIN (
                SELECT dealerid,
                    MIN(salespersonid) AS "salesperson_id"
                FROM OdsDealerSalesperson
                GROUP BY dealerid
            ) DLRSAL ON SAL.id = DLRSAL.salesperson_id
    ) SALPER ON SALPER.dealerid = DLR.id
    LEFT JOIN (
        SELECT DLRBILSAL.dealerBillingId,
            SAL.name AS "salesperson"
        FROM OdsSalesPerson SAL
            JOIN (
                SELECT dealerBillingId,
                    MIN(salespersonid) AS "salesperson_id"
                FROM OdsDealerBillingSalesperson
                GROUP BY dealerBillingId
            ) DLRBILSAL ON SAL.id = DLRBILSAL.salesperson_id
    ) BILSALPER ON BILSALPER.dealerBillingId = DLRBIL.id
    JOIN OdsAgreementCompany AGRCO ON AGRCO.id = SCHED.agrCompanyId
    LEFT JOIN OdsAsset ASSETDET ON ASSETDET.agreementid = AGR.id
    AND ASSETDET.isMainAsset = True
    LEFT JOIN (
        SELECT PAYABLE.scheduleid,
            SUM(PAYABLE.amount) AS "commission"
        FROM OdsPayable PAYABLE
            JOIN OdsPayableChargeType PCT ON PCT.id = PAYABLE.payableChargeTypeId
            AND PCT.payableChargeTypeCode = 605
        GROUP BY PAYABLE.scheduleid
    ) COMM ON COMM.scheduleid = SCHED.id
    LEFT JOIN (
        SELECT RECEIVABLE.scheduleid,
            SUM(RECEIVABLE.amount) AS "documentation_fee"
        FROM OdsReceivable RECEIVABLE
            JOIN OdsReceivableChargeType RCT ON RCT.id = RECEIVABLE.recvchargetypeid
            AND RCT.recvchargetypecode = 103
        GROUP BY RECEIVABLE.scheduleid
    ) DOCFEE ON DOCFEE.scheduleid = SCHED.id
    LEFT JOIN (
        SELECT PAYABLE.scheduleid,
            SUM(PAYABLE.amount) AS "documentation_fee_share"
        FROM OdsPayable PAYABLE
            JOIN OdsPayableChargeType PCT ON PCT.id = PAYABLE.payableChargeTypeId
            AND PCT.payableChargeTypeCode = 614
        GROUP BY PAYABLE.scheduleid
    ) FEESPLIT ON FEESPLIT.scheduleid = SCHED.id
    LEFT JOIN (
        SELECT RECEIVABLE.scheduleid,
            SUM(RECEIVABLE.amount) AS "deposit"
        FROM OdsReceivable RECEIVABLE
            JOIN OdsReceivableChargeType RCT ON RCT.id = RECEIVABLE.recvchargetypeid
            AND RCT.recvchargetypecode = 101
        GROUP BY RECEIVABLE.scheduleid
    ) DEP ON DEP.scheduleid = SCHED.id
    LEFT JOIN (
        SELECT scheduleid,
            rentalProfileAmount AS balloonrental
        FROM OdsScheduleRentalProfile
        WHERE rentalSubType = 'Balloon'
    ) BALLOONRENTAL ON BALLOONRENTAL.scheduleid = SCHED.id
    LEFT JOIN (
        SELECT scheduleid,
            SUM(earnings) AS fixedrateinterest,
            SUM(floatingRateIncome) AS floatingrateinterest
        FROM OdsScheduleIncome A
        WHERE isPrimaryAccountingStandard = true
        GROUP BY scheduleid
    ) ALLINT ON ALLINT.scheduleid = SCHED.id
WHERE (
        DRAWDOWN.drawdown_date >= to_date(:Drawdown_from, 'YYYY-MM-DD')
        AND DRAWDOWN.drawdown_date <= to_date(:Drawdown_to, 'YYYY-MM-DD')
    )
    AND (
        PRACTICAL.agreementId IS NOT NULL
        OR SCHED.scheduleStatus != 'Proposal'
    )
ORDER BY DLR.dealername,
    DRAWDOWN.drawdown_date,
    AGRCO.agrCompanyName,
    AGR.product,
    SCHED.startdate