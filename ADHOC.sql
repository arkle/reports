-- SFAGRREL:        Salesforce Integration - Agreement Relations Information v1.0
-- Title:               
-- Summary:         **INTEGRATION USE ONLY - DO NOT RUN MANUALLY** SALESFORCE CONTRACT INTEGRATION
--                  Provides data points required for upsert of Agreement Relations object in Salesforce
-- Parameter:       agreementnumber (String,20)
SELECT AGR.agreementnumber AS "CONTRACT__C",
    RELATED.actor_code AS "INVOLVED_PARTY__C",
    '0123z000000KtpzAAC' AS "RECORDTYPEID",
    RELATED.type AS "ROLE__C",
    CONCAT(
        RELATED.actor_code,
        AGR.agreementnumber,
        RELATED.type
    ) AS "EXTERNAL_ID"
FROM OdsAgreement AGR
    JOIN (
        SELECT 'Su' AS "type",
            AGR.id AS "agrid",
            CASE
                WHEN ACTOR.reference > '' AND SUPBIL.supplrBillingBillingNumber = 1 THEN ACTOR.reference
                ELSE SUPBIL.supplrBillingUniqueRef
            END AS "actor_code"
        FROM OdsAgreement AGR
            JOIN OdsAsset ASS ON ASS.agreementId = AGR.id
            JOIN OdsSupplierBilling SUPBIL ON SUPBIL.id = ASS.supplrBillingId
            AND SUPBIL.id > 1
            LEFT JOIN (
                SELECT thirdpartyid,
                    reference -- External reference - Actor Code
                FROM OdsExternalSystemReference
                WHERE systemcode = 'ACR'
            ) ACTOR ON ACTOR.thirdpartyid = ASS.supplierId
        UNION
        SELECT 'Gu' AS "type",
            AGR.id AS "agrid",
            CASE
                WHEN ACTOR.reference IS NULL AND GUABILL.guarBillingBillingNumber = 1 THEN ACTOR.reference
                ELSE GUABILL.guarBillingUniqueRef
            END AS "actor_code"
        FROM OdsAgreement AGR
            JOIN OdsSecurity SEC ON SEC.agreementId = AGR.id
            JOIN OdsGuarantorBilling GUABILL ON GUABILL.id = SEC.guarBillingId
            AND GUABILL.id > 1
            LEFT JOIN (
                SELECT thirdpartyid,
                    reference -- External reference - Actor Code
                FROM OdsExternalSystemReference
                WHERE systemcode = 'ACR'
            ) ACTOR ON ACTOR.thirdpartyid = SEC.guarantorId
    ) RELATED ON RELATED.agrid = AGR.id
WHERE AGR.id > 2
    AND TRIM(AGR.agreementnumber) = TRIM(:agreementnumber)