-- FIN-FUT-CASHFLOW-RV:    Future Cashflow
-- Title:               Portfolio Report (Finance Version)
-- Summary:             Future cashflow for all live agreements. Data is extracted from the beginning of the current month (based on date of day prior).
SELECT DISTINCT DATAA."AGREEMENT",
                DATAA."Alt_Agreement_Number",
                DATAA."Type",
                DATAA."Pricing",
                DATAA."CASHFLOW_EVENT_DATE",
                SUM(DATAA."REPAYMENT") AS "REPAYMENT",
                SUM(DATAA."REPAYMENTS") AS "REPAYMENTS",
                SUM(DATAA."CAPITAL_REPAID") AS "CAPITAL_REPAID",
                SUM(DATAA."INTEREST") AS "INTEREST",
                SUM(DATAA."CAP_OS") AS "CAP_OS",
                DATAA."Status"
FROM (SELECT 
AGR.agreementnumber AS "AGREEMENT",
    CASE
        WHEN MISC.agrid = '<None>' THEN ''
        ELSE MISC.agrid
    END AS "Alt_Agreement_Number",
    AGR.agreementType as "Type",
    CASE
        WHEN AGRC.agrcompanycode = '1' THEN 'Core'
        WHEN AGRC.agrcompanycode IN ('2', '4') THEN 'Consumer'
        WHEN AGRC.agrcompanycode = '3' THEN 'Prime'
    END AS "Pricing",
    --to_char(CASHFLOW.cashfloweventdate, 'DD/MM/YYYY') AS "CASHFLOW_EVENT_DATE",
    CASHFLOW.cashfloweventdate AS "CASHFLOW_EVENT_DATE",
    CASHFLOW.repayments AS "REPAYMENT",
    case when CASHFLOW.isPrimaryAccountingStandard = false then expenses else repayments end as "REPAYMENTS",
    CASHFLOW.repayments + CASHFLOW.profit AS "CAPITAL_REPAID",
    CASHFLOW.profit * -1 AS "INTEREST",
    CASHFLOW.capitaloutstanding * -1 AS "CAP_OS",
    SCHED.scheduleStatus AS "Status"
FROM eodScheduleCashFlow CASHFLOW
    JOIN eodSchedule SCHED ON SCHED.odsid = CASHFLOW.scheduleId
    JOIN eodAgreement AGR ON AGR.odsid = SCHED.agreementid
    JOIN odsAgreementCompany AGRC on AGR.agrCompanyId = AGRC.id
    JOIN OdsAgreementMiscellaneous MISC ON AGR.odsId = MISC.agreementId
    JOIN eodSystemDate ON 1 = 1
WHERE CASHFLOW.isPrimaryAccountingStandard = false
    AND CASHFLOW.cashfloweventdate >= date_trunc('month', systemdate)
AND AGR.agreementnumber NOT IN ('<None>','<Unknown>')

UNION ALL

SELECT distinct
AGR.agreementnumber AS "AGREEMENT",
    CASE
        WHEN MISC.agrid = '<None>' THEN ''
        ELSE MISC.agrid
    END AS "Alt_Agreement_Number",
    AGR.agreementType as "Type",
    CASE
        WHEN AGRC.agrcompanycode = '1' THEN 'Core'
        WHEN AGRC.agrcompanycode IN ('2', '4') THEN 'Consumer'
        WHEN AGRC.agrcompanycode = '3' THEN 'Prime'
    END AS "Pricing",
    --to_char(CASHFLOW.cashfloweventdate, 'DD/MM/YYYY') AS "CASHFLOW_EVENT_DATE",
    REC.duedate AS "CASHFLOW_EVENT_DATE", --need to add dates here
    REC.amount AS "REPAYMENT", -- payments need adding here
    CASE WHEN REC.recvChargeTypeId = '1019' THEN REC.amount ELSE 0.00 END as "REPAYMENTS",
    0.00 AS "CAPITAL_REPAID",
    0.00 AS "INTEREST",
    0.00 AS "CAP_OS",
    SCHED.scheduleStatus AS "Status"
FROM eodScheduleCashFlow CASHFLOW
    JOIN eodSchedule SCHED ON SCHED.odsid = CASHFLOW.scheduleId
    JOIN eodReceivable REC ON SCHED.odsid = REC.scheduleId
    JOIN eodAgreement AGR ON AGR.odsid = SCHED.agreementid
    JOIN odsAgreementCompany AGRC on AGR.agrCompanyId = AGRC.id
    JOIN OdsAgreementMiscellaneous MISC ON AGR.odsId = MISC.agreementId
    JOIN eodSystemDate ON 1 = 1
WHERE REC.recvChargeTypeId IN ('1019','1106','1107')
    AND REC.duedate >= date_trunc('month', systemdate)
AND AGR.agreementnumber NOT IN ('<None>','<Unknown>')

UNION ALL

SELECT distinct
AGR.agreementnumber AS "AGREEMENT",
    CASE
        WHEN MISC.agrid = '<None>' THEN ''
        ELSE MISC.agrid
    END AS "Alt_Agreement_Number",
    AGR.agreementType as "Type",
    CASE
        WHEN AGRC.agrcompanycode = '1' THEN 'Core'
        WHEN AGRC.agrcompanycode IN ('2', '4') THEN 'Consumer'
        WHEN AGRC.agrcompanycode = '3' THEN 'Prime'
    END AS "Pricing",
    --to_char(CASHFLOW.cashfloweventdate, 'DD/MM/YYYY') AS "CASHFLOW_EVENT_DATE",
    SCHED.maturityDate + INTERVAL '1 month' AS "CASHFLOW_EVENT_DATE", --need to add dates here
    SCHED.totalResidualVaLue AS "REPAYMENT", -- payments need adding here
    SCHED.totalResidualVaLue as "REPAYMENTS",
    0.00 AS "CAPITAL_REPAID",
    0.00 AS "INTEREST",
    0.00 AS "CAP_OS",
    SCHED.scheduleStatus AS "Status"
FROM eodAgreement AGR
LEFT OUTER JOIN eodSchedule SCHED ON AGR.odsid = SCHED.agreementId
JOIN odsAgreementCompany AGRC on AGR.agrCompanyId = AGRC.id
JOIN OdsAgreementMiscellaneous MISC ON AGR.odsId = MISC.agreementId
LEFT OUTER JOIN (SELECT distinct
AGR.agreementnumber AS "AGREEMENT",
    CASE
        WHEN MISC.agrid = '<None>' THEN ''
        ELSE MISC.agrid
    END AS "Alt_Agreement_Number",
    AGR.agreementType as "Type",
    CASE
        WHEN AGRC.agrcompanycode = '1' THEN 'Core'
        WHEN AGRC.agrcompanycode IN ('2', '4') THEN 'Consumer'
        WHEN AGRC.agrcompanycode = '3' THEN 'Prime'
    END AS "Pricing",
    --to_char(CASHFLOW.cashfloweventdate, 'DD/MM/YYYY') AS "CASHFLOW_EVENT_DATE",
    REC.duedate AS "CASHFLOW_EVENT_DATE", --need to add dates here
    REC.amount AS "REPAYMENT", -- payments need adding here
    0.00 as "REPAYMENTS",
    0.00 AS "CAPITAL_REPAID",
    0.00 AS "INTEREST",
    0.00 AS "CAP_OS",
    SCHED.scheduleStatus AS "Status"
FROM eodScheduleCashFlow CASHFLOW
    JOIN eodSchedule SCHED ON SCHED.odsid = CASHFLOW.scheduleId
    JOIN eodReceivable REC ON SCHED.odsid = REC.scheduleId
    JOIN eodAgreement AGR ON AGR.odsid = SCHED.agreementid
    JOIN odsAgreementCompany AGRC on AGR.agrCompanyId = AGRC.id
    JOIN OdsAgreementMiscellaneous MISC ON AGR.odsId = MISC.agreementId
    JOIN eodSystemDate ON 1 = 1
WHERE REC.recvChargeTypeId IN ('1019','1106','1107')
    AND REC.duedate >= date_trunc('month', systemdate)
AND AGR.agreementnumber NOT IN ('<None>','<Unknown>')) NOTMIS ON AGR.agreementNumber = NOTMIS."AGREEMENT"
WHERE SCHED.scheduleStatus NOT IN ('Proposal')
AND SCHED.totalResidualValue > 0
AND NOTMIS."AGREEMENT" IS NULL
ORDER BY "AGREEMENT", "CASHFLOW_EVENT_DATE" ASC) DATAA
--WHERE DATAA."AGREEMENT" = 'A000000429'
--WHERE DATAA."Alt_Agreement_Number" <> ''
GROUP BY DATAA."AGREEMENT", DATAA."Type", DATAA."Pricing", DATAA."CASHFLOW_EVENT_DATE", DATAA."Status", DATAA."Alt_Agreement_Number"
ORDER BY DATAA."AGREEMENT", DATAA."CASHFLOW_EVENT_DATE" ASC