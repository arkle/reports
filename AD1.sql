-- AD1:         ALFA Users Report v1.0
-- Title:       ALFA User Listing
-- Summary:     Extract of ALFA Users showing their department, last login date and time, and role (note there may be multiple roles and so multiple rows in the report)
-- Parameter:   User_code_contains
SELECT 
    ALFAUSER.name                   AS "User name",    
    ALFAUSER.code                   AS "User code",
    DEPARTMENT.code                 AS "Dept Code",
    DEPARTMENT.description          AS "Dept_desc",
    ALFAUSER.lastLoginDate	        AS "Last login date",
    to_char(ALFAUSER.lastLoginTime, '00:00:00')	    
                                    AS "Last login time",
    ALFAUSER.enabledFrom	        AS "Enabled from",	
    ALFAUSER.disabledFrom	        AS "Disabled from",
    ALFAUSER.isAlfaManaged	        AS "ALFA Managed",
    ALFAUSER.telephoneNumber	    AS "Telephone",
    ALFAUSER.emailAddress	        AS "Email",
    USERGROUP.userGroupCode         AS "User_Group_Code",
    USERGROUP.userGroupDescription  AS "User_Group_Description"

FROM OdsAlfaUser ALFAUSER 
    JOIN OdsDepartment DEPARTMENT 
        ON ALFAUSER.departmentId = DEPARTMENT.id
    LEFT JOIN OdsAlfaUserGroupMembership USERGROUPMBR 
        ON USERGROUPMBR.userId = ALFAUSER.id 
    JOIN OdsAlfaUserGroup USERGROUP 
        ON USERGROUP.id = USERGROUPMBR.userGroupId

WHERE LOWER(ALFAUSER.code) LIKE LOWER(CONCAT('%',:User_code_contains,'%'))

ORDER BY DEPARTMENT.code, ALFAUSER.name, USERGROUP.userGroupCode