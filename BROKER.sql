-- BROKER:      Broker Details Report 
-- Title:       Broker Detail
-- Summary:     Provides details of all Broker third parties - limits to Brokers based on presence of Broker Manager (SalesPerson)
SELECT CASE
        WHEN BILSALPER.salesperson > '' THEN BILSALPER.salesperson
        ELSE SALPER.salesperson
    END AS "Broker_Manager",
    DLR.dealerThirdPartyNumber AS "Broker_ID",
    EXTREF.reference AS "Actor_Code",
    DLR.dealerName AS "Broker_Name",
    DLRBIL.dealerbillingname AS "Internal_Name",
    DLR.dealerAddressLine1 AS "address_1",
    DLR.dealerAddressLine2 AS "address_2",
    DLR.dealerAddressLine3 AS "address_3",
    DLR.dealerAddressLine4 AS "address_4",
    DLR.dealerAddressLine5 AS "address_5",
    DLR.dealerPostalCode AS "Post_Code",
    BALANCE.balanceos AS "Balance_OS",
    BALANCE.capos AS "Cap_OS",
    BALANCE.futrcv AS "Future_receivables",
    BALANCE.arrears AS "Arrears"
FROM OdsDealerBilling DLRBIL
    JOIN OdsDealer DLR ON DLR.id = DLRBIL.dealerBillingThirdPartyId -- Identify the Broker Manager at the Billing Address Level - select just one Salesperson associated with the third party in case there are more than one    
    LEFT JOIN (
        SELECT DLRBILSAL.dealerBillingId,
            SAL.name AS "salesperson"
        FROM OdsSalesPerson SAL
            JOIN (
                SELECT dealerBillingId,
                    MIN(salespersonid) AS "salesperson_id"
                FROM OdsDealerBillingSalesperson
                GROUP BY dealerBillingId
            ) DLRBILSAL ON SAL.id = DLRBILSAL.salesperson_id
    ) BILSALPER ON BILSALPER.dealerBillingId = DLRBIL.id -- Identify the Broker Manager at the Third Party Level - select just one Salesperson associated with the third party in case there are more than one
    LEFT JOIN (
        SELECT DLRSAL.dealerid,
            SAL.name AS "salesperson"
        FROM OdsSalesPerson SAL
            JOIN (
                SELECT dealerid,
                    MIN(salespersonid) AS "salesperson_id"
                FROM OdsDealerSalesperson
                GROUP BY dealerid
            ) DLRSAL ON SAL.id = DLRSAL.salesperson_id
    ) SALPER ON SALPER.dealerid = DLRBIL.dealerBillingThirdPartyId -- External customer reference (Third Party level)- Actor Code
    LEFT JOIN (
        SELECT thirdpartyid,
            reference
        FROM OdsExternalSystemReference
        WHERE systemcode = 'ACR'
    ) EXTREF ON EXTREF.thirdpartyid = DLRBIL.dealerBillingThirdPartyId -- Outstanding Balances
    LEFT JOIN (
        SELECT dealerId,
            SUM(grossBalanceRemaining) as balanceos,
            SUM(capitalOutstanding) AS capos,
            SUM(futureReceivables) AS futrcv,
            SUM(totalScheduleArrears) AS arrears
        FROM OdsSchedule
        GROUP BY dealerId
    ) BALANCE ON BALANCE.dealerId = DLR.id
WHERE BILSALPER.salesperson > ''
    OR SALPER.salesperson > ''
ORDER BY CASE
        WHEN BILSALPER.salesperson > '' THEN BILSALPER.salesperson
        ELSE SALPER.salesperson
    END,
    DLR.dealerThirdPartyNumber