SELECT DISTINCT AGR.agreementNumber AS "Agreement Number",
       SCHED.scheduleStatus AS "Schedule Status",
       EXPCUS.expCusName AS "Exposure Customer Name",
       SCHED.scheduleDescription AS "Description",
       DLR.dealerName AS "Broker",
       PAY.dueDate AS "Due Date",
       PAY.amount + PAY.salesTaxAmount AS "Payable Amount",
       SCHED.nextRentalFrequency AS "Rental Frequency",
       CASE WHEN SCHED.totalScheduleArrears >0 THEN 'Y' ELSE 'N' END AS "In Arrears?",
       CASE WHEN PAY.isPaid = '1' THEN 'Y' ELSE 'N' END AS "Is Paid?"
FROM OdsSchedule SCHED
LEFT OUTER JOIN OdsAgreement AGR ON SCHED.agreementId = AGR.id
LEFT OUTER JOIN OdsExposureCustomer EXPCUS ON SCHED.ExpCusId = EXPCUS.id
LEFT OUTER JOIN OdsDealer DLR ON SCHED.dealerId = DLR.id
LEFT OUTER JOIN OdsPayable PAY ON SCHED.id = PAY.scheduleid
WHERE PAY.payablechargeTypeId = '1620'
AND SCHED.scheduleStatus NOT IN ('Proposal','Live (Primary)')
AND PAY.dueDate >= :PERIOD_START AND PAY.dueDate <= :PERIOD_END
order by 1 desc, 5 asc;