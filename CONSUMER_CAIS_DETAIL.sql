             SELECT 
                                           -- #1 | 1 to 19 (+19) | Account number | 

                                           RPAD(eodAgreement.agreementNumber, 19, ' ') AS "Account_Number",              

                                          -- #1a | 20 (+1) | Sequence number for co-borrowers
                                           CASE WHEN COBORROWER.name IS NULL THEN ' ' 
                                           ELSE to_char(COBORROWER.seq,'FM9')
                                           END AS "Sequence",
    
                                           -- #2 | 21 to 22 (+2)| Account Type 
                                           CASE
                                           --  Set to ‘01’ (Hire Purchase) if the ALFA Agreement Type is ‘HIP’ (Hire Purchase) and the ‘Interest Variation Flag’ for the product is set to ‘N’
                                           WHEN eodAgreement.agreementType = 'Hire Purchase' AND eodAgreement.isFloatingRate = FALSE THEN '01'
                                           -- Set to ‘20’ (Variable Subscription) if the ALFA Agreement Type is ‘HIP’ and the ‘Interest Variation Flag’ for the product is ‘Y’
                                           WHEN eodAgreement.agreementType = 'Hire Purchase' AND eodAgreement.isFloatingRate = TRUE THEN '20'
                                           -- Set to ‘02’ (Loan) if the ALFA Agreement Type is ‘LON’ (Loan)
                                           WHEN eodAgreement.agreementType = 'Loan' THEN '02'
                                           -- Set to ‘22’ if the ALFA Agreement Type is ‘LSE’(Finance Lease)
                                           WHEN eodAgreement.agreementType = 'Finance Lease' THEN '22'
                                           -- Set to ‘23’ if the ALFA Agreement Type is ‘OPL’ (Operating Lease)
                                           WHEN eodAgreement.agreementType = 'Operating Lease' THEN '23'
                                           END AS "Account_Type",
                                           
                                           --
                             
                                           -- #3 | 23 to 30 (+8)| Schedule start date DDMMCCYY format 
                                           TO_CHAR(eodSchedule.startDate, 'DDMMYYYY') AS "Start_Date",
                                           
                                           --                                                                                                                 
                                                          
                                           -- #4 | 31 to 38 (+8) | The closing date in DDMMCCYY format |
                                           CASE
                                             -- If the agreement has been set to ‘in default’ by means of a CAIS override record, the close date will be taken from the record;
                                                WHEN eodScheduleCaisDefault.caisDefaultNoticeDate IS NOT NULL THEN TO_CHAR(eodScheduleCaisDefault.caisDefaultNoticeDate, 'DDMMYYYY')
                                               -- If the schedule is live (i.e. has an outstanding balance or arrears) the field will hold zeros.
                                                WHEN eodSchedule.scheduleStatus = 'Live (Primary)' THEN '00000000'
                                               -- If the schedule is in secondary period (the schedule status field is ‘SEC’) then the close date will be zeros.
                                                WHEN eodSchedule.scheduleStatus = 'Live (Secondary)' THEN '00000000'
                                               -- If the deal is matured it will be the schedule maturity date.
                                                WHEN eodSchedule.scheduleStatus = 'Matured' THEN TO_CHAR(eodSchedule.maturityDate, 'DDMMYYYY')
                                               -- If the deal is terminated it will hold the schedule termination date.
                                                WHEN eodSchedule.scheduleStatus = 'Terminated' THEN TO_CHAR(eodSchedule.terminationDate, 'DDMMYYYY') 
                                               -- If the last receipt date is past the maturity or the schedule termination date, the last receipt date will be used
                                                ELSE '99999999'
                                           END AS "Close_Date",
                                              
                                           --
                                           
                                           -- #5 |                                
                                           CASE
                                             -- This will be the monthly standard rental if the rental frequency is monthly.
                                                WHEN eodScheduleRentalProfile.rentalFrequency = '1 month' THEN LPAD(cast(ROUND(eodScheduleRentalProfile.rentalProfileAmount, 0) as text), 6, '0')
                                             -- For rental frequencies other than monthly, this will be calculated to produce a monthly amount. (e.g. divide by 3 if quarterly)
                                             -- 2 months
                                                WHEN eodScheduleRentalProfile.rentalFrequency = '2 months' THEN LPAD(cast(ROUND((eodScheduleRentalProfile.rentalProfileAmount / 2), 0) as text), 6, '0')
                                             -- 3 months
                                                WHEN eodScheduleRentalProfile.rentalFrequency = '3 months' THEN LPAD(cast(ROUND((eodScheduleRentalProfile.rentalProfileAmount / 3), 0) as text), 6, '0')
                                             -- 4 months
                                                WHEN eodScheduleRentalProfile.rentalFrequency = '4 months' THEN LPAD(cast(ROUND((eodScheduleRentalProfile.rentalProfileAmount / 4), 0) as text), 6, '0')
                                             -- 5 months
                                                WHEN eodScheduleRentalProfile.rentalFrequency = '5 months' THEN LPAD(cast(ROUND((eodScheduleRentalProfile.rentalProfileAmount / 5), 0) as text), 6, '0')
                                             -- 6 months
                                                WHEN eodScheduleRentalProfile.rentalFrequency = '6 months' THEN LPAD(cast(ROUND((eodScheduleRentalProfile.rentalProfileAmount / 6), 0) as text), 6, '0')
                                             -- 7 months
                                                WHEN eodScheduleRentalProfile.rentalFrequency = '7 months' THEN LPAD(cast(ROUND((eodScheduleRentalProfile.rentalProfileAmount / 7), 0) as text), 6, '0')
                                             -- 8 months
                                                WHEN eodScheduleRentalProfile.rentalFrequency = '8 months' THEN LPAD(cast(ROUND((eodScheduleRentalProfile.rentalProfileAmount / 8), 0) as text), 6, '0')
                                             -- 9 months
                                                WHEN eodScheduleRentalProfile.rentalFrequency = '9 months' THEN LPAD(cast(ROUND((eodScheduleRentalProfile.rentalProfileAmount / 9), 0) as text), 6, '0')
                                             -- 10 months
                                                WHEN eodScheduleRentalProfile.rentalFrequency = '10 months' THEN LPAD(cast(ROUND((eodScheduleRentalProfile.rentalProfileAmount / 10), 0) as text), 6, '0')
                                             -- 11 months
                                                WHEN eodScheduleRentalProfile.rentalFrequency = '11 months' THEN LPAD(cast(ROUND((eodScheduleRentalProfile.rentalProfileAmount / 11), 0) as text), 6, '0')
                                             -- 12 months
                                                WHEN eodScheduleRentalProfile.rentalFrequency = '12 months' THEN LPAD(cast(ROUND((eodScheduleRentalProfile.rentalProfileAmount / 12), 0) as text), 6, '0')
                                           END AS "Monthly_Payment",                                 
                                           
                                           --
                                           
                                           -- #6 | 45 to 47 (+3)| Repayment Period | NEEDS PADDING FOR LEADING 0s
                                           LPAD(cast(eodSchedule.termInMonths as text), 3, '0') AS "Repayment_Period",
                                           
                                           --
                                                                        
                                           -- #7 | 48 to 54 (+7)| Currnet balance                                 
                                           CASE     -- If the default amount on the CAIS override is zero (or no override record exists):
                                                          WHEN eodScheduleCaisDefault.originalCaisDefaultBalance = 0 OR eodScheduleCaisDefault.caisDefaultNoticeDate IS NULL
                                                                        THEN (LPAD(
                                                                        -- Schedule arrears - schedule suspense + the balance which depends on status and presence of manual override
                                                                                      cast(ROUND(eodSchedule.totalCustomerArrears 
                                                                                      - eodSchedule.scheduleSuspenseTotal + ( 
                                                                                                     CASE
                                                                                                                   -- If schedule status is MAT, SEC, EXP or TRM, the balance = 0
                                                                                                                   WHEN eodSchedule.scheduleStatus IN ('Terminated', 'Matured', 'Live (Secondary)', 'Expired') THEN 0
                                                                                                                   -- Otherwise balance = total primary rentals - rentals due
                                                                                                                   ELSE eodSchedule.totalPrimaryRentals - eodSchedule.totalRentalsDue
                                                                                                     END
                                                                                                     )
                                                                                                     , 0) as text) -- Vaules rounded for only full pounds (GBP) as per specification
                                                                                                     , 7, '0') -- Padding                                                                                      
                                                                                           )
                                                                                           
                                                          -- Balance = Default amount - payments received after default date 
                                                          WHEN eodScheduleCaisDefault.originalCaisDefaultBalance > 0 AND eodScheduleCaisDefault.caisDefaultNoticeDate IS NOT NULL
                                                                        THEN 
                                                                        LPAD(
                                                                                      cast(ROUND(
                                                                                      eodScheduleCaisDefault.originalCaisDefaultBalance -

                                                                                                     -- Payments received after default date if there are any
                                                                                                     COALESCE(
                                                                                                                   (SELECT SUM(totalAmount) FROM eodCashAllocation eodCashAllocation
                                                                                                                                 WHERE eodCashAllocation.locationType IN ('Invoice Line', 'Schedule suspense', 'Agreement suspense')
                                                                                                                                 AND eodCashAllocation.transactionStatus = 'Applied'
                                                                                                                                  AND eodCashAllocation.scheduleId = eodSchedule.odsId                                                                                                                    
                                                                                                                                  AND eodCashAllocation.transactionEffectiveDate >= eodScheduleCaisDefault.caisDefaultNoticeDate
                                                                                                                                  ),
                                                                                                                  0)                                                                                                                              
                                                                                                     , 0) as text) -- Vaules rounded for only full pounds (GBP) as per specification
                                                                                                     , 7, '0') -- Padding                                                                        

                                                          
                                           END AS "Current_Balance", 
                                           
                                           --

                                           -- #8 | 55 (+1)| Credit Balance Indicator | This field will always be set to blank.
                                             LPAD(' ', 1, ' ') AS "Credit_Balance_Indicator", -- x1 -- blank field
                                           
                                           --
                                           
                                           -- #9 | 56 (+1)| Account Status Code                       
                                           -- The CAIS account status code is a monthly indication of the payment performance of the account.
                                           CASE
                                                          WHEN eodScheduleCaisDefault.caisDefaultNoticeDate IS NOT NULL AND eodScheduleCaisDefault.originalCaisDefaultBalance > 0
                                                                                      THEN '8'
                                                          -- Where the arrears are negative or less than the monthly rental amount use status 0
                                                          -- In advance, up-to-date or less than one payment due but unpaid (or were due and unpaid when settled).
                                                          WHEN eodSchedule.totalCustomerArrears <= eodScheduleRentalProfile.rentalProfileAmount
                                                                                      THEN '0'                                                         
                                                          -- More than one but less than two payments due but unpaid (or were due and unpaid when settled).
                                                          WHEN eodSchedule.totalCustomerArrears >= eodScheduleRentalProfile.rentalProfileAmount
                                                                        AND eodSchedule.totalCustomerArrears <= 2*eodScheduleRentalProfile.rentalProfileAmount
                                                                                      THEN '1'
                                                          -- More than two but less than three payments due but unpaid (or were due and unpaid when settled).
                                                          WHEN eodSchedule.totalCustomerArrears >= 2*eodScheduleRentalProfile.rentalProfileAmount
                                                                        AND eodSchedule.totalCustomerArrears <= 3*eodScheduleRentalProfile.rentalProfileAmount
                                                                                      THEN '2'
                                                          -- More than three but less than four payments due but unpaid (or were due and unpaid when settled).
                                                          WHEN eodSchedule.totalCustomerArrears >= 3*eodScheduleRentalProfile.rentalProfileAmount
                                                                        AND eodSchedule.totalCustomerArrears <= 4*eodScheduleRentalProfile.rentalProfileAmount
                                                                                      THEN '3'
                                                          -- More than four but less than five payments due but unpaid (or were due and unpaid when settled).
                                                          WHEN eodSchedule.totalCustomerArrears >= 4*eodScheduleRentalProfile.rentalProfileAmount
                                                                        AND eodSchedule.totalCustomerArrears <= 5*eodScheduleRentalProfile.rentalProfileAmount
                                                                                      THEN '4'
                                                          -- More than five but less than six payments due but unpaid (or were due and unpaid when settled).
                                                          WHEN eodSchedule.totalCustomerArrears >= 5*eodScheduleRentalProfile.rentalProfileAmount
                                                                        AND eodSchedule.totalCustomerArrears <= 6*eodScheduleRentalProfile.rentalProfileAmount
                                                                                      THEN '5'
                                                          -- Six or more payments due but unpaid or (or due and unpaid when settled).
                                                          WHEN eodSchedule.totalCustomerArrears >= 6*eodScheduleRentalProfile.rentalProfileAmount
                                                                                      THEN '6'                                                                                      
                                                          ELSE 'U'
                                           END AS "Account_Status",
                                                                                                                                  
                                                                                                                                                                                                                                                                                                                                                                                                         
                                           -- #23 | 265 (+1) | Flag Setting | This field will be set to blank.
                                           CASE
                                             -- This will be set from any CAIS Override record for this Third Party Billing Address and Schedule combination.
                                             WHEN eodScheduleCaisDefault.applicableCaisFlagCode != '-' AND eodScheduleCaisDefault.applicableCaisFlagCode IN ('D', 'P', 'C', 'S', 'G', 'R', 'V', 'A', 'M', 'I', 'Q') THEN eodScheduleCaisDefault.applicableCaisFlagCode
                                             -- If a CAIS override flag other than the above, or no override record exists, then the flag will be set to blank.
                                             ELSE LPAD(' ', 1, ' ') -- x1 -- blank field
                                           END AS "Flag_Setting",
                                           
                                           --
                                           
                                           -- #24 | 266 to 440 (+175) | Name and Address | The name will be the Billing Address Internal Name on the Billing Address file for the invoicing Customer Billing Address.                                             
                                           
                                           -- Formatted as follows: Name 39 characters, Address Line 1 32 characters, Address Line 2 32 characters, Address Line 3 32 characters, Address Line 4 32 characters, Post Code 8 characters 
                                           -- Fields will be left justified with following blanks.

                                           -- Use related third party name when joint consumer
                                           CASE WHEN COBORROWER.name IS NULL THEN                                            
                                              LPAD(COALESCE(eodBillingAddress.name, '0'), 39, ' ')
                                           ELSE 
                                              LPAD(COALESCE(COBORROWER.name, '0'), 39, ' ')
                                           END AS "Name",
                                           
                                           CASE WHEN COBORROWER.name IS NULL THEN   
                                           LPAD(COALESCE(eodBillingAddress.addressLine1, '0'), 32, ' ')
                                           ELSE  LPAD(COALESCE(COBORROWER.addressLine1, '0'), 32, ' ')
                                           END AS "Address_1",
                                           CASE WHEN COBORROWER.name IS NULL THEN   
                                           LPAD(COALESCE(eodBillingAddress.addressLine2, '0'), 32, ' ')
                                           ELSE  LPAD(COALESCE(COBORROWER.addressLine2, '0'), 32, ' ')
                                           END AS "Address_2",                                           
                                           CASE WHEN COBORROWER.name IS NULL THEN   
                                           LPAD(COALESCE(eodBillingAddress.addressLine3, '0'), 32, ' ')
                                           ELSE  LPAD(COALESCE(COBORROWER.addressLine3, '0'), 32, ' ')
                                           END AS "Address_3",                                           
                                           CASE WHEN COBORROWER.name IS NULL THEN   
                                           LPAD(COALESCE(eodBillingAddress.addressLine4, '0'), 32, ' ')
                                           ELSE  LPAD(COALESCE(COBORROWER.addressLine4, '0'), 32, ' ')
                                           END AS "Address_4",
                                           CASE WHEN COBORROWER.name IS NULL THEN   
                                           LPAD(COALESCE(eodBillingAddress.postalCode, '0'), 8, ' ')
                                           ELSE  LPAD(COALESCE(COBORROWER.postalCode, '0'), 8, ' ')
                                           END AS "Postcode",                                           

                                           
                                           
                                           -- #32 | 469 to 476 (+8) | Default Satisfaction Date |                                             

                                           -- If the CAIS Account Status is 8 (in default) and there are no outstanding items due from the customer, 
                                            -- Alfa will set the Default Satisfaction Date to the date of the receipt that settled the account in DDMMCCYY format.
                                             CASE
                                                          WHEN eodScheduleCaisDefault.caisDefaultNoticeDate IS NOT NULL AND eodSchedule.totalCustomerArrears <= 0
                                                                        THEN TO_CHAR(
                                                                                      (SELECT MAX(transactionEffectiveDate) FROM eodCashAllocation eodCashAllocation
                                                                                                     WHERE eodCashAllocation.scheduleId = eodSchedule.odsId
                                                                                                     AND eodCashAllocation.locationType IN ('Invoice Line', 'Schedule suspense', 'Agreement suspense')

                                                                                                     AND eodCashAllocation.transactionStatus = 'Applied'
                                                                                      ), 'DDMMYYYY')
                                                          ELSE LPAD('0', 8, '0')
                                           END AS "Default_Satisfaction_Date",
                                             
                                       
                                             
                                            -- #35 | 503 to 509 (+7) | Original Default Balance
                                           -- If the CAIS Account Status is 8 (in default) then this will be set to the Default Amount from the CAIS Override record for this Third Party Billing Address and Schedule combination. 
                                            -- The Default Original Balance is manually entered and should be the Current Balance as at the Default Date. Only full pounds (GBP) will be entered.
                                           -- Otherwise filled with zeros
                                           CASE 
                                                         WHEN eodScheduleCaisDefault.originalCaisDefaultBalance != 0 THEN LPAD(cast(ROUND(eodScheduleCaisDefault.originalCaisDefaultBalance, 0) as text), 7, '0')
                                                          ELSE '0000000'
                                           END AS "Original_Default_Balance",
                                           
                                            --
                                           
                                            -- #36 | 510 (+1)| Payment Frequency Indicator | This will be set from the Rental Frequency of the main Rental Profile as follows
                                                          -- The main rental profile will be considered to be the rental profile with the highest number of rentals in the primary period. 
                                                           -- If there is a tie, the rental profile with the lowest frequency will be chosen (e.g. monthly would be chosen over quarterly).
                                           CASE                                 
                                                          -- Set to ‘M’ (Monthly) if the Rental Frequency is 1. 
                                                          WHEN eodScheduleRentalProfile.rentalFrequency = '1 month' THEN 'M'
                                                          -- Set to ‘Q’ (Quarterly) if the Rental Frequency is 3.
                                                          WHEN eodScheduleRentalProfile.rentalFrequency = '3 months' THEN 'Q'
                                                          -- Set to ‘A’ (Annually) if the Rental Frequency is 12.
                                                          WHEN eodScheduleRentalProfile.rentalFrequency = '12 months' THEN 'A'
                                                          -- Set to ‘P’ (Periodically) for all other values.
                                                          ELSE 'P'
                                           END AS "Payment_Frequency"                              
                                                                                                                                                                                                                  
                                                                                      
                                                                        
              
              -- Scheudle are the primary source for records in the extract        
                            FROM eodSchedule eodSchedule          
              
              -- Joining to the Agreement                       
                             JOIN eodAgreement eodAgreement ON eodSchedule.agreementId = eodAgreement.odsId
                             
              -- Joining to the Company and assocuated credit reference agencies
                             JOIN eodCompanyCreditRefAgency eodCompanyCreditRefAgency ON eodAgreement.agrCompanyId = eodCompanyCreditRefAgency.companyId                        
                                                                                                                                  
              -- Joining to ThirdPartyType on exposure customer
                             JOIN eodThirdParty eodThirdParty ON eodAgreement.expCusId = eodThirdParty.odsId
                             JOIN eodBillingAddress eodBillingAddress ON eodBillingAddress.OdsId = eodAgreement.expCusBillingId

              -- Join with related billing addresses of type co-borrower (joint consumer agreements)               
                             LEFT JOIN (
                                  SELECT eodAgreement.odsId, eodBillingAddress.name, eodBillingAddress.addressLine1, eodBillingAddress.addressLine2, eodBillingAddress.addressLine3, eodBillingAddress.addressLine4, eodBillingAddress.postalCode,
                                         (select count(*) FROM eodbillingAddRelationship A where A.billingaddressid=eodAgreement.expCusBillingId and A.relatedbillingaddressid <= eodbillingAddRelationship.relatedbillingaddressid) AS seq
                                  FROM eodAgreement eodAgreement 
                                  JOIN eodbillingAddRelationship eodbillingAddRelationship ON eodbillingAddRelationship.billingaddressid = eodAgreement.expCusBillingId
                                  JOIN eodBillingAddress eodBillingAddress ON eodBillingAddress.OdsId = eodbillingAddRelationship.relatedbillingaddressid
                                  JOIN (SELECT billingaddressid, min(relatedbillingaddressid) AS minid FROM eodbillingAddRelationship GROUP BY billingaddressid) COUNTER ON COUNTER.billingaddressid =  eodbillingAddRelationship.billingaddressid
                                  WHERE relationshipTypeCode='9' 
                                  ORDER BY eodbillingAddRelationship.billingaddressid, seq ASC
                                  ) COBORROWER ON COBORROWER.odsId = eodAgreement.odsId
                                           
              -- Need to filter for main rental profile
                             JOIN eodScheduleRentalProfile eodScheduleRentalProfile ON eodScheduleRentalProfile.scheduleId = eodSchedule.odsId                                                                                                                                                                                                                                                                                                                                                                                          
                                                                                                                   
              -- Joining to agreement currency to filter for only GBP
                             JOIN eodAgreementCurrency eodAgreementCurrency ON eodAgreement.agrCurrencyId = eodAgreementCurrency.odsId          
                             
              -- Left join to eodScheduleCaisDefault for any overrides                                                                           
                             LEFT OUTER JOIN eodScheduleCaisDefault eodScheduleCaisDefault ON eodScheduleCaisDefault.scheduleId = eodSchedule.odsId     
                             
              -- Join to any processed terminations
                             LEFT OUTER JOIN eodTerminationQuote eodTerminationQuote ON eodTerminationQuote.scheduleId = eodSchedule.odsId
                             
              -- Join to latest cash allocation transaction
                             LEFT OUTER JOIN eodCashAllocation eodCashAllocationLatest ON eodCashAllocationLatest.scheduleId = eodSchedule.odsId                                                                                                                                                                                                                                                                    

              -- Schedules set to ‘in default’ via a CAIS Override record will continue to be sent for 6 years from the default date.                                                                                                                             
                             WHERE 
                             (
                                           CASE 
                                           -- Schedules set to ‘in default’ via a CAIS Override record will continue to be sent for 6 years from the default date,
                                                          WHEN eodScheduleCaisDefault.caisDefaultNoticeDate IS NOT NULL 
                                                          -- TODO AND eodScheduleCaisDefault.caisDefaultNoticeDate > 0
                                                                        then -- Subtract 6 years and compare to the system date 
                                                                        (eodScheduleCaisDefault.caisDefaultNoticeDate - INTERVAL '6 years') <= (SELECT systemDateAsDate FROM eodTmpSystemDate)
                                                          ELSE TRUE
                                           END
                                           )                                          

                             
              -- If a schedule has no outstanding balance and zero arrears and is expired, matured, or terminated and there has been no activity for over 2 months, the schedule will be excluded. 
              -- The ‘last activity’ will be the latest payment or a termination being processed.
                           AND (
                                          CASE
                                                          -- If a schedule has no outstanding balance and zero arrears and is expired, matured, or terminated
                                                         WHEN eodSchedule.totalCustomerArrears <= 0 AND eodSchedule.capitalOutstanding <= 0 AND eodSchedule.scheduleStatus IN ('Terminated', 'Matured', 'Expired')
                                                                        THEN 
                                                                        (
                                                                                      -- and there has been no activity for over 2 months.
                                                                                      -- The ‘last activity’ will be the latest payment or a termination being processed
                                                                                      CASE 
                                                                                                     WHEN eodSchedule.terminationNumber = 1                                                                                                                                                                
                                                                                                                   THEN 
                                                                                                                   -- If terminated, get the termination date                                                                                                                 
                                                                                                                                 (SELECT terminationDate FROM eodTerminationQuote 
                                                                                                                                 WHERE terminationType = 'Full' 
                                                                                                                                  AND quoteProcessingStatus = 'Terminated'
                                                                                                                                  AND scheduleId = eodSchedule.odsId)
                                                                                                                                 -- Subtract 2 months and compare to the system date
                                                                                                                                  - INTERVAL '2 months' <= (SELECT systemDateAsDate FROM eodTmpSystemDate)
                                                                                                                                  
                                                                                                     -- If not terminated use the laste cash allocation transaction date
                                                                                                     ELSE  eodCashAllocationLatest.transactionEffectiveDate
                                                                                                     -- Subtract 2 months and compare to the system date 
                                                                                                                   - INTERVAL '2 months' <= (SELECT systemDateAsDate FROM eodTmpSystemDate)
                                                                                      END
                                                                        )
                                                         ELSE TRUE
                                          END
                                          )
                                          
                                                                                                     
              -- Filter for latest cash allocation transaction
                             AND eodCashAllocationLatest.odsId IN (select distinct on(scheduleid) odsId FROM eodCashAllocation order by scheduleid, transactionEffectiveDate desc) 
                             
              -- Filter for only the primary rental profiles
                             AND eodScheduleRentalProfile.odsId IN (SELECT distinct on(scheduleid) odsId FROM eodScheduleRentalProfile order by scheduleid, numberOfRentals desc)
                             
              -- Filter for only live and fully terminated schedules where schedule start date is on or before system date                                                                              
                             AND eodSchedule.terminationNumber IN (0, 1)                                         
                             AND eodSchedule.startDate <= (SELECT systemDateAsDate FROM eodTmpSystemDate)
                             
              -- Filter for only GBP
                             AND eodAgreementCurrency.agrCurrencyCode = 'GBP'
                             
              -- Schedules will be included in the Consumer CAIS extract if the invoicing customer on the agreement has the appropriate credit agency type/legal entity type/CAIS limited status set.
                             AND eodCompanyCreditRefAgency.creditRefAgencyCode = 'CAIS UK personal'
                             AND eodThirdParty.caisLegalEntityType = 'Personal'
                             AND eodThirdParty.thirdPartyType IN ('IND','JOI') 


        ORDER BY eodAgreement.agreementNumber,COBORROWER.seq