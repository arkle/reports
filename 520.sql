-- Creditors [520] v1.0
SELECT AGR.agreementnumber as "Agreement Number",
    EXPCUSBIL.expCusBillingName as "Exposure Customer",
    PAYBILL.payeeBillingUniqueRef as "Payee Third Party Reference",
    payeeBillingName as "Payee Name",
    payableChargeTypeName as "Charge Type",
    dueDate as "Due Date",
    AMOUNT as "Net Amount",
    salesTaxAmount as "VAT amount",
    AMOUNT + salesTaxAmount as "Total amount"
FROM OdsPayable PAYB
    JOIN OdsPayeeBilling PAYBILL ON PAYBILL.id = PAYB.payeeBillingId
    JOIN OdsAgreement AGR ON PAYB.agreementId = AGR.id
    JOIN OdsPayableChargeType CHAT ON PAYB.payableChargeTypeId = CHAT.id
    JOIN OdsExposureCustomerBilling EXPCUSBIL ON AGR.expCusBillingId = EXPCUSBIL.id
WHERE ISPAID = false
    and isdue = TRUE
ORDER BY AGR.agreementnumber, dueDate