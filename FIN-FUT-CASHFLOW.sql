-- FIN-FUT-CASHFLOW:    Future Cashflow
-- Title:               Portfolio Report (Finance Version)
-- Summary:             Future cashflow for all live agreements. Data is extracted from the beginning of the current month (based on date of day prior).
SELECT AGR.agreementnumber AS "AGREEMENT",
    to_char(CASHFLOW.cashfloweventdate, 'DD/MM/YYYY') AS "CASHFLOW_EVENT_DATE",
    CASHFLOW.repayments AS "REPAYMENT",
    CASHFLOW.repayments + CASHFLOW.profit AS "CAPITAL_REPAID",
    CASHFLOW.profit * -1 AS "INTEREST",
    CASHFLOW.capitaloutstanding * -1 AS "CAP_OS"
FROM eodScheduleCashFlow CASHFLOW
    JOIN eodSchedule SCHED ON SCHED.odsid = CASHFLOW.scheduleId
    JOIN eodAgreement AGR ON AGR.odsid = SCHED.agreementid
    JOIN eodSystemDate ON 1 = 1 
WHERE CASHFLOW.isPrimaryAccountingStandard = false
    AND CASHFLOW.cashfloweventdate >= date_trunc('month', systemdate)