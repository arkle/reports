-- Cash [500] v1.0
SELECT BIL.uniqueRef AS "TP Reference",
    AGR.agreementNumber AS "Schedule Reference",
    CTX.inputDate AS "Input_date",
    CSH.settlementDate AS "Effective Date",
    cshAllcPaymentTypeName AS "Payment Type",
    C.chargeName AS "Outbound Charge Type",
    B.chargeName AS "Inbound Charge Type",
    CUR.cshAllcCurrencyCode AS "Currency",
    CAL.totalAmount
FROM OdsCashAllocation CAL
    JOIN OdsCashAllocationTxn CTX ON CAL.cashAllocationTransactionId = CTX.id
    JOIN OdsCashAllocationCurrency CUR ON CAL.allocationCurrencyId = CUR.id
    JOIN OdsAgreement AGR ON CAL.agreementId = AGR.id
    JOIN OdsBillingAddress BIL ON CAL.billingAddressId = BIL.id
    JOIN OdsCashInOut CSH ON CTX.cashId = CSH.id
    JOIN OdsCashAllocationPaymentType PYT ON CSH.paymentTypeId = PYT.id
    LEFT OUTER JOIN (
        SELECT IVL.id AS invoiceLineId,
            CTP.code AS chargeCode,
            CTP.name AS chargeName
        FROM OdsInvoiceLine IVL
            JOIN OdsChargeType CTP ON IVL.chargeTypeId = CTP.id
    ) AS B ON CAL.invoiceLineId = B.invoicelineid
    LEFT OUTER JOIN (
        SELECT SIL.id AS SupplierInvoiceLineId,
            CTP2.payableChargeTypeCode AS chargeCode,
            CTP2.payableChargeTypeName AS chargeName
        FROM OdsSupplierInvoiceLine SIL
            JOIN OdsPayableChargeType CTP2 ON SIL.payableChargeTypeId = CTP2.id
    ) AS C ON CAL.supplierInvoiceLineId = C.SupplierInvoiceLineId
WHERE CAL.transactionStatus = 'Applied'
ORDER BY BIL.uniqueRef;