-- ACQUIS:  Acquis Report
-- Title:   Acquis Report
-- Summary: Provides details of agreements and their Acquis status. Excludes agreements where the Acquis Status is not applicable.
SELECT AGR.agreementNumber AS "Agreement",
    CASE
        WHEN MISC.agrid = '<None>' THEN ''
        ELSE MISC.agrid
    END AS "Alternative_Agreement",
    SCHED.scheduleStatus AS "Agreement_Status",
    CASE
        WHEN ALERT.cais = false THEN ''
        ELSE 'Y'
    END AS "Undisclosed",
    SCHED.totalScheduleArrears AS "Total_Arrears",
    AGR.product AS "Product",
    SCHED.termInMonths AS "Term",
    SCHED.startDate AS "Start_date",
    SCHED.expectedEndDate AS "Expected_end_date",
    --  Customer info
    INVCUS.invCusThirdPartyNumber AS "Invoice_cust_id",
    INVCUS.invCusName AS "Invoice_cust_name",
    INVCUS.invCusCaisLegalEntityType AS "Invoice_cust_legal_type",
    INVCUS.invCusCompanyRegNo AS "Company_reg",
    INVBIL.invCusBillingEmailAddressEnc AS "Customer_Email",
    --  Asset information
    ASSET.assetTypeName AS "Asset_type",
    ASSET.assetDescription AS "Asset_description",
    ASSET.assetclassificationcode AS "Asset_Classification_Code",
    ASSET.assetclassificationname AS "Asset_Classification_Name",
    --  Acquis info
    ACQUIS.insuranceStatus AS "Acquis_status",
    ACQUISTODATE.acquis_no_to_date AS "Instalments_to_date",
    ACQUISTODATE.acquis_billed_to_date AS "Billed_to_date",
    ACQUISPAIDTODATE.acquis_paid_to_date AS "Paid_to_date",
    ACQUISFUTURE.acquis_no_future AS "Instalments_future",
    ACQUISFUTURE.acquis_to_be_billed AS "Future_billing",
    VAP.agrVapInsuranceTypeCode AS "Insurance_type_code",
    VAP.agrVapInsuranceType AS "Insurance_type",
    VAP.cancellationDate AS "Cancellation_date"
FROM OdsSchedule SCHED
    JOIN OdsAgreement AGR ON AGR.id = SCHED.agreementid
    JOIN OdsScheduleLlisl ACQUIS ON ACQUIS.scheduleId = SCHED.id
    AND ACQUIS.insuranceStatus != 'Not Applicable'
    JOIN OdsInvoicingCustomer INVCUS ON INVCUS.id = SCHED.invCusId
    JOIN OdsInvoicingCustomerBilling INVBIL ON INVBIL.id = SCHED.invCusBillingId
    JOIN OdsExposureCustomer EXPCUS ON EXPCUS.id = SCHED.expCusId
    JOIN OdsAgreementMiscellaneous MISC ON MISC.agreementid = AGR.id
    JOIN OdsAgreementAlert ALERT ON ALERT.agreementid = AGR.id
    LEFT JOIN (
        SELECT scheduleid,
            vapId,
            agrVapInsuranceTypeCode,
            agrVapInsuranceType,
            cancellationDate
        FROM OdsScheduleVap
            JOIN OdsAgrVap ON OdsAgrVap.id = OdsScheduleVap.vapId
            AND OdsAgrVap.agrVapCode = 'ASPR'
    ) VAP ON VAP.scheduleid = SCHED.id
    LEFT JOIN (
        SELECT scheduleid,
            assetTypeName,
            assetDescription,
            assetclassificationcode,
            assetclassificationname
        FROM OdsAsset
        WHERE isMainAsset = '1'
    ) ASSET ON ASSET.scheduleid = SCHED.id
    LEFT JOIN (
        SELECT scheduleid,
 AGR.agreementnumber AS "Agreement_Number", 
    SUM(INL.invoicelineamount) AS "acquis_paid_to_date"
FROM OdsInvoiceLine INL
JOIN OdsChargeType CHG ON INL.chargetypeid = CHG.Id
JOIN OdsAgreement AGR ON INL.agreementId = AGR.Id
WHERE CHG.code = '105' 
     AND INL.ispaid = TRUE
    GROUP BY AGR.agreementnumber,
INL.ispaid, 
inl.scheduleid
    ) ACQUISPAIDTODATE ON ACQUISPAIDTODATE.scheduleid = SCHED.id
    LEFT JOIN (
        SELECT RECEIVABLE.scheduleid,
            RECEIVABLE.vapId,
            COUNT(receivableIdentifier) AS acquis_no_to_date,
            SUM(RECEIVABLE.amount) AS acquis_billed_to_date
        FROM OdsReceivable RECEIVABLE
        WHERE RECEIVABLE.isDue = true
        GROUP BY RECEIVABLE.scheduleid,
            RECEIVABLE.vapId
    ) ACQUISTODATE ON ACQUISTODATE.scheduleid = SCHED.id
    AND ACQUISTODATE.vapId = VAP.vapId
    LEFT JOIN (
        SELECT RECEIVABLE.scheduleid,
            RECEIVABLE.vapId,
            COUNT(receivableIdentifier) AS acquis_no_future,
            SUM(RECEIVABLE.amount) AS acquis_to_be_billed
        FROM OdsReceivable RECEIVABLE
        WHERE RECEIVABLE.isInvoiced = false
        GROUP BY RECEIVABLE.scheduleid,
            RECEIVABLE.vapId
    ) ACQUISFUTURE ON ACQUISFUTURE.scheduleid = SCHED.id
    AND ACQUISFUTURE.vapId = VAP.vapId
ORDER BY AGR.agreementNumber