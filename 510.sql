-- Debtors [510] v1.0
SELECT SCHE.scheduleRef AS "Schedule Reference",
    SCHE.activationDate AS "Activated Date",
    CUST.invCusBillingUniqueRef AS "Invoicee Billing Address Reference",
    CUST.invCusBillingName AS "Invoicee Name",
    CHGT.name AS "Charge Type",
    CURR.agrCurrencyCode AS "Transaction Currency",
    SUM(INVL.unpaidAmount) AS "Total Overdue"
FROM OdsInvoiceLine INVL
    JOIN OdsInvoiceHeader INVH ON INVL.invoiceHeaderId = INVH.id
    JOIN OdsChargeType CHGT ON INVL.chargeTypeId = CHGT.id
    JOIN OdsScheduleMain SCHE ON INVL.scheduleId = SCHE.id
    AND SCHE.totalScheduleArrears > 0
    JOIN OdsAgreement AGRE ON SCHE.agreementId = AGRE.id
    JOIN OdsInvoicingCustomerBilling CUST ON INVH.inv3pyBillingId = CUST.id
    JOIN OdsAgreementCurrency CURR ON SCHE.agrCurrencyId = CURR.id
WHERE INVL.isDue = true
    AND INVL.isPaid = false
GROUP BY SCHE.scheduleRef,
    SCHE.activationDate,
    CUST.invCusBillingUniqueRef,
    CUST.invCusBillingName,
    CHGT.name,
    CURR.agrCurrencyCode
ORDER BY SCHE.scheduleRef,
    CUST.invCusBillingUniqueRef,
    CUST.invCusBillingName,
    CHGT.name,
    CURR.agrCurrencyCode ASC;