SELECT STAT.currentstatusstatusname AS "Workflow_Stage",
    ALLOC.allocatedtousername AS "Allocated_to",
    CAS.caseidsequencenumber AS "Case_ID",
    AGR.agreementnumber AS "Agreement",
    TPY.case3pyname AS "Third_Party",
    CAS.openeddate AS "Case_Opened_Date",
CREDIT.decisionstatus,   CREDIT.approvalexpirydate AS "EXPIRY_DATE__C"
FROM OdsCase CAS
    LEFT JOIN OdsAgreement AGR ON AGR.id = CAS.agreementid
    LEFT JOIN OdsAgreementCreditDecision CREDIT ON CREDIT.agreementId = AGR.id 
    JOIN OdsCurrentCaseStatus STAT ON STAT.id = CAS.currentstatusid
    JOIN OdsAllocatedToUser ALLOC ON ALLOC.id = CAS.allocatedtouserid
    LEFT JOIN OdsCaseThirdParty TPY ON TPY.id = CAS.case3pyid
WHERE CAS.status = 'Open'
    AND CREDIT.decisionstatus = 'Approved'
ORDER BY CREDIT.approvalexpirydate 