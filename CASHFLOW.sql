-- CASHFLOW:    Cashflow Report v1.0
-- Title:       Cashflow Report
-- Summary:     Cashflow showing unearned income by day in selected range
-- Parameter:   Date_from, Date_to
-- IMPORTANT:   Changes made to this report should also be considered for the Finance equivalent

SELECT

        CASHFLOW.date                                   AS "Date",
        CASHFLOW.instalments                            AS "Instalments",
        ABS(CASHFLOW.income)                            AS "Income",
--      TOTALVAT.check,             
        to_char(TOTALVAT.vat_amount,'999999.999')      AS "VAT"

FROM ( SELECT cashFlowEventDate AS "date", SUM(repayments) AS "instalments", SUM(profit) AS "income", SUM(expenses) AS "exp"
        FROM OdsScheduleCashFlow
        WHERE isPrimaryAccountingStandard = false  
        GROUP BY cashFlowEventDate 
        HAVING ( cashFlowEventDate >= to_date(:Date_from, 'YYYY-MM-DD') 
           AND cashFlowEventDate <= to_date(:Date_to, 'YYYY-MM-DD')) ) CASHFLOW 

        JOIN (  SELECT VAT.dueDate, SUM(VAT.rcvamount) AS "check", SUM(vatamount) AS vat_amount 
                FROM (                
                        SELECT RECV.dueDate, VATRATE.rate, SUM(RECV.amount) AS "rcvamount", SUM(RECV.amount)*(VATRATE.rate/100) AS "vatamount"    
                        FROM OdsReceivable RECV 
                        JOIN OdsReceivableChargeType CHRGTYP 
                        ON CHRGTYP.id = RECV.recvChargeTypeId
                        AND CHRGTYP.recvchargetypecode IN (1,2,3,45) -- Lease Rental, Loan/HP Repayment, Loan/HP Repayment - Interest, Lease Rental - Interest
                        JOIN OdsReceivableVatType VATTYP 
                        ON VATTYP.id = RECV.recvVatTypeId        
                        JOIN (  SELECT B.vatTypeCode, A.rate 
                                FROM OdsVatRate A
                                JOIN OdsVatType B ON A.vatTypeId = B.id
                                JOIN ( SELECT vatTypeCode, max(rateDate) AS effectiverateDate
                                FROM OdsVatRate 
                                JOIN OdsVatType ON OdsVatType.id = OdsVatRate.vatTypeId
                                JOIN OdsSystemDate ON 1 = 1 
                                WHERE rateDate <= systemdate
                                GROUP BY vatTypeCode ) C
                                ON C.vatTypeCode = B.vatTypeCode
                                AND C.effectiverateDate = A.rateDate ) VATRATE 
                                ON VATRATE.vatTypeCode = VATTYP.recvVatTypeCode  
                        WHERE RECV.isterminated = false             
                        GROUP BY RECV.dueDate, VATRATE.rate ) VAT 
                GROUP BY VAT.dueDate) TOTALVAT 
        ON TOTALVAT.dueDate = CASHFLOW.date

ORDER BY CASHFLOW.date