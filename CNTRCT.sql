-- CNTRCT:      Contract Analysis Report 
-- Summary:     Provides details of the portfolio covering all schedules in all statuses for a specified broker
-- Parameter:   Broker_name -- free entry due to limit on 100 returns from sql used for a parameter - report will do a like match
SELECT --  Company info
    CASE
        WHEN AGRCO.agrcompanycode IN ('2', '4') THEN 'Evington'
        ELSE 'Arkle'
    END AS "Company",
    CASE
        WHEN AGRCO.agrcompanycode = '1' THEN 'Core'
        WHEN AGRCO.agrcompanycode IN ('2', '4') THEN 'Consumer'
        WHEN AGRCO.agrcompanycode = '3' THEN 'Prime'
    END AS "Pricing",
    --  Broker info
    DLR.dealerName AS "Broker",
    DLRBILL.dealerBillingAddressLine1 AS "Billing_address_line_1",
    CASE
        WHEN BILSALPER.salesperson > '' THEN BILSALPER.salesperson
        ELSE SALPER.salesperson
    END AS "Broker_Manager",
    --  Customer info
    INVCUS.invCusThirdPartyNumber AS "Invoice_cust_id",
    INVCUS.invCusName AS "Invoice_cust_name",
    --  Flag if the customer undisclosed
    CASE
        WHEN EXPCUS.expCusThirdPartyNumber = INVCUS.invCusThirdPartyNumber THEN ''
        ELSE 'Y'
    END AS "Undisclosed",
    --  Flag if the customer is vulnerable or has an active complaint (based on the Invoicing customer)  
    CASE
        WHEN VULNERABLE.thirdpartyid > 0 THEN 'Y'
        ELSE ''
    END AS "Vulnerable",
    CASE
        WHEN COMPLAINT.thirdpartyid > 0 THEN 'Y'
        ELSE ''
    END AS "Complaint_open",
    --  Agreement information                                           
    AGR.agreementNumber AS "Agreement",
    AGR.agreementType AS "Agreement_type",
    SCHED.scheduleDescription AS "Description",
    CASE
        WHEN AGR.isCcaRegulated = '1' THEN 'Y'
        ELSE ''
    END AS "Regulated",
    --  Agrement details
    AGR.product AS "Product",
    ASSETDESC.assetDescription AS "Asset",
    SCHED.termInMonths AS "Term",
    CASE
        WHEN PRACTICAL.agreementid IS NULL THEN SCHED.activationDate
        ELSE SCHED.startDate
    END AS "Activated_date",
    SCHED.startDate AS "Start_date",
    SCHED.expectedEndDate AS "Expected_end_date",
    SCHED.maturityDate AS "Maturity_date",
    SCHED.secondaryPeriodExpiryDate AS "Secondary_expiry",
    --  ACTUSER.activatedByUserName             AS "Activated_by",
    CASE
        WHEN PRACTICAL.agreementid IS NULL THEN SCHED.scheduleStatus
        ELSE 'Practical DAC (Paid_Out)'
    END AS "Status",
    CASE
        WHEN SCHED.isTerminated = '1' THEN 'Y'
        ELSE ''
    END AS "Terminated",
    SCHED.originalAssetCostFinanced AS "Amount_financed",
    DOCFEE.doc_fee AS "Document_fee",
    DOCFEESPLIT.doc_fee_split AS "Document_fee_split",
    OTPFEE.optpfee AS "OTP_Fee",
    RENTALSHARE.rental_share_paid AS "Rental_share",
    --  Rates
    COMM.commission AS "Commission",
    ROUND(
        (
            COMM.commission / SCHED.originalAssetCostFinanced
        ) * 100,
        2
    ) AS "Commission_pct",
    SCHED.combinedLessorPtoRate AS "Yield",
    --  Current balances
    SCHED.grossBalanceRemaining AS "Balance_OS",
    SCHED.capitalOutstanding AS "Capital_OS",
    AGR.agreementSuspense AS "Suspense",
    SCHED.totalScheduleArrears AS "Schedule_arrears",
    SCHED.totalCustomerArrears AS "Customer_arrears",
    SCHED.inArrearsSinceDate AS "In_Arrears_since",
    --  Rental info
    SCHED.monthsInExtension AS "Months_in_extension",
    SCHED.numberOfFutureRentals AS "Future_rentals",
    sched.nextRentalFrequency AS "Next_rental_freq"
FROM OdsSchedule SCHED
    JOIN OdsAgreement AGR ON AGR.id = SCHED.agreementid
    JOIN OdsInvoicingCustomer INVCUS ON INVCUS.id = SCHED.invCusId
    JOIN OdsExposureCustomer EXPCUS ON EXPCUS.id = SCHED.expCusId
    JOIN OdsDealer DLR ON DLR.id = SCHED.dealerId
    AND LOWER(DLR.dealername) LIKE LOWER(CONCAT('%', :Broker_name, '%'))
    JOIN OdsDealerBilling DLRBILL ON DLRBILL.id = SCHED.dealerBillingId
    LEFT JOIN (
        SELECT agreementId,
            'Y' AS practical_paid_out
        FROM OdsAgreementAlert
        WHERE ppo = '1'
    ) PRACTICAL ON PRACTICAL.agreementId = AGR.id --  Asset description of most value
    JOIN OdsAsset ASSETDESC ON ASSETDESC.agreementid = AGR.id
    AND ASSETDESC.isMainAsset = True
    JOIN OdsAgreementCompany AGRCO ON AGRCO.id = SCHED.agrCompanyId --  JOIN OdsActivatedByUser ACTUSER  
    --      ON ACTUSER.id = SCHED.activatedByUserId
    LEFT JOIN OdsScheduleLlisl ACQUIS ON ACQUIS.scheduleId = SCHED.id
    LEFT JOIN OdsNextRentalPaymentType NEXTRENTAL ON NEXTRENTAL.id = SCHED.nextRentalPaymentTypeId -- Identify Third Parties with a Complaint Alert
    LEFT JOIN (
        SELECT thirdpartyid
        FROM OdsThirdPartyAlert
        WHERE com_code = '1'
            AND (
                com_code_start <= CURRENT_DATE
                AND com_code_exp >= CURRENT_DATE
            )
    ) COMPLAINT ON COMPLAINT.thirdpartyid = INVCUS.id -- Identify Third Parties with a Vulnerable Customer Alert
    LEFT JOIN (
        SELECT thirdpartyid
        FROM OdsThirdPartyAlert
        WHERE tex_code = '1'
            AND (
                tex_code_start <= CURRENT_DATE
                AND tex_code_exp >= CURRENT_DATE
            )
    ) VULNERABLE ON VULNERABLE.thirdpartyid = INVCUS.id -- Identify Asset of most value
    LEFT JOIN (
        SELECT scheduleid,
            assetTypeName,
            assetDescription
        FROM OdsAsset
        WHERE isMainAsset = '1'
    ) ASSET ON ASSET.scheduleid = SCHED.id -- Identify the Broker Manager - select just one Salesperson associated with the third party in case there are more than one
    LEFT JOIN (
        SELECT DLRSAL.dealerid,
            SAL.name AS "salesperson"
        FROM OdsSalesPerson SAL
            JOIN (
                SELECT dealerid,
                    MIN(salespersonid) AS "salesperson_id"
                FROM OdsDealerSalesperson
                GROUP BY dealerid
            ) DLRSAL ON SAL.id = DLRSAL.salesperson_id
    ) SALPER ON SALPER.dealerid = DLR.id -- Identify the Broker Manager at the Billing Address Level - select just one Salesperson associated with the third party in case there are more than one
    LEFT JOIN (
        SELECT DLRBILSAL.dealerBillingId,
            SAL.name AS "salesperson"
        FROM OdsSalesPerson SAL
            JOIN (
                SELECT dealerBillingId,
                    MIN(salespersonid) AS "salesperson_id"
                FROM OdsDealerBillingSalesperson
                GROUP BY dealerBillingId
            ) DLRBILSAL ON SAL.id = DLRBILSAL.salesperson_id
    ) BILSALPER ON BILSALPER.dealerBillingId = DLRBILL.id -- Identify commission
    LEFT JOIN (
        SELECT PAYABLE.scheduleid,
            SUM(PAYABLE.amount) AS "commission"
        FROM OdsPayable PAYABLE
            JOIN OdsPayableChargeType PCT ON PCT.id = PAYABLE.payableChargeTypeId
            AND PCT.payableChargeTypeCode = 605
        GROUP BY PAYABLE.scheduleid
    ) COMM ON COMM.scheduleid = SCHED.id -- Identify document fee
    LEFT JOIN (
        SELECT RECEIVABLE.scheduleid,
            SUM(RECEIVABLE.amount) AS "doc_fee"
        FROM OdsReceivable RECEIVABLE
            JOIN OdsReceivableChargeType RCT ON RCT.id = RECEIVABLE.recvchargetypeid
            AND RCT.recvchargetypecode = 103
        GROUP BY RECEIVABLE.scheduleid
    ) DOCFEE ON DOCFEE.scheduleid = SCHED.id -- Identify document fee split paid to broker
    LEFT JOIN (
        SELECT PAYABLE.scheduleid,
            SUM(PAYABLE.amount) AS "doc_fee_split"
        FROM OdsPayable PAYABLE
            JOIN OdsPayableChargeType PCT ON PCT.id = PAYABLE.payableChargeTypeId
            AND PCT.payableChargeTypeCode = 614
        GROUP BY PAYABLE.scheduleid
    ) DOCFEESPLIT ON DOCFEESPLIT.scheduleid = SCHED.id -- Identify OTP fee - recvchargetypecode 13
    LEFT JOIN (
        SELECT RECEIVABLE.scheduleid,
            SUM(RECEIVABLE.amount) AS "optpfee"
        FROM OdsReceivable RECEIVABLE
            JOIN OdsReceivableChargeType RCT ON RCT.id = RECEIVABLE.recvchargetypeid
            AND RCT.recvchargetypecode = 13
        GROUP BY RECEIVABLE.scheduleid
    ) OTPFEE ON OTPFEE.scheduleid = SCHED.id -- Identify rental share (minimum term splits)
    LEFT JOIN (
        SELECT PAYABLE.scheduleid,
            SUM(PAYABLE.amount) AS "rental_share_paid"
        FROM OdsPayable PAYABLE
            JOIN OdsPayableChargeType PCT ON PCT.id = PAYABLE.payableChargeTypeId
            AND PCT.payableChargeTypeCode = 615
        GROUP BY PAYABLE.scheduleid
    ) RENTALSHARE ON RENTALSHARE.scheduleid = SCHED.id
ORDER BY DLR.dealername,
    SCHED.scheduleStatus,
    AGR.agreementnumber