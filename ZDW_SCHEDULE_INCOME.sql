-- ZDW_SCHEDULE_INCOME: xxxxxxxxxx
SELECT AGR.agreementNumber,
    STD.accountingStandardCode,
    to_char(INCOME.periodStartDate, 'DD/MM/YYYY') AS "Period_Start_Date",
    to_char(INCOME.periodEndDate, 'DD/MM/YYYY') AS "Period_End_Date",
    INCOME.periodStartDate,
    INCOME.periodEndDate,
    INCOME.rentalAccrual,
    INCOME.depreciation,
    INCOME.impairmentAccrual,
    INCOME.preEarningsExpenses,
    INCOME.earnings,
    INCOME.postEarningsExpenses,
    INCOME.fundingInterest,
    INCOME.profitBeforeTax,
    INCOME.floatingRateIncome,
    INCOME.netBookValue,
    INCOME.isRecognised,
    INCOME.isRentalRecognised,
    INCOME.isInterestRecognised,
    INCOME.isInterestRcgnsdAsSuspended,
    INCOME.isDepreciationRecognised,
    INCOME.isSuspended,
    INCOME.isTerminated,
    INCOME.isAccrued,
    INCOME.capitalOutstanding,
    INCOME.weightedAverageCapitalOS
FROM OdsScheduleIncome INCOME
    JOIN eodAccountingStandard STD ON STD.odsid = INCOME.accountingStandardId
    JOIN eodSchedule SCHED ON SCHED.odsid = INCOME.scheduleId
    JOIN eodAgreement AGR ON AGR.odsid = SCHED.agreementid
    JOIN eodSystemDate ON 1 = 1 -- Remove the JOIN below when completing a full load
    LEFT JOIN (
        SELECT scheduleId,
            max(rescheduleQuoteAppliedDate) AS "rescheduled"
        FROM eodRescheduleSummary
        WHERE rescheduleStatus NOT IN ('Cancelled', '<Unknown>')
        GROUP BY scheduleId
    ) RESCHED ON RESCHED.scheduleId = SCHED.odsid
WHERE INCOME.periodStartDate >= date_trunc('month', systemdate)
    AND (
        RESCHED.rescheduled > systemdate - 3
        OR SCHED.activationDate > systemdate - 3
    )

ORDER BY AGR.agreementNumber, INCOME.periodStartDate