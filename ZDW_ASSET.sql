-- ZDW_ASSET: Asset extract for Data Warehouse
SELECT "asset_identifier",
    "asset_secondary_identifier",
    "individual_asset_count",
    "asset_description", 
    "asset_classification_code",
    "is_main_asset",
    "is_sold",
    "supplier_ref",
    "agreement_number",
    "list_price",
    "residual_value",
    "purchase_date",
    "asset_drawdown_date",
    "vehicle_registration",
    "first_registered",
    "asset_drawdown_amount",
    "asset_drawdown_vat",
    "serial_number",
    "manufacturer_code",
    "manufacturer_name",
    "model_code",
    "model_name",
    --REGEXP_REPLACE("model_name", chr(10), '') AS "ModelName",
    "asset_source_code",
    "asset_source_name",
    "year_of_manufacture",
    "etlLogId"

FROM

(SELECT ASSET.assetIdentifier AS "asset_identifier",
    ASSET.uniqueAssetIdentifier AS "asset_secondary_identifier",
    ASSET.individualAssetCount AS "individual_asset_count",
    REPLACE(ASSET.assetDescription, ',', ';') AS "asset_description", 
    ASSET.assetclassificationcode AS "asset_classification_code",
    ASSET.isMainAsset AS "is_main_asset",
    ASSET.isSold AS "is_sold",
    BILL.uniqueRef AS "supplier_ref",
    AGR.agreementnumber AS "agreement_number",
    ASSET.listPrice AS "list_price",
    ASSET.assetResidualValueAGR AS "residual_value",
    to_char(ASSET.assetPurchaseDate, 'DD/MM/YYYY') AS "purchase_date",
    to_char(ASSET.assetDrawdownDate, 'DD/MM/YYYY') AS "asset_drawdown_date",
    CASE
        WHEN ASSET.vehicleRegistration = '<None>' THEN ''
        ELSE ASSET.vehicleRegistration
    END AS "vehicle_registration",
    to_char(VEHICLE.firstRegisteredDate, 'DD/MM/YYYY') AS "first_registered",
    ASSET.assetDrawdownAmount as "asset_drawdown_amount",
    ASSET.assetDrawdownVat as "asset_drawdown_vat",
    CASE
        WHEN ASSET.serialNumber = '<None>' THEN ''
        ELSE TRIM(REPLACE(ASSET.serialNumber, ',', ';'))
    END AS "serial_number",
    CASE
        WHEN ASSET.manufacturerCode = '<None>' THEN ''
        ELSE ASSET.manufacturerCode
    END AS "manufacturer_code",
    CASE
        WHEN ASSET.manufacturerName = '<None>' THEN ''
        ELSE REPLACE(ASSET.manufacturerName, ',', ';')
    END AS "manufacturer_name",
    CASE
        WHEN ASSET.modelCode = '<None>' THEN ''
        ELSE ASSET.modelCode
    END AS "model_code",
    CASE
        WHEN ASSET.modelName = '<None>' THEN ''
        ELSE REPLACE(REGEXP_REPLACE(ASSET.modelName, chr(13), ''), ',', '')
        --ELSE REGEXP_REPLACE(REPLACE(ASSET.modelName, ',', ';'), '\s+$', '')
        --ELSE TRIM(REGEXP_REPLACE(ASSET.modelName, '\s+', ''))
        --ELSE ASSET.modelName
        --ELSE REPLACE(ASSET.modelName, ',', ';')
        --ELSE regexp_replace(ASSET.modelName || chr(13) || chr(10) || chr(13) || chr(10) , 
           --'(' || chr(13) || chr(10) || '?|' || chr(10) || '){1,}','\1')
        --ELSE REPLACE(ASSET.modelName, ' ', '')
        --ELSE REPLACE(ASSET.modelName, chr(10), '')
    END AS "model_name",
    ASSET.assetSourceCode AS "asset_source_code",
    ASSET.assetSourceName AS "asset_source_name",
    ASSET.yearOfManufacture AS "year_of_manufacture",
    ASSET.etlLogId AS "etlLogId"
FROM eodAsset ASSET
    JOIN eodAgreement AGR ON AGR.odsid = ASSET.agreementId
    LEFT JOIN eodVehicle VEHICLE ON VEHICLE.assetId = ASSET.odsid
    LEFT JOIN eodBillingAddress BILL ON BILL.odsid = ASSET.supplrBillingId
WHERE ASSET.assetIdentifier > 0) AS DATA