-- ZDW_SCHEDULE_EXPENSE_INCOME: Schedule expense income extract for Data Warehouse
SELECT AGR.agreementNumber AS "AGREEMENT",
    EXPENSE.isTerminated AS "IS_TERMINATED",
    CHRG.expIncChargeTypeCode AS "CHARGE_TYPE_CODE",
    CHRG.expIncChargeTypeName AS "CHARGE_TYPE_NAME",
    EXPENSE.expenseIncomeAccrued AS "EXPENSE_INCOME_ACCRUED",
    CHRG.expIncChargeTypeDirection AS "CHARGE_TYPE_DIRECTION",
    STD.accountingStandardCode AS "ACCOUNTING_STANDARD_CODE",
    to_char(EXPENSE.periodStartDate, 'DD/MM/YYYY') AS "PERIOD_START_DATE",
    to_char(EXPENSE.periodEndDate, 'DD/MM/YYYY') AS "PERIOD_END_DATE"
FROM eodScheduleExpenseIncome EXPENSE
    JOIN eodExpenseIncomeChargeType CHRG ON CHRG.odsid = EXPENSE.expenseIncomeChargeTypeId
    JOIN OdsScheduleIncome INCOME ON INCOME.id = EXPENSE.scheduleIncomeId
    JOIN eodAccountingStandard STD ON STD.odsid = INCOME.accountingStandardId
    JOIN eodSchedule SCHED ON SCHED.odsid = INCOME.scheduleId
    JOIN eodAgreement AGR ON AGR.odsid = SCHED.agreementid
    JOIN eodSystemDate ON 1 = 1 -- Remove the JOIN below when completing a full load
    LEFT JOIN (
        SELECT scheduleId,
            max(rescheduleQuoteAppliedDate) AS "rescheduled"
        FROM eodRescheduleSummary
        WHERE rescheduleStatus NOT IN ('Cancelled', '<Unknown>')
        GROUP BY scheduleId
    ) RESCHED ON RESCHED.scheduleId = SCHED.odsid
WHERE INCOME.periodStartDate >= date_trunc('month', systemdate)
    AND (
        RESCHED.rescheduled > systemdate - 3
        OR SCHED.activationDate > systemdate - 3
    )
ORDER BY AGR.agreementNumber,
    INCOME.periodStartDate