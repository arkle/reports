-- CASOPNQ:     Open Cases by Workflow Status v1.0
-- Title:       Open cases by Workflow Status
-- Summary:     Provides a summary of open cases by workflow status (selected using a parameter providing the department of interest)
-- Parameters:  underwriting Boolean, New Business Boolean
SELECT STAT.currentstatusdefaultqueuename AS "Default_queue",
    STAT.currentstatusstatusname AS "Workflow_Stage",
    ALLOC.allocatedtousername AS "Allocated_to",
    CAS.caseidsequencenumber AS "Case_ID",
    AGR.agreementnumber AS "Agreement",
    SCHED.totalAssetCostFinanced AS "Amount_Financed",
    TPY.case3pyname AS "Third_Party",
    CAS.openeddate AS "Case_Opened_Date",
    to_char(CAS.openedtime, '00:00:00') AS "Case_Opened_Time",
    CAS.lastactiondate AS "Last_Action_Date",
    to_char(CAS.lastactiontime, '00:00:00') AS "Last_Action_Time"
FROM OdsCase CAS
    LEFT JOIN OdsAgreement AGR ON AGR.id = CAS.agreementid
    LEFT JOIN OdsSchedule SCHED ON SCHED.agreementid = AGR.id
    JOIN OdsCurrentCaseStatus STAT ON STAT.id = currentstatusid
    JOIN OdsAllocatedToUser ALLOC ON ALLOC.id = CAS.allocatedtouserid
    LEFT JOIN OdsCaseThirdParty TPY ON TPY.id = CAS.case3pyid
WHERE CAS.status = 'Open'
    AND (
        (
            :Underwriting = true
            AND STAT.currentstatusdefaultqueuecode IN (
                'APP',
                'DEC',
                'FIN',
                'FLW',
                'REF',
                'SUW',
                'UW',
                'UWA',
                'UWR'
            )
        )
        OR (
            :New_Business = true
            AND STAT.currentstatusdefaultqueuecode IN ('DOC', 'EXP', 'NBA', 'NBM', 'PAC', 'POA', 'PP', 'PEC', 'STA', 'SUA')
        )
    )
ORDER BY STAT.currentStatusStatusName,
    CAS.openeddate