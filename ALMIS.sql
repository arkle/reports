-- ALMIS:       ALMIS Report
-- Summary:     Used by Weatherbys Bank for Liquidity Analysis
--              Uses Exposure Customer
--              Limited to contracts where the Cap OS or Arrears is >0
SELECT systemdate AS "REPORTING_DATE",
    AGRCO.agrCompanyName AS "COMPANY",
    CASE
        WHEN SCHED.scheduleStatus = 'Live (Primary)' THEN 'In Service'
        WHEN SCHED.scheduleStatus = 'Live (Secondary)' THEN 'In Service'
        WHEN SCHED.scheduleStatus = 'Terminated' THEN 'Terminated'
        WHEN SCHED.scheduleStatus = 'Matured' THEN 'Terminated'
    END AS "CONTRACT_STATUS",
    '' AS "SUB_STATUS",
    AGR.agreementNumber AS "CONTRACT_NO",
    EXPCUS.expCusThirdPartyNumber AS "CUSTOMER",
    EXPCUS.expCusIndustryCode AS "SICCODE",
    EXPCUS.expCusIndustryName AS "SICCODE_DESC",
    AGR.product AS "PRODUCT",
    CASE
        WHEN MISC.ors IS NOT NULL THEN MISC.ors
        ELSE SCHED.startDate
    END AS "START_DATE",
    SCHED.termInMonths AS "TERM",
    SCHED.maturityDate AS "CONTRACTUAL_MATURITY_DATE",
    CASE
        WHEN MISC.ora IS NOT NULL THEN MISC.ora / 100
        ELSE SCHED.annualPercentageRate / 100
    END AS "APR",
    CASE
        WHEN MISC.oaf IS NOT NULL THEN MISC.oaf
        ELSE SCHED.totalAssetCostFinanced
    END AS "FUNDING_REQUEST_AMOUNT",
    sched.nextRentalFrequency AS "PAYMENT_FREQUENCY",
    EXTRACT(
        DAY
        FROM SCHED.nextRentalDate
    ) AS "PAYMENT_DUE_DT",
    INC.income AS "INCOME",
    INC.income - INCAC.incomeac AS "INCOMEOS",
    SCHED.capitalOutstanding AS "CAPITAL_OS",
    SCHED.totalResidualValue AS "RESIDUAL_VALUE",
    SCHED.balloonAmount AS "BALLOON",
    SCHED.totalScheduleArrears AS "ARREARS",
    zerothirty AS "TOT30",
    thirtysixty AS "TOT60",
    sixtyninety AS "TOT90",
    ninetyplus AS "TOT90P"
FROM eodSchedule SCHED
    JOIN eodAgreementCompany AGRCO ON AGRCO.odsid = SCHED.agrCompanyId
    JOIN eodAgreement AGR ON AGR.odsid = SCHED.agreementid
    JOIN eodAgreementMiscellaneous MISC ON MISC.agreementid = AGR.odsid
    JOIN eodExposureCustomer EXPCUS ON EXPCUS.odsid = SCHED.expCusId
    JOIN eodDealer DLR ON DLR.odsid = SCHED.dealerId
    JOIN (
        SELECT scheduleid,
            SUM(A.earnings) as income
        FROM eodScheduleIncome A
        WHERE isPrimaryAccountingStandard = true
        GROUP BY scheduleid
    ) INC ON INC.scheduleId = SCHED.odsid
    JOIN (
        SELECT scheduleid,
            SUM(A.earnings) as incomeac
        FROM eodScheduleIncome A
        WHERE isPrimaryAccountingStandard = true
            and isRecognised = true
        GROUP BY scheduleid
    ) INCAC ON INCAC.scheduleId = SCHED.odsid
    LEFT JOIN eodNextRentalPaymentType NEXTRENTAL ON NEXTRENTAL.odsid = SCHED.nextRentalPaymentTypeId -- Arrears - Unpaid and overdue invoices 0-30
    LEFT JOIN (
        SELECT INV.scheduleid,
            SUM(unpaidAmount) AS "zerothirty"
        FROM eodInvoiceLine INV
            JOIN eodSystemDate on 1 = 1
        WHERE unpaidAmount > 0
            AND isDue = '1'
            AND isPaid = '0'
            AND systemdate - invoiceReferenceDate >= 0
            AND systemdate - invoiceReferenceDate < 31
        GROUP BY scheduleid
    ) ARR30 ON ARR30.scheduleid = SCHED.odsid
    AND zerothirty > 0 -- Arrears - Unpaid and overdue invoices 31-60
    LEFT JOIN (
        SELECT INV.scheduleid,
            SUM(unpaidAmount) AS "thirtysixty"
        FROM eodInvoiceLine INV
            JOIN eodSystemDate on 1 = 1
        WHERE unpaidAmount > 0
            AND isDue = '1'
            AND isPaid = '0'
            AND systemdate - invoiceReferenceDate > 30
            AND systemdate - invoiceReferenceDate < 61
        GROUP BY scheduleid
    ) ARR60 ON ARR60.scheduleid = SCHED.odsid
    AND thirtysixty > 0 -- Arrears - Unpaid and overdue invoices 61-90
    LEFT JOIN (
        SELECT INV.scheduleid,
            SUM(unpaidAmount) AS "sixtyninety"
        FROM eodInvoiceLine INV
            JOIN eodSystemDate on 1 = 1
        WHERE unpaidAmount > 0
            AND isDue = '1'
            AND isPaid = '0'
            AND systemdate - invoiceReferenceDate > 60
            AND systemdate - invoiceReferenceDate < 91
        GROUP BY scheduleid
    ) ARR90 ON ARR90.scheduleid = SCHED.odsid
    AND sixtyninety > 0 -- Arrears - Unpaid and overdue invoices 90+
    LEFT JOIN (
        SELECT INV.scheduleid,
            SUM(unpaidAmount) AS "ninetyplus"
        FROM eodInvoiceLine INV
            JOIN eodSystemDate on 1 = 1
        WHERE unpaidAmount > 0
            AND isDue = '1'
            AND isPaid = '0'
            AND systemdate - invoiceReferenceDate > 90
        GROUP BY scheduleid
    ) ninetyplus ON ninetyplus.scheduleid = SCHED.odsid
    AND ninetyplus > 0
    JOIN eodSystemDate ON 1 = 1
WHERE SCHED.schedulenumber > 0
    AND SCHED.scheduleStatus IN (
        'Live (Primary)',
        'Live (Secondary)',
        'Terminated',
        'Matured'
    )
    AND (
        SCHED.capitalOutstanding + SCHED.totalScheduleArrears
    ) > 0