-- CASEHIST:    Case History Report v1.0
-- Title:       Case History Report
-- Summary:     provides the detailed progression of a case
-- Parameter:   caseid Number
SELECT CASHIST.historySequenceNumber,
    ACTIONBY.actionedbyusername AS "Actioned_by",
    ACTIONTYPE.caseAction AS "Case_Action",
    STARTING.startStatusStatusName AS "Starting_Status",
    CASHIST.startDate AS "Start_Date",
    to_char(CASHIST.startTime, '99:99:99') AS "Start_Time",
    ENDING.endStatusStatusName AS "Ending_Status",
    CASHIST.endDate AS "End_Date",
    to_char(CASHIST.endTime, '99:99:99') AS "End_Time",
    make_timestamp(
        EXTRACT(
            YEAR
            FROM NEXTSTAT.next_status_start
        ),
        EXTRACT(
            MONTH
            FROM NEXTSTAT.next_status_start
        ),
        EXTRACT(
            DAY
            FROM NEXTSTAT.next_status_start
        ),
        LEFT(NEXTSTAT.next_status_time, 2),
        SUBSTRING(NEXTSTAT.next_status_time, 3, 2),
        RIGHT(NEXTSTAT.next_status_time, 2) ) AS "Next_Status_Start",
        to_char(CASHIST.durationSeconds / 360, '000:00') AS "Duration_Hours"
        FROM OdsCaseHistory CASHIST
            JOIN OdsCase CAS ON CAS.id = CASHIST.caseid
            JOIN OdsActionedByUser ACTIONBY ON ACTIONBY.id = CASHIST.actionedbyuserid
            JOIN OdsCaseActionType ACTIONTYPE ON ACTIONTYPE.id = CASHIST.actionTypeId
            JOIN OdsStartingCaseStatus STARTING ON STARTING.id = CASHIST.startStatusId
            JOIN OdsEndingCaseStatus ENDING ON ENDING.id = endStatusId
            JOIN (
                SELECT caseid,
                    historySequenceNumber -1 AS history_sequence_number,
                    startdate AS next_status_start,
                    startTime AS next_status_time
                FROM OdsCaseHistory
            ) NEXTSTAT ON NEXTSTAT.caseid = CASHIST.caseid
            AND NEXTSTAT.history_sequence_number = CASHIST.historySequenceNumber
    
WHERE CAS.caseIdSequenceNumber = :caseid
ORDER BY CASHIST.casehistoryidentifier