-- OPS_CONTROL: Operations Control Report
-- Summary:     Runs automatically each day to highlight issues to be addressed
-- Key fields:  Reference, Message
--
-- Drawdown date should match start date unless a staged payment loan
SELECT CONCAT(
        AGR.agreementnumber,
        ' Asset: ',
        ASSET.assetIdentifier
    ) AS "Reference",
    'Drawdown date does not match agreement start date' AS "Message"
FROM OdsAgreement AGR
    JOIN OdsSchedule SCHED ON SCHED.agreementid = AGR.id
    JOIN OdsAsset ASSET ON ASSET.scheduleid = SCHED.Id
    JOIN OdsAgreementAlert ALERT ON ALERT.agreementId = AGR.id
WHERE SCHED.scheduleStatus LIKE 'Live%'
    AND AGR.productcode != 'STG'
    AND ALERT.mig != '1'
    AND ASSET.assetDrawdownDate != SCHED.startdate
UNION ALL
-- Mandate company must match the agreement company
SELECT CONCAT(MANDATE.reference) AS "Reference",
    'Mandate Company & Agreement Company Mismatch' AS "Message"
FROM OdsDirectDebitMandate MANDATE
    JOIN OdsAgreement AGR ON AGR.agreementnumber = TRIM(SPLIT_PART(MANDATE.reference, ' ', 1))
    JOIN OdsAgreementCompany AGRCO ON AGRCO.id = AGR.agrCompanyId
    JOIN OdsSchedule SCHED ON SCHED.agreementid = AGR.id
    JOIN OdsDebitMandateCompany MANDCO ON MANDCO.id = MANDATE.companyId
WHERE AGRCO.agrCompanyName != MANDCO.mandateCompanyName
    AND SCHED.scheduleStatus LIKE 'Live%'
    AND MANDATE.status != 'Closed'
UNION ALL
-- A contact should have been added to PT-X for agreements activated today
SELECT CONCAT(MANDATE.reference) AS "Reference",
    'Contact not added to PT-X (check for valid contact details)' AS "Message"
FROM OdsDirectDebitMandate MANDATE
    JOIN OdsAgreement AGR ON AGR.agreementnumber = TRIM(SPLIT_PART(MANDATE.reference, ' ', 1))
    JOIN OdsSchedule SCHED ON SCHED.agreementid = AGR.id
    AND SCHED.terminationnumber = 0
    LEFT JOIN OdsSchExternalSystemReference EXTREFMAN ON EXTREFMAN.scheduleid = SCHED.id
    LEFT JOIN OdsExternalSystemReference EXTREFBIL ON EXTREFBIL.billingaddressid = MANDATE.billingAddressId
    AND EXTREFBIL.systemcode = 'PTXCO'
    JOIN OdsSystemDate ON 1 = 1
WHERE MANDATE.status != 'Closed'
    AND EXTREFBIL.reference = '<None>'
    AND SCHED.activationDate = systemdate
UNION ALL
-- A mandate should have been added to PT-X for agreements activated before today
SELECT CONCAT(MANDATE.reference) AS "Reference",
    CONCAT(
        'Mandate not added to PT-X',
        CASE
            WHEN EXTREFBIL.reference = '<None>' THEN ', Contact also failed to add to PT-X'
            ELSE ''
        END
    ) AS "Message"
FROM OdsDirectDebitMandate MANDATE
    JOIN OdsAgreement AGR ON AGR.agreementnumber = TRIM(SPLIT_PART(MANDATE.reference, ' ', 1))
    JOIN OdsSchedule SCHED ON SCHED.agreementid = AGR.id
    AND SCHED.terminationnumber = 0
    LEFT JOIN OdsSchExternalSystemReference EXTREFMAN ON EXTREFMAN.scheduleid = SCHED.id
    LEFT JOIN OdsExternalSystemReference EXTREFBIL ON EXTREFBIL.billingaddressid = MANDATE.billingAddressId
    AND EXTREFBIL.systemcode = 'PTXCO'
    JOIN OdsSystemDate ON 1 = 1
WHERE MANDATE.status != 'Closed'
    AND EXTREFMAN.ptxmd = '<None>'
    AND SCHED.activationDate != systemdate
UNION ALL
-- Third party external references should be unique
SELECT BILL.uniqueRef AS "Reference",
    'Duplication of third party external reference (not allowed)' AS "Message"
FROM OdsBillingAddress BILL
    JOIN (
        SELECT thirdpartyid
        FROM OdsExternalSystemReference
        WHERE reference IN (
                SELECT reference
                FROM (
                        SELECT reference,
                            count(*) AS actor_code_count
                        FROM OdsExternalSystemReference
                        WHERE systemcode = 'ACR'
                        GROUP BY reference
                    ) A
                WHERE actor_code_count > 1
            )
    ) DUPCHECK ON DUPCHECK.thirdpartyid = BILL.thirdPartyId
UNION ALL
-- Undisclosed agreements should have the undisclosed agreement alert
SELECT AGR.agreementnumber AS "Reference",
    'Invoice and Exposure Customer are different but there is no undisclosed alert' AS "Message"
FROM OdsSchedule SCHED
    JOIN OdsAgreement AGR ON AGR.id = SCHED.agreementid
    LEFT JOIN OdsAgreementAlert AGRALERT ON AGRALERT.agreementId = AGR.id
WHERE SCHED.invCusId != SCHED.expCusId
    AND SCHED.scheduleStatus LIKE 'Live%'
    AND AGRALERT.cais = false
UNION ALL
-- Staged payment loans should have a "future" agreement that exists in ALFA
SELECT AGR.agreementnumber AS "Reference",
    CASE
        WHEN AGRMISC.STP = '<None>' THEN 'Staged payment loan but no value in the <Future Agreement # for a staged payment start> miscellaneous field'
        WHEN AGRMISC.STP = AGR.agreementnumber THEN 'Staged payment loan but <Future Agreement # for a staged payment start> miscellaneous field has been set to original agreement'
        ELSE 'Staged payment loan but invalid agreement number in the <Future Agreement # for a staged payment start> miscellaneous field'
    END AS "Message"
FROM OdsAgreement AGR
    LEFT JOIN OdsAgreementMiscellaneous AGRMISC ON AGRMISC.agreementId = AGR.id
    LEFT JOIN OdsAgreement RELATED ON RELATED.agreementnumber = AGRMISC.STP
    JOIN OdsSchedule SCHED ON SCHED.agreementid = AGR.id
WHERE AGR.productCode = 'STG'
    AND (
        AGRMISC.STP = '<None>'
        OR RELATED.id IS NULL
        OR AGRMISC.STP = AGR.agreementnumber
    )
    AND AGR.agreementnumber NOT IN ('<None>', '<Unknown>')
    AND SCHED.scheduleStatus != 'Proposal'
UNION ALL
-- Staged payment loans "future" agreements without the STPL flag set
SELECT RELATED.agreementnumber AS "Reference",
    'Final agreement for staged payment loan but the <Linked to staged payment schedule> flag is not selected in miscellaneous data' AS "Message"
FROM OdsAgreement AGR
    LEFT JOIN OdsAgreementMiscellaneous AGRMISC ON AGRMISC.agreementId = AGR.id
    LEFT JOIN OdsAgreement RELATED ON RELATED.agreementnumber = AGRMISC.STP
    LEFT JOIN OdsAgreementMiscellaneous AGRMISCREL ON AGRMISCREL.agreementId = RELATED.id
    JOIN OdsSchedule SCHED ON SCHED.agreementid = AGR.id
WHERE AGR.productCode = 'STG'
    AND AGRMISCREL.STPL != true
    AND AGR.agreementnumber NOT IN ('<None>', '<Unknown>')
    AND SCHED.scheduleStatus != 'Proposal'
UNION ALL
-- Staged payment loans "future" agreements with the "Future agreement" field not being blank
SELECT AGR.agreementnumber AS "Reference",
    'Final agreement for staged payment loan but there is a value in the <Future Agreement # for a staged payment start> field' AS "Message"
FROM OdsAgreement AGR
    LEFT JOIN OdsAgreementMiscellaneous AGRMISC ON AGRMISC.agreementId = AGR.id
    JOIN OdsSchedule SCHED ON SCHED.agreementid = AGR.id
WHERE AGR.productCode != 'STG'
    AND AGRMISC.STPL = true
    AND TRIM(AGR.agreementnumber) NOT IN ('<None>', '<Unknown>', '', Null)
    AND SCHED.scheduleStatus != 'Proposal'
UNION ALL
-- Billing address names should not contain non-ASCII characters
SELECT BILL.uniqueRef AS "Reference",
    CONCAT(
        'Billing address name contains invalid characters :',
        regexp_replace(BILL.name, '([^[:ascii:] ''''])', '[\1]', 'g')
    ) AS "Message"
FROM OdsBillingAddress BILL
    LEFT JOIN OdsSchedule SCHEDI ON SCHEDI.invCusBillingId = BILL.id
    AND SCHEDI.schedulestatus != 'Proposal'
    LEFT JOIN OdsSchedule SCHEDE ON SCHEDE.expCusBillingId = BILL.id
    AND SCHEDE.schedulestatus != 'Proposal'
WHERE regexp_replace(BILL.name, '([^[:ascii:] ''''])', '[\1]', 'g') != BILL.name
    AND (
        SCHEDI.id IS NOT NULL
        OR SCHEDE.id IS NOT NULL
    )
GROUP BY BILL.uniqueRef,
    BILL.name
UNION ALL
-- Billing address external names should not contain non-ASCII characters
SELECT BILL.uniqueRef AS "Reference",
    CONCAT(
        'Trading name contains invalid characters :',
        regexp_replace(BILL.externalName, '([^[:ascii:] ''''])', '[\1]', 'g')
    ) AS "Message"
FROM OdsBillingAddress BILL
    LEFT JOIN OdsSchedule SCHEDI ON SCHEDI.invCusBillingId = BILL.id
    AND SCHEDI.schedulestatus != 'Proposal'
    LEFT JOIN OdsSchedule SCHEDE ON SCHEDE.expCusBillingId = BILL.id
    AND SCHEDE.schedulestatus != 'Proposal'
WHERE regexp_replace(BILL.externalName, '([^[:ascii:] ''''])', '[\1]', 'g') != BILL.externalName
    AND (
        SCHEDI.id IS NOT NULL
        OR SCHEDE.id IS NOT NULL
    )
GROUP BY BILL.uniqueRef,
    BILL.externalName
UNION ALL
-- Billing address address line 1 should not contain non-ASCII characters
SELECT BILL.uniqueRef AS "Reference",
    CONCAT(
        'Billing addressLine1 contains invalid characters :',
        regexp_replace(
            BILL.addressLine1,
            '([^[:ascii:] ''''])',
            '[\1]',
            'g'
        )
    ) AS "Message"
FROM OdsBillingAddress BILL
    LEFT JOIN OdsSchedule SCHEDI ON SCHEDI.invCusBillingId = BILL.id
    AND SCHEDI.schedulestatus != 'Proposal'
    LEFT JOIN OdsSchedule SCHEDE ON SCHEDE.expCusBillingId = BILL.id
    AND SCHEDE.schedulestatus != 'Proposal'
WHERE regexp_replace(
        BILL.addressLine1,
        '([^[:ascii:] ''''])',
        '[\1]',
        'g'
    ) != BILL.addressLine1
    AND (
        SCHEDI.id IS NOT NULL
        OR SCHEDE.id IS NOT NULL
    )
GROUP BY BILL.uniqueRef,
    BILL.addressLine1
UNION ALL
-- Billing address address line 2 should not contain non-ASCII characters
SELECT BILL.uniqueRef AS "Reference",
    CONCAT(
        'Billing addressLine2 contains invalid characters :',
        regexp_replace(
            BILL.addressLine2,
            '([^[:ascii:] ''''])',
            '[\1]',
            'g'
        )
    ) AS "Message"
FROM OdsBillingAddress BILL
    LEFT JOIN OdsSchedule SCHEDI ON SCHEDI.invCusBillingId = BILL.id
    AND SCHEDI.schedulestatus != 'Proposal'
    LEFT JOIN OdsSchedule SCHEDE ON SCHEDE.expCusBillingId = BILL.id
    AND SCHEDE.schedulestatus != 'Proposal'
WHERE regexp_replace(
        BILL.addressLine2,
        '([^[:ascii:] ''''])',
        '[\1]',
        'g'
    ) != BILL.addressLine2
    AND (
        SCHEDI.id IS NOT NULL
        OR SCHEDE.id IS NOT NULL
    )
GROUP BY BILL.uniqueRef,
    BILL.addressLine2
UNION ALL
-- Billing address address line 3 should not contain non-ASCII characters
SELECT BILL.uniqueRef AS "Reference",
    CONCAT(
        'Billing addressLine3 contains invalid characters :',
        regexp_replace(
            BILL.addressLine3,
            '([^[:ascii:] ''''])',
            '[\1]',
            'g'
        )
    ) AS "Message"
FROM OdsBillingAddress BILL
    LEFT JOIN OdsSchedule SCHEDI ON SCHEDI.invCusBillingId = BILL.id
    AND SCHEDI.schedulestatus != 'Proposal'
    LEFT JOIN OdsSchedule SCHEDE ON SCHEDE.expCusBillingId = BILL.id
    AND SCHEDE.schedulestatus != 'Proposal'
WHERE regexp_replace(
        BILL.addressLine3,
        '([^[:ascii:] ''''])',
        '[\1]',
        'g'
    ) != BILL.addressLine3
    AND (
        SCHEDI.id IS NOT NULL
        OR SCHEDE.id IS NOT NULL
    )
GROUP BY BILL.uniqueRef,
    BILL.addressLine3
UNION ALL
-- Billing address address line 4 should not contain non-ASCII characters
SELECT BILL.uniqueRef AS "Reference",
    CONCAT(
        'Billing addressLine4 contains invalid characters :',
        regexp_replace(
            BILL.addressLine4,
            '([^[:ascii:] ''''])',
            '[\1]',
            'g'
        )
    ) AS "Message"
FROM OdsBillingAddress BILL
    LEFT JOIN OdsSchedule SCHEDI ON SCHEDI.invCusBillingId = BILL.id
    AND SCHEDI.schedulestatus != 'Proposal'
    LEFT JOIN OdsSchedule SCHEDE ON SCHEDE.expCusBillingId = BILL.id
    AND SCHEDE.schedulestatus != 'Proposal'
WHERE regexp_replace(
        BILL.addressLine4,
        '([^[:ascii:] ''''])',
        '[\1]',
        'g'
    ) != BILL.addressLine4
    AND (
        SCHEDI.id IS NOT NULL
        OR SCHEDE.id IS NOT NULL
    )
GROUP BY BILL.uniqueRef,
    BILL.addressLine4
UNION ALL
-- Billing address address line 5 should not contain non-ASCII characters
SELECT BILL.uniqueRef AS "Reference",
    CONCAT(
        'Billing addressLine5 contains invalid characters :',
        regexp_replace(
            BILL.addressLine5,
            '([^[:ascii:] ''''])',
            '[\1]',
            'g'
        )
    ) AS "Message"
FROM OdsBillingAddress BILL
    LEFT JOIN OdsSchedule SCHEDI ON SCHEDI.invCusBillingId = BILL.id
    AND SCHEDI.schedulestatus != 'Proposal'
    LEFT JOIN OdsSchedule SCHEDE ON SCHEDE.expCusBillingId = BILL.id
    AND SCHEDE.schedulestatus != 'Proposal'
WHERE regexp_replace(
        BILL.addressLine5,
        '([^[:ascii:] ''''])',
        '[\1]',
        'g'
    ) != BILL.addressLine5
    AND (
        SCHEDI.id IS NOT NULL
        OR SCHEDE.id IS NOT NULL
    )
GROUP BY BILL.uniqueRef,
    BILL.addressLine5
ORDER BY 1