-- SECTOR:      Sector Analysis report v1.1
-- Title:       Sector Analysis
-- Summary:     Provides details of agreements by their SIC classification based on the exposure customer associated with an agreement
-- Parameters:  Start_from - Date, Start_to - Date
-- Parameters:  Live_Primary, Live_Secondary, Practical_DAC_Paid_Out, Terminated, Matured, Proposal
-- SIC code:    5 digits - 1-2 Division, 1-3 Group, 1-4 Class, 1-5 Sub-Class
SELECT substring(EXPCUS.expCusIndustryCode, 1, 2) AS "Division",
    substring(EXPCUS.expCusIndustryCode, 1, 3) AS "Group",
    substring(EXPCUS.expCusIndustryCode, 1, 4) AS "Class",
    substring(EXPCUS.expCusIndustryCode, 1, 5) AS "Sub_Class",
    AGR.agreementNumber AS "Agreement",
    EXPCUS.expCusName AS "Customer",
    DLR.dealerName AS "Broker",
    AGR.productCode AS "Product_code",
    AGR.product AS "Product",
    SCHED.startDate AS "Start_date",
    EXPCUS.expCusIndustryCode AS "Industry_code",
    EXPCUS.expCusIndustryName AS "Industry_name",
    SCHED.originalAssetCostFinanced AS "Amount_financed",
    SCHED.capitalOutstanding AS "Capital_OS",
    SCHED.totalScheduleArrears AS "Arrears"
FROM OdsAgreement AGR
    JOIN OdsSchedule SCHED ON SCHED.agreementid = AGR.id
    JOIN OdsExposureCustomer EXPCUS ON EXPCUS.id = AGR.expCusId
    JOIN OdsDealer DLR ON DLR.id = SCHED.dealerId
    LEFT JOIN (
        SELECT agreementId,
            'Y' AS practical_paid_out
        FROM OdsAgreementAlert
        WHERE ppo = '1'
    ) PRACTICAL ON PRACTICAL.agreementId = AGR.id
WHERE SCHED.schedulenumber > 0
    AND (
        SCHED.startDate >= to_date(:Start_from, 'YYYY-MM-DD')
        AND SCHED.startDate <= to_date(:Start_to, 'YYYY-MM-DD')
    ) -- Limit scope of report based on parameters
    AND (
        SCHED.scheduleStatus = CASE
            WHEN :Live_Primary = true THEN 'Live (Primary)'
            ELSE ''
        END
        OR SCHED.scheduleStatus = CASE
            WHEN :Live_Secondary = true THEN 'Live (Secondary)'
            ELSE ''
        END
        OR SCHED.scheduleStatus = CASE
            WHEN :Terminated = true THEN 'Terminated'
            ELSE ''
        END
        OR SCHED.scheduleStatus = CASE
            WHEN :Matured = true THEN 'Matured'
            ELSE ''
        END
        OR SCHED.scheduleStatus = CASE
            WHEN :Proposal = true THEN 'Proposal'
            ELSE ''
        END
        OR PRACTICAL.practical_paid_out = CASE
            WHEN :Practical_DAC_Paid_Out = true THEN 'Y'
            ELSE ''
        END
    )