-- ZACQUIS: Extract to support creation of files to send to Acquis for the insurance tracking product
-- Extracts all live agreements with filtering performed in downstream processes
-- Cut-off date also applied to limit scope of agreements included:
SELECT AGR.agreementnumber AS "AgreeementNumber",
    BILL.thirdPartyNumber AS "CustomerNumber",
    BILL.addressLine1 AS "EquipmentAddress1",
    BILL.addressLine2 AS "EquipmentAddress2",
    BILL.addressLine3 AS "EquipmentAddress3",
    BILL.addressLine4 AS "EquipmentAddress4",
    BILL.postalCode AS "EquipmentAddressPostcode",
    ASSET.assetDescription AS "EquipmentDescription",
    SUP.supplrBillingName AS "EquipmentSupplier",
    SCHED.totalAssetCost AS "EquipmentValue",
    systemdate AS "FileCreationDate",
    '' AS Insuranceamountcollected,
    SCHED.startDate AS "LeaseEffectiveDate",
    CASE
        WHEN SCHED.nextRentalFrequency = '1 month' THEN 'M'
        WHEN SCHED.nextRentalFrequency = '3 months' THEN 'Q'
        ELSE ''
    END AS "LeaseFrequency",
    SCHED.maturityDate AS "Leasematuritydate",
    SCHED.termInMonths AS "Leaseterm",
    BILL.name AS "Lessee",
    BILL.addressLine1 AS "Lesseeaddress1",
    BILL.addressLine2 AS "Lesseeaddress2",
    BILL.addressLine3 AS "Lesseeaddress3",
    BILL.addressLine4 AS "Lesseeaddress4",
    BILL.postalCode AS "LesseePostcode",
    BILL.contactName AS "Lesseeontact",
    '' AS "LessorEntity",
    '3006' AS "LessorID",
    '' AS "lessorsector",
    '' AS "LessorWhiteLabel",
    SCHED.nextRentalAmount AS "MonthlyLeaseAmount",
    SCHED.nextRentalDate AS "NextInvoiceDate",
    '25' AS "PreBillDays",
    '' AS "PreviousLeaseNumber",
    'TR' AS "Productcode",
    SCHED.numberOfFutureRentals AS "RemainingInvoices",
    CASE
        WHEN TPY.industryCode = '<None>' THEN ''
        ELSE TPY.industryCode
    END AS "SICode",
    ASSET.assetclassificationcode AS "assetclassificationcode",
    SCHED.scheduleStatus AS "Status"
FROM OdsAgreement AGR
    JOIN OdsSchedule SCHED ON SCHED.agreementid = AGR.id
    JOIN OdsBillingAddress BILL ON BILL.id = AGR.expCusBillingId
    JOIN OdsThirdParty TPY ON TPY.id = BILL.thirdPartyId
    LEFT JOIN (
        SELECT scheduleid,
            assetclassificationcode,
            assetTypeName,
            assetDescription,
            supplrBillingId
        FROM OdsAsset
        WHERE isMainAsset = '1'
    ) ASSET ON ASSET.scheduleid = SCHED.id
    JOIN OdsSupplierBilling SUP ON SUP.id = ASSET.supplrBillingId
    LEFT JOIN OdsAgreementAlert ALERT ON ALERT.agreementId = AGR.id
    JOIN OdsSystemDate ON 1 = 1
WHERE SCHED.scheduleStatus LIKE 'Live%'
    AND AGR.agreementType != 'Loan'
    AND ALERT.mig != '1'
    AND ALERT.CAIS != '1'