-- FIN-ACCOUNTS:    Chart of Accounts
-- Summary:         Provides a simple extract of the chart of accounts configured in ALFA that is mapped to the base ALFA accounting via account mappings
SELECT accountNumber AS "Account_Number",
    accountName AS "Account_Name",
    accountType AS "Account_Type",
    externalQualifier1 AS "External_Qualifier_1",
    externalQualifier2 AS "External_Qualifier_2"
FROM OdsAccount
ORDER BY accountNumber