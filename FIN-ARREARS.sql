-- FIN-ARREARS: Arrears Report (Finance Version)
-- Title:       Aged Schedule Arrears by Third Party (Finance Version)
-- Summary:     Provides a view of all schedules by third party with arrears (as of the end of the Previous day). Includes details of the most recent open Arrears case (Standard Arrears Process/Regulated Arrears Process/Non-Customer Arrears Process) associated with the schedule.
-- IMPORTANT:   Changes made to this report should also be considered for the non-Finance equivalent
SELECT --  Company info
    CASE
        WHEN AGRCO.agrcompanycode IN ('2', '4') THEN 'Evington'
        ELSE 'Arkle'
    END AS "Company",
    CASE
        WHEN AGRCO.agrcompanycode = '1' THEN 'Core'
        WHEN AGRCO.agrcompanycode IN ('2', '4') THEN 'Consumer'
        WHEN AGRCO.agrcompanycode = '3' THEN 'Prime'
    END AS "Pricing",
    --  Broker info
    DLR.dealerName AS "Broker",
    --  Customer info
    TPY.thirdPartyNumber AS "Customer_Id",
    TPY.name AS "Customer_Name",
    TPY.caisLegalEntityType AS "Customer_Type",
    --  Flag if the customer undisclosed
    CASE
        WHEN AGR.invCusBillingId = AGR.expCusBillingId THEN ''
        ELSE 'Y'
    END AS "Undisclosed",
    --  Flag if the customer is vulnerable or has an active complaint (based on the Invoicing customer)  
    CASE
        WHEN VULNERABLE.thirdpartyid > 0 THEN 'Y'
        ELSE ''
    END AS "Vulnerable",
    --  Agreement information                                           
    AGR.agreementNumber AS "Agreement",
    AGR.agreementType AS "Agreement_type",
    SCHED.scheduleDescription AS "Schedule_description",
    CASE
        WHEN AGR.isCcaRegulated = '1' THEN 'Y'
        ELSE ''
    END AS "Regulated",
    --  Arrears
    SCHED.inArrearsSinceDate AS "In_Arrears_since",
    SCHED.totalScheduleArrears AS "Schedule_arrears",
    zerothirty AS "0-30",
    thirtysixty AS "31-60",
    sixtyninety AS "61-90",
    ninetyplus AS "91P",
    --  Balances
    SCHED.grossBalanceRemaining AS "Balance_OS",
    SCHED.capitalOutstanding AS "Capital_OS",
    --  Case Info
    to_char(CASEINFO.caseidsequencenumber, '999999999') AS "Case_ID",
    CASEINFO.currentStatusTypeName AS "Case_Type",
    CASEINFO.currentstatusstatusname AS "Case_Stage",
    CASEINFO.department AS "Department",
    CASEINFO.allocatedtousername AS "Allocated_to",
    CASEINFO.openeddate AS "Opened",
    CASEINFO.lastactiondate AS "Last_Action"
FROM OdsThirdParty TPY
    JOIN eodAgreement AGR ON AGR.invCusId = TPY.id
    JOIN eodSchedule SCHED ON SCHED.agreementId = AGR.odsid
    AND SCHED.totalScheduleArrears <> 0
    JOIN OdsAgreementCompany AGRCO ON AGRCO.id = SCHED.agrCompanyId
    JOIN OdsDealer DLR ON DLR.id = SCHED.dealerId -- Identify Third Parties with a Vulnerable Customer Alert
    LEFT JOIN (
        SELECT thirdpartyid
        FROM OdsThirdPartyAlert
            JOIN OdsSystemDate ON 1 = 1
        WHERE tex_code = '1'
            AND (
                tex_code_start <= systemdate
                AND tex_code_exp >= systemdate
            )
    ) VULNERABLE ON VULNERABLE.thirdpartyid = TPY.id -- Most recent Arrears case associated with the schedule
    LEFT JOIN (
        SELECT CAS.agreementid,
            CAS.caseidsequencenumber,
            STAT.currentStatusTypeName,
            STAT.currentstatusstatusname,
            DEP.description AS department,
            ALLOC.allocatedtousername,
            CAS.openeddate,
            CAS.lastactiondate
        FROM OdsCase CAS
            JOIN OdsCurrentCaseStatus STAT ON STAT.id = CAS.currentstatusid
            AND STAT.currentStatusArea = 'Arrears'
            AND STAT.currentstatustypecode IN ('SAR', 'CCA', 'NCA')
            JOIN OdsAllocatedToUser ALLOC ON ALLOC.id = CAS.allocatedtouserid
            LEFT JOIN OdsDepartment DEP ON DEP.id = ALLOC.allocatedToUserDepartmentId
            JOIN (
                SELECT CA.agreementid,
                    MAX(CA.id) AS "caseid"
                FROM OdsCase CA
                    JOIN OdsCurrentCaseStatus CS ON CS.id = CA.currentstatusid
                    AND CS.currentStatusArea = 'Arrears'
                    AND CS.currentstatustypecode IN ('SAR', 'CCA', 'NCA')
                WHERE CA.status = 'Open'
                GROUP BY agreementid
            ) RECENTCASE ON RECENTCASE.caseid = CAS.id
    ) CASEINFO ON CASEINFO.agreementid = AGR.odsid -- Arrears - Unpaid and overdue invoices 0-30
    LEFT JOIN (
        SELECT INV.scheduleid,
            SUM(unpaidAmount) AS "zerothirty"
        FROM OdsInvoiceLine INV
            JOIN OdsSystemDate on 1 = 1
        WHERE unpaidAmount > 0
            AND isDue = '1'
            AND isPaid = '0'
            AND systemdate - invoiceReferenceDate >= 0
            AND systemdate - invoiceReferenceDate < 31
        GROUP BY scheduleid
    ) ARREARS ON ARREARS.scheduleid = SCHED.odsid
    AND zerothirty > 0 -- Arrears - Unpaid and overdue invoices 31-60
    LEFT JOIN (
        SELECT INV.scheduleid,
            SUM(unpaidAmount) AS "thirtysixty"
        FROM OdsInvoiceLine INV
            JOIN OdsSystemDate on 1 = 1
        WHERE unpaidAmount > 0
            AND isDue = '1'
            AND isPaid = '0'
            AND systemdate - invoiceReferenceDate > 30
            AND systemdate - invoiceReferenceDate < 61
        GROUP BY scheduleid
    ) ARR60 ON ARR60.scheduleid = SCHED.odsid
    AND thirtysixty > 0 -- Arrears - Unpaid and overdue invoices 61-90
    LEFT JOIN (
        SELECT INV.scheduleid,
            SUM(unpaidAmount) AS "sixtyninety"
        FROM OdsInvoiceLine INV
            JOIN OdsSystemDate on 1 = 1
        WHERE unpaidAmount > 0
            AND isDue = '1'
            AND isPaid = '0'
            AND systemdate - invoiceReferenceDate > 60
            AND systemdate - invoiceReferenceDate < 91
        GROUP BY scheduleid
    ) ARR90 ON ARR90.scheduleid = SCHED.odsid
    AND sixtyninety > 0 -- Arrears - Unpaid and overdue invoices 90+
    LEFT JOIN (
        SELECT INV.scheduleid,
            SUM(unpaidAmount) AS "ninetyplus"
        FROM OdsInvoiceLine INV
            JOIN OdsSystemDate on 1 = 1
        WHERE unpaidAmount > 0
            AND isDue = '1'
            AND isPaid = '0'
            AND systemdate - invoiceReferenceDate > 90
        GROUP BY scheduleid
    ) ninetyplus ON ninetyplus.scheduleid = SCHED.odsid
    AND ninetyplus > 0