-- UW1:     Underwriting Activity Report
-- Summary: Details of the last action performed on cases by an Underwriter for a given day (date for report is supplied via a parameter)
-- Uses OdsCaseHistory to ensure all cases are detected which had an action from the Underwriting Team (i.e. the Case could have already moved on to New Business for payout)
SELECT -- High-Level Case Information
    CAS.id AS "Case ID",
    CONCAT(
        to_char(CAS.openedDate, 'YYYY-MM-DD'),
        ' ',
        to_char(CAS.openedTime, '99:99:99')
    ) AS "Case_Opened",
    ACTIONBY.actionedbyusername AS "Actioned_by_UW",
    CASSTAT.currentstatusstatusname AS "Case_Current_Status",
    CAS.lastactiondate AS "Case_Last_Action_Date",
    AGRCO.agrcompanyname AS "Agreement Company",
    DLR.dealerName AS "Broker",
    CASE
        WHEN DLRALERT.prb = '1' THEN 'Y'
        ELSE ''
    END AS "Priority_Broker",
    EXPCUS.expCusName AS "Customer",
    AGR.agreementNumber AS "Agreement",
    AGR.product AS "Product",
    -- Asset Information    
    ASSETDESC.assetdescription AS "Asset_description",
    ASSET.amountfinanced AS "Amount_Financed",
    ASSET.RV AS "RV",
    ASSET.balloonAmount AS "Balloon",
    -- Credit Decision
    CASE
        WHEN (
            CASSTAT.currentStatusTypeCode = 'ORG'
            AND CASSTAT.currentStatusTypeName LIKE '%Further Information%'
            AND CASSTAT.currentStatusDefaultQueueCode LIKE '%Underwriting%'
        ) THEN CAS.lastactiondate
        ELSE CREDIT.decisionDate
    END AS "Credit_decision_date",
    CASE
        WHEN (
            CASSTAT.currentStatusTypeCode = 'ORG'
            AND CASSTAT.currentStatusTypeName LIKE '%Further Information%'
            AND CASSTAT.currentStatusDefaultQueueCode LIKE '%Underwriting%'
        ) THEN CASSTAT.currentstatusstatusname
        ELSE CREDIT.decisionStatus
    END AS "Decision",
    CREDIT.decisionDate AS "Decision Date",
    CREDIT.decisionStatus AS "Status",
    CASE
        WHEN ALERT.rls = true THEN 'Y'
        ELSE ''
    END AS "RLS",
    CASE
        WHEN ALERT.oop = true THEN 'Y'
        ELSE ''
    END AS "Credit - Policy Exceptions",
    CASE
        WHEN ALERT.oop2 = true THEN 'Y'
        ELSE ''
    END AS "Credit - Out of Policy",
    CASE
        WHEN ALERT.atr = '1' THEN 'Y'
        ELSE ''
    END AS "Asset_Tag_Required",
        CASE
        WHEN MISC.dec3 = true THEN 'Y'
        ELSE ''
    END AS "Dec_Bank_Stmt_DNS",
    CASE
        WHEN MISC.dec4 = true THEN 'Y'
        ELSE ''
    END AS "Dec_Adverse_Credit",
    CASE
        WHEN MISC.dec5 = true THEN 'Y'
        ELSE ''
    END AS "Dec_Suff_exp",
    CASE
        WHEN MISC.dec6 = true THEN 'Y'
        ELSE ''
    END AS "Dec_Outside_policy",
    CASE
        WHEN MISC.dec6 = true THEN 'Y'
        ELSE ''
    END AS "Dec_Insufficient_info",
    CASE
        WHEN MISC.dec1 = '<None>' THEN ''
        ELSE MISC.dec1
    END AS "Dec_Other_1",
    CASE
        WHEN MISC.dec2 = '<None>' THEN ''
        ELSE MISC.dec2
    END AS "Dec_Other_2"
FROM OdsCaseHistory CASHIST
    JOIN OdsCase CAS ON CAS.id = CASHIST.caseid
    JOIN OdsActionedByUser ACTIONBY ON ACTIONBY.id = CASHIST.actionedbyuserid
    JOIN OdsAgreement AGR ON AGR.id = CAS.agreementid
    JOIN OdsExposureCustomer EXPCUS ON EXPCUS.id = AGR.expCusId
    JOIN OdsDealer DLR ON DLR.id = AGR.dealerId
    JOIN OdsDealerBilling DLRBIL ON DLRBIL.id = AGR.dealerBillingId
    LEFT JOIN OdsBillingAddressAlert DLRALERT ON DLRALERT.billingAddressId = DLRBIL.id
    JOIN OdsAgreementMiscellaneous MISC ON MISC.agreementid = AGR.id
    JOIN OdsAsset ASSETDESC ON ASSETDESC.agreementid = AGR.id
    AND ASSETDESC.isMainAsset = True
    JOIN (
        SELECT agreementid,
            SUM(assetdrawdownamountagr) AS amountfinanced,
            SUM(assetdeposit) AS deposit,
            SUM(assetresidualvalueagr) AS RV,
            SUM(balloonContribution) AS balloonAmount
        FROM OdsAsset
        GROUP BY agreementid
    ) ASSET ON ASSET.agreementid = AGR.id
    JOIN OdsAgreementCompany AGRCO ON AGRCO.id = AGR.agrcompanyid
    JOIN OdsCurrentCaseStatus CASSTAT ON CASSTAT.id = CAS.currentstatusid
    LEFT JOIN OdsAgreementAlert ALERT ON ALERT.agreementid = AGR.id
    LEFT JOIN OdsAgreementCreditDecision CREDIT ON CREDIT.agreementId = AGR.id
WHERE CASHIST.casehistoryidentifier IN (
        SELECT MAX(OdsCaseHistory.casehistoryidentifier)
        FROM OdsCaseHistory
            JOIN OdsActionedByUser ON OdsActionedByUser.id = OdsCaseHistory.actionedbyuserid
            JOIN OdsDepartment ON OdsDepartment.id = OdsActionedByUser.actionedbyuserdepartmentid
            JOIN OdsEndingCaseStatus ON OdsEndingCaseStatus.id = OdsCaseHistory.endStatusId
        WHERE (
                OdsCaseHistory.startdate = to_date(:Processing_Date, 'YYYY-MM-DD')
                OR OdsCaseHistory.enddate = to_date(:Processing_Date, 'YYYY-MM-DD')
            )
            AND OdsDepartment.code = 'UW'
            AND OdsEndingCaseStatus.endStatusTypeCode = 'ORG'
            AND OdsEndingCaseStatus.endStatusDefaultQueueName LIKE '%Underwriting%'
        GROUP BY OdsCaseHistory.caseid
    ) -- Sequence by the historysequencenumber so actions appear in order
ORDER BY CASHIST.casehistoryidentifier