-- ZDW_CASE_HISTORY:    Case History extract for reporting
SELECT to_char(CURRENT_TIMESTAMP, 'dd/mm/yyyy hh24:mi:ss') AS "extract_timestamp",
    CAS.caseidsequencenumber AS "case_id",
    HIST.caseHistoryIdentifier AS "caseHistoryIdentifier",
    HIST.historySequenceNumber AS "history_sequence",
    CASE
        WHEN ACTUSER.actionedByUserName = '<None>' THEN ''
        ELSE ACTUSER.actionedByUserName
    END AS "actioned_by_user",
    CASE
        WHEN DEPT.description = '<None>' THEN ''
        ELSE DEPT.description
    END AS "actioned_by_user_dept",
    STARTSTAT.startStatusStatusName AS "status_start",
    ENDSTAT.endStatusStatusName AS "status_end",
    to_char(HIST.startDate, 'DD/MM/YYYY') AS "start_date",
    to_char(HIST.startTime, '00:00:00') AS "start_time",
    to_char(HIST.endDate, 'DD/MM/YYYY') AS "end_date",
    to_char(HIST.endTime, '00:00:00') AS "end_time",
    HIST.durationSeconds AS "duration_seconds",
    HIST.etlLogId AS "etlLogId"
FROM OdsCaseHistory HIST
    JOIN OdsCase CAS ON CAS.id = HIST.caseid
    JOIN OdsCurrentCaseStatus STAT ON STAT.id = CAS.currentstatusid
    JOIN OdsStartingCaseStatus STARTSTAT ON STARTSTAT.id = HIST.startStatusId
    JOIN OdsEndingCaseStatus ENDSTAT ON ENDSTAT.id = HIST.endStatusId
    JOIN OdsActionedByUser ACTUSER ON ACTUSER.id = HIST.actionedByUserId
    JOIN OdsDepartment DEPT ON DEPT.id = ACTUSER.actionedByUserDepartmentId
    JOIN OdsSystemDate ON 1 = 1
WHERE STAT.currentstatustypecode IN ('ORG', 'UWR')
    AND (
        HIST.startDate >= systemdate - 1
        OR HIST.endDate >= systemdate - 1
    )