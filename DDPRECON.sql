-- DDPRECON:  	DD Payment Reconciliation Report v1.0
-- Title:       DD Payments Reconciliation
-- Summary:     Identify direct debits based on cash allocation transactions
-- Parameter:   From_date (Date), To_date (Date)
SELECT DISTINCT  CSHALLOC.transactionEffectiveDate AS "Effective_Date",
    AGR.agreementnumber AS "Agreement",
    BILL.uniqueRef AS "Third_Party_Ref",
    CSHALLOC.locationType,
    CSHALLOC.transactionStatus AS "Status",
    CSHALLOC.totalAmount AS "Amount", 
CASE 
   WHEN SCHED.terminationnumber >= '2' THEN 'Live (Primary)' 
   ELSE SCHED.schedulestatus 
   END AS "Schedule_Status"
FROM OdsCashAllocation CSHALLOC
    JOIN OdsBillingAddress BILL ON BILL.id = CSHALLOC.billingAddressId
    JOIN OdsCashAllocationTxn TXN ON TXN.id = CSHALLOC.cashAllocationTransactionId
    JOIN OdsTransactionType TXNTYPE ON TXNTYPE.id = TXN.inputTxnTypeId
    JOIN OdsAgreement AGR ON AGR.id = CSHALLOC.agreementId
    JOIN OdsSchedule SCHED ON SCHED.agreementid = AGR.id
    JOIN OdsCashInOut CSH ON CSH.id = TXN.cashId
    JOIN OdsCashAllocationPaymentType PMTTYPE ON PMTTYPE.id = CSH.paymentTypeId
    JOIN OdsSystemDate ON 1 = 1
WHERE PMTTYPE.cshAllcPaymentTypeCode IN ('DD', 'DI')
    AND TXNTYPE.transactionCode = 'A04' -- Input Receipt
    AND TXN.status = 'Applied'
    AND CSH.processingstatus != 'Rejected'
    AND CSHALLOC.transactionEffectiveDate <= systemdate
ORDER BY AGR.agreementnumber,
    CSHALLOC.transactionEffectiveDate