--ASSETDETAIL
--Asset Detail
--Data dump of Asset data for Emissions reporting
----------------------------------------------------------------------------------
SELECT ASSET.assetIdentifier as "System Identifier",
    CASE
        WHEN ASSET.manufacturerName = '<None>' THEN ''
        ELSE REPLACE(ASSET.manufacturerName, ',', ';')
    END AS "Manufacturer Name",
    CASE
        WHEN ASSET.modelName = '<None>' THEN ''
        ELSE REPLACE(
            REGEXP_REPLACE(ASSET.modelName, chr(13), ''),
            ',',
            ''
        )
    END AS "Model Name",
    ASSET.assetDescription as "Description",
    ASSET.assetSourceName AS "New/Used",
    ASSET.yearOfManufacture AS "Year of Manufacture",
    ASSET.assetTypeName as "Asset Type",
    ASSET.assetclassificationname AS "Asset Classification",
    CASE
        WHEN ASCLAS.assetCategory1 <> '<None>' THEN ASCLAS.assetCategory1
        WHEN ASCLAS.assetCategory2 <> '<None>' THEN ASCLAS.assetCategory2
        WHEN ASCLAS.assetCategory3 <> '<None>' THEN ASCLAS.assetCategory3
        WHEN ASCLAS.assetCategory4 <> '<None>' THEN ASCLAS.assetCategory4
        ELSE ''
    END AS "Asset Sub-Category",
    ASSET.listPrice AS "List Price",
    VEHICLE.co2Emissions as "CO2 Emissions",
    VEHICLE.engineSizeInCC as "Engine Size (cc)",
    VEHICLE.fuelType as "Fuel Type",
    VEHICLE.maximumPowerRating as "Max Power Rating",
    VEHICLE.maximumPowerRatingType as "Power Rating Type",
    VEHICLE.grossWeight as "Gross Weight",
    VEHICLE.grossWeightUnitType as "Gross Weight Unit Type"
FROM odsAsset ASSET
    LEFT OUTER JOIN odsAssetClassification ASCLAS on ASSET.assetClassificationId = ASCLAS.id
    LEFT JOIN odsAgreement AGR ON AGR.id = ASSET.agreementId
    LEFT JOIN odsVehicle VEHICLE ON VEHICLE.assetId = ASSET.id
WHERE ASSET.assetIdentifier > 0
ORDER BY 1 ASC