-- SFCONTRACTEOD:   Salesforce Integration - End of Day Contract Updates
-- Summary:         **INTEGRATION USE ONLY - DO NOT RUN MANUALLY** SALESFORCE CONTRACT INTEGRATION
--                  Provides data points required for upsert of Contract object in Salesforce
--                  Assumes will always run after EOD which will start no earlier than midnight
SELECT CASE
        WHEN AGRCO.agrcompanycode IN ('2', '4') THEN 'Evington'
        ELSE 'Arkle'
    END AS "BOOK__C",
    CASE
        WHEN AGRCO.agrcompanycode = '1' THEN 'CORE'
        WHEN AGRCO.agrcompanycode = '3' THEN 'PRIME'
        ELSE 'CONSUMER'
    END AS "PRICING",
    COMM.commission AS "COMMISSION",
    LEFT(
        concat(
            AGR.agreementnumber,
            ' ',
            SCHED.scheduleDescription
        ),
        80
    ) AS "NAME",
    -- Some allowances for data alignment following the Iceman migration into ALFA
    CASE
        WHEN AGRCO.agrcompanycode = '4'
        AND EXTREF.reference IS NOT NULL
        AND LEFT(EXTREF.reference, 1) != 'E'
        AND BILL.uniqueref NOT IN ('T000010005/2') THEN EXTREF.reference
        ELSE TRIM(BILL.uniqueref)
    END AS "ACTOR_CODE",
    CASE
        WHEN DLRBIL.dealerBillingUniqueRef = '<None>' THEN Null
        ELSE DLRBIL.dealerBillingUniqueRef
    END AS "BROKER__C",
    CASE
        WHEN AGR.invCusId != AGR.expCusId THEN TRIM(EXPBILL.expCusBillingUniqueRef)
        ELSE NULL
    END AS "SUB_LESSEE__C",
    AGR.agreementnumber AS "CONTRACT_NUMBER__C",
    AGR.agreementnumber AS "OPPORTUNITY__C",
    AGR.product AS "PRODUCT_TYPE__C ",
    '' AS "LOADSTATUS",
    CASE
        WHEN AGR.product = 'Staged Payment Loan' THEN ROUND(DRAWDOWNTD.drawdown_amount_to_date, 2)
        WHEN AGRMISC.stpl = true THEN ROUND(
            SCHED.totalAssetCostFinanced - coalesce(DRAWDOWNTD.drawdown_amount_to_date, 0),
            2
        )
        ELSE SCHED.totalAssetCostFinanced
    END AS "AMOUNT_ADVANCED",
    CASE
        WHEN AGR.isCcaRegulated = '1' THEN 'Regulated'
        ELSE 'Unregulated'
    END AS "REGULATION_TYPE__C",
    SCHED.startDate AS "STARTDATE",
    SCHED.maturityDate AS "ENDDATE",
    ASSET.assetDescription AS "EQUIPMENT_ASSET_DESCRIPTION__C",
    ASSET.assetClassificationName AS "ASSET_CLASSIFICATION",
    SCHED.termInMonths AS "CONTRACTTERM",
    UNPINV.unpaid_invoices AS "UNPAID_INVOICES__C",
    ROUND(UNPINV.unpaid_inv_amount, 2) AS "INVOICED_AMOUNT_UNPAID__C",
    '' AS "NOTES__C",
    ROUND(SCHED.nextRentalAmount, 2) AS "REGULAR_PAYMENT__C",
    ROUND(SCHED.firstRentalAmount, 2) AS "INITIAL_PAYMENT__C",
    extract(
        DAY
        from SCHED.nextRentalDate
    ) AS "PAYMENT_DATE__C",
    SCHED.numberOfFutureRentals AS "OUTSTANDING_INSTALLMENTS__C",
    SCHED.nextRentalDate AS "NEXT_RENTAL_DATE__C",
    CASE
        WHEN SCHED.nextRentalFrequency = '<None>' THEN ''
        ELSE SCHED.nextRentalFrequency
    END AS "FREQUENCY__C",
    CASE
        WHEN NEXTRENTAL.nextRentalPaymentTypeName = '<None>' THEN ''
        ELSE NEXTRENTAL.nextRentalPaymentTypeName
    END AS "PAYMENT_METHOD__C",
    SCHED.previousRentalDate AS "LAST_RENTAL_DUE_DATE__C",
    ROUND(SCHED.previousRentalAmount, 2) AS "LAST_RENTAL_DUE_AMOUNT__C",
    ROUND(SCHED.grossBalanceRemaining, 2) AS "BALANCE__C",
    ROUND(SCHED.capitalOutstanding, 2) AS "CAPITAL_OS__C",
    ROUND(coalesce(BALLOONRENTAL.balloonrental, 0), 2) AS "BALLOON__C",
    ROUND(SCHED.totalResidualValue, 2) AS "RESIDUAL_VALUE__C",
    ROUND(SCHED.annualPercentageRate, 2) AS "APR__C",
    ROUND(SCHED.combinedLessorPtoRate, 2) AS "YIELD__C",
    ROUND(SCHED.totalChargesDue, 2) AS "TOTAL_CHARGES__C",
    SCHED.informallyExtends AS "AUTOMATIC_EXTENSION__C",
    LASTRCT.transactionEffectiveDate AS "LAST_RECEIPT_DATE__C",
    ROUND(LASTRCT.totalAmount, 2) AS "LAST_RECEIPT_AMOUNT__C",
    CASE
        WHEN SCHED.totalScheduleArrears > 0 THEN SCHED.inArrearsSinceDate
        ELSE NULL
    END AS "LAST_ARREARS_DATE__C",
    CASE
        WHEN SCHED.totalScheduleArrears > 0 THEN systemdate - SCHED.inArrearsSinceDate
        ELSE 0
    END AS "DAYS_IN_ARREARS__C",
    0 AS "ARREARS_REPAYMENTS__C",
    CASE
        WHEN coalesce(SCHED.totalScheduleArrears, 0) > 0
        AND coalesce(SCHED.nextRentalAmount, 0) > 0 THEN ROUND(
            SCHED.totalScheduleArrears / SCHED.nextRentalAmount,
            0
        )
        ELSE 0
    END AS "NUMBER_OF_INSTALMENTS_IN_ARREARS__C",
    ROUND(SCHED.totalScheduleArrears, 2) AS "TOTAL_ARREARS__C",
    ROUND(coalesce(zerothirty, 0), 2) AS "X30__C",
    ROUND(coalesce(thirtysixty, 0), 2) AS "X060__C",
    ROUND(coalesce(sixtyninety, 0), 2) AS "X090__C",
    ROUND(coalesce(ninetyplus, 0), 2) AS "X0__C",
    ROUND(coalesce(ARRFEE.arrears_fee, 0), 2) AS "ARREARS_FEES__C",
    ROUND(coalesce(PAYMENTS.payments_received, 0), 2) AS "PAYMENTS__C",
    ROUND(INVOICED.invoiced_amount, 2) AS "INVOICED_AMOUNT__C",
    SCHED.scheduleStatus AS "STATUS",
    CASE
        WHEN PARTIAL.agreementid IS NOT Null THEN 'Partially Terminated'
        ELSE Null
    END AS "SUB_STATUS__C",
    '0123z000000KtmvAAC' AS "RECORDTYPEID"
FROM OdsAgreement AGR
    JOIN OdsSystemDate ON 1 = 1
    JOIN OdsBillingAddress BILL ON BILL.id = AGR.invCusBillingId
    JOIN OdsExposureCustomerBilling EXPBILL on EXPBILL.id = AGR.expCusBillingId
    JOIN OdsSchedule SCHED ON SCHED.agreementid = AGR.id
    AND SCHED.terminationnumber <= 1
    LEFT JOIN (
        SELECT agreementid
        FROM OdsSchedule
        WHERE terminationNumber > 1
        GROUP BY agreementid
    ) PARTIAL ON PARTIAL.agreementid = AGR.id
    JOIN OdsAgreementCompany AGRCO ON AGRCO.id = SCHED.agrCompanyId
    JOIN OdsDealer DLR ON DLR.id = SCHED.dealerId
    JOIN OdsDealerBilling DLRBIL ON DLRBIL.id = SCHED.dealerBillingId
    LEFT JOIN OdsNextRentalPaymentType NEXTRENTAL ON NEXTRENTAL.id = SCHED.nextRentalPaymentTypeId
    LEFT JOIN (
        SELECT RECEIVABLE.scheduleid,
            SUM(RECEIVABLE.amount) AS "arrears_fee" -- Identify arrears fee
        FROM OdsReceivable RECEIVABLE
            JOIN OdsReceivableChargeType RCT ON RCT.id = RECEIVABLE.recvchargetypeid
            AND RCT.recvchargetypecode = 213
        GROUP BY RECEIVABLE.scheduleid
    ) ARRFEE ON ARRFEE.scheduleid = SCHED.id
    LEFT JOIN (
        SELECT RECEIVABLE.scheduleid,
            SUM(RECEIVABLE.amount) AS "invoiced_amount"
        FROM OdsReceivable RECEIVABLE
        WHERE RECEIVABLE.isInvoiced = true
        GROUP BY RECEIVABLE.scheduleid
    ) INVOICED ON INVOICED.scheduleid = SCHED.id
    LEFT JOIN (
        SELECT RECEIVABLE.scheduleid,
            SUM(RECEIVABLE.amount) AS "payments_received"
        FROM OdsReceivable RECEIVABLE
        WHERE RECEIVABLE.isInvoiced = true
            AND RECEIVABLE.isDue = false
        GROUP BY RECEIVABLE.scheduleid
    ) PAYMENTS ON PAYMENTS.scheduleid = SCHED.id
    LEFT JOIN (
        SELECT scheduleid,
            transactionEffectiveDate,
            totalAmount
        FROM OdsCashAllocation
        WHERE id IN (
                SELECT MAX (CA.id)
                FROM OdsCashAllocation CA
                    JOIN OdsSystemDate ON 1 = 1
                WHERE CA.transactionEffectiveDate <= systemdate
                GROUP BY CA.scheduleid
            )
    ) LASTRCT ON LASTRCT.scheduleid = SCHED.id
    LEFT JOIN (
        SELECT scheduleid,
            assetTypeName,
            assetTypeCode,
            assetClassificationName,
            assetDescription -- Identify Asset of most value
        FROM OdsAsset
        WHERE isMainAsset = '1'
    ) ASSET ON ASSET.scheduleid = SCHED.id
    LEFT JOIN (
        SELECT scheduleid,
            rentalProfileAmount AS balloonrental
        FROM OdsScheduleRentalProfile
        WHERE rentalSubType = 'Balloon'
    ) BALLOONRENTAL ON BALLOONRENTAL.scheduleid = SCHED.id
    LEFT JOIN (
        SELECT INV.scheduleid,
            SUM(unpaidAmount) AS "zerothirty"
        FROM OdsInvoiceLine INV
            JOIN OdsSystemDate on 1 = 1
        WHERE unpaidAmount > 0
            AND isDue = '1'
            AND isPaid = '0'
            AND systemdate - invoiceReferenceDate >= 0
            AND systemdate - invoiceReferenceDate < 31
        GROUP BY scheduleid
    ) ARR30 ON ARR30.scheduleid = SCHED.id
    AND zerothirty > 0
    LEFT JOIN (
        SELECT INV.scheduleid,
            SUM(unpaidAmount) AS "thirtysixty"
        FROM OdsInvoiceLine INV
            JOIN OdsSystemDate on 1 = 1
        WHERE unpaidAmount > 0
            AND isDue = '1'
            AND isPaid = '0'
            AND systemdate - invoiceReferenceDate > 30
            AND systemdate - invoiceReferenceDate < 61
        GROUP BY scheduleid
    ) ARR60 ON ARR60.scheduleid = SCHED.id
    AND thirtysixty > 0
    LEFT JOIN (
        SELECT INV.scheduleid,
            SUM(unpaidAmount) AS "sixtyninety"
        FROM OdsInvoiceLine INV
            JOIN OdsSystemDate on 1 = 1
        WHERE unpaidAmount > 0
            AND isDue = '1'
            AND isPaid = '0'
            AND systemdate - invoiceReferenceDate > 60
            AND systemdate - invoiceReferenceDate < 91
        GROUP BY scheduleid
    ) ARR90 ON ARR90.scheduleid = SCHED.id
    AND sixtyninety > 0
    LEFT JOIN (
        SELECT INV.scheduleid,
            SUM(unpaidAmount) AS "ninetyplus"
        FROM OdsInvoiceLine INV
            JOIN OdsSystemDate on 1 = 1
        WHERE unpaidAmount > 0
            AND isDue = '1'
            AND isPaid = '0'
            AND systemdate - invoiceReferenceDate > 90
        GROUP BY scheduleid
    ) ninetyplus ON ninetyplus.scheduleid = SCHED.id
    AND ninetyplus > 0
    LEFT JOIN (
        SELECT IL.agreementid,
            COUNT(IH.invoiceNumber) AS "unpaid_invoices",
            SUM(invoiceLineAmount) AS "unpaid_inv_amount"
        FROM OdsInvoiceHeader IH
            JOIN OdsInvoiceLine IL ON IL.invoiceHeaderId = IH.ID
            AND IL.isPaid = false
            JOIN OdsSystemDate ON 1 = 1
        WHERE IH.invoiceDueDate < systemdate
        GROUP BY IL.agreementid
    ) UNPINV ON UNPINV.agreementid = AGR.id
    LEFT JOIN (
        SELECT PAYABLE.scheduleid,
            SUM(PAYABLE.amount) AS "commission"
        FROM OdsPayable PAYABLE
            JOIN OdsPayableChargeType PCT ON PCT.id = PAYABLE.payableChargeTypeId
            AND PCT.payableChargeTypeCode = 605
        GROUP BY PAYABLE.scheduleid
    ) COMM ON COMM.scheduleid = SCHED.id
    LEFT JOIN (
        SELECT scheduleId,
            SUM(assetDrawdownAmount) AS "drawdown_amount_to_date"
        FROM OdsAsset
            JOIN OdsSystemDate ON 1 = 1
        WHERE assetDrawdownDate <= systemdate
        GROUP BY scheduleId
    ) DRAWDOWNTD ON DRAWDOWNTD.scheduleid = SCHED.id
    LEFT JOIN OdsAgreementMiscellaneous AGRMISC ON AGRMISC.agreementid = AGR.id
    LEFT JOIN OdsExternalSystemReference EXTREF ON EXTREF.thirdPartyId = AGR.invCusId
    AND EXTREF.systemcode = 'ACR'
WHERE SCHED.scheduleStatus != 'Proposal'
    AND SCHED.scheduleStatus NOT LIKE '%<%'
    AND SCHED.activationDate != systemdate