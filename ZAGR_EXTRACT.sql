-- ZAGR_EXTRACT:    Agreement extract for Data Warehouse v1.0
-- Title:           DW Agreement Extract
SELECT to_CHAR(systemdate-1, 'YYYYMMDD') || AGR.agreementNumber AS "KEY",
    'ALFA' AS "SOURCE",
    CASE
        WHEN AGRCO.agrcompanycode in ('2','4') THEN 'Evington'
        ELSE 'Arkle'
    END AS "BOOK",
    to_CHAR(systemdate-1, 'DD/MM/YYYY') AS "DATE",
    AGR.agreementNumber AS "AGREEMENT",
    SCHED.scheduleStatus AS "STAUS",
    '' AS "SUB_STATUS",
    INVCUS.invCusThirdPartyNumber AS "INVOICE_CUSTOMER",
    EXPCUS.expCusThirdPartyNumber AS "EXPOSURE_CUSTOMER",
    SCHED.grossBalanceRemaining AS "BAL_OS",
    SCHED.capitalOutstanding AS "CAP_OS",
    systemdate - SCHED.inArrearsSinceDate AS "DAYS_IN_ARREARS",
    SCHED.totalScheduleArrears AS "TOTAL_ARREARS",
    zerothirty AS "0_30",
    thirtysixty AS "31_60",
    sixtyninety AS "61_90",
    ninetyplus AS "91P",
    NEXTRENTAL.nextRentalPaymentTypeName AS "PAYMENT_METHOD"
FROM OdsSchedule SCHED
    join OdsSystemDate ON 1 = 1
    JOIN OdsAgreement AGR ON AGR.id = SCHED.agreementid
    JOIN OdsInvoicingCustomer INVCUS ON INVCUS.id = SCHED.invCusId
    JOIN OdsExposureCustomer EXPCUS ON EXPCUS.id = SCHED.expCusId
    JOIN OdsAgreementCompany AGRCO ON AGRCO.id = SCHED.agrCompanyId
    LEFT JOIN OdsNextRentalPaymentType NEXTRENTAL ON NEXTRENTAL.id = SCHED.nextRentalPaymentTypeId
    LEFT JOIN (
        SELECT INV.scheduleid,
            SUM(unpaidAmount) AS "zerothirty"
        FROM OdsInvoiceLine INV
            JOIN OdsSystemDate on 1 = 1
        WHERE unpaidAmount > 0
            AND isDue = '1'
            AND isPaid = '0'
            AND systemdate - invoiceReferenceDate >= 0
            AND systemdate - invoiceReferenceDate < 31
        GROUP BY scheduleid
    ) ARR30 ON ARR30.scheduleid = SCHED.id
    AND zerothirty > 0 -- Arrears - Unpaid and overdue invoices 31-60
    LEFT JOIN (
        SELECT INV.scheduleid,
            SUM(unpaidAmount) AS "thirtysixty"
        FROM OdsInvoiceLine INV
            JOIN OdsSystemDate on 1 = 1
        WHERE unpaidAmount > 0
            AND isDue = '1'
            AND isPaid = '0'
            AND systemdate - invoiceReferenceDate > 30
            AND systemdate - invoiceReferenceDate < 61
        GROUP BY scheduleid
    ) ARR60 ON ARR60.scheduleid = SCHED.id
    AND thirtysixty > 0 -- Arrears - Unpaid and overdue invoices 61-90
    LEFT JOIN (
        SELECT INV.scheduleid,
            SUM(unpaidAmount) AS "sixtyninety"
        FROM OdsInvoiceLine INV
            JOIN OdsSystemDate on 1 = 1
        WHERE unpaidAmount > 0
            AND isDue = '1'
            AND isPaid = '0'
            AND systemdate - invoiceReferenceDate > 60
            AND systemdate - invoiceReferenceDate < 91
        GROUP BY scheduleid
    ) ARR90 ON ARR90.scheduleid = SCHED.id
    AND sixtyninety > 0 -- Arrears - Unpaid and overdue invoices 90+
    LEFT JOIN (
        SELECT INV.scheduleid,
            SUM(unpaidAmount) AS "ninetyplus"
        FROM OdsInvoiceLine INV
            JOIN OdsSystemDate on 1 = 1
        WHERE unpaidAmount > 0
            AND isDue = '1'
            AND isPaid = '0'
            AND systemdate - invoiceReferenceDate > 90
        GROUP BY scheduleid
    ) ninetyplus ON ninetyplus.scheduleid = SCHED.id
    AND ninetyplus > 0
WHERE SCHED.scheduleStatus IN (
        'Live (Primary)',
        'Live (Secondary)',
        'Terminated',
        'Matured'
    )