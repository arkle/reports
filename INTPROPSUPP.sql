-- INTPROPSUPP:     Integration - Obtain details for proposal drawdowns
-- Summary:         **INTEGRATION USE ONLY - DO NOT RUN MANUALLY**
--                  Finds Supplier identifiers to use in the save proposal web service
-- Parameters:      SUPPLIER_EXTREF String(20)
SELECT CASE
        WHEN SUPPLIER.thirdpartynumber IS NOT NULL THEN SUPPLIER.thirdPartyNumber
        WHEN SUPPEXIST.thirdPartyNumber IS NOT NULL THEN SUPPEXIST.thirdPartyNumber
        ELSE ''
    END AS "SUPPLIER_THIRD_PARTY_NUMBER",
    CASE
        WHEN SUPPLIER.billingNumber IS NOT NULL THEN SUPPLIER.billingNumber
        WHEN SUPPEXIST.billingNumber IS NOT NULL THEN SUPPEXIST.billingNumber
        ELSE NULL
    END AS "SUPPLIER_BILLING_ADDRESS_NUMBER"
FROM OdsSystemDate
    LEFT JOIN (
        SELECT thirdPartyNumber,
            billingNumber
        FROM OdsBillingAddress
            JOIN OdsExternalSystemReference ON systemcode = 'ACR'
            AND OdsBillingAddress.thirdpartyid = OdsExternalSystemReference.thirdpartyid
            AND reference = TRIM(:SUPPLIER_EXTREF)
        LIMIT 1
    ) SUPPLIER ON 1 = 1
    LEFT JOIN (
        SELECT thirdPartyNumber,
            billingNumber
        FROM OdsBillingAddress
        WHERE uniqueRef = TRIM(:SUPPLIER_EXTREF)
    ) SUPPEXIST ON 1 = 1