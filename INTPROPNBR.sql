-- INTPROPNBR:  Integration - Obtain details to activate ALFA proposals ALFA v1.0
-- Summary:     **INTEGRATION USE ONLY - DO NOT RUN MANUALLY**
--              Finds the ALFA agreement identifiers and the New Business workflow case associated with it
-- Parameters:  AGREEMENT_EXTREF String(20)
SELECT CASE
        WHEN EXISTAGR.agreementNumber = TRIM(:AGREEMENT_EXTREF) THEN EXISTAGR.agreementNumber
        WHEN AGR.agreementNumber IS NOT NULL THEN AGR.agreementNumber
        ELSE NULL
    END AS "AGREEMENT_NUMBER",
    CASE
        WHEN EXISTAGR.agreementNumber = TRIM(:AGREEMENT_EXTREF) THEN EXISTSCHED.schedulenumber
        ELSE SCHED.schedulenumber
    END AS "SCHEDULE_NUMBER",
    CASE
        WHEN EXISTSCHED.scheduleStatus != 'Proposal' THEN 'Y'
        WHEN SCHED.scheduleStatus != 'Proposal'
        AND MISC.agrid = TRIM(:AGREEMENT_EXTREF) THEN 'Y'
        ELSE 'N'
    END AS "LIVE_AGREEMENT",
    CASE
        WHEN EXISTAGR.agreementNumber = TRIM(:AGREEMENT_EXTREF) THEN CASB.caseIdSequenceNumber
        ELSE CASA.caseIdSequenceNumber
    END AS "CASE_ID"
FROM OdsAgreement AGR
    LEFT JOIN OdsAgreementMiscellaneous MISC ON MISC.agreementid = AGR.id
    AND MISC.agrid = TRIM(:AGREEMENT_EXTREF)
    LEFT JOIN OdsSchedule SCHED ON SCHED.agreementid = AGR.id
    LEFT JOIN OdsAgreement EXISTAGR ON EXISTAGR.agreementnumber = TRIM(:AGREEMENT_EXTREF)
    LEFT JOIN OdsSchedule EXISTSCHED ON EXISTSCHED.agreementid = EXISTAGR.id
    LEFT JOIN (
        SELECT OdsCase.agreementId,
            OdsCase.caseIdSequenceNumber
        FROM OdsCase
            JOIN OdsCurrentCaseStatus ON OdsCase.currentStatusId = OdsCurrentCaseStatus.id
            AND OdsCurrentCaseStatus.currentStatusTypeCode = 'ORG'
        WHERE OdsCase.status = 'Open'
    ) CASA ON CASA.agreementId = AGR.id
    LEFT JOIN (
        SELECT OdsCase.agreementId,
            OdsCase.caseIdSequenceNumber
        FROM OdsCase
            JOIN OdsCurrentCaseStatus ON OdsCase.currentStatusId = OdsCurrentCaseStatus.id
            AND OdsCurrentCaseStatus.currentStatusTypeCode = 'ORG'
        WHERE OdsCase.status = 'Open'
    ) CASB ON CASB.agreementId = EXISTAGR.id
WHERE MISC.agrid = TRIM(:AGREEMENT_EXTREF)
    OR EXISTAGR.agreementNumber = TRIM(:AGREEMENT_EXTREF)
LIMIT 1