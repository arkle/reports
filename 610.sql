-- Sales Tax - Input [610]
SELECT SI.supplierInvoiceNumber AS "Invoice Reference",
    AGR.agreementType AS "Agreement Type",
    VAT.vatTypeName AS "VAT Type",
    CASE
        WHEN SIL.supplierInvoiceAmount > 0 THEN CONCAT(
            ROUND(
                (
                    SIL.supplierInvoiceVATAmount / SIL.supplierInvoiceAmount
                ) * 100,
                2
            ),
            '%'
        )
        ELSE '0%'
    END AS "VAT %",
    COM.payableCompanyCode AS "Payer Code",
    COM.payableCompanyName AS "Payer Name",
    SI.invoiceTaxPointDate AS "Tax Point",
    SI.invoiceReceivedDate AS "Invoice Received Date",
    PAY.payeeName AS "Payee 3rd Party",
    PAY.payeeThirdPartyNumber AS "3rd Party Nr",
    PAY.payeeCountryCode AS "Country Code",
    CUR.payableCurrencyCode AS "Currency",
    CHRG.payableChargeTypeCode AS "Charge Type Code",
    CHRG.payableChargeTypeName AS "Charge Type Name",
    SUM(SIL.supplierInvoiceAmount) AS "Value",
    SUM(SIL.supplierInvoiceVATAmount) AS "VAT Amount"
FROM OdsSupplierInvoice SI
    JOIN OdsSupplierInvoiceLine SIL ON SIL.supplierInvoiceId = SI.id
    JOIN OdsPayableChargeType CHRG ON CHRG.id = SIL.payableChargeTypeId
    JOIN OdsAgreement AGR ON AGR.id = SIL.agreementId
    JOIN OdsVatType VAT ON SIL.vatTypeId = VAT.id
    JOIN OdsPayee PAY ON SI.payeeId = PAY.id
    JOIN OdsPayableCompany COM ON SIL.payableCompanyId = COM.id
    JOIN OdsPayableCurrency CUR ON SIL.payableCurrencyId = CUR.id
WHERE SI.invoiceAuthorisedDate >= to_date(:startPeriod, 'YYYY-MM-DD')
    AND SI.invoiceAuthorisedDate <= to_date(:endPeriod, 'YYYY-MM-DD')
GROUP BY SI.supplierInvoiceNumber,
    AGR.agreementType,
    CHRG.payableChargeTypeCode,
    CHRG.payableChargeTypeName,
    VAT.vatTypeName,
    CASE
        WHEN SIL.supplierInvoiceAmount > 0 THEN CONCAT(
            ROUND(
                (
                    SIL.supplierInvoiceVATAmount / SIL.supplierInvoiceAmount
                ) * 100,
                2
            ),
            '%'
        )
        ELSE '0%'
    END,
    -- "VAT %" column
    COM.payableCompanyCode,
    COM.payableCompanyName,
    SI.invoiceTaxPointDate,
    SI.invoiceReceivedDate,
    PAY.payeeName,
    PAY.payeeThirdPartyNumber,
    PAY.payeeCountryCode,
    CUR.payableCurrencyCode
having (
        SUM(SIL.supplierInvoiceVATAmount) > 0
        or SUM(SIL.supplierInvoiceVATAmount) < 0
    ) -- "VAT Amount" column  
ORDER BY SI.supplierInvoiceNumber,
    COM.payableCompanyName,
    PAY.payeeName;