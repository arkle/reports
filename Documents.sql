-- DOCUMENTS:   Documents report v2.0
-- Title:       ALFA Generated Documents
-- Summary:     Provides details of all ALFA generated documents. Used for second line reviews and housekeeping.
-- Parameters:  Date_from, Date_to

SELECT 
    to_char(DOCS.alfaLetterNumber,'9999999999') AS "ALFA_Ref",
    BA.thirdPartyNumber        AS "Recipient_id",
    BA.name                    AS "Recipient_Name",
    AGR.agreementNumber         AS "Agreement",
    SCH.scheduleStatus          AS "Agreement_Status", 
    AGR.agreementtype           AS " Agreement_type",
    SCH.terminationreason       AS "Termination_Reason",
    NPT.nextRentalPaymentTypeName AS "Payment_Type",
    DOCS.statusDate             AS "Date",
    DOCTYPE.name                AS "Document_Name",
    DOCS.status                 AS "Status", 
    ACQUIS.insuranceStatus AS "Insurance_status",
CASE
WHEN AAL.Mig = TRUE THEN 'Y' 
ELSE 'N' 
END AS "Migrated", 
CASE 
WHEN SCH.totalCustomerArrears = 1 THEN 'Y'
ELSE 'N' 
END AS "In_Arrears"
FROM OdsDocument DOCS 
LEFT JOIN OdsAgreement AGR 
    ON AGR.id = DOCS.agreementId
LEFT JOIN OdsDocumentType DOCTYPE
    ON DOCTYPE.id = DOCS.documentTypeId
JOIN OdsBillingAddress BA 
    ON BA.id = DOCS.recipientBillingAddressId
INNER JOIN Odsschedule SCH ON AGR.Id = SCh.agreementid
INNER JOIN OdsNextRentalPaymentType NPT ON SCH.nextRentalPaymentTypeId = NPT.Id
INNER JOIN OdsAgreementAlert AAL ON AAL.agreementId = AGR.Id
      JOIN OdsScheduleLlisl ACQUIS ON ACQUIS.scheduleId = SCH.id
WHERE (DOCS.statusDate >= to_date(:Date_from, 'YYYY-MM-DD') 
        AND DOCS.statusDate <= to_date(:Date_to, 'YYYY-MM-DD')) 

ORDER BY DOCS.statusDate, BA.thirdPartyNumber, AGR.agreementNumber