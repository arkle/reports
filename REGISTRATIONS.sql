-- REGISTRATIONS:   Registrations v1.0
-- Title:           Security Registrations
-- Summary:         Provides details for contracts within a date range and support oversight of registering and de-registering assets
-- Parameter:       Activated_from, Activated_to, Asset_type
SELECT AGR.agreementNumber AS "Agreement",
    CASE
        WHEN EXPCUS.expCusThirdPartyNumber = INVCUS.invCusThirdPartyNumber THEN ''
        ELSE 'Y'
    END AS "Undisclosed",
    RLS.rlsflag AS "RLS",
    ASSETDESC.assetDescription AS "Asset",
    ASSETDESC.assetTypeName AS "Asset_type",
    ASSETDESC.assetclassificationname AS "Asset_classification",
    AGRMISC.sectydesc AS "Security_Type",
    AGRMISC.regon AS "Registered",
    AGRMISC.regen AS "De-registered",
    CASE
        WHEN AGRCO.agrcompanycode IN ('2', '4') THEN 'Evington'
        ELSE 'Arkle'
    END AS "Company",
    AGR.product AS "Product",
    SCHED.termInMonths AS "Term",
    CASE
        WHEN AGR.isCcaRegulated = '1' THEN 'Y'
        ELSE ''
    END AS "Regulated",
    INVCUS.invCusThirdPartyNumber AS "Invoice_cust_id",
    INVCUS.invCusName AS "Invoice_cust_name",
    CASE
        WHEN PRACTICAL.agreementid IS NULL THEN SCHED.activationDate
        ELSE SCHED.startDate
    END AS "Activated_date",
    SCHED.startDate AS "Start_date",
    SCHED.totalAssetCost AS "Total_asset_cost",
    SCHED.totalassetcostfinanced AS "Amount_financed",
    BALLOONRENTAL.balloonrental AS "Balloon",
    SCHED.numberOfFutureRentals AS "No_of_future_rentals",
    CASE
        WHEN PRACTICAL.agreementid IS NULL THEN SCHED.scheduleStatus
        ELSE 'Practical DAC (Paid_Out)'
    END AS "Status"
FROM OdsSchedule SCHED
    JOIN OdsAgreement AGR ON AGR.id = SCHED.agreementid
    JOIN OdsAgreementCompany AGRCO ON AGRCO.id = AGR.agrcompanyid
    LEFT JOIN (
        SELECT agreementid,
            sectydesc,
            regon,
            regen
        FROM OdsAgreementMiscellaneous
        WHERE sectycode IS NOT Null
    ) AGRMISC ON AGRMISC.agreementid = AGR.id
    LEFT JOIN (
        SELECT agreementId,
            'Y' AS practical_paid_out
        FROM OdsAgreementAlert
        WHERE ppo = '1'
    ) PRACTICAL ON PRACTICAL.agreementId = AGR.id
    LEFT JOIN (
        SELECT agreementId,
            'Y' AS rlsflag
        FROM OdsAgreementAlert
        WHERE rls = '1'
    ) RLS ON RLS.agreementId = AGR.id
    JOIN OdsInvoicingCustomer INVCUS ON INVCUS.id = SCHED.invCusId
    JOIN OdsExposureCustomer EXPCUS ON EXPCUS.id = SCHED.expCusId
    JOIN OdsAsset ASSETDESC ON ASSETDESC.agreementid = AGR.id
    AND ASSETDESC.isMainAsset = True
    LEFT JOIN (
        SELECT scheduleid,
            rentalProfileAmount AS balloonrental
        FROM OdsScheduleRentalProfile
        WHERE rentalSubType = 'Balloon'
    ) BALLOONRENTAL ON BALLOONRENTAL.scheduleid = SCHED.id
WHERE (
        SCHED.activationDate >= to_date(:Activated_from, 'YYYY-MM-DD')
        AND SCHED.activationDate <= to_date(:Activated_to, 'YYYY-MM-DD')
    )
    OR (
        PRACTICAL.agreementId IS NOT NULL
        AND (
            SCHED.startDate >= to_date(:Activated_from, 'YYYY-MM-DD')
            AND SCHED.startDate <= to_date(:Activated_to, 'YYYY-MM-DD')
        )
    )
    AND LOWER(ASSETDESC.assetTypeName) LIKE LOWER(CONCAT('%', :Asset_type, '%'))
ORDER BY SCHED.startdate