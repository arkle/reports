SELECT DISTINCT AGR.agreementNumber AS "Agreement Number",
    SCHED.schedulestatus,
    AGR.product AS "Product Type",
    CASE WHEN EXPCUSBIL.expCusBillingContactName = '<None>' THEN 'Valued Customer' ELSE EXPCUSBIL.expCusBillingContactName END AS "Primary Contact",
    EXPCUS.expCusName AS "Agreement Name",
    EXPCUS.expCusAddressLine1 AS "Address Line 1",
    EXPCUS.expCusAddressLine2 AS "Address Line 2",
    EXPCUS.expCusAddressLine3 AS "Address Line 3",
    EXPCUS.expCusAddressLine4 AS "Address Line 4",
    EXPCUS.expCusAddressLine5 AS "Address Line 5",
    EXPCUS.expCusPostalCode AS "Post Code",
    SCHED.maturityDate AS "Maturity Date",
    SCHED.balloonAmount AS "Balloon Amount",
    SCHED.balloonDate AS "Balloon Date",
    SCHED.scheduleDescription AS "Asset Description",
    CASE WHEN SCHED.totalScheduleArrears > 0 THEN 'Y' ELSE 'N' END AS "In Arrears"
FROM OdsSchedule SCHED
    INNER JOIN OdsAgreement AGR ON SCHED.agreementId = AGR.id
    INNER JOIN OdsExposureCustomer EXPCUS ON AGR.expCusId = EXPCUS.id
    INNER JOIN OdsExposureCustomerBilling EXPCUSBIL ON AGR.expCusBillingId = EXPCUSBIL.id
WHERE SCHED.scheduleStatus = 'Live (Primary)'
AND SCHED.maturityDate >= :START_DATE AND SCHED.maturityDate <= :MATURITY_DATE
order by 11 ASC;