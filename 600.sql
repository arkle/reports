SELECT IVH.formattedInvoiceNumber AS "Invoice Number ",
    AGR.agreementNumber AS "Agreement Number",
    AGR.agreementType AS "Agreement Type",
    SCH.scheduleNumber AS "Schedule Number",
    IVH.invoiceTaxPointDate AS "Invoice Tax Point Date",
    IVH.invoiceDueDate AS "Invoice Due Date",
    IVH.invoiceIssueDate AS "Invoice Issue Date",
    CST.invCusThirdPartyNumber AS "Invoicing Customer Number",
    CST.invCusName AS "Invoicing Customer Name",
    CUR.agrCurrencyCode AS "Agreement Currency",
    CHRG.code AS "Charge Type Code",
    CHRG.name AS "Charge Type Name",
    SUM(IVL.invoiceLineAmount) AS "Net Amount",
    VAT.vatTypeName AS "VAT Type",
    SUM(IVL.invoiceVatAmount) AS "VAT Amount",
    SUM(IVL.invoiceLineAmount) + SUM(IVL.invoiceVatAmount) AS "Total Amount"
FROM OdsInvoiceHeader IVH
    JOIN OdsInvoiceLine IVL ON IVL.invoiceHeaderId = IVH.id
    JOIN OdsChargeType CHRG ON CHRG.id = IVL.chargeTypeId
    JOIN OdsVatType VAT ON IVL.vatTypeId = VAT.id
    JOIN OdsInvoicingCompany COM ON IVH.invCompanyId = COM.id
    JOIN OdsInvoicingCustomer CST ON IVL.invCusId = CST.id
    JOIN OdsDealer DLR ON IVL.dealerId = DLR.id
    JOIN OdsAgreementCurrency CUR ON IVL.agrCurrencyId = CUR.id
    JOIN OdsAgreement AGR ON IVL.agreementId = AGR.id
    JOIN OdsScheduleMain SCH ON IVL.scheduleId = SCH.id
WHERE IVH.invoiceDueDate >= to_date(:startDate, 'YYYY-MM-DD')
    AND IVH.invoiceDueDate <= to_date(:endDate, 'YYYY-MM-DD')
GROUP BY IVH.formattedInvoiceNumber,
    AGR.agreementNumber,
    AGR.agreementType,
    SCH.scheduleNumber,
    IVH.invoiceTaxPointDate,
    IVH.invoiceDueDate,
    IVH.invoiceIssueDate,
    CST.invCusThirdPartyNumber,
    CST.invCusName,
    CUR.agrCurrencyCode,
    CHRG.code,
    CHRG.name,
    VAT.vatTypeName;