-- ZDW_AGREEMENT: Agreement extract for Data Warehouse
SELECT to_char(systemdate, 'DD/MM/YYYY') AS "end_of_day",
    AGR.agreementNumber AS "agreement_number",
    CASE
        WHEN MISC.agrid = '<None>' THEN ''
        ELSE MISC.agrid
    END AS "alt_agreement_number",
    LEFT(
        concat(
            AGR.agreementnumber,
            ' ',
            SCHED.scheduleDescription
        ),
        80
    ) AS "agreement_name",
    CASE
        WHEN ASSET.assetclassificationcode = '<None>' THEN ''
        ELSE ASSET.assetclassificationcode
    END AS "asset_classification_code",
    INVCUS.invCusBillingUniqueRef AS "invoice_customer",
    EXPCUS.expCusBillingUniqueRef AS "exposure_customer",
    -- Account for migrated agreements wh=en identifying what is expected to be the primary account in Salesforce
    CASE
        WHEN AGRCO.agrcompanycode = '4'
        AND EXTREF.reference IS NOT NULL
        AND LEFT(EXTREF.reference, 1) != 'E'
        AND INVCUS.invCusBillingUniqueRef NOT IN ('T000010005/1', 'T000010005/2')
        AND AGR.agreementNumber != 'A000006320' THEN EXTREF.reference
        ELSE TRIM(INVCUS.invCusBillingUniqueRef)
    END AS "salesforce_primary_account",
    CASE
        WHEN INVCUS.invCusBillingUniqueRef = EXPCUS.expCusBillingUniqueRef THEN ''
        ELSE 'Y'
    END AS "undisclosed",
    CASE
        WHEN MISC.bro <> '<None>' THEN MISC.bro
        WHEN DLRBIL.dealerBillingUniqueRef = '<None>' THEN ''
        ELSE DLRBIL.dealerBillingUniqueRef
    END AS "broker",
    CASE
        WHEN SCHED.schedulestatus LIKE 'Live%'
        AND PARTIAL.agreementid IS NOT Null THEN 'PT'
        WHEN SCHED.scheduleStatus = 'Live (Primary)' THEN 'LP'
        WHEN SCHED.scheduleStatus = 'Live (Secondary)' THEN 'LS'
        WHEN SCHED.scheduleStatus = 'Terminated' THEN 'TE'
        WHEN SCHED.scheduleStatus = 'Matured' THEN 'MA'
        WHEN AGRALERT.ppo = '1' THEN 'PP'
        WHEN SCHED.schedulestatus = 'Proposal'
        AND CREDIT.decisionstatus NOT IN ('<None>', '<Unknown>')
        AND AGRALERT.ntu = '1' THEN 'NT'
        WHEN SCHED.schedulestatus = 'Proposal'
        AND AGRALERT.ntu = '1' THEN 'NT'
        WHEN SCHED.schedulestatus = 'Proposal'
        AND CAS.agreementid IS Null THEN 'CA'
        ELSE 'PR'
    END AS "agreement_status_code",
    AGR.productCode AS "product_code",
    --  Set proposal date for migrated agreements to the agreement start date
    CASE
        WHEN MISC.orp IS NOT NULL THEN to_char(MISC.orp, 'DD/MM/YYYY')
        WHEN AGRALERT.mig = '1' THEN to_char(SCHED.startDate, 'DD/MM/YYYY')
        ELSE to_char(CASEOPEN.openedDate, 'DD/MM/YYYY')
    END AS "proposal_date",
    CASE
        WHEN MISC.ors IS NOT NULL THEN to_char(MISC.ors, 'DD/MM/YYYY')
        ELSE to_char(SCHED.startDate, 'DD/MM/YYYY')
    END AS "start_date",
    to_char(SCHED.maturityDate, 'DD/MM/YYYY') AS "maturity_date",
    CASE
        WHEN MISC.oaf IS NOT NULL THEN MISC.oaf
        ELSE SCHED.totalAssetCostFinanced
    END AS "amount_financed",
    SCHED.balloonAmount AS "balloon_amount",
    SCHED.totalResidualValue AS "residual_value",
    CASE
        WHEN MISC.ory IS NOT NULL THEN MISC.ory / 100
        ELSE SCHED.combinedLessorPtoRate / 100
    END AS "yield",
    SCHED.originalLessorIrr / 100 AS "yield_irr",
    SCHED.originallesseeirr / 100 as "lessee_irr",
    SCHED.currentlesseeptorate / 100 as "lessee_pto",
    CASE
        WHEN MISC.ora IS NOT NULL THEN MISC.ora / 100
        ELSE SCHED.annualPercentageRate / 100
    END AS "apr",
    COMM.commission AS "commission",
    SCHED.totalExposure AS "total_exposure",
    SCHED.grossBalanceRemaining AS "balance_OS",
    CASE
        WHEN SCHED.scheduleStatus IN ('Terminated', 'Matured') THEN 0
        ELSE SCHED.capitalOutstanding
    END AS "capital_OS",
    CASE
        WHEN SCHED.scheduleStatus IN ('Terminated', 'Matured') THEN 0
        ELSE INTEREST_LESSEE.interest_os_lessee * -1
    END AS "interest_OS_lessee",
    CASE
        WHEN SCHED.scheduleStatus IN ('Terminated', 'Matured') THEN 0
        ELSE CAPOS_LESSOR.capitalOutstanding * -1
    END AS "capital_OS_lessor",
    CASE
        WHEN SCHED.scheduleStatus IN ('Terminated', 'Matured') THEN 0
        ELSE INTEREST_LESSOR.interest_os_lessor * -1
    END AS "interest_OS_lessor",
    AGR.agreementSuspense AS "suspense",
    SCHED.totalReceipts AS "total_receipts",
    CASE
        WHEN SCHED.scheduleStatus IN ('Terminated', 'Matured') THEN 0
        ELSE SCHED.futureReceivables
    END AS "future_receivables",
    CASE
        WHEN SCHED.scheduleStatus IN ('Terminated', 'Matured') THEN 0
        ELSE coalesce(URINC.UnearnedIncome, 0.00) - coalesce(DepositUnearned, 0.00) - coalesce(NotDueDeposit, 0.00)
    END AS "unearned_income",
    SCHEDINC.total_income AS "total_income",
    SCHEDINCA.income_accrued AS "income_accrued",
    CASE
        WHEN thirtysixty + sixtyninety + ninetyplus > 0 THEN systemdate - SCHED.inArrearsSinceDate + 1
        WHEN SCHED.totalScheduleArrears != 0
        AND (SCHED.activationDate = SCHED.inArrearsSinceDate) THEN Null
        WHEN SCHED.totalCustomerArrears != 0 THEN systemdate - SCHED.inArrearsSinceDate + 1
        ELSE 0
    END AS "days_in_arrears",
    zerothirty AS "arrears_0_30",
    ARR30.sum AS "arrears_0_30_VAT",
    thirtysixty AS "arrears_31_60",
    ARR60.sum AS "arrears_31_60_VAT",
    sixtyninety AS "arrears_61_90",
    ARR90.sum AS "arrears_61_90_VAT",
    ninetyplus AS "arrears_90p",
    ARR90P.sum AS "arrears_90p_VAT",
    pczerothirty AS "PC_0-30",
    pcthirtysixty AS "PC_31-60",
    pcsixtyninety AS "PC_61-90",
    pcninetyplus AS "PC_91P",
    PCARR30.sum AS "PC_0-30_VAT",
    PCARR60.sum AS "PC_31-60_VAT",
    PCARR90.sum AS "PC_61-90_VAT",
    PCARR90P.sum AS "PC_91P_VAT",
    CASE
        WHEN SCHED.totalCustomerArrears > 0
        AND CURRENT_DATE = SCHED.inArrearsSinceDate THEN 0
        ELSE SCHED.totalCustomerArrears
    END as "Cust_Arrears",
    SCHED.scheduleSuspenseTotal AS "Schedule_Suspense",
    CASE
        WHEN MISC.pcompcode = 'PRIM' THEN 'Prime'
        WHEN MISC.pcompcode = 'CORE' THEN 'Core'
        WHEN MISC.pcompcode = 'EVIN' THEN 'Consumer'
        WHEN AGRCO.agrcompanycode = '1' THEN 'Core'
        WHEN AGRCO.agrcompanycode IN ('2', '4') THEN 'Consumer'
        WHEN AGRCO.agrcompanycode = '3' THEN 'Prime'
    END AS "pricing",
    CASE
        WHEN MISC.pcompcode = 'EVIN' THEN 'Evington'
        WHEN AGRCO.agrcompanycode IN ('2', '4') THEN 'Evington'
        ELSE 'Arkle'
    END AS "book",
    DOCFEE.documentation_fee AS "docfee",
    SCHED.incomeSuspendedDate,
    SCHED.totalRentalsDue,
    SCHED.totalRentalsVatDue,
    SCHED.totalWriteOffs,
    SCHED.totalChargesDue,
    SCHED.totalChargesVatDue,
    SCHED.totalTerminationAmountDue,
    SCHED.totalTerminationAmountVatDue,
    SCHED.totalNetBookValue,
    SCHED.isInExtension,
    SCHED.isExtended,
    CASE
        WHEN MISC.ors IS NOT NULL THEN to_char(MISC.ors, 'DD/MM/YYYY')
        WHEN AGRALERT.ppo = '1' THEN to_char(SCHED.startDate, 'DD/MM/YYYY')
        WHEN AGRALERT.mig = '1' THEN to_char(SCHED.startDate, 'DD/MM/YYYY')
        ELSE to_char(SCHED.activationDate, 'DD/MM/YYYY')
    END AS "activated_date",
    CASE
        WHEN AGR.isCcaRegulated = '1' THEN 'Y'
        ELSE ''
    END AS "regulated",
    CASE
        WHEN AGRALERT.mig = '1' THEN true
        ELSE false
    END AS "Migrated",
    CASE
        WHEN SCHED.isTerminated IS true THEN to_char(SCHED.terminationDate, 'DD/MM/YYYY')
        ELSE NULL
    END AS "termination_date",
    CASE
        WHEN SCHED.terminationReason = '<None>' THEN NULL
        ELSE SCHED.terminationReason
    END AS "termination_reason",
    SCHED.terminationAmount as "termination_amount",
    CASE
        WHEN AGRALERT.rls = '1' THEN 'RLS'
        ELSE ''
    END AS "BBB",
    CASE
        WHEN RENTAL.scheduleid IS NULL THEN ''
        WHEN CAISDEFAULT.caisDefaultNoticeDate IS NOT NULL
        AND CAISDEFAULT.originalCaisDefaultBalance > 0 THEN '8'
        WHEN SCHED.totalCustomerArrears <= RENTAL.rentalProfileAmount THEN '0'
        WHEN SCHED.totalCustomerArrears >= RENTAL.rentalProfileAmount
        AND SCHED.totalCustomerArrears <= 2 * RENTAL.rentalProfileAmount THEN '1'
        WHEN SCHED.totalCustomerArrears >= 2 * RENTAL.rentalProfileAmount
        AND SCHED.totalCustomerArrears <= 3 * RENTAL.rentalProfileAmount THEN '2'
        WHEN SCHED.totalCustomerArrears >= 3 * RENTAL.rentalProfileAmount
        AND SCHED.totalCustomerArrears <= 4 * RENTAL.rentalProfileAmount THEN '3'
        WHEN SCHED.totalCustomerArrears >= 4 * RENTAL.rentalProfileAmount
        AND SCHED.totalCustomerArrears <= 5 * RENTAL.rentalProfileAmount THEN '4'
        WHEN SCHED.totalCustomerArrears >= 5 * RENTAL.rentalProfileAmount
        AND SCHED.totalCustomerArrears <= 6 * RENTAL.rentalProfileAmount THEN '5'
        WHEN SCHED.totalCustomerArrears >= 6 * RENTAL.rentalProfileAmount THEN '6'
        ELSE 'U'
    END AS "cais_status",
    CASE
        WHEN SCHED.nextRentalDate IS NULL THEN ''
        ELSE to_char(SCHED.nextRentalDate, 'DD')
    END AS "payment_day",
    CASE
        WHEN NEXTRENTAL.nextRentalPaymentTypeName = '<None>' THEN ''
        ELSE NEXTRENTAL.nextRentalPaymentTypeName
    END AS "payment_method",
    CASE
        WHEN SCHED.nextRentalFrequency = '<None>' THEN ''
        ELSE SCHED.nextRentalFrequency
    END AS "payment_frequency",
    SCHED.nextRentalAmount as "next_rental_amount",
    UNPINV.unpaid_invoices AS "instalments_in_arrears",
    CASE
        WHEN MISC.stpl = true
        AND AGR.productCode != 'STG' THEN true
        ELSE false
    END AS "linked_to_stage_payment_schedule",
    to_char(AGRALERT.ntu_start, 'DD/MM/YYYY') as "NTU_Start"
FROM odsSchedule SCHED
    LEFT JOIN odsScheduleCaisDefault CAISDEFAULT ON CAISDEFAULT.scheduleid = SCHED.id
    LEFT JOIN (
        SELECT scheduleid,
            rentalProfileAmount
        FROM odsScheduleRentalProfile
        WHERE id IN (
                select distinct on(scheduleid) id
                FROM odsScheduleRentalProfile
                order by scheduleid,
                    numberOfRentals desc
            )
    ) RENTAL ON RENTAL.scheduleid = SCHED.id
    JOIN eodSystemDate ON 1 = 1
    JOIN odsAgreement AGR ON AGR.id = SCHED.agreementid
    JOIN odsAgreementMiscellaneous MISC ON MISC.agreementid = AGR.id
    JOIN odsInvoicingCustomerBilling INVCUS ON INVCUS.id = SCHED.invCusBillingId
    JOIN odsExposureCustomerBilling EXPCUS ON EXPCUS.id = SCHED.expCusBillingId
    JOIN odsAgreementCompany AGRCO ON AGRCO.id = SCHED.agrCompanyId
    LEFT JOIN (
        SELECT scheduleid,
            assetclassificationcode
        FROM odsAsset
        WHERE isMainAsset = '1'
    ) ASSET ON ASSET.scheduleid = SCHED.id
    LEFT JOIN odsAgreementAlert AGRALERT ON AGRALERT.agreementid = AGR.id
    LEFT JOIN (
        SELECT RECEIVABLE.scheduleid,
            SUM(RECEIVABLE.amount) AS "documentation_fee"
        FROM odsReceivable RECEIVABLE
            JOIN odsReceivableChargeType RCT ON RCT.id = RECEIVABLE.recvchargetypeid
            AND RCT.recvchargetypecode = 103
        GROUP BY RECEIVABLE.scheduleid
    ) DOCFEE ON DOCFEE.scheduleid = SCHED.id
    JOIN odsDealerBilling DLRBIL ON DLRBIL.id = SCHED.dealerBillingId
    LEFT JOIN odsAgreementCreditDecision CREDIT ON CREDIT.agreementId = AGR.id
    LEFT JOIN (
        Select agreementId,
            openedDate
        FROM odsCase
        WHERE id IN (
                SELECT min(CAS.id)
                FROM odsCase CAS
                    JOIN odsCurrentCaseStatus STAT ON STAT.id = CAS.currentStatusId
                    AND STAT.currentStatusTypeCode = 'ORG'
                GROUP BY agreementid
            )
    ) CASEOPEN ON CASEOPEN.agreementid = AGR.id
    LEFT JOIN (
        SELECT PAYABLE.scheduleid,
            SUM(PAYABLE.amount) AS "commission"
        FROM odsPayable PAYABLE
            JOIN odsPayableChargeType PCT ON PCT.id = PAYABLE.payableChargeTypeId
            AND PCT.payableChargeTypeCode IN (605, 630)
        GROUP BY PAYABLE.scheduleid
    ) COMM ON COMM.scheduleid = SCHED.id
    LEFT JOIN (
        SELECT INV.scheduleid,
            SUM(unpaidAmount) AS "zerothirty",
            SUM(unpaidVATAmount)
        FROM odsInvoiceLine INV
            JOIN eodSystemDate on 1 = 1
        WHERE unpaidAmount > 0
            AND isDue = '1'
            AND isPaid = '0'
            AND systemdate - invoiceReferenceDate >= 0
            AND systemdate - invoiceReferenceDate < 31
        GROUP BY scheduleid
    ) ARR30 ON ARR30.scheduleid = SCHED.id
    AND zerothirty > 0
    LEFT JOIN (
        SELECT INV.scheduleid,
            SUM(unpaidAmount) AS "thirtysixty",
            SUM(unpaidVATAmount)
        FROM odsInvoiceLine INV
            JOIN eodSystemDate on 1 = 1
        WHERE unpaidAmount > 0
            AND isDue = '1'
            AND isPaid = '0'
            AND systemdate - invoiceReferenceDate > 30
            AND systemdate - invoiceReferenceDate < 61
        GROUP BY scheduleid
    ) ARR60 ON ARR60.scheduleid = SCHED.id
    AND thirtysixty > 0
    LEFT JOIN (
        SELECT INV.scheduleid,
            SUM(unpaidAmount) AS "sixtyninety",
            SUM(unpaidVATAmount)
        FROM odsInvoiceLine INV
            JOIN eodSystemDate on 1 = 1
        WHERE unpaidAmount > 0
            AND isDue = '1'
            AND isPaid = '0'
            AND systemdate - invoiceReferenceDate > 60
            AND systemdate - invoiceReferenceDate < 91
        GROUP BY scheduleid
    ) ARR90 ON ARR90.scheduleid = SCHED.id
    AND sixtyninety > 0
    LEFT JOIN (
        SELECT INV.scheduleid,
            SUM(unpaidAmount) AS "ninetyplus",
            SUM(unpaidVATAmount)
        FROM odsInvoiceLine INV
            JOIN eodSystemDate on 1 = 1
        WHERE unpaidAmount > 0
            AND isDue = '1'
            AND isPaid = '0'
            AND systemdate - invoiceReferenceDate > 90
        GROUP BY scheduleid
    ) ARR90P ON ARR90P.scheduleid = SCHED.id
    AND ninetyplus > 0
    LEFT JOIN (
        SELECT CASHFLOW.scheduleid,
            SUM(CASHFLOW.profit) AS "interest_os_lessee"
        FROM odsScheduleCashflow CASHFLOW
            JOIN eodSystemDate ON 1 = 1
        WHERE CASHFLOW.isPrimaryAccountingStandard = false
            AND CASHFLOW.cashFlowEventDate > systemdate
        GROUP BY CASHFLOW.scheduleId
    ) INTEREST_LESSEE ON INTEREST_LESSEE.scheduleid = SCHED.id
    LEFT JOIN (
        SELECT CASHFLOW.scheduleid,
            SUM(CASHFLOW.profit) AS "interest_os_lessor"
        FROM odsScheduleCashflow CASHFLOW
            JOIN eodSystemDate ON 1 = 1
        WHERE CASHFLOW.isPrimaryAccountingStandard = true
            AND CASHFLOW.cashFlowEventDate > systemdate
        GROUP BY CASHFLOW.scheduleId
    ) INTEREST_LESSOR ON INTEREST_LESSOR.scheduleid = SCHED.id
    LEFT JOIN (
        SELECT CASHFLOW.scheduleid,
            CASHFLOW.capitaloutstanding
        FROM odsScheduleCashflow CASHFLOW
        WHERE CASHFLOW.id IN (
                SELECT MAX(id)
                FROM odsScheduleCashflow
                    JOIN eodSystemDate ON 1 = 1
                WHERE cashFlowEventDate <= systemdate
                    AND isPrimaryAccountingStandard = true
                GROUP BY scheduleid
            )
    ) CAPOS_LESSOR ON CAPOS_LESSOR.scheduleid = SCHED.id
    LEFT JOIN (
        SELECT agreementid
        FROM odsCase
        WHERE status = 'Open'
        GROUP BY agreementid
    ) CAS ON CAS.agreementid = AGR.id
    LEFT JOIN (
        SELECT agreementid,
            min(terminationNumber) AS "termno"
        FROM odsSchedule
        GROUP BY agreementid
    ) MIN_TERMNO ON MIN_TERMNO.agreementid = SCHED.agreementid
    LEFT JOIN (
        SELECT agreementid
        FROM odsSchedule
        WHERE terminationNumber > 1
        GROUP BY agreementid
    ) PARTIAL ON PARTIAL.agreementid = SCHED.agreementid
    LEFT JOIN odsExternalSystemReference EXTREF ON EXTREF.thirdPartyId = AGR.invCusId
    AND EXTREF.systemcode = 'ACR'
    LEFT JOIN odsNextRentalPaymentType NEXTRENTAL ON NEXTRENTAL.id = SCHED.nextRentalPaymentTypeId
    LEFT JOIN (
        SELECT IL.agreementid,
            COUNT(IH.invoiceNumber) AS "unpaid_invoices",
            SUM(invoiceLineAmount) AS "unpaid_inv_amount"
        FROM odsInvoiceHeader IH
            JOIN odsInvoiceLine IL ON IL.invoiceHeaderId = IH.id
            AND IL.isPaid = false
            JOIN odsSystemDate ON 1 = 1
        WHERE IH.invoiceDueDate < systemdate
        GROUP BY IL.agreementid
    ) UNPINV ON UNPINV.agreementid = AGR.id
    LEFT JOIN (
        SELECT scheduleid,
            SUM(A.rentalaccrual - A.depreciation) as UnearnedIncome
        FROM odsScheduleIncome A
        WHERE isPrimaryAccountingStandard = true
            and isRecognised = false
        GROUP BY scheduleid
    ) URINC ON SCHED.id = URINC.scheduleId
    LEFT JOIN (
        SELECT scheduleid,
            SUM(rentalAccrual) AS "total_repayments",
            SUM(earnings) AS "total_income"
        FROM odsScheduleIncome
        GROUP BY scheduleid
    ) SCHEDINC ON SCHEDINC.scheduleid = SCHED.id
    LEFT JOIN (
        SELECT scheduleid,
            SUM(earnings) AS "income_accrued"
        FROM odsScheduleIncome
        WHERE isaccrued = true
        GROUP BY scheduleid
    ) SCHEDINCA ON SCHEDINCA.scheduleid = SCHED.id
    LEFT JOIN (
        SELECT scheduleid,
            SUM(expenseIncomeAccrued) as DepositUnearned
        FROM odsScheduleExpenseIncome A
            JOIN odsExpenseIncomeChargeType B ON A.expenseIncomeChargeTypeId = B.id
        WHERE isRecognised = false
            AND expIncChargeTypeCode in (101, 102, 602)
        GROUP BY scheduleid
    ) URDEP ON SCHED.id = URDEP.scheduleId
    LEFT JOIN (
        SELECT scheduleid,
            SUM(amount) as NotDueDeposit
        FROM odsReceivable A
            JOIN odsReceivableChargeType B ON A.recvChargeTypeId = B.id
        WHERE isdue = false
            AND recvChargeTypeCode in (101, 102, 602)
        GROUP BY scheduleid
    ) NDDEP ON SCHED.id = NDDEP.scheduleId
    LEFT JOIN (
        SELECT DISTINCT INV.scheduleid,
            INVH.inv3pyid,
            SUM(unpaidAmount) AS "pczerothirty",
            SUM(unpaidVATAmount)
        FROM odsInvoiceLine INV
            JOIN eodSystemDate on 1 = 1
            JOIN odsInvoiceHeader INVH on INV.invoiceheaderid = INVH.id
        WHERE unpaidAmount > 0
            AND isDue = '1'
            AND isPaid = '0'
            AND systemdate - invoiceReferenceDate >= 0
            AND systemdate - invoiceReferenceDate < 31
        GROUP BY scheduleid,
            inv3pyid
    ) PCARR30 ON PCARR30.inv3pyid = SCHED.invCusId
    AND PCARR30.scheduleId = SCHED.id
    AND pczerothirty > 0 -- Arrears - Unpaid and overdue invoices 31-60
    LEFT JOIN (
        SELECT DISTINCT INV.scheduleid,
            INVH.inv3pyid,
            SUM(unpaidAmount) AS "pcthirtysixty",
            SUM(unpaidVATAmount)
        FROM odsInvoiceLine INV
            JOIN eodSystemDate on 1 = 1
            JOIN odsInvoiceHeader INVH on INV.invoiceheaderid = INVH.id
        WHERE unpaidAmount > 0
            AND isDue = '1'
            AND isPaid = '0'
            AND systemdate - invoiceReferenceDate > 30
            AND systemdate - invoiceReferenceDate < 61
        GROUP BY scheduleid,
            inv3pyid
    ) PCARR60 ON PCARR60.inv3pyid = SCHED.invCusId
    AND PCARR60.scheduleId = SCHED.id
    AND pcthirtysixty > 0 -- Arrears - Unpaid and overdue invoices 61-90
    LEFT JOIN (
        SELECT DISTINCT INV.scheduleid,
            INVH.inv3pyid,
            SUM(unpaidAmount) AS "pcsixtyninety",
            SUM(unpaidVATAmount)
        FROM odsInvoiceLine INV
            JOIN eodSystemDate on 1 = 1
            JOIN odsInvoiceHeader INVH on INV.invoiceheaderid = INVH.id
        WHERE unpaidAmount > 0
            AND isDue = '1'
            AND isPaid = '0'
            AND systemdate - invoiceReferenceDate > 60
            AND systemdate - invoiceReferenceDate < 91
        GROUP BY scheduleid,
            inv3pyid
    ) PCARR90 ON PCARR90.inv3pyid = SCHED.invCusId
    AND PCARR90.scheduleId = SCHED.id
    AND pcsixtyninety > 0 -- Arrears - Unpaid and overdue invoices 90+
    LEFT JOIN (
        SELECT DISTINCT INV.scheduleid,
            INVH.inv3pyid,
            SUM(unpaidAmount) AS "pcninetyplus",
            SUM(unpaidVATAmount)
        FROM odsInvoiceLine INV
            JOIN eodSystemDate on 1 = 1
            JOIN odsInvoiceHeader INVH on INV.invoiceheaderid = INVH.id
        WHERE unpaidAmount > 0
            AND isDue = '1'
            AND isPaid = '0'
            AND systemdate - invoiceReferenceDate > 90
        GROUP BY scheduleid,
            inv3pyid
    ) PCARR90P ON PCARR90P.inv3pyid = SCHED.invCusId
    AND PCARR90P.scheduleId = SCHED.id
    AND PCARR90P.pcninetyplus > 0
WHERE AGR.agreementnumber != '<None>'
    AND MIN_TERMNO.termno = SCHED.terminationNumber
order by 2 asc