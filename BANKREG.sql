-- BANKREG:     Bank Regulatory Report 
-- Summary:     Provides detail of all agreements in all stages, based on exposure customer versus invoicing customer
SELECT CASE
        WHEN AGRCO.agrcompanycode IN ('2','4') THEN 'Evington'
        ELSE 'Arkle'
    END AS "Agreement_Company",
    EXPCUS.expCusThirdPartyTypeName AS "Classification",
    EXPCUS.expCusIndustryName AS "SIC Code",
    EXPCUS.expCusThirdPartyNumber AS "Exposure_Third_Party_Number",
    EXTREF.reference AS "Exposure_Actor_Code",
    EXPCUS.expCusName AS "Exposure_Customer",
    AGR.agreementNumber AS "Agreement",
    CASE
        WHEN AGRALERT.mig = '1' THEN MISC.agrid
        ELSE ''
    END AS "Alternative_Agreement",
    SCHED.scheduleStatus AS "Stage",
    CASE
        WHEN AGRCO.agrcompanycode = '1' THEN 'CORE'
        WHEN AGRCO.agrcompanycode IN ('2','4') THEN 'CONSUMER'
        WHEN AGRCO.agrcompanycode = '3' THEN 'PRIME'
    END AS "Pricing",
    CASE
        WHEN AGR.isCcaRegulated = '1' THEN 'R'
        ELSE 'U'
    END AS "Regulated",
    AGR.agreementType AS "Agreement_type",
    AGR.product AS "Product",
    SCHED.startDate AS "Agreement_date",
    ASSET.assetTypeName AS "Asset Category",
    ASSET.assetDescription AS "Asset_Description",
    SCHED.originalAssetCostFinanced AS "Amount_Advanced",
    SCHEDINC.income AS "Total_Income",
    SCHED.totalResidualValue AS "Residual",
    SCHED.termInMonths AS "Duration",
    SCHED.nextRentalAmount AS "Next_rental",
    SCHED.nextRentalFrequency AS "Frequency",
    SCHEDINC.repayments AS "Total_Repayment",
    SCHED.grossBalanceRemaining AS "Balance_OS",
    CASHFLOW.incomeos * -1 AS "Income_OS",
    SCHED.capitalOutstanding AS "Capital_OS",
    SCHED.numberOfFutureRentals AS "Remaining_Instal",
    EXTRACT(
        DAY
        FROM SCHED.nextRentalDate
    ) AS "Rental_Day",
    SCHED.nextRentalDate AS "Next_Instalment_Date",
    SCHED.expectedEndDate AS "End_Date",
    SCHED.combinedLessorIrr AS "IRR",
    SCHED.annualPercentageRate AS "APR",
    SCHED.combinedLessorPtoRate AS "Yield",
    SCHED.currentLesseePtoRate AS "Lessee_PTO",
    SCHED.totalScheduleArrears AS "Arrears",
    zerothirty AS "0-30",
    thirtysixty AS "31-60",
    sixtyninety AS "61-90",
    ninetyplus AS "91P"
FROM EodSchedule SCHED
    JOIN EodAgreement AGR ON AGR.odsid = SCHED.agreementid
    JOIN OdsInvoicingCustomer INVCUS ON INVCUS.id = SCHED.invCusId
    JOIN OdsExposureCustomer EXPCUS ON EXPCUS.id = SCHED.expCusId
    JOIN OdsAgreementCompany AGRCO ON AGRCO.id = SCHED.agrCompanyId
    JOIN OdsAgreementMiscellaneous MISC ON MISC.agreementid = AGR.odsid
    LEFT JOIN OdsNextRentalPaymentType NEXTRENTAL ON NEXTRENTAL.id = SCHED.nextRentalPaymentTypeId -- Identify Asset of most value
    LEFT JOIN (
        SELECT scheduleid,
            assetTypeName,
            assetDescription
        FROM OdsAsset
        WHERE isMainAsset = '1'
    ) ASSET ON ASSET.scheduleid = SCHED.odsid -- External customer reference - Actor Code (for Exposure Customer)
    LEFT JOIN (
        SELECT thirdpartyid,
            reference
        FROM OdsExternalSystemReference
        WHERE systemcode = 'ACR'
    ) EXTREF ON EXTREF.thirdpartyid = SCHED.expCusId -- Repayments & Income
    LEFT JOIN (
        SELECT scheduleid,
            SUM(rentalAccrual) AS "repayments",
            SUM(earnings) AS "income"
        FROM OdsScheduleIncome
        GROUP BY scheduleid
    ) SCHEDINC ON SCHEDINC.scheduleid = SCHED.odsid
    LEFT JOIN (
        SELECT scheduleid,
            SUM(profit) AS "incomeos"
        FROM OdsScheduleCashFlow
            JOIN OdsSystemDate ON 1 = 1
        WHERE isPrimaryAccountingStandard = false
            AND cashFlowEventDate > systemdate
        GROUP BY scheduleid
    ) CASHFLOW ON CASHFLOW.scheduleid = SCHED.odsid
    LEFT JOIN (
        SELECT INV.scheduleid,
            SUM(unpaidAmount) AS "zerothirty"
        FROM eodInvoiceLine INV
            JOIN eodSystemDate on 1 = 1
        WHERE unpaidAmount > 0
            AND isDue = '1'
            AND isPaid = '0'
            AND systemdate - invoiceReferenceDate >= 0
            AND systemdate - invoiceReferenceDate < 31
        GROUP BY scheduleid
    ) ARR30 ON ARR30.scheduleid = SCHED.odsid
    AND zerothirty > 0 -- Arrears - Unpaid and overdue invoices 31-60
    LEFT JOIN (
        SELECT INV.scheduleid,
            SUM(unpaidAmount) AS "thirtysixty"
        FROM eodInvoiceLine INV
            JOIN eodSystemDate on 1 = 1
        WHERE unpaidAmount > 0
            AND isDue = '1'
            AND isPaid = '0'
            AND systemdate - invoiceReferenceDate > 30
            AND systemdate - invoiceReferenceDate < 61
        GROUP BY scheduleid
    ) ARR60 ON ARR60.scheduleid = SCHED.odsid
    AND thirtysixty > 0 -- Arrears - Unpaid and overdue invoices 61-90
    LEFT JOIN (
        SELECT INV.scheduleid,
            SUM(unpaidAmount) AS "sixtyninety"
        FROM eodInvoiceLine INV
            JOIN eodSystemDate on 1 = 1
        WHERE unpaidAmount > 0
            AND isDue = '1'
            AND isPaid = '0'
            AND systemdate - invoiceReferenceDate > 60
            AND systemdate - invoiceReferenceDate < 91
        GROUP BY scheduleid
    ) ARR90 ON ARR90.scheduleid = SCHED.odsid
    AND sixtyninety > 0 -- Arrears - Unpaid and overdue invoices 90+
    LEFT JOIN (
        SELECT INV.scheduleid,
            SUM(unpaidAmount) AS "ninetyplus"
        FROM eodInvoiceLine INV
            JOIN eodSystemDate on 1 = 1
        WHERE unpaidAmount > 0
            AND isDue = '1'
            AND isPaid = '0'
            AND systemdate - invoiceReferenceDate > 90
        GROUP BY scheduleid
    ) ninetyplus ON ninetyplus.scheduleid = SCHED.odsid
    LEFT JOIN (
        SELECT agreementId,
            mig
        FROM OdsAgreementAlert
    ) AGRALERT ON AGRALERT.agreementId = AGR.odsid
WHERE SCHED.schedulenumber > 0
ORDER BY SCHED.startDate