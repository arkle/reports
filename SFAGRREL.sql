-- SFAGRREL:        Salesforce Integration - Agreement Relations Information v1.0
-- Title:           Contract Relations Update    
-- Summary:         **INTEGRATION USE ONLY - DO NOT RUN MANUALLY** SALESFORCE CONTRACT INTEGRATION
--                  Provides data points required for upsert of Agreement Relations object in Salesforce
SELECT AGR.agreementnumber AS "CONTRACT__C",
    RELATED.actor_code AS "INVOLVED_PARTY__C",
    '0123z000000KtpzAAC' AS "RECORDTYPEID",
    RELATED.role AS "ROLE__C",
    CONCAT(
        RELATED.actor_code,
        AGR.agreementnumber,
        RELATED.type
    ) AS "EXTERNAL_ID"
FROM eodAgreement AGR
    JOIN eodSchedule SCHED ON SCHED.agreementid = AGR.odsid
    JOIN (
        SELECT 'Su' AS "type",
            AGR.odsid AS "agrid",
            'Supplier' AS "role",
            SUPBIL.supplrBillingUniqueRef AS "actor_code"
        FROM eodAgreement AGR
            JOIN eodAsset ASS ON ASS.agreementId = AGR.odsid
            JOIN eodSupplierBilling SUPBIL ON SUPBIL.odsid = ASS.supplrBillingId
            AND SUPBIL.odsid > 1
        UNION
        SELECT 'Gu' AS "type",
            AGR.odsid AS "agrid",
            'Guarantor' AS "role",
            GUABILL.guarBillingUniqueRef AS "actor_code"
        FROM eodAgreement AGR
            JOIN eodSecurity SEC ON SEC.agreementId = AGR.odsid
            JOIN eodGuarantorBilling GUABILL ON GUABILL.odsid = SEC.guarBillingId
            AND GUABILL.odsid > 1
    ) RELATED ON RELATED.agrid = AGR.odsid
    JOIN eodSystemDate ON 1 = 1
    LEFT JOIN (
        SELECT agreementid,
            startdate,
            true AS "flag"
        FROM eodCase CAS
            JOIN eodCaseHistory CASHIST ON CASHIST.caseId = CAS.odsid
            JOIN eodStartingCaseStatus STAT ON STAT.odsid = CASHIST.startStatusId
            AND STAT.startStatusTypeCode = 'NOV'
            AND STAT.startStatusStatusCode = '007'
        GROUP BY agreementid,
            startdate
    ) NOVATED ON NOVATED.agreementid = AGR.odsid
    AND NOVATED.startDate = systemdate
WHERE AGR.odsid > 2
    AND SCHED.scheduleStatus != 'Proposal'
    AND (
        SCHED.activationDate = systemdate - 1
        OR NOVATED.flag = true
    )