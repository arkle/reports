SELECT * 
FROM
(SELECT DISTINCT SCH.agreementNumber AS "Agreement_Number",
    SCH.inv3pyName AS "Third_Party_Name",
    SCH.inv3pyThirdPartyNumber AS "Third_Party_Number",
    SCH.schedulestatus AS "Schedule_status", 
    SCH.inarrearssincedate "In_arrears_since",
    zerothirty AS "0-30",
    "zerothirtyVAT" as "0-30_VAT",
    thirtysixty AS "31-60",
    "thirtysixtyVAT" as "31-60_VAT",
    sixtyninety AS "61-90",
    "sixtyninetyVAT" as "61-90_VAT",
    ninetyplus AS "91P",
    "ninetyplusVAT" as "91P_VAT",
    CASE
        WHEN "zerothirty" is null THEN 0
        else "zerothirty"
    END + CASE
        WHEN "thirtysixty" is null THEN 0
        else "thirtysixty"
    END + CASE
        WHEN "sixtyninety" is null THEN 0
        else "sixtyninety"
    END + CASE
        WHEN "ninetyplus" is null THEN 0
        else "ninetyplus"
    END AS "Arrears_Total",
    CASE
        WHEN "zerothirtyVAT" is null THEN 0
        else "zerothirtyVAT"
    END + CASE
        WHEN "thirtysixtyVAT" is null THEN 0
        else "thirtysixtyVAT"
    END + CASE
        WHEN "sixtyninetyVAT" is null THEN 0
        else "sixtyninetyVAT"
    END + CASE
        WHEN "ninetyplusVAT" is null THEN 0
        else "ninetyplusVAT"
    END AS "Arrears_Total_VAT",
    "WHO",
    "PCS"
from
(SELECT DISTINCT AGR.agreementNumber,
       SCHED.id,
       INVH.inv3pyid,
       INVP.inv3pyName,
       INVP.inv3pyThirdPartyNumber,
       SCHED.schedulestatus, 
       SCHED.inarrearssincedate,
      CASE 
    WHEN INVH.inv3pyid = AGR.dealerId THEN 'Broker'
    ELSE 'Other_Debtor'
END AS "WHO",
      CASE 
    WHEN INVH.inv3pyid = agr.invCusId THEN 1
    WHEN INVH.inv3pyid = agr.expCusId THEN 1
    WHEN INVH.inv3pyid = agr.dealerId THEN 2
    ELSE 0 
END AS "PCS",
       SCHED.totalschedulearrears,
       SCHED.totalcustomerarrears
FROM odsagreement AGR
LEFT OUTER JOIN odsschedule SCHED on AGR.id = SCHED.agreementId
LEFT OUTER JOIN odsInvoiceLine INV on SCHED.id = INV.scheduleId
JOIN odsInvoiceHeader INVH on INV.invoiceHeaderId = INVH.id
JOIN OdsInvoicingThirdParty INVP ON INVH.inv3pyId = INVP.Id
) SCH
LEFT JOIN (
        SELECT INV.scheduleid,
            INVH.inv3pyid,
            INVP.inv3pyName,
            INVP.inv3pyThirdPartyNumber,
            SUM(unpaidAmount) AS "zerothirty",
            SUM(unpaidVATAmount) as "zerothirtyVAT"
        FROM OdsInvoiceLine INV
            JOIN odsInvoiceHeader INVH on INV.invoiceHeaderId = INVH.id
            JOIN OdsInvoicingThirdParty INVP ON INVH.inv3pyId = INVP.Id
            JOIN OdsSystemDate on 1 = 1
        WHERE unpaidAmount > 0
            AND isDue = '1'
            AND isPaid = '0'
            AND systemdate - invoiceReferenceDate >= 0
            AND systemdate - invoiceReferenceDate < 31
        GROUP BY scheduleid, inv3pyid, inv3pyName, inv3pyThirdPartyNumber
    ) ARREARS ON ARREARS.scheduleid = SCH.id
    and ARREARS.inv3pyid = SCH.inv3pyid
    AND zerothirty > 0 -- Arrears - Unpaid and overdue invoices 31-60
    LEFT JOIN (
        SELECT INV.scheduleid,
            INVH.inv3pyid,
            INVP.inv3pyName,
            INVP.inv3pyThirdPartyNumber,
            SUM(unpaidAmount) AS "thirtysixty",
            SUM(unpaidVATAmount) AS "thirtysixtyVAT"
        FROM OdsInvoiceLine INV
            JOIN odsInvoiceHeader INVH on INV.invoiceHeaderId = INVH.id
            JOIN OdsInvoicingThirdParty INVP ON INVH.inv3pyId = INVP.Id
            JOIN OdsSystemDate on 1 = 1
        WHERE unpaidAmount > 0
            AND isDue = '1'
            AND isPaid = '0'
            AND systemdate - invoiceReferenceDate > 30
            AND systemdate - invoiceReferenceDate < 61
        GROUP BY scheduleid, inv3pyid, inv3pyName, inv3pyThirdPartyNumber
    ) ARR60 ON ARR60.scheduleid = SCH.id
    and ARR60.inv3pyid = SCH.inv3pyid
    AND thirtysixty > 0 -- Arrears - Unpaid and overdue invoices 61-90
    LEFT JOIN (
        SELECT INV.scheduleid,
            INVH.inv3pyid,
            INVP.inv3pyName,
            INVP.inv3pyThirdPartyNumber,
            SUM(unpaidAmount) AS "sixtyninety",
            SUM(unpaidVATAmount) AS "sixtyninetyVAT"
        FROM OdsInvoiceLine INV
            JOIN odsInvoiceHeader INVH on INV.invoiceHeaderId = INVH.id
            JOIN OdsInvoicingThirdParty INVP ON INVH.inv3pyId = INVP.Id
            JOIN OdsSystemDate on 1 = 1
        WHERE unpaidAmount > 0
            AND isDue = '1'
            AND isPaid = '0'
            AND systemdate - invoiceReferenceDate > 60
            AND systemdate - invoiceReferenceDate < 91
        GROUP BY scheduleid, inv3pyid, inv3pyName, inv3pyThirdPartyNumber
    ) ARR90 ON ARR90.scheduleid = SCH.id
    and ARR90.inv3pyid = SCH.inv3pyid
    AND sixtyninety > 0 -- Arrears - Unpaid and overdue invoices 90+
    LEFT JOIN (
        SELECT INV.scheduleid,
            INVH.inv3pyId,
            INVP.inv3pyName,
            INVP.inv3pyThirdPartyNumber,
            SUM(unpaidAmount) AS "ninetyplus",
            SUM(unpaidVATAmount) AS "ninetyplusVAT"
        FROM OdsInvoiceLine INV
            JOIN odsInvoiceHeader INVH on INV.invoiceHeaderId = INVH.id
            JOIN OdsInvoicingThirdParty INVP ON INVH.inv3pyId = INVP.Id
            JOIN OdsSystemDate on 1 = 1
        WHERE unpaidAmount > 0
            AND isDue = '1'
            AND isPaid = '0'
            AND systemdate - invoiceReferenceDate > 90
        GROUP BY scheduleid, inv3pyid, inv3pyName, inv3pyThirdPartyNumber
    ) ninetyplus ON ninetyplus.scheduleid = SCH.id
    and ninetyplus.inv3pyid = SCH.inv3pyid
    AND ninetyplus > 0
WHERE SCH.totalschedulearrears > SCH.totalcustomerarrears) BASEDATA
WHERE "Arrears_Total" > 0
AND "PCS" <> 1