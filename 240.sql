--Terminated Assets
SELECT
	SCHE.scheduleRef               		AS "Contract Number",
	AGRE.agreementType         		AS "Agreement Type",
	ASST.assetIdentifier            		AS "Asset Number", 
	SCHE.terminationDate                	AS "Termination Date",
        CASE WHEN TERM.terminationReason = 'Recoveries (Hostile Termination)' THEN 'Yes' ELSE 'No' END AS "Is Hostile",
	CASE WHEN TRMA.assetSaleStatus = 'Sold' THEN 'Yes' ELSE 'No' END	AS "Asset Disposed",
	TERM.totalAmountDue       		AS "Settlement Amount",
	TRMA.salesProceedsAmount   	AS "Sales Proceeds",
	TERM.settlementFeeAmount    	AS "Settlement Fee",
	TERM.netBookValueTerminated	AS "Net Book Value",
	SCHE.totalResidualValue   		AS "Residual Value" 
FROM OdsScheduleMain SCHE
	JOIN OdsAgreement AGRE
		ON  SCHE.agreementId = AGRE.id
		AND SCHE.terminationNumber = 1
	JOIN OdsTerminationQuote TERM
		ON 	TERM.scheduleId = SCHE.id
		AND TERM.terminationType = 'Full'
	JOIN OdsTerminationQuoteAsset TRMA
		ON TRMA.terminationQuoteId = TERM.id
	JOIN OdsAsset ASST
		ON TRMA.assetId = ASST.id
ORDER BY SCHE.terminationDate,SCHE.scheduleRef DESC;