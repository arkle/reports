-- SFAGRREL:        Salesforce Integration - Agreement Relations Information v1.0
-- Title:           Contract Relations Update    
-- Summary:         **INTEGRATION USE ONLY - DO NOT RUN MANUALLY** SALESFORCE CONTRACT INTEGRATION
--                  Provides data points required for upsert of Agreement Relations object in Salesforce
SELECT AGR.agreementnumber AS "CONTRACT__C",
    RELATED.actor_code AS "INVOLVED_PARTY__C",
    '0123z000000KtpzAAC' AS "RECORDTYPEID",
    RELATED.role AS "ROLE__C",
    CONCAT(
        RELATED.actor_code,
        AGR.agreementnumber,
        RELATED.type
    ) AS "EXTERNAL_ID"
FROM OdsAgreement AGR
    JOIN OdsSchedule SCHED ON SCHED.agreementid = AGR.id
    JOIN (
        SELECT 'Su' AS "type",
            AGR.id AS "agrid",
            'Supplier' AS "role",
            SUPBIL.supplrBillingUniqueRef AS "actor_code"
        FROM OdsAgreement AGR
            JOIN OdsAsset ASS ON ASS.agreementId = AGR.id
            JOIN OdsSupplierBilling SUPBIL ON SUPBIL.id = ASS.supplrBillingId
            AND SUPBIL.id > 1
        UNION
        SELECT 'Gu' AS "type",
            AGR.id AS "agrid",
            'Guarantor' AS "role",
            GUABILL.guarBillingUniqueRef AS "actor_code"
        FROM OdsAgreement AGR
            JOIN OdsSecurity SEC ON SEC.agreementId = AGR.id
            JOIN OdsGuarantorBilling GUABILL ON GUABILL.id = SEC.guarBillingId
            AND GUABILL.id > 1
    ) RELATED ON RELATED.agrid = AGR.id
    JOIN OdsSystemDate ON 1 = 1
WHERE AGR.id > 2
    AND SCHED.scheduleStatus != 'Proposal'
GROUP BY AGR.agreementnumber,
    RELATED.actor_code,
    RELATED.role,
    RELATED.type