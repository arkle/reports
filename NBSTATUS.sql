-- NBSTATUS:    Open New Business Cases in a chosen status
-- Summary:     Provides a list of deals in the status selected via the parameter
-- Parameter:   status (SQL defined - SELECT currentstatusstatusname FROM OdsCurrentCaseStatus WHERE currentstatusdefaultqueuecode NOT LIKE 'UW%' AND currentstatustypecode = 'ORG' ORDER BY currentstatusstatusname)
SELECT CAS.openedDate AS "Proposal_Received",
    CREDIT.decisionDate AS "UW_Decision_date",
    CREDIT.decisionStatus as "UW_Decision",
    CREDIT.approvalExpiryDate AS "Approval_Expiry_Date",
    HISTORY.startDate AS "In_status_as_of",
    AGRCO.agrcompanyname AS "Agreement Company",
    AGR.agreementnumber AS "Agreement",
    SCHED.scheduleDescription AS "Schedule_description",
    CASE
        WHEN AGR.isCcaRegulated = '1' THEN 'Y'
        ELSE ''
    END AS "Regulated",
    ASSET.assetTypeName AS "Asset_type",
    ASSET.assetDescription AS "Asset_description",
    AGR.product AS "Product",
    DLR.dealerName AS "Broker",
    INVCUS.invcusname AS "Customer",
    SCHED.totalassetcostfinanced AS "Amount_financed",
    HISTORY.actionedByUserName AS "Moved_to_status_by"
FROM OdsCase CAS
    JOIN OdsAgreement AGR ON AGR.id = CAS.agreementid
    JOIN OdsSchedule SCHED ON SCHED.agreementid = AGR.id
    JOIN OdsAgreementCompany AGRCO ON AGRCO.id = AGR.agrcompanyid
    JOIN OdsDealer DLR ON DLR.id = AGR.dealerId
    JOIN OdsInvoicingCustomer INVCUS ON INVCUS.id = AGR.invcusid
    JOIN OdsCurrentCaseStatus CASSTAT ON CASSTAT.id = CAS.currentStatusId
    AND CASSTAT.currentStatusTypeCode = 'ORG'
    AND CASSTAT.currentStatusStatusName = :status
    JOIN OdsAgreementCreditDecision CREDIT ON CREDIT.agreementId = AGR.id
    LEFT JOIN (
        SELECT HIST.caseid,
            HIST.startDate,
            HIST.startTime,
            ACTIONED.actionedByUserName
        FROM OdsCaseHistory HIST
            JOIN OdsActionedByUser ACTIONED ON ACTIONED.id = HIST.actionedByUserId
            JOIN (
                SELECT H.caseid,
                    MIN(H.caseHistoryIdentifier) AS "seq"
                FROM OdsCaseHistory H
                    JOIN OdsEndingCaseStatus ENDST ON ENDST.id = H.startStatusId
                    AND ENDST.endStatusTypeCode = 'ORG'
                    AND ENDST.endStatusStatusName = :status
                GROUP BY H.caseid
            ) LATEST ON LATEST.caseid = HIST.caseid
            AND LATEST.seq = HIST.caseHistoryIdentifier
    ) HISTORY ON HISTORY.caseid = CAS.id
    LEFT JOIN OdsAsset ASSET ON ASSET.agreementid = AGR.id
    AND ASSET.isMainAsset = True
WHERE CAS.status = 'Open'