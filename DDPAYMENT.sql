-- DDPAYMENT:  	DD Payment Report v1.0
-- Title:       DD Payments
-- Summary:     Identify direct debit payments from receivables
-- Parameter:   Invoiced_only (Boolean) - used to signal that only invoiced receivables should be included
SELECT RECV.dueDate AS "Due_Date",
    AGR.agreementnumber AS "Agreement",
    BIL.inv3pyBillingUniqueRef AS "Third_Party_Ref",
    BIL.inv3pyBillingName AS "Third_Party_Name",
    RECV.amount AS "Amount",
    CASE
        WHEN VAT.recvVatTypeCode = 1 THEN ROUND(RECV.Amount * 1.2,2)
        WHEN VAT.recvVatTypeCode = 4 THEN ROUND(RECV.Amount * 1.05,2)
        ELSE RECV.Amount
    END AS "Amount_inc_VAT",
    CASE
        WHEN RECV.isDue = true THEN 'Y'
        ELSE ''
    END AS "Due?",
    CASE
        WHEN RECV.isinvoiced = true THEN 'Y'
        ELSE ''
    END AS "Invoiced?",
    RECV.targetInvoiceProductionDate AS "Target_Invoice_Production",
    VAT.recvVatTypeCode AS "VAT_Type_Code",
    VAT.recvVatTypeName AS "VAT_Type_Name",
    TYP.advanceOrArrears AS "Advance_or_Arrears"
FROM OdsReceivable RECV
    JOIN OdsReceivablePaymentType PMTTYPE ON PMTTYPE.id = RECV.recvPaymentTypeId
    JOIN OdsSchedule SCHED ON SCHED.id = RECV.scheduleId
    JOIN OdsAgreement AGR ON AGR.id = SCHED.agreementid
    JOIN OdsInvoicingThirdPartyBilling BIL ON BIL.id = RECV.inv3pyBillingId
    JOIN OdsReceivableVatType VAT ON VAT.id = RECV.recvVatTypeId
    JOIN OdsAdvanceOrArrearsType TYP ON TYP.id = RECV.advanceOrArrearsTypeId
WHERE PMTTYPE.recvPaymentTypeName = 'Direct Debit'
    AND RECV.isinvoiced IN (
        true,
        CASE
            WHEN :Invoiced_only = TRUE THEN true
            ELSE false
        END
    )
ORDER BY RECV.dueDate,
    AGR.agreementnumber