-- PROP:    Proposals report 
-- Title:   Proposal sheet
-- Summary: Overview of any proposal loaded within a date range (includes detail of the asset with the highest drawdown value)
-- Looks for the first instance of a case being opened for the agreement to identify the "Created" date
-- Only includes the details for the Asset of highest value (uses isMainAsset flag on OdsAsset)
-- Sector and Post Code information is based on the Exposure Customer
SELECT AGRCO.agrcompanyname AS "Agreement Company",
    AGR.agreementNumber AS "Agreement",
    CASE
        WHEN SCHED.schedulestatus = 'Proposal'
        AND NTU.agreementid IS Not Null THEN 'Proposal (NTU Holding)'
        WHEN SCHED.schedulestatus = 'Proposal'
        AND AGRALERT.ntu = '1' THEN 'Proposal (NTU)'
        WHEN SCHED.schedulestatus = 'Proposal'
        AND CASESTAT.status = 'Closed'
        AND CREDIT.decisionStatus = 'Approved'
        AND CREDIT.approvalExpiryDate < systemdate THEN 'Proposal (NTU)'
        WHEN CASESTAT.status = 'Closed'
        AND CREDIT.decisionstatus = 'Approved' THEN 'Proposal (Approved)'
        WHEN CASESTAT.status = 'Closed'
        AND CREDIT.decisionStatus IN ('Not Approved', 'Rejected') THEN 'Proposal (Declined)'
        WHEN SCHED.schedulestatus = 'Proposal'
        AND ORG.agreementid IS NOT Null THEN CONCAT(
            'Proposal (',
            CASESTAT.currentStatusStatusName,
            ')'
        )
        WHEN SCHED.schedulestatus = 'Proposal'
        AND ORG.agreementid IS Null THEN 'Proposal (Cancelled)'
        WHEN AGRALERT.ppo != '1' THEN SCHED.scheduleStatus
        ELSE 'Practical DAC (Paid_Out)'
    END AS "Agreement_Status",
    CASE
        WHEN MISC.ntu_reasondesc <> '<None>' THEN MISC.ntu_reasondesc
        WHEN CASESTAT.status = 'Closed'
        AND CREDIT.decisionStatus = 'Approved'
        AND CREDIT.approvalExpiryDate < systemdate THEN 'Expired'
        ELSE ''
    END AS "NTU_Reason",
    CASESTAT.status AS "ORG_Case_Status",
    CONCAT(
        to_char(CASESTAT.openedDate, 'YYYY-MM-DD'),
        to_char(CASESTAT.openedTime, ' 99:99:99')
    ) AS "Created",
    CASESTAT.allocatedToUserName AS "Case_allocated_to",
    CONCAT(
        to_char(CASESTAT.lastActionDate, 'YYYY-MM-DD'),
        to_char(CASESTAT.lastActionTime, ' 99:99:99')
    ) AS "Last_action",
    DLR.dealerName AS "Broker",
    CASE
        WHEN DLRALERT.prb = '1' THEN 'Y'
        ELSE ''
    END AS "Priority_Broker",
    DLRBIL.dealerBillingAddressLine1 AS "Broker_Addr_1",
    CASE
        WHEN BILSALPER.salesperson > '' THEN BILSALPER.salesperson
        ELSE SALPER.salesperson
    END AS "Broker_Manager",
    EXPCUS.expCusName AS "Customer",
    EXPCUS.expCusCompanyRegNo AS "Company_Registration_Number",
    AGR.product AS "Product",
    CASE
        WHEN SCHED.originalAssetCostFinanced > 0 THEN SCHED.originalAssetCostFinanced
        ELSE SCHED.totalAssetCostFinanced
    END AS "Amount_financed",
    SCHED.totalresidualvalue AS "RV",
    SCHED.balloonAmount AS "Balloon",
    SCHED.combinedLessorPtoRate AS "Yield",
    SCHED.scheduleStatus AS "Schedule_status",
    CASE
        WHEN CREDIT.decisionstatus = 'Approved' THEN CONCAT(
            to_char(APPDEC.startDate, 'YYYY-MM-DD'),
            to_char(APPDEC.startTime, ' 99:99:99')
        )
        WHEN CREDIT.decisionstatus = 'Rejected' THEN CONCAT(
            to_char(DECDEC.startDate, 'YYYY-MM-DD'),
            to_char(DECDEC.startTime, ' 99:99:99')
        )
        WHEN FIDEC.startDate IS NOT NULL THEN CONCAT(
            to_char(FIDEC.startDate, 'YYYY-MM-DD'),
            to_char(FIDEC.startTime, ' 99:99:99')
        )
    END AS "Credit_decision_datetime",
    CASE
        WHEN AGRALERT.oop = true THEN 'Y'
        ELSE ''
    END AS "Credit - Policy Exceptions",
    CASE
        WHEN AGRALERT.oop2 = true THEN 'Y'
        ELSE ''
    END AS "Credit - Out of Policy",
    CASESTAT.actionedByUserName AS "Underwriter",
    CASE
        WHEN CREDIT.decisionstatus = 'Approved' THEN APPDEC.actionedByUserName
        WHEN CREDIT.decisionstatus = 'Rejected' THEN DECDEC.actionedByUserName
        WHEN FIDEC.startDate IS NOT NULL THEN FIDEC.actionedByUserName
    END AS "Decision_Letter_by",
    CASE
        WHEN CREDIT.decisionDate IS NOT NULL THEN CREDIT.decisionStatus
        ELSE CASESTAT.currentStatusStatusName
    END AS "Decision",
    CASE
        WHEN MISC.dec3 = true THEN 'Y'
        ELSE ''
    END AS "Dec_Bank_Stmt_DNS",
    CASE
        WHEN MISC.dec4 = true THEN 'Y'
        ELSE ''
    END AS "Dec_Adverse_Credit",
    CASE
        WHEN MISC.dec5 = true THEN 'Y'
        ELSE ''
    END AS "Dec_Suff_exp",
    CASE
        WHEN MISC.dec6 = true THEN 'Y'
        ELSE ''
    END AS "Dec_Outside_policy",
    CASE
        WHEN MISC.dec6 = true THEN 'Y'
        ELSE ''
    END AS "Dec_Insufficient_info",
    CASE
        WHEN MISC.dec1 = '<None>' THEN ''
        ELSE MISC.dec1
    END AS "Dec_Other_1",
    CASE
        WHEN MISC.dec2 = '<None>' THEN ''
        ELSE MISC.dec2
    END AS "Dec_Other_2",
    CASE
        WHEN MISC.BANK= true THEN 'Y'
        ELSE ''
    END AS "Req_3_mon_statement",
    CASE
        WHEN MISC.OPN= true THEN 'Y'
        ELSE ''
    END AS "Req_open_banking",
    CASE
        WHEN MISC.OVR= true THEN 'Y'
        ELSE ''
    END AS "Req_overdraft",
    CASE
        WHEN MISC.ACC= true THEN 'Y'
        ELSE ''
    END AS "Req_latest_filed_accounts",
    CASE
        WHEN MISC.MAN= true THEN 'Y'
        ELSE ''
    END AS "Req_latest_draft_accounts",
    CASE
        WHEN MISC.SAL= true THEN 'Y'
        ELSE ''
    END AS "Req_assets_and_liabilities",
    CASE
        WHEN MISC.EQUIP = true THEN 'Y'
        ELSE ''
    END AS "Req_equip_specs",
    CASE
        WHEN MISC.DPD = true THEN 'Y'
        ELSE ''
    END AS "Req_dir_personal_details",
    CASE
        WHEN MISC.FURI1 = '<None>' THEN ''
        ELSE MISC.FURI1
    END AS "Fur_Other_1",
    CASE
        WHEN MISC.FURI2 = '<None>' THEN ''
        ELSE MISC.FURI2
    END AS "Fur_Other_2",
    CASE
        WHEN MISC.FURI3 = '<None>' THEN ''
        ELSE MISC.FURI3
    END AS "Fur_Other_3",
    ASSET.assetTypeName AS "Asset_type",
    ASSET.assetdescription AS "Asset",
    CASE
        WHEN AGRALERT.atr = '1' THEN 'Y'
        ELSE ''
    END AS "Asset_Tag_Required",
     --  Exposure customer attributes
    EXPCUS.expCusThirdPartyTypeName AS "Customer_Type",
    EXPCUS.expCusIndustryCode AS "Industry_code",
    EXPCUS.expCusIndustryName AS "Industry_name",
    EXPCUS.expCusPostalCode AS "Customer_Post_Code",
    EXPCUS.expCusCountry AS "Customer_Country",
    CASE
        WHEN AGRALERT.rls = '1' THEN 'Y'
        ELSE ''
    END AS "RLS",
    CASE WHEN GGS.ggsflag = 'Y' THEN 'Y'
        ELSE ''
    END AS "GGS"
FROM OdsAgreement AGR
    JOIN OdsSystemDate ON 1 = 1
    JOIN OdsSchedule SCHED ON SCHED.agreementid = AGR.id
    LEFT JOIN OdsAgreementAlert AGRALERT ON AGRALERT.agreementId = AGR.id
    JOIN OdsInvoicingCustomer INVCUS ON INVCUS.id = AGR.invcusid
    JOIN OdsExposureCustomer EXPCUS ON EXPCUS.id = AGR.expCusId
    JOIN OdsDealer DLR ON DLR.id = AGR.dealerId
    JOIN OdsDealerBilling DLRBIL ON DLRBIL.id = SCHED.dealerBillingId
    LEFT JOIN OdsBillingAddressAlert DLRALERT ON DLRALERT.billingAddressId = DLRBIL.id
    JOIN OdsAgreementCompany AGRCO ON AGRCO.id = AGR.agrcompanyid
    JOIN OdsAgreementMiscellaneous MISC ON MISC.agreementid = AGR.id
    LEFT JOIN OdsAsset ASSET ON ASSET.agreementid = AGR.id
    AND ASSET.isMainAsset = True
    LEFT JOIN (
        SELECT CAS.id,
            CAS.agreementid,
            CAS.status,
            CAS.openedDate,
            CAS.openedTime,
            STAT.currentStatusStatusName,
            CAS.lastActionDate,
            CAS.lastActionTime,
            ALLOC.allocatedToUserName,
            CAS.openeddate AS "case_date",
            DECISION.actionedByUserName
        FROM OdsCase CAS
            JOIN OdsAllocatedToUser ALLOC ON ALLOC.id = CAS.allocatedToUserId
            JOIN OdsCurrentCaseStatus STAT ON STAT.id = CAS.currentStatusId
            LEFT JOIN (
                SELECT HIST.caseid,
                    ACTIONED.actionedByUserName
                FROM OdsCaseHistory HIST
                    JOIN OdsActionedByUser ACTIONED ON ACTIONED.id = HIST.actionedByUserId
                    JOIN (
                        SELECT H.caseid,
                            MAX(H.caseHistoryIdentifier) AS "seq"
                        FROM OdsCaseHistory H
                            JOIN OdsStartingCaseStatus STRT ON STRT.id = H.startStatusId
                            AND STRT.startstatustypecode = 'ORG'
                            AND STRT.startstatusstatuscode = '044'
                        GROUP BY H.caseid
                    ) LATEST ON LATEST.caseid = HIST.caseid
                    AND LATEST.seq = HIST.caseHistoryIdentifier
            ) DECISION ON DECISION.caseid = CAS.id
        WHERE CAS.id IN (
                SELECT max(OdsCase.id)
                FROM OdsCase
                    JOIN OdsCurrentCaseStatus ON OdsCurrentCaseStatus.id = OdsCase.currentStatusId
                    AND OdsCurrentCaseStatus.currentStatusTypeCode = 'ORG'
                GROUP BY agreementid
            )
    ) CASESTAT ON CASESTAT.agreementid = AGR.id
    LEFT JOIN OdsAgreementCreditDecision CREDIT ON CREDIT.agreementId = AGR.id -- Identify the Broker Manager - select just one Salesperson associated with the third party in case there are more than one
    LEFT JOIN (
        SELECT DLRSAL.dealerid,
            SAL.name AS "salesperson"
        FROM OdsSalesPerson SAL
            JOIN (
                SELECT dealerid,
                    MIN(salespersonid) AS "salesperson_id"
                FROM OdsDealerSalesperson
                GROUP BY dealerid
            ) DLRSAL ON SAL.id = DLRSAL.salesperson_id
    ) SALPER ON SALPER.dealerid = DLR.id -- Identify the Broker Manager at the Billing Address Level - select just one Salesperson associated with the third party in case there are more than one
    LEFT JOIN (
        SELECT DLRBILSAL.dealerBillingId,
            SAL.name AS "salesperson"
        FROM OdsSalesPerson SAL
            JOIN (
                SELECT dealerBillingId,
                    MIN(salespersonid) AS "salesperson_id"
                FROM OdsDealerBillingSalesperson
                GROUP BY dealerBillingId
            ) DLRBILSAL ON SAL.id = DLRBILSAL.salesperson_id
    ) BILSALPER ON BILSALPER.dealerBillingId = DLRBIL.id
    LEFT JOIN (
        SELECT CASHIST.caseid,
            CASHIST.startDate,
            CASHIST.startTime,
            ACTIONED.actionedByUserName
        FROM OdsCaseHistory CASHIST
            JOIN OdsActionedByUser ACTIONED ON ACTIONED.id = CASHIST.actionedByUserId
        WHERE CASHIST.id IN (
                SELECT min(H.id)
                FROM OdsCaseHistory H
                    JOIN OdsStartingCaseStatus STRT ON STRT.id = H.startStatusId
                WHERE STRT.startstatusstatuscode = '020'
                    AND STRT.startstatustypecode = 'ORG'
                GROUP BY H.caseid
            )
    ) APPDEC ON APPDEC.caseid = CASESTAT.id
    LEFT JOIN (
        SELECT CASHIST.caseid,
            CASHIST.startDate,
            CASHIST.startTime,
            ACTIONED.actionedByUserName
        FROM OdsCaseHistory CASHIST
            JOIN OdsActionedByUser ACTIONED ON ACTIONED.id = CASHIST.actionedByUserId
        WHERE CASHIST.id IN (
                SELECT min(H.id)
                FROM OdsCaseHistory H
                    JOIN OdsStartingCaseStatus STRT ON STRT.id = H.startStatusId
                WHERE STRT.startstatusstatuscode = '021'
                    AND STRT.startstatustypecode = 'ORG'
                GROUP BY H.caseid
            )
    ) DECDEC ON DECDEC.caseid = CASESTAT.id
    LEFT JOIN (
        SELECT CASHIST.caseid,
            CASHIST.startDate,
            CASHIST.startTime,
            ACTIONED.actionedByUserName
        FROM OdsCaseHistory CASHIST
            JOIN OdsActionedByUser ACTIONED ON ACTIONED.id = CASHIST.actionedByUserId
        WHERE CASHIST.id IN (
                SELECT min(H.id)
                FROM OdsCaseHistory H
                    JOIN OdsStartingCaseStatus STRT ON STRT.id = H.startStatusId
                WHERE STRT.startstatusstatuscode = '050'
                    AND STRT.startstatustypecode = 'ORG'
                GROUP BY H.caseid
            )
    ) FIDEC ON FIDEC.caseid = CASESTAT.id
    LEFT JOIN (
        SELECT CAS.agreementid
        FROM OdsCase CAS
            JOIN OdsCurrentCaseStatus STAT ON STAT.id = CAS.currentStatusId
        WHERE CAS.status = 'Open'
            AND STAT.currentStatusTypeCode = 'NTU'
            AND STAT.currentStatusStatusCode = '004'
        GROUP BY CAS.agreementid
    ) NTU ON NTU.agreementid = AGR.id
    LEFT JOIN (
        SELECT CAS.agreementid
        FROM OdsCase CAS
            JOIN OdsCurrentCaseStatus STAT ON STAT.id = CAS.currentStatusId
        WHERE CAS.status = 'Open'
            AND STAT.currentStatusTypeCode = 'ORG'
        GROUP BY CAS.agreementid
    ) ORG ON ORG.agreementid = AGR.id
LEFT JOIN (
        SELECT agreementId,
            'Y' AS ggsflag
        FROM odsAgreementAlert
        WHERE ggs = '1'
    ) GGS ON GGS.agreementId = AGR.id
WHERE (
        CASESTAT.openedDate >= to_date(:Created_from, 'YYYY-MM-DD')
        AND CASESTAT.openedDate <= to_date(:Created_to, 'YYYY-MM-DD')
    )
ORDER BY CASESTAT.openedDate,
    AGR.agreementNumber,
    ASSET.assetIdentifier