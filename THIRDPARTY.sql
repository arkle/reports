-- THIRDPARTY:  Third Party Inventory
-- Summary:     Details ALFA third parties. The use of the parameter is optional to limit the return to a specific external reference.
--              The parameter can include the % wildcard
-- Parameter:   External_Ref (String 20) - Optional, Duplicate_extref_check (String,1 - Values Y,N) - Optional
SELECT BILL.uniqueRef AS "Third_Party_Ref",
    BILL.name AS "Name",
    EXTREF.reference "External_Ref",
    CASE
        WHEN DUPCHECK.thirdpartyid IS NOT NULL THEN 'Y'
        ELSE ''
    END AS "Duplicate_Extref",
    CASE
        WHEN PARENT.id IS NOT NULL
        AND CHILD.relatedbillingaddressid IS NOT NULL THEN 'Parent & Child'
        WHEN PARENT.id IS NOT NULL THEN 'Child'
        WHEN CHILD.relatedbillingaddressid IS NOT NULL THEN 'Parent'
        ELSE ''
    END AS "Relationships",
    BILL.addressLine1 AS "Address_Line_1",
    BILL.addressLine2 AS "Address_Line_2",
    BILL.addressLine3 AS "Address_Line_3",
    BILL.addressLine4 AS "Address_Line_4",
    BILL.addressLine5 AS "Address_Line_5",
    BILL.postalCode   AS "PostCode",
    BILL.externalName AS "Trading_as",
    TPY.industryCode AS "SIC_Code",
    TPY.industryName AS "Industry_Name",
    TPY.companyRegNo AS "Company_Regno",
    TPY.thirdPartyTypeName AS "Type",
    TPY.caisLegalEntityType AS "CAIS_Type",
    PARENT.uniqueRef AS "Parent_Third_Party_Ref",
    EXTREFPARENT.reference "Parent_External_Ref",
    PARENT.name AS "Parent_Third_Party_Name",
    REL.relationshiptypename AS "Relationship_Type"
FROM OdsBillingAddress BILL
    JOIN OdsThirdParty TPY ON TPY.id = BILL.thirdPartyId
    LEFT JOIN OdsBillingAddRelationship REL ON REL.billingaddressid = BILL.id
    LEFT JOIN (
        SELECT relatedbillingaddressid
        FROM OdsBillingAddRelationship
        GROUP BY relatedbillingaddressid
    ) CHILD ON CHILD.relatedbillingaddressid = BILL.id
    LEFT JOIN OdsBillingAddress PARENT ON PARENT.id = REL.relatedbillingaddressid
    LEFT JOIN OdsExternalSystemReference EXTREF ON EXTREF.systemcode = 'ACR'
    AND EXTREF.thirdPartyId = BILL.thirdPartyId
    LEFT JOIN OdsExternalSystemReference EXTREFPARENT ON EXTREFPARENT.systemcode = 'ACR'
    AND EXTREFPARENT.thirdPartyId = PARENT.thirdPartyId
    LEFT JOIN (
        SELECT thirdpartyid,
            'Y' AS "duplicate"
        FROM OdsExternalSystemReference
        WHERE reference IN (
                SELECT reference
                FROM (
                        SELECT reference,
                            count(*) AS actor_code_count
                        FROM OdsExternalSystemReference
                        WHERE systemcode = 'ACR'
                        GROUP BY reference
                    ) A
                WHERE actor_code_count > 1
            )
    ) DUPCHECK ON DUPCHECK.thirdpartyid = BILL.thirdPartyId
WHERE coalesce(EXTREF.reference,'') LIKE CASE
        WHEN :External_ref > '' THEN :External_ref
        ELSE '%'
    END