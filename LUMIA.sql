-- LUMIA:   Lumia extract to send to Acquis
SELECT '3006' AS "LessorID",
    AGR.agreementnumber AS "Lease Contract",
    INV.invCusCompanyRegNo AS "Company Reg No",
    INV.invCusName AS "Lessee",
    SCHED.capitaloutstanding + SCHED.totalschedulearrears AS "Current NBV",
    to_char(SCHED.startDate, 'DD/MM/YYYY') AS "Lease Start",
    to_char(SCHED.maturityDate, 'DD/MM/YYYY') AS "Lease Expiry"
FROM OdsAgreement AGR
    JOIN OdsInvoicingCustomer INV ON INV.id = AGR.invCusId
    JOIN OdsSchedule SCHED ON SCHED.agreementid = AGR.id
WHERE (
        SCHED.capitaloutstanding + SCHED.totalschedulearrears
    ) > 0
    AND INV.invCusCaisLegalEntityType = 'Limited Company'
ORDER BY AGR.agreementnumber ASC