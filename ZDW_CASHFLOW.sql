-- ZDW_CASHFLOW: Future cashflow for all live agreements. Data is extracted from the beginning of the current month.
SELECT AGR.agreementnumber AS "AGREEMENT",
    to_char(CASHFLOW.cashfloweventdate, 'DD/MM/YYYY') AS "CASHFLOW_EVENT_DATE",
    STD.accountingStandardCode AS "ACCOUNTING_STANDARD_CODE",
    STD.accountingStandardName AS "ACCOUNTING_STANDARD_NAME",
    CASHFLOW.repayments AS "REPAYMENT",
    CASHFLOW.repayments + CASHFLOW.profit AS "CAPITAL_REPAID",
    CASHFLOW.profit * -1 AS "INTEREST",
    CASHFLOW.terminationAdjustments AS "TERMINATION_ADJUSTMENTS",
    CASHFLOW.net AS "NET",
    CASHFLOW.capitaloutstanding * -1 AS "CAP_OS"
FROM eodScheduleCashFlow CASHFLOW
    JOIN eodAccountingStandard STD ON STD.odsid = CASHFLOW.accountingStandardId
    JOIN eodSchedule SCHED ON SCHED.odsid = CASHFLOW.scheduleId
    JOIN eodAgreement AGR ON AGR.odsid = SCHED.agreementid
    JOIN eodSystemDate ON 1 = 1 -- Remove the JOIN below when completing a full load
    LEFT JOIN (
        SELECT scheduleId,
            max(rescheduleQuoteAppliedDate) AS "rescheduled"
        FROM eodRescheduleSummary
        WHERE rescheduleStatus NOT IN ('Cancelled', '<Unknown>')
        GROUP BY scheduleId
    ) RESCHED ON RESCHED.scheduleId = SCHED.odsid
WHERE CASHFLOW.cashfloweventdate >= date_trunc('month', systemdate)
    AND SCHED.scheduleStatus LIKE 'Live%'
    AND (
        RESCHED.rescheduled > systemdate - 3
        OR SCHED.activationDate > systemdate - 3
    )