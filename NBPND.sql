-- NBPND:    New Business Pending Report v1.2
-- Title:    Pending Deals
-- Summary:  Provides a view of deals in the Pending stages of the New Business workflow
SELECT AGRCO.agrcompanyname AS "Agreement_Company",
    AGR.agreementNumber AS "Agreement",
    DLR.dealerName AS "Broker",
    INVCUS.invcusname AS "Customer",
    CSTAT.currentstatusstatusname AS "Status",
    ALLOC.allocatedtousername AS "Case_Owner",
    HIS.docs_received AS "Docs_received",
    systemdate - HIS.docs_received AS "Pending_days",
    CREDIT.approvalExpiryDate AS "Approval_expiry",
    CREDIT.approvalExpiryDate - systemdate AS "Days_until_expiry",
    AGR.product AS "Product",
    SCHED.totalassetcostfinanced AS "Amount_financed",
    APPROVED.actionedByUserName AS "Underwriter",
    CASE
        WHEN MISC.repur = true THEN 'Y'
        ELSE ''
    END AS "Repurchase_Undertaking",
    CASE
        WHEN MISC.intp = true THEN 'Y'
        ELSE ''
    END AS "Init_pmt_admin_clear_funds",
    CASE
        WHEN MISC.ccgd = '<None>' THEN ''
        ELSE MISC.ccgd
    END AS "CCG_description",
    CASE
        WHEN MISC.law = true THEN 'Y'
        ELSE ''
    END AS "Landlords_Waiver",
    CASE
        WHEN MISC.assi = true THEN 'Y'
        ELSE ''
    END AS "Asset_inspection",
    CASE
        WHEN MISC.pgsd = '<None>' THEN ''
        ELSE MISC.pgsd
    END AS "PG_description",
    CASE
        WHEN MISC.supd = '<None>' THEN ''
        ELSE MISC.supd
    END AS "Supplier_description",
    CASE
        WHEN MISC.cond1 = '<None>' THEN ''
        ELSE MISC.cond1
    END AS "Add_Condition_1",
    CASE
        WHEN MISC.cond2 = '<None>' THEN ''
        ELSE MISC.cond2
    END AS "Add_Condition_2",
    CASE
        WHEN MISC.cond3 = '<None>' THEN ''
        ELSE MISC.cond3
    END AS "Add_Condition_3",
    CASE
        WHEN MISC.cond4 = '<None>' THEN ''
        ELSE MISC.cond4
    END AS "Add_Condition_4",
    CASE
        WHEN MISC.cond5 = '<None>' THEN ''
        ELSE MISC.cond5
    END AS "Add_Condition_5",
    CASE
        WHEN MISC.vatde = true THEN 'Y'
        ELSE ''
    END AS "VAT_deferral_approved"
FROM OdsCase CAS
    JOIN OdsCurrentCaseStatus CSTAT ON CSTAT.id = CAS.currentstatusid
    JOIN OdsAllocatedToUser ALLOC ON ALLOC.id = CAS.allocatedtouserid
    JOIN OdsAgreement AGR ON AGR.id = CAS.agreementid --  Get underwriting conditions
    JOIN OdsAgreementMiscellaneous MISC ON MISC.agreementid = AGR.id
    JOIN OdsInvoicingCustomer INVCUS ON INVCUS.id = AGR.invcusid
    JOIN OdsDealer DLR ON DLR.id = AGR.dealerId
    JOIN OdsAgreementCompany AGRCO ON AGRCO.id = AGR.agrcompanyid
    JOIN OdsSchedule SCHED ON SCHED.agreementid = AGR.id --  Identify the start date as when the case first moved to Documents Received status
    LEFT JOIN (
        SELECT caseid,
            MIN(endDate) AS "docs_received"
        FROM OdsCaseHistory
            JOIN OdsEndingCaseStatus ON OdsEndingCaseStatus.id = OdsCaseHistory.endStatusId
            AND (
                OdsEndingCaseStatus.endstatustypecode = 'ORG'
                AND OdsEndingCaseStatus.endstatusstatuscode = '025'
            )
        GROUP BY caseid
    ) HIS ON HIS.caseid = CAS.id --  Credit Expiry
    JOIN OdsAgreementCreditDecision CREDIT ON CREDIT.agreementId = AGR.id
    LEFT JOIN (
        SELECT caseid,
            STARTSTAT.startStatusStatusName,
            startDate,
            startTime,
            actionedByUserName
        FROM OdsCaseHistory CASHIST
            JOIN OdsStartingCaseStatus STARTSTAT ON STARTSTAT.id = CASHIST.startStatusId
            JOIN OdsActionedByUser ACTIONED ON ACTIONED.id = CASHIST.actionedByUserId
        WHERE STARTSTAT.startStatusStatusCode IN ('018', '019')
            AND STARTSTAT.startStatusTypeCode = 'ORG'
    ) APPROVED ON APPROVED.caseid = CAS.id
    JOIN OdsSystemDate ON 1 = 1
WHERE CSTAT.currentstatusstatusname IN (
        'Pending - Outstanding Conditions',
        'Pending - Outstanding Documents',
        'Pending - Verbal Check'
    ) -- Sequence by the Agreement Company and Pending from date
ORDER BY AGRCO.agrcompanyname,
    HIS.docs_received -- Change History
    -- v1.1 Change to accommodate additional status values - 'Pending - Outstanding conditions', 'Pending - Outstanding Documents', 'Pending - Verbal Check'
    -- Possible extra fields
    --  CASE WHEN AGR.isccaregulated = true THEN 'Y' ELSE '' END
    --                                          AS "CCA",
    --  SCHED.totalresidualvalue                AS "RV",
    --  Credit Review