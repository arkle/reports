-- SFACCOUNT:   Salesforce Integration - Account Information 
-- Title:       Salesforce - Account
-- Summary:     **INTEGRATION USE ONLY - DO NOT RUN MANUALLY** SALESFORCE ACCOUNT INTEGRATION
--              Provides data points required for upsert of Account object in Salesforce
-- Parameter:   thirdpartynumber (String,10), billingaddressnumber (String, 3)
SELECT 'Workflow' AS "ACCOUNT_SOURCE",
    BILL.uniqueref AS "ACTOR_CODE",
    CASE
        -- Assign a parent when the billing address number is GT 1
        WHEN BILL.billingNumber > 1 THEN CONCAT(TRIM(BILL.thirdPartyNumber), '/1') -- The following should be removed following the merger of Cassiopae and ALFA accounts
        WHEN BRKR.dealerId IS NOT NULL THEN Null
        ELSE Null
    END AS "PARENT_ACTOR_CODE",
    CASE
        WHEN VULNERABLE.thirdpartyid IS NOT NULL THEN true
        ELSE false
    END AS "TEXAS__C",
    BILL.postalCode AS "BILLINGPOSTALCODE",
    BILL.country AS "BILLINGCOUNTRY",
    CASE
        WHEN BILL.addressLine5 IS NOT NULL THEN CONCAT(
            BILL.addressLine1,
            ', ',
            BILL.addressLine2,
            ', ',
            BILL.addressLine3
        )
        WHEN BILL.addressLine4 IS NOT NULL THEN CONCAT(
            BILL.addressLine1,
            ', ',
            BILL.addressLine2
        )
        ELSE BILL.addressLine1
    END AS "BILLINGSTREET",
    LEFT(
        CASE
            WHEN BILL.addressLine5 IS NOT NULL THEN BILL.addressLine4
            WHEN BILL.addressLine4 IS NOT NULL THEN BILL.addressLine3
            ELSE BILL.addressLine2
        END,
        40
    ) AS "BILLINGCITY",
    CASE
        WHEN BILL.addressLine5 IS NOT NULL THEN BILL.addressLine5
        WHEN BILL.addressLine4 IS NOT NULL THEN BILL.addressLine4
        ELSE BILL.addressLine3
    END AS "BILLINGSTATE",
    CASE
        WHEN TPY.companyRegNo = '<None>' THEN Null
        ELSE TPY.companyRegNo
    END AS "COMPANY_NUMBER__C",
    TPY.thirdPartyTypeName AS "THIRD_PARTY_TYPE",
    false AS "IS_AUCTION_HOUSE__C",
    CASE
        WHEN BORR.invCusBillingId IS NOT NULL THEN true
        ELSE false
    END AS "IS_BORROWER__C",
    CASE
        WHEN CUST.invCusBillingId IS NOT NULL THEN true
        ELSE false
    END AS "IS_CUSTOMER__C",
    CASE
        WHEN BRKR.dealerId IS NOT NULL THEN true
        ELSE false
    END AS "IS_BROKER__C",
    false AS "IS_BUYBACK_PARTY__C",
    CASE
        WHEN GAR.guarBillingId IS NOT NULL THEN true
        ELSE false
    END AS "IS_GUARANTOR__C",
    CASE
        WHEN GAR.guarBillingId IS NOT NULL
        AND GAR.thirdPartyType = 'IND' THEN true
        ELSE false
    END AS "IS_PERSONAL_GUARANTOR__C",
    CASE
        WHEN GAR.guarBillingId IS NOT NULL
        AND GAR.thirdPartyType != 'IND' THEN true
        ELSE false
    END AS "IS_CORPORATE_GUARANTOR__C",
    false AS "IS_OWNER__C",
    false AS "IS_PARTNER__C",
    CASE
        WHEN SUBL.expCusBillingId IS NOT NULL THEN true
        ELSE false
    END AS "IS_SUBLESSEE__C",
    CASE
        WHEN SUPP.supplrBillingId IS NOT NULL THEN true
        ELSE false
    END AS "IS_SUPPLIER__C",
    'None' AS "LOYALTY_PROGRAM_C",
    false AS "LOYALTY_SCHEME_MEMBER__C",
    CASE
        WHEN BILL.billingNumber > 1 THEN CONCAT(
            BILL.name,
            ' (',
            SPLIT_PART(BILL.addressLine1, ',', 1),
            ')'
        )
        ELSE BILL.name
    END AS "NAME",
    Null AS "OWNERID",
    CASE
        -- Overrides for record types due to alignment with legacy data (Iceman migration related)
        WHEN BILL.uniqueref IN (
            'T000010287/1',
            'T000010351/1',
            'T000010299/1',
            'T000010295/1',
            'T000010291/1',
            'T000010302/1',
            'T000010311/1',
            'T000010288/1',
            'T000010315/1',
            'T000010343/1',
            'T000010314/1',
            'T000010297/1',
            'T000010347/1',
            'T000010330/1',
            'T000010327/1',
            'T000010313/1'
        ) THEN 'Account_Individual'
        WHEN BILL.uniqueref IN (
            'T000026631/1',
            'T000027723/1',
            'T000027915/1',
            'T000028276/1',
            'T000028551/1'
        ) THEN 'Account_Business'
        WHEN TPY.thirdPartyType = 'IND' AND TPY.thirdPartyNumber > 'T000037521' THEN 'Account_Business'
        WHEN TPY.thirdPartyType = 'IND' THEN 'Account_Individual'
        WHEN SUPP.supplrBillingId IS NOT NULL
        AND BRKR.dealerId IS NOT NULL THEN 'Account_Multi'
        WHEN SUPP.supplrBillingId IS NOT NULL THEN 'Account_Supplier'
        WHEN BRKR.dealerId IS NOT NULL THEN 'Account_Broker'
        ELSE 'Account_Business'
    END AS "ACCOUNT_TYPE",
    CASE
        -- Overrides for record types due to alignment with legacy data (Iceman migration related)
        WHEN BILL.uniqueref IN (
            'T000010287/1',
            'T000010351/1',
            'T000010299/1',
            'T000010295/1',
            'T000010291/1',
            'T000010302/1',
            'T000010311/1',
            'T000010288/1',
            'T000010315/1',
            'T000010343/1',
            'T000010314/1',
            'T000010297/1',
            'T000010347/1',
            'T000010330/1',
            'T000010327/1',
            'T000010313/1'
        ) THEN '0123z0000010GqwAAE'
        WHEN BILL.uniqueref IN (
            'T000026631/1',
            'T000027723/1',
            'T000027915/1',
            'T000028276/1',
            'T000028551/1'
        ) THEN '0123z0000010GqsAAE'
        WHEN TPY.thirdPartyType = 'IND' THEN '0123z0000010GqwAAE'
        WHEN TPY.thirdPartyType = 'IND' AND TPY.thirdPartyNumber > 'T000037521' THEN '0123z0000010GqwAAE'
        WHEN SUPP.supplrBillingId IS NOT NULL
        AND BRKR.dealerId IS NOT NULL THEN '0123z000000KtlTAAS'
        WHEN SUPP.supplrBillingId IS NOT NULL THEN '0123z000000KtfVAAS'
        WHEN BRKR.dealerId IS NOT NULL THEN '0123z000000RTHmAAO'
        ELSE '0123z0000010GqsAAE'
    END AS "RECORDTYPEID",
    BILL.postalCode AS "SHIPPINGPOSTALCODE",
    BILL.country AS "SHIPPINGCOUNTRY",
    CASE
        WHEN BILL.addressLine5 IS NOT NULL THEN CONCAT(
            BILL.addressLine1,
            ', ',
            BILL.addressLine2,
            ', ',
            BILL.addressLine3
        )
        WHEN BILL.addressLine4 IS NOT NULL THEN CONCAT(
            BILL.addressLine1,
            ', ',
            BILL.addressLine2
        )
        ELSE BILL.addressLine1
    END AS "SHIPPINGSTREET",
    LEFT(
        CASE
            WHEN BILL.addressLine5 IS NOT NULL THEN BILL.addressLine4
            WHEN BILL.addressLine4 IS NOT NULL THEN BILL.addressLine3
            ELSE BILL.addressLine2
        END,
        40
    ) AS "SHIPPINGCITY",
    CASE
        WHEN BILL.addressLine5 IS NOT NULL THEN BILL.addressLine5
        WHEN BILL.addressLine4 IS NOT NULL THEN BILL.addressLine4
        ELSE BILL.addressLine3
    END AS "SHIPPINGSTATE",
    CASE
        WHEN TPY.industryCode = '<None>' THEN Null
        WHEN TPY.industryCode = '' THEN Null
        ELSE TPY.industryCode
    END AS "SIC",
    CASE
        WHEN TPY.industryName = '<None>' THEN Null
        WHEN TPY.industryName = '' THEN Null
        ELSE LEFT(TPY.industryName, 80)
    END AS "SICDESC",
    BILL.externalName AS "TRADING_AS__C",
    CASE
        WHEN BILL.emailAddressEnc = '<None>' THEN Null
        ELSE BILL.emailAddressEnc
    END AS "EMAIL__C",
    CASE
        WHEN BILL.telephoneEnc = '<None>' THEN Null
        ELSE BILL.telephoneEnc
    END AS "PHONE",
    CASE
        WHEN BILL.mobileTelEnc = '<None>' THEN Null
        ELSE BILL.mobileTelEnc
    END AS "MOBILE"
FROM OdsBillingAddress BILL
    JOIN OdsThirdParty TPY ON TPY.id = BILL.thirdPartyId
    LEFT JOIN (
        SELECT guarBillingId,
            thirdPartyType
        FROM OdsSecurity
            JOIN OdsGuarantorBilling ON OdsGuarantorBilling.id = OdsSecurity.guarBillingId
            JOIN OdsThirdParty ON OdsThirdParty.id = OdsGuarantorBilling.guarBillingThirdPartyId
        LIMIT 1
    ) GAR ON GAR.guarBillingId = TPY.id
    LEFT JOIN (
        SELECT thirdpartyid
        FROM OdsThirdPartyAlert
            JOIN OdsSystemDate ON 1 = 1
        WHERE tex_code = '1'
            AND (
                tex_code_start <= systemdate
                AND (
                    tex_code_exp >= systemdate
                    OR tex_code_exp IS NULL
                )
            )
    ) VULNERABLE ON VULNERABLE.thirdpartyid = TPY.id
    LEFT JOIN (
        SELECT thirdpartyid,
            reference
        FROM OdsExternalSystemReference
        WHERE systemcode = 'ACR'
    ) EXTREF ON EXTREF.thirdpartyid = BILL.thirdPartyId
    LEFT JOIN (
        SELECT invCusBillingId
        FROM OdsAgreement
        WHERE agreementType IN ('Hire Purchase', 'Loan')
        GROUP BY invCusBillingId
    ) BORR ON BORR.invCusBillingId = BILL.id
    LEFT JOIN (
        SELECT invCusBillingId
        FROM OdsAgreement
        WHERE agreementType IN ('Finance Lease', 'Operating Lease')
        GROUP BY invCusBillingId
    ) CUST ON CUST.invCusBillingId = BILL.id
    LEFT JOIN (
        SELECT dealerId
        FROM OdsAgreement
        GROUP BY dealerId
    ) BRKR ON BRKR.dealerId = BILL.thirdPartyId
    LEFT JOIN (
        SELECT expCusBillingId
        FROM OdsAgreement
        WHERE expCusBillingId != invCusBillingId
        GROUP BY expCusBillingId
    ) SUBL ON SUBL.expCusBillingId = BILL.id
    LEFT JOIN (
        SELECT supplrBillingId
        FROM OdsAsset
        GROUP BY supplrBillingId
    ) SUPP ON SUPP.supplrBillingId = BILL.id
WHERE TRIM (BILL.thirdPartyNumber) = TRIM (:thirdpartynumber)
    AND BILL.billingNumber = to_number (:billingaddressnumber, '999')