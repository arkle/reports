-- BROKERAUTH:  Broker payments pending original docs v1.0
-- Title:       Broker Payment Hold
-- Summary:     Provides details of agreements where the broker payments are being held pending receipt of original documents
SELECT DLR.dealerName AS "Broker",
    AGRCO.agrcompanyname AS "Agreement Company",
    AGR.agreementnumber AS "Agreement",
    SCHED.activationDate AS "Activated",
    CAS.openedDate AS "Brkr_Pmt_Case_Opened",
    CASSTAT.currentStatusStatusName AS "Current_status",
    COMM.commission AS "Commission",
    DOCSRCVD.actionedByUserName AS "Moved_to_Docs_Rcvd_by",
    CASE
        WHEN DOCSRCVD.caseid IS NOT NULL THEN ROUND(
            CAST (
                (
                    SELECT count(*) AS work_interval
                    FROM generate_series (
                            TO_TIMESTAMP(
                                CONCAT(
                                    CAS.openedDate,
                                    to_char(CAS.openedTime, ' 99:99:99')
                                ),
                                'YYYY-MM-DD HH24:MI:SS'
                            ),
                            TO_TIMESTAMP(
                                CONCAT(
                                    DOCSRCVD.startDate,
                                    to_char(DOCSRCVD.startTime, ' 99:99:99')
                                ),
                                'YYYY-MM-DD HH24:MI:SS'
                            ),
                            interval '5 min'
                        ) h
                ) AS decimal
            ) / 12,
            2
        )
        ELSE 0
    END AS "Ready_to_activate_TO_Docs_Received",
    AUTH.actionedByUserName AS "Moved_to_Authorize_Payment_by",
    CASE
        WHEN AUTH.caseid IS NOT NULL THEN ROUND(
            CAST (
                (
                    SELECT count(*) AS work_interval
                    FROM generate_series (
                            TO_TIMESTAMP(
                                CONCAT(
                                    DOCSRCVD.startDate,
                                    to_char(DOCSRCVD.startTime, ' 99:99:99')
                                ),
                                'YYYY-MM-DD HH24:MI:SS'
                            ),
                            TO_TIMESTAMP(
                                CONCAT(
                                    AUTH.startDate,
                                    to_char(AUTH.startTime, ' 99:99:99')
                                ),
                                'YYYY-MM-DD HH24:MI:SS'
                            ),
                            interval '5 min'
                        ) h
                ) AS decimal
            ) / 12,
            2
        )
        ELSE 0
    END AS "Docs_Received_TO_Payment_Authorized"
FROM OdsCase CAS
    JOIN OdsAgreement AGR ON AGR.id = CAS.agreementid
    JOIN OdsSchedule SCHED ON SCHED.agreementid = AGR.id
    JOIN OdsAgreementCompany AGRCO ON AGRCO.id = AGR.agrcompanyid
    JOIN OdsDealer DLR ON DLR.id = AGR.dealerId
    JOIN OdsCurrentCaseStatus CASSTAT ON CASSTAT.id = CAS.currentStatusId
    AND CASSTAT.currentStatusTypeCode = 'BRK'
    LEFT JOIN (
        SELECT caseid,
            startDate,
            startTime,
            actionedByUserName
        FROM OdsCaseHistory CASHIST
            JOIN OdsStartingCaseStatus STARTSTAT ON STARTSTAT.id = CASHIST.startStatusId
            JOIN OdsActionedByUser ACTIONED ON ACTIONED.id = CASHIST.actionedByUserId
        WHERE STARTSTAT.startStatusStatusCode = '005'
            AND STARTSTAT.startStatusTypeCode = 'BRK'
    ) DOCSRCVD ON DOCSRCVD.caseid = CAS.id
    LEFT JOIN (
        SELECT caseid,
            startDate,
            startTime,
            actionedByUserName
        FROM OdsCaseHistory CASHIST
            JOIN OdsStartingCaseStatus STARTSTAT ON STARTSTAT.id = CASHIST.startStatusId
            JOIN OdsActionedByUser ACTIONED ON ACTIONED.id = CASHIST.actionedByUserId
        WHERE STARTSTAT.startStatusStatusCode = '011'
            AND STARTSTAT.startStatusTypeCode = 'BRK'
    ) AUTH ON AUTH.caseid = CAS.id
    LEFT JOIN (
        SELECT PAYABLE.scheduleid,
            SUM(PAYABLE.amount) AS "commission"
        FROM OdsPayable PAYABLE
            JOIN OdsPayableChargeType PCT ON PCT.id = PAYABLE.payableChargeTypeId
            AND PCT.payableChargeTypeCode = 605
        GROUP BY PAYABLE.scheduleid
    ) COMM ON COMM.scheduleid = SCHED.id
WHERE CAS.status = 'Open'
ORDER BY DLR.dealerName,
    SCHED.activationDate