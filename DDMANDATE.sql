-- DDMANDATE:  	DD Mandate Report
-- Title:       DD Mandates
-- Summary:     Identify mandates and confirms presence of IDs required for PT-X integration
-- Parameter:   Include_closed (Boolean) - used to signal that Closed Mandates should be included
--              Mandate_not_added_to_PTX (Boolean) - used to identify those that have not yet been added to PT-X
SELECT AGRCO.agrCompanyName AS "Agreement_Company",
    MANDCO.mandateCompanyName AS "Mandate_Company",
    CASE
        WHEN AGRCO.agrCompanyName = MANDCO.mandateCompanyName THEN ''
        ELSE 'Y'
    END AS "Company_Mismatch",
    MANDATE.reference AS "reference",
    CASE
        WHEN MISC.agrid = '<None>' THEN ''
        ELSE MISC.agrid
    END AS "Alternative_Agreement",
    CASE
        WHEN AGRALERT.mig = '1' THEN 'Y'
        ELSE ''
    END AS "Migrated",
    BILLADD.mandateBillingUniqueRef AS "Contact_reference",
    SCHED.activationDate AS "Agreement_activated_on",
    MANDATE.lodgedDate AS "Lodged_date",
    MANDATE.status AS "Mandate_Status",
    MANDATE.activeDate AS "Active_date",
    SCHED.scheduleStatus AS "Agreement_Status",
    MANDATE.claimReferenceType AS "Reference_type",
    BILLACCT.mndBnkAccountName AS "Account_Name",
    BILLACCT.mndBnkBankCodeEnc AS "Sort_Code",
    BILLACCT.mndBnkAccountNumberEnc "Account",
    BILLADD.mandateBillingContactName AS "Contact_Name",
    BILLADD.mandateBillingTelephoneEnc AS "Telephone",
    BILLADD.mandateBillingMobileTelEnc AS "Mobile",
    BILLADD.mandateBillingEmailAddressEnc AS "Email_Address",
    EXTREFBIL.reference AS "PTX_contactid",
    EXTREFMAN.ptxmd AS "PTX_mandateid"
FROM OdsDirectDebitMandate MANDATE
    JOIN OdsDebitMandateBillAdd BILLADD ON BILLADD.id = MANDATE.billingAddressId
    JOIN OdsMandateBillBankAccount BILLACCT ON BILLACCT.id = MANDATE.billingAddressBankAccountId
    JOIN OdsDebitMandateCompany MANDCO ON MANDCO.id = MANDATE.companyId
    JOIN OdsBillingAddressMiscellaneous BILLMISC ON BILLMISC.billingAddressId = MANDATE.billingAddressId
    JOIN OdsAgreement AGR ON AGR.agreementnumber = TRIM(SPLIT_PART(MANDATE.reference, ' ', 1))
    JOIN OdsAgreementMiscellaneous MISC ON MISC.agreementid = AGR.id
    LEFT JOIN OdsAgreementAlert AGRALERT ON AGRALERT.agreementid = AGR.id
    JOIN OdsAgreementCompany AGRCO ON AGRCO.id = AGR.agrCompanyId
    JOIN OdsSchedule SCHED ON SCHED.agreementid = AGR.id
    AND SCHED.terminationnumber = 0
    LEFT JOIN OdsExternalSystemReference EXTREFBIL ON EXTREFBIL.billingaddressid = MANDATE.billingAddressId
    AND EXTREFBIL.systemcode = 'PTXCO'
    LEFT JOIN OdsSchExternalSystemReference EXTREFMAN ON EXTREFMAN.scheduleid = SCHED.id
WHERE MANDATE.status != CASE
        WHEN :Include_closed != true THEN 'Closed'
        ELSE ''
    END
    AND EXTREFMAN.ptxmd LIKE CASE
        WHEN :Mandate_not_added_to_PTX = true THEN '<None>'
        ELSE '%'
    END
ORDER BY MANDATE.reference,
    MANDATE.lodgeddate