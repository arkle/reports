-- LOYALTY:     LOYALTY
-- Summary:     Provides a view of drawdowns for Brokers on Loyalty Schemed for the date range specified
SELECT DRAWDOWN.drawdown_date AS "Drawdown_Date",
    AGR.agreementNumber AS "Agreement",
    DLR.dealerName AS "Broker",
    CASE
        WHEN AGR.product = 'Staged Payment Loan' THEN DRAWDOWN.drawdown_amount
        WHEN AGRMISC.stpl = true THEN SCHED.totalAssetCostFinanced - coalesce(DRAWDOWNTD.drawdown_amount_to_date, 0)
        ELSE SCHED.totalAssetCostFinanced
    END AS "Drawdown_amount",
    CASE
        WHEN GGS.ggsflag = 'Y' THEN 'GGS'
        WHEN ALERT.rls = true THEN 'RLS'
        ELSE ''
    END AS "BBB"
FROM OdsSchedule SCHED
    JOIN OdsAgreement AGR ON AGR.id = SCHED.agreementid
    JOIN OdsAsset ASSET ON ASSET.agreementId = AGR.id
    AND ASSET.isMainAsset = true
    JOIN OdsInvoicingCustomer INVCUS ON INVCUS.id = AGR.invcusid
    JOIN OdsDealer DLR ON DLR.id = AGR.dealerId
    JOIN OdsActivatedByUser ACTUSER ON ACTUSER.id = SCHED.activatedbyuserid
    JOIN OdsAgreementCompany AGRCO ON AGRCO.id = AGR.agrcompanyid
    LEFT JOIN OdsAgreementAlert AGRALERT ON AGRALERT.agreementId = AGR.id
    LEFT JOIN (
        SELECT agreementid,
            stpl
        FROM OdsAgreementMiscellaneous
        WHERE stpl = true
    ) AGRMISC ON AGRMISC.agreementid = AGR.id
    LEFT JOIN (
        SELECT agreementnumber,
            MIN(startdate) AS "docsreceived"
        FROM OdsCase
            JOIN OdsAgreement ON OdsAgreement.id = OdsCase.agreementid
            JOIN OdsCaseHistory ON OdsCaseHistory.caseid = OdsCase.id
            JOIN OdsStartingCaseStatus ON OdsStartingCaseStatus.id = OdsCaseHistory.startstatusid
        WHERE OdsCase.status = 'Closed'
            AND OdsStartingCaseStatus.startstatustypecode = 'ORG'
            AND OdsStartingCaseStatus.startstatusstatuscode = '025'
        GROUP BY agreementnumber
    ) DOCSRCV ON DOCSRCV.agreementnumber = AGR.agreementnumber
    LEFT JOIN OdsAgreementAlert ALERT ON ALERT.agreementid = AGR.id
    LEFT JOIN (
        SELECT scheduleId,
            assetDrawdownDate AS "drawdown_date",
            SUM(assetDrawdownAmount) AS "drawdown_amount"
        FROM OdsAsset
        GROUP BY scheduleId,
            assetDrawdownDate
    ) DRAWDOWN ON DRAWDOWN.scheduleid = SCHED.id
    LEFT JOIN (
        SELECT scheduleId,
            SUM(assetDrawdownAmount) AS "drawdown_amount_to_date"
        FROM OdsAsset
            JOIN OdsSystemDate ON 1 = 1
        WHERE assetDrawdownDate <= to_date(:Drawdown_to, 'YYYY-MM-DD')
        GROUP BY scheduleId
    ) DRAWDOWNTD ON DRAWDOWNTD.scheduleid = SCHED.id
LEFT JOIN (
        SELECT agreementId,
            'Y' AS ggsflag
        FROM odsAgreementAlert
        WHERE ggs = '1'
    ) GGS ON GGS.agreementId = AGR.id
WHERE DRAWDOWN.drawdown_date >= to_date(:Drawdown_from, 'YYYY-MM-DD')
    AND DRAWDOWN.drawdown_date <= to_date(:Drawdown_to, 'YYYY-MM-DD')
    AND (
        ALERT.ppo = true
        OR SCHED.scheduleStatus != 'Proposal'
    )
AND DLR.dealerthirdpartynumber IN ('T000000006',
                                   'T000000004',
                                   'T000000780',
                                   'T000000029',
                                   'T000000250',
                                   'T000000783',
                                   'T000000008',
                                   'T000000718',
                                   'T000000901',
                                   'T000000025',
                                   'T000000848',
                                   'T000000155',
                                   'T000000851',
                                   'T000000320',
                                   'T000000850',
                                   'T000000035',
                                   'T000000089',
                                   'T000000032',
                                   'T000000012',
                                   'T000000094',
                                   'T000000761',
                                   'T000000964',
                                   'T000033551')
ORDER BY DRAWDOWN.drawdown_date,
    AGRCO.agrcompanyname,
    AGR.agreementNumber