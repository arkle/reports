-- ZDW_CREDIT:  Credit extract for Data Warehouse
SELECT DLR.dealerBillingUniqueRef AS "broker_uniqueref",
    AGR.agreementnumber AS "agreement_number",
    CREDIT.decisionstatus AS "credit_decision",
    CASE
        WHEN OOP.oopflag = '1' THEN 1
        ELSE 0
    END AS "out_of_policy",
    CONCAT(
        to_char(CAS.openedDate, 'DD/MM/YYYY'),
        to_char(CAS.openedTime, ' 00:00:00')
    ) AS "proposal_created",
    STAT.currentStatusStatusName AS "current_status",
    UWADMIN.actionedByUserName AS "uw_admin",
    CONCAT(
        to_char(REVIEW.startDate, 'DD/MM/YYYY'),
        to_char(REVIEW.startTime, ' 00:00:00')
    ) AS "ready_for_review_dt",
    CASE
        WHEN REVIEW.caseid IS NOT NULL THEN ROUND(
            CAST (
                (
                    SELECT count(*) AS work_interval
                    FROM generate_series (
                            TO_TIMESTAMP(
                                CONCAT(
                                    CAS.openedDate,
                                    to_char(CAS.openedTime, ' 00:00:00')
                                ),
                                'YYYY-MM-DD HH24:MI:SS'
                            ),
                            TO_TIMESTAMP(
                                CONCAT(
                                    REVIEW.startDate,
                                    to_char(REVIEW.startTime, ' 00:00:00')
                                ),
                                'YYYY-MM-DD HH24:MI:SS'
                            ),
                            interval '5 min'
                        ) h
                    WHERE EXTRACT(
                            ISODOW
                            FROM h
                        ) < 6
                        AND h.time >= '09:00'
                        AND h.time <= '16:55'
                        AND to_char(date(h), 'DD/MM/YYYY') NOT IN (
                            '2021-01-01',
                            '2021-04-02',
                            '2021-04-05',
                            '2021-05-03',
                            '2021-05-31',
                            '2021-08-30',
                            '2021-12-27',
                            '2021-12-28',
                            '2022-04-05',
                            '2022-04-18',
                            '2022-05-02',
                            '2022-06-02',
                            '2022-06-03',
                            '2022-08-29',
                            '2022-12-26',
                            '2022-12-27',
                            '2023-01-02',
                            '2023-04-07',
                            '2023-04-10',
                            '2023-05-01',
                            '2023-05-29',
                            '2023-08-28',
                            '2023-12-25',
                            '2023-12-26'
                        )
                ) AS decimal
            ) / 12,
            2
        )
        ELSE 0
    END AS "created_to_review_bh",
    CASE
        WHEN REVIEW.actionedByUserName IS NOT NULL THEN REVIEW.actionedByUserName
        ELSE NULL
    END AS "reviewed_by",
    CONCAT(
        to_char(MOREINFO.startDate, 'DD/MM/YYYY'),
        to_char(MOREINFO.startTime, ' 00:00:00')
    ) AS "more_information_dt",
    CASE
        WHEN MOREINFO.caseid IS NOT NULL THEN ROUND(
            CAST (
                (
                    SELECT count(*) AS work_interval
                    FROM generate_series (
                            TO_TIMESTAMP(
                                CONCAT(
                                    CAS.openedDate,
                                    to_char(CAS.openedTime, ' 00:00:00')
                                ),
                                'YYYY-MM-DD HH24:MI:SS'
                            ),
                            TO_TIMESTAMP(
                                CONCAT(
                                    MOREINFO.startDate,
                                    to_char(MOREINFO.startTime, ' 00:00:00')
                                ),
                                'YYYY-MM-DD HH24:MI:SS'
                            ),
                            interval '5 min'
                        ) h
                    WHERE EXTRACT(
                            ISODOW
                            FROM h
                        ) < 6
                        AND h.time >= '09:00'
                        AND h.time <= '16:55'
                        AND to_char(date(h), 'DD/MM/YYYY') NOT IN (
                            '2021-01-01',
                            '2021-04-02',
                            '2021-04-05',
                            '2021-05-03',
                            '2021-05-31',
                            '2021-08-30',
                            '2021-12-27',
                            '2021-12-28',
                            '2022-04-05',
                            '2022-04-18',
                            '2022-05-02',
                            '2022-06-02',
                            '2022-06-03',
                            '2022-08-29',
                            '2022-12-26',
                            '2022-12-27',
                            '2023-01-02',
                            '2023-04-07',
                            '2023-04-10',
                            '2023-05-01',
                            '2023-05-29',
                            '2023-08-28',
                            '2023-12-25',
                            '2023-12-26'
                        )
                ) AS decimal
            ) / 12,
            2
        )
        ELSE 0
    END AS "created_to_more_info_bh",
    MOREINFO.actionedByUserName AS "more_info_by",
    CONCAT(
        to_char(APPROVED.startDate, 'DD/MM/YYYY'),
        to_char(APPROVED.startTime, ' 00:00:00')
    ) AS "approved_dt",
    CASE
        WHEN APPROVED.caseid IS NOT NULL THEN ROUND(
            CAST (
                (
                    SELECT count(*) AS work_interval
                    FROM generate_series (
                            TO_TIMESTAMP(
                                CONCAT(
                                    CAS.openedDate,
                                    to_char(CAS.openedTime, ' 00:00:00')
                                ),
                                'YYYY-MM-DD HH24:MI:SS'
                            ),
                            TO_TIMESTAMP(
                                CONCAT(
                                    APPROVED.startDate,
                                    to_char(APPROVED.startTime, ' 00:00:00')
                                ),
                                'YYYY-MM-DD HH24:MI:SS'
                            ),
                            interval '5 min'
                        ) h
                    WHERE EXTRACT(
                            ISODOW
                            FROM h
                        ) < 6
                        AND h.time >= '09:00'
                        AND h.time <= '16:55'
                        AND to_char(date(h), 'DD/MM/YYYY') NOT IN (
                            '2021-01-01',
                            '2021-04-02',
                            '2021-04-05',
                            '2021-05-03',
                            '2021-05-31',
                            '2021-08-30',
                            '2021-12-27',
                            '2021-12-28',
                            '2022-04-05',
                            '2022-04-18',
                            '2022-05-02',
                            '2022-06-02',
                            '2022-06-03',
                            '2022-08-29',
                            '2022-12-26',
                            '2022-12-27',
                            '2023-01-02',
                            '2023-04-07',
                            '2023-04-10',
                            '2023-05-01',
                            '2023-05-29',
                            '2023-08-28',
                            '2023-12-25',
                            '2023-12-26'
                        )
                ) AS decimal
            ) / 12,
            2
        )
        ELSE 0
    END AS "created_to_approved_bh",
    APPROVED.actionedByUserName AS "approved_by",
    CONCAT(
        to_char(DECLINED.startDate, 'DD/MM/YYYY'),
        to_char(DECLINED.startTime, ' 00:00:00')
    ) AS "declined_dt",
    CASE
        WHEN DECLINED.caseid IS NOT NULL THEN ROUND(
            CAST (
                (
                    SELECT count(*) AS work_interval
                    FROM generate_series (
                            TO_TIMESTAMP(
                                CONCAT(
                                    CAS.openedDate,
                                    to_char(CAS.openedTime, ' 00:00:00')
                                ),
                                'YYYY-MM-DD HH24:MI:SS'
                            ),
                            TO_TIMESTAMP(
                                CONCAT(
                                    DECLINED.startDate,
                                    to_char(DECLINED.startTime, ' 00:00:00')
                                ),
                                'YYYY-MM-DD HH24:MI:SS'
                            ),
                            interval '5 min'
                        ) h
                    WHERE EXTRACT(
                            ISODOW
                            FROM h
                        ) < 6
                        AND h.time >= '09:00'
                        AND h.time <= '16:55'
                        AND to_char(date(h), 'DD/MM/YYYY') NOT IN (
                            '2021-01-01',
                            '2021-04-02',
                            '2021-04-05',
                            '2021-05-03',
                            '2021-05-31',
                            '2021-08-30',
                            '2021-12-27',
                            '2021-12-28',
                            '2022-04-05',
                            '2022-04-18',
                            '2022-05-02',
                            '2022-06-02',
                            '2022-06-03',
                            '2022-08-29',
                            '2022-12-26',
                            '2022-12-27',
                            '2023-01-02',
                            '2023-04-07',
                            '2023-04-10',
                            '2023-05-01',
                            '2023-05-29',
                            '2023-08-28',
                            '2023-12-25',
                            '2023-12-26'
                        )
                ) AS decimal
            ) / 12,
            2
        )
        ELSE 0
    END AS "created_to_declined_bh",
    CASE
        WHEN DECLINED.caseid IS NULL
        AND APPROVED.caseid IS NULL
        AND MOREINFO.caseid IS NULL
        AND CAS.status != 'Closed' THEN ROUND(
            CAST (
                (
                    SELECT count(*) AS work_interval
                    FROM generate_series (
                            TO_TIMESTAMP(
                                CONCAT(
                                    CAS.openedDate,
                                    to_char(CAS.openedTime, ' 00:00:00')
                                ),
                                'YYYY-MM-DD HH24:MI:SS'
                            ),
                            CURRENT_TIMESTAMP,
                            interval '5 min'
                        ) h
                    WHERE EXTRACT(
                            ISODOW
                            FROM h
                        ) < 6
                        AND h.time >= '09:00'
                        AND h.time <= '16:55'
                        AND to_char(date(h), 'DD/MM/YYYY') NOT IN (
                            '2021-01-01',
                            '2021-04-02',
                            '2021-04-05',
                            '2021-05-03',
                            '2021-05-31',
                            '2021-08-30',
                            '2021-12-27',
                            '2021-12-28',
                            '2022-04-05',
                            '2022-04-18',
                            '2022-05-02',
                            '2022-06-02',
                            '2022-06-03',
                            '2022-08-29',
                            '2022-12-26',
                            '2022-12-27',
                            '2023-01-02',
                            '2023-04-07',
                            '2023-04-10',
                            '2023-05-01',
                            '2023-05-29',
                            '2023-08-28',
                            '2023-12-25',
                            '2023-12-26'
                        )
                ) AS decimal
            ) / 12,
            2
        )
        ELSE 0
    END AS "wip_bh",
    DECLINED.actionedByUserName AS "declined_by",
    CASE
        WHEN MISC.repur = '1' THEN 1
        ELSE 0
    END AS "cr_rep_und",
    CASE
        WHEN MISC.intp = '1' THEN 1
        ELSE 0
    END AS "cr_init_pmt_clr_funds",
    CASE
        WHEN MISC.ccgd = '1' THEN 1
        ELSE 0
    END AS "cr_ccg_as_specified",
    CASE
        WHEN MISC.law = '1' THEN 1
        ELSE 0
    END AS "cr_landlords_waiver",
    CASE
        WHEN MISC.assi = '1' THEN 1
        ELSE 0
    END AS "cr_asset_inspection",
    MISC.pgsd AS "cr_PG_description",
    MISC.supd AS "cr_supplier_description",
    MISC.cond1 AS "cr_cond_1",
    MISC.cond2 AS "cr_cond_2",
    MISC.cond3 AS "cr_cond_3",
    MISC.cond4 AS "cr_cond_4",
    MISC.cond5 AS "cr_cond_5",
    CASE
        WHEN MISC.dec3 = '1' THEN 1
        ELSE 0
    END AS "dec_bank_stmt_DNS",
    CASE
        WHEN MISC.dec4 = '1' THEN 1
        ELSE 0
    END AS "dec_adverse_credit",
    CASE
        WHEN MISC.dec5 = '1' THEN 1
        ELSE 0
    END AS "dec_sufficiently_exp",
    CASE
        WHEN MISC.dec6 = '1' THEN 1
        ELSE 0
    END AS "dec_outside_policy",
    CASE
        WHEN MISC.dec7 = '1' THEN 1
        ELSE 0
    END AS "dec_insufficient_info",
    MISC.dec1 AS "dec_other_1",
    MISC.dec2 AS "dec_other_2",
    CASE
        WHEN MISC.bank = '1' THEN 1
        ELSE 0
    END AS "more_info_3_mth_bank_stmt",
    CASE
        WHEN MISC.opn = '1' THEN 1
        ELSE 0
    END AS "more_info_open_banking",
    CASE
        WHEN MISC.ovr = '1' THEN 1
        ELSE 0
    END AS "more_info_overdraft",
    CASE
        WHEN MISC.acc = '1' THEN 1
        ELSE 0
    END AS "more_info_last_filed_acc",
    CASE
        WHEN MISC.man = '1' THEN 1
        ELSE 0
    END AS "more_info_latest_mgmt_acc",
    CASE
        WHEN MISC.sal = '1' THEN 1
        ELSE 0
    END AS "more_info_SAL",
    CASE
        WHEN MISC.equip = '1' THEN 1
        ELSE 0
    END AS "more_info_equip_spec",
    CASE
        WHEN MISC.dpd = '1' THEN 1
        ELSE 0
    END AS "more_info_director_pd",
    MISC.furi1 AS "more_info_other_1",
    MISC.furi2 AS "more_info_other_2",
    MISC.furi3 AS "more_info_other_3",
    CASE
        WHEN MISC.vatde = '1' THEN 1
        ELSE 0
    END AS "vat_deferral_app"
FROM eodCase CAS
    JOIN eodAllocatedToUser ALLOC ON ALLOC.odsid = CAS.allocatedToUserId
    JOIN eodCurrentCaseStatus STAT ON STAT.odsid = CAS.currentStatusId
    JOIN eodAgreement AGR ON AGR.odsid = CAS.agreementid
    LEFT JOIN eodAgreementCreditDecision CREDIT ON CREDIT.agreementId = AGR.odsid
    JOIN eodDealerBilling DLR ON DLR.odsid = AGR.dealerBillingId
    JOIN eodSchedule SCHED ON SCHED.agreementid = AGR.odsid
    LEFT JOIN eodAgreementMiscellaneous MISC ON MISC.agreementId = AGR.odsid
    LEFT JOIN (
        SELECT CASHIST.caseid,
            CASHIST.startDate,
            CASHIST.startTime,
            ACTIONED.actionedByUserName
        FROM eodCaseHistory CASHIST
            JOIN eodActionedByUser ACTIONED ON ACTIONED.odsid = CASHIST.actionedByUserId
        WHERE CASHIST.id IN (
                SELECT min(H.id)
                FROM eodCaseHistory H
                    JOIN eodEndingCaseStatus ENDSTAT ON ENDSTAT.odsid = H.endStatusId
                WHERE ENDSTAT.endStatusStatusCode = '016'
                    AND ENDSTAT.endStatusTypeCode = 'ORG'
                GROUP BY H.caseid
            )
    ) MOREINFO ON MOREINFO.caseid = CAS.odsid
    LEFT JOIN (
        SELECT CASHIST.caseid,
            CASHIST.startDate,
            CASHIST.startTime,
            ACTIONED.actionedByUserName
        FROM eodCaseHistory CASHIST
            JOIN eodActionedByUser ACTIONED ON ACTIONED.odsid = CASHIST.actionedByUserId
        WHERE CASHIST.id IN (
                SELECT min(H.id)
                FROM eodCaseHistory H
                    JOIN eodEndingCaseStatus ENDSTAT ON ENDSTAT.odsid = H.endStatusId
                WHERE ENDSTAT.endStatusStatusCode = '020'
                    AND ENDSTAT.endStatusTypeCode = 'ORG'
                GROUP BY H.caseid
            )
    ) APPROVED ON APPROVED.caseid = CAS.odsid
    LEFT JOIN (
        SELECT CASHIST.caseid,
            CASHIST.startDate,
            CASHIST.startTime,
            ACTIONED.actionedByUserName
        FROM eodCaseHistory CASHIST
            JOIN eodActionedByUser ACTIONED ON ACTIONED.odsid = CASHIST.actionedByUserId
        WHERE CASHIST.id IN (
                SELECT min(H.id)
                FROM eodCaseHistory H
                    JOIN eodEndingCaseStatus ENDSTAT ON ENDSTAT.odsid = H.endStatusId
                WHERE ENDSTAT.endStatusStatusCode = '021'
                    AND ENDSTAT.endStatusTypeCode = 'ORG'
                GROUP BY H.caseid
            )
    ) DECLINED ON DECLINED.caseid = CAS.odsid
    LEFT JOIN (
        SELECT CASHIST.caseid,
            CASHIST.startDate,
            CASHIST.startTime,
            ACTIONED.actionedByUserName
        FROM eodCaseHistory CASHIST
            JOIN eodActionedByUser ACTIONED ON ACTIONED.odsid = CASHIST.actionedByUserId
        WHERE CASHIST.id IN (
                SELECT min(H.id)
                FROM eodCaseHistory H
                    JOIN eodStartingCaseStatus STARTSTAT ON STARTSTAT.odsid = H.startStatusId
                WHERE STARTSTAT.startStatusStatusCode IN ('008', '055')
                    AND STARTSTAT.startStatusTypeCode = 'ORG'
                GROUP BY H.caseid
            )
    ) REVIEW ON REVIEW.caseid = CAS.odsid
    LEFT JOIN (
        SELECT CASHIST.caseid,
            CASHIST.startDate,
            CASHIST.startTime,
            ACTIONED.actionedByUserName
        FROM eodCaseHistory CASHIST
            JOIN eodActionedByUser ACTIONED ON ACTIONED.odsid = CASHIST.actionedByUserId
        WHERE CASHIST.id IN (
                SELECT min(H.id)
                FROM eodCaseHistory H
                    JOIN eodStartingCaseStatus STARTSTAT ON STARTSTAT.odsid = H.startStatusId
                WHERE STARTSTAT.startStatusStatusCode IN ('001')
                    AND STARTSTAT.startStatusTypeCode = 'ORG'
                GROUP BY H.caseid
            )
    ) UWADMIN ON UWADMIN.caseid = CAS.odsid
    LEFT JOIN (
        SELECT agreementId,
            'Y' AS oopflag
        FROM odsAgreementAlert
        WHERE oop = '1'
    ) OOP ON OOP.agreementId = AGR.odsid
    JOIN eodSystemDate ON 1 = 1
WHERE CAS.odsid IN (
        SELECT MAX(id)
        FROM OdsCase
            JOIN eodSystemDate ON 1 = 1
            JOIN eodCurrentCaseStatus ON eodCurrentCaseStatus.ODSID = currentStatusId
        WHERE lastActionDate >= systemdate -2
            AND currentStatusTypeCode IN ('ORG', 'NTU', 'UWR')
        GROUP BY agreementid
    ) 
    
-- *****************************************************************************************************
-- Initial Load
WHERE CAS.odsid IN (
        SELECT MAX(id)
        FROM OdsCase CAS
            JOIN eodSystemDate ON 1 = 1
            JOIN eodCurrentCaseStatus STAT ON STAT.ODSID = currentStatusId
        WHERE (
                CAS.status != 'Closed'
                AND STAT.currentStatusTypeCode IN ('ORG', 'NTU', 'UWR')
                OR (
                    CAS.openedDate > '2021-12-31'
                    AND currentStatusTypeCode IN ('ORG', 'NTU', 'UWR')
                )
            )
        GROUP BY agreementid
    )