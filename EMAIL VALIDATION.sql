SELECT CASE WHEN 'tets@email.com;ddd' !~ '^[A-Za-z0-9._%-]+@[A-Za-z0-9.-]+[.][A-Za-z]+$' then 'a' else 'b' END

-- You don't use LIKE with regexes in PostgreSQL, you use the ~, ~*, !~, and !~* operators: