-- CCDS:    CCDS Extract v1.0
-- Title:   CCDS Extract 
-- Summary: ** INTEGRATION USE ONLY - DO NOT RUN MANUALLY ** 
--          CCDS extract per CCDS File Layout Specification version 2.0 April 2017

-- Header record
  SELECT 
                             concat(
                                           -- #H1 | 1 to 20 (+20) | Header Identifier ||   ‘HEADER’ i.e. right justified with leading blanks 
                                           LPAD('HEADER', 20, ' '), -- x20 blanks
                                           
                                           -- #H2 | 21 to 23 (+3) | Source Code Number |
                                           -- Hard coded value to be confirmed with client.
                                           '420',
                                           
                                           -- #H3 | 24 to 31 (+8) | The ALFA System Date at the time the extract is run in DDMMCCYY format.
                                           TO_CHAR((SELECT systemDateAsDate FROM odsTmpSystemDate),'DDMMYYYY'),
                                           
                                           -- #H4 | 32 to 61 (+30) | Company/Portfolio Name | The Group Name as specified on the Group table.
                                           -- Hard coded value to be confirmed with client.
                                           LPAD('Arkle Finance Ltd - ALFA', 30, ' '),
                                           
                                           -- #H5 | 62 to 81 (+20) | Filler 
                                           LPAD(' ', 20, ' '), -- x20 blanks
                                                                        
                                           -- #H6 | 82 to 89 (+8) | Will always be set to CCDSV001
                                           'CCDSV001',
                                           
                                           -- #H7 | 90 to 961 (+872) | Currently unused | Filled with blanks
                                           LPAD(' ', 872, ' ')                                  
                              ) AS headerRecord

-- Add a detail record for eached schedule that is to be included in the extract
UNION ALL
             SELECT 
                             CONCAT(
                                           -- #1 | 1 to 19 (+19) | Account number | 
                                           RPAD(odsAgreement.agreementNumber, 19, ' '),              

                                           -- #2 | 20 to 20 (+1)| Business Type Indicator
                                           CASE
                                           --  Set to 'L' (Limited Company) if the ALFA CAIS Legal Entity Type is "Limited Company"
                                           WHEN odsthirdparty.caisLegalEntityType = 'Limited Company' THEN 'L'
                                           --  Set to 'N' (Non-Limited Business)  if the ALFA CAIS Legal Entity Type is "Limited Company"
                                           WHEN odsthirdparty.caisLegalEntityType = 'Non-Limited Company' THEN 'N'
                                           --  Otherwise set to 'O' (Other)                                           
                                           ELSE 'O'
                                           END,


                                           -- #3 | 21 to 488 (+468)| Business Name and Address
                                           RPAD(COALESCE(REGEXP_REPLACE(odsBillingAddress.name,'[^a-zA-Z0-9 &-]+', '', 'g'), ' '), 100, ' '),
                                           RPAD(COALESCE(REGEXP_REPLACE(odsBillingAddress.addressLine1,'[^a-zA-Z0-9 &-]+', '', 'g'), ' '), 60, ' '),
                                           RPAD(COALESCE(REGEXP_REPLACE(odsBillingAddress.addressLine2,'[^a-zA-Z0-9 &-]+', '', 'g'), ' '), 60, ' '),
                                           RPAD(COALESCE(REGEXP_REPLACE(odsBillingAddress.addressLine3,'[^a-zA-Z0-9 &-]+', '', 'g'), ' '), 60, ' '),
                                           RPAD(COALESCE(REGEXP_REPLACE(odsBillingAddress.addressLine4,'[^a-zA-Z0-9 &-]+', '', 'g'), ' '), 60, ' '),
                                           RPAD(COALESCE(REGEXP_REPLACE(odsBillingAddress.addressLine5,'[^a-zA-Z0-9 &-]+', '', 'g'), ' '), 60, ' '),
                                           RPAD(' ', 60, ' '), 
                                           RPAD(COALESCE(REGEXP_REPLACE(odsBillingAddress.postalCode,'[^a-zA-Z0-9 &-]+', '', 'g'), ' '), 8, ' '),
                                         
                                           -- #4 | 489 to 588 (+100)| Additional Trading Style
                                           RPAD(COALESCE(REGEXP_REPLACE(odsBillingAddress.externalname,'[^a-zA-Z0-9 &-]+', '', 'g'), ' '), 100, ' '),

                                           -- #5 | 589 to 606 (+18)| Business Telephone Number
                                           CASE 
                                            WHEN odsBillingAddress.telephoneEnc = '<None>' THEN LPAD(' ', 18, ' ')
                                        --  ELSE RPAD(COALESCE(odsBillingAddress.telephoneEnc, ' '), 18, ' ')
                                            ELSE RPAD(' ', 18, ' ')
                                           END,            

                                           -- #6 | 607 to 614 (+8)| Company Registration Number
                                           CASE
                                            WHEN odsthirdparty.companyRegNo = '<None>' THEN LPAD(' ', 8, ' ')                                           
                                            ELSE RPAD(COALESCE(odsthirdparty.companyRegNo, ' '), 8, ' ')
                                            END,    

                                           -- #7 | 615 to 623 (+9)| VAT Number
                                           CASE
                                            WHEN odsBillingAddress.vatRegNumber = '<None>' THEN LPAD(' ', 9, ' ')
                                            ELSE RPAD(COALESCE(odsBillingAddress.vatRegNumber, ' '), 9, ' ')
                                           END, 

                                           -- #8 | 624 to 624 (+1) | Special Instruction Indicator | This field will always be filled with blanks.
                                           ' ',  -- blank field

                                           -- #9 | 625 to 626 (+2)| Facility Type
                                           CASE
                                           -- Set to ‘01’ (Hire Purchase) if the ALFA Product Code is ' Type is ‘HIP’ (Hire Purchase) and the ‘Interest Variation Flag’ for the product is set to ‘N’
                                           WHEN odsAgreement.agreementType = 'Hire Purchase' AND odsAgreement.isFloatingRate = FALSE THEN '01'
                                           -- Set to ‘20’ (Variable Subscription) if the ALFA Agreement Type is ‘HIP’ and the ‘Interest Variation Flag’ for the product is ‘Y’
                                           WHEN odsAgreement.agreementType = 'Hire Purchase' AND odsAgreement.isFloatingRate = TRUE THEN '20'
                                           -- Set to ‘23’ if the ALFA Agreement Type is ‘OPL’ (Operating Lease)
                                           WHEN odsAgreement.agreementType = 'Operating Lease' THEN '23'
                                           -- Set to ‘16’ (Secued Loan) if the ALFA Product Code is ‘SLON’ (Secured Loan)
                                           WHEN odsAgreement.productCode IN ('SLON','BLK','STK') THEN '16'
                                           -- Set to ‘02’ (Loan) if the ALFA Agreement Type is ‘Loan’ (Loan)
                                           WHEN odsAgreement.agreementType = 'Loan' THEN '02'                                           
                                           -- Set to ‘22’ if the ALFA Agreement Type is ‘LSE’(Finance Lease) 
                                           WHEN odsAgreement.agreementType = 'Finance Lease' THEN '22'
                                           END,

                                           -- #10 | 627 to 634 (+8)| Schedule start date DDMMCCYY format 
                                           TO_CHAR(odsSchedule.startDate, 'DDMMYYYY'),
                                           
                                           -- #11 | 635 to 642 (+8) | The closing date in DDMMCCYY format |
                                           CASE
                                             -- If the agreement has been set to ‘in default’ by means of a CAIS override record, the close date will be taken from the record;
                                                WHEN odsScheduleCaisDefault.caisDefaultNoticeDate IS NOT NULL THEN TO_CHAR(odsScheduleCaisDefault.caisDefaultNoticeDate, 'DDMMYYYY')
                                               -- If the schedule is live (i.e. has an outstanding balance or arrears) the field will hold zeros.
                                                WHEN odsSchedule.scheduleStatus = 'Live (Primary)' THEN '00000000'
                                               -- If the schedule is in secondary period (the schedule status field is ‘SEC’) then the close date will be zeros.
                                                WHEN odsSchedule.scheduleStatus = 'Live (Secondary)' THEN '00000000'
                                               -- If the deal is matured it will be the schedule maturity date.
                                                WHEN odsSchedule.scheduleStatus = 'Matured' THEN TO_CHAR(odsSchedule.maturityDate, 'DDMMYYYY')
                                               -- If the deal is terminated it will hold the schedule termination date.
                                                WHEN odsSchedule.scheduleStatus = 'Terminated' THEN TO_CHAR(odsSchedule.terminationDate, 'DDMMYYYY') 
                                               -- If the last receipt date is past the maturity or the schedule termination date, the last receipt date will be used
                                                ELSE '99999999'
                                           END,
                                              
                                           -- #12 | 643 to 654 (+12)| Current balance                                 
                                           CASE     -- If the default amount on the CAIS override is zero (or no override record exists):
                                                          WHEN odsScheduleCaisDefault.originalCaisDefaultBalance = 0 OR odsScheduleCaisDefault.caisDefaultNoticeDate IS NULL
                                                                        THEN (LPAD(
                                                                        -- Schedule arrears - schedule suspense + the balance which depends on status and presence of manual override
                                                                                      cast(ROUND(case when ARREARS."PC_Arrears" is null then 0 else ARREARS."PC_Arrears" end 
                                                                                      - odsSchedule.scheduleSuspenseTotal + ( 
                                                                                                     CASE
                                                                                                                   -- If schedule status is MAT, SEC, EXP or TRM, the balance = 0
                                                                                                                   WHEN odsSchedule.scheduleStatus IN ('Terminated', 'Matured', 'Live (Secondary)', 'Expired') THEN 0
                                                                                                                   -- Otherwise balance = total primary rentals - rentals due
                                                                                                                   ELSE odsSchedule.totalPrimaryRentals - odsSchedule.totalRentalsDue
                                                                                                     END
                                                                                                     )
                                                                                                     , 0) as text) -- Values rounded for only full pounds (GBP) as per specification
                                                                                                     , 12, '0') -- Padding                                                                                      
                                                                                           )
                                                                                           
                                                          -- Balance = Default amount - payments received after default date 
                                                          WHEN odsScheduleCaisDefault.originalCaisDefaultBalance > 0 AND odsScheduleCaisDefault.caisDefaultNoticeDate IS NOT NULL
                                                                        THEN 
                                                                        LPAD(
                                                                                      cast(ROUND(
                                                                                      odsScheduleCaisDefault.originalCaisDefaultBalance -

                                                                                                     -- Payments received after default date if there are any
                                                                                                     COALESCE(
                                                                                                                   (SELECT SUM(totalAmount) FROM odsCashAllocation odsCashAllocation
                                                                                                                                 WHERE odsCashAllocation.locationType IN ('Invoice Line', 'Schedule suspense', 'Agreement suspense')
                                                                                                                                 AND odsCashAllocation.transactionStatus = 'Applied'
                                                                                                                                  AND odsCashAllocation.scheduleId = odsSchedule.id                                                                                                                    
                                                                                                                                  AND odsCashAllocation.transactionEffectiveDate >= odsScheduleCaisDefault.caisDefaultNoticeDate
                                                                                                                                  ),
                                                                                                                  0)                                                                                                                              
                                                                                                     , 0) as text) -- Values rounded for only full pounds (GBP) as per specification
                                                                                                     , 12, '0') -- Padding                                                                        

                                                          
                                           END,                                            --

                                           -- #13 | 655 (+1)| Credit Balance Indicator | This field will always be set to blank.
                                             LPAD(' ', 1, ' '), -- x1 -- blank field

                                           -- #14 | 656 (+1)| Facility Status Code                       
                                           -- The CAIS account status code is a monthly indication of the payment performance of the account.
                                           CASE
                                                          WHEN odsScheduleCaisDefault.caisDefaultNoticeDate IS NOT NULL AND odsScheduleCaisDefault.originalCaisDefaultBalance > 0
                                                                                      THEN '8'
                                                          -- Where the arrears are negative or less than the monthly rental amount use status 0
                                                          -- In advance, up-to-date or less than one payment due but unpaid (or were due and unpaid when settled).
                                                          WHEN odsSchedule.totalCustomerArrears <= odsScheduleRentalProfile.rentalProfileAmount
                                                                                      THEN '0'                                                         
                                                          --For new deals activated in the current month use status 0
                                                          WHEN ODSSchedule.activationDate >= (select systemdateasdate - interval '31 days' from eodTmpSystemDate)
                                                               THEN '0'                                                         
                                                          -- More than one but less than two payments due but unpaid (or were due and unpaid when settled).
                                                          WHEN odsSchedule.totalCustomerArrears >= odsScheduleRentalProfile.rentalProfileAmount
                                                                        AND odsSchedule.totalCustomerArrears <= 2*odsScheduleRentalProfile.rentalProfileAmount AND odsSchedule.previousRentalDate >= (select systemdateasdate - interval '7 days' from eodTmpSystemDate)
                                                                                      THEN '0'
                                                          WHEN odsSchedule.totalCustomerArrears >= odsScheduleRentalProfile.rentalProfileAmount
                                                                        AND odsSchedule.totalCustomerArrears <= 2*odsScheduleRentalProfile.rentalProfileAmount
                                                                                      THEN '1'
                                                          WHEN odsSchedule.totalCustomerArrears >= odsScheduleRentalProfile.rentalProfileAmount
                                                                        AND odsSchedule.totalCustomerArrears <= 2*odsScheduleRentalProfile.rentalProfileAmount AND odsSchedule.previousRentalDate >= (select systemdateasdate - interval '7 days' from eodTmpSystemDate)
                                                                                      THEN '1'
                                                          -- More than two but less than three payments due but unpaid (or were due and unpaid when settled).
                                                          WHEN odsSchedule.totalCustomerArrears >= 2*odsScheduleRentalProfile.rentalProfileAmount
                                                                        AND odsSchedule.totalCustomerArrears <= 3*odsScheduleRentalProfile.rentalProfileAmount
                                                                                      THEN '2'
                                                          WHEN odsSchedule.totalCustomerArrears >= odsScheduleRentalProfile.rentalProfileAmount
                                                                        AND odsSchedule.totalCustomerArrears <= 3*odsScheduleRentalProfile.rentalProfileAmount AND odsSchedule.previousRentalDate >= (select systemdateasdate - interval '7 days' from eodTmpSystemDate)
                                                                                      THEN '2'
                                                          -- More than three but less than four payments due but unpaid (or were due and unpaid when settled).
                                                          WHEN odsSchedule.totalCustomerArrears >= 3*odsScheduleRentalProfile.rentalProfileAmount
                                                                        AND odsSchedule.totalCustomerArrears <= 4*odsScheduleRentalProfile.rentalProfileAmount
                                                                                      THEN '3'
                                                          WHEN odsSchedule.totalCustomerArrears >= odsScheduleRentalProfile.rentalProfileAmount
                                                                        AND odsSchedule.totalCustomerArrears <= 4*odsScheduleRentalProfile.rentalProfileAmount AND odsSchedule.previousRentalDate >= (select systemdateasdate - interval '7 days' from eodTmpSystemDate)
                                                                                      THEN '3'
                                                          -- More than four but less than five payments due but unpaid (or were due and unpaid when settled).
                                                          WHEN odsSchedule.totalCustomerArrears >= 4*odsScheduleRentalProfile.rentalProfileAmount
                                                                        AND odsSchedule.totalCustomerArrears <= 5*odsScheduleRentalProfile.rentalProfileAmount
                                                                                      THEN '4'
                                                          WHEN odsSchedule.totalCustomerArrears >= odsScheduleRentalProfile.rentalProfileAmount
                                                                        AND odsSchedule.totalCustomerArrears <= 5*odsScheduleRentalProfile.rentalProfileAmount AND odsSchedule.previousRentalDate >= (select systemdateasdate - interval '7 days' from eodTmpSystemDate)
                                                                                      THEN '4'
                                                          -- More than five but less than six payments due but unpaid (or were due and unpaid when settled).
                                                          WHEN odsSchedule.totalCustomerArrears >= 5*odsScheduleRentalProfile.rentalProfileAmount
                                                                        AND odsSchedule.totalCustomerArrears <= 6*odsScheduleRentalProfile.rentalProfileAmount
                                                                                      THEN '5'
                                                          WHEN odsSchedule.totalCustomerArrears >= odsScheduleRentalProfile.rentalProfileAmount
                                                                        AND odsSchedule.totalCustomerArrears <= 6*odsScheduleRentalProfile.rentalProfileAmount AND odsSchedule.previousRentalDate >= (select systemdateasdate - interval '7 days' from eodTmpSystemDate)
                                                                                      THEN '5'
                                                          -- Six or more payments due but unpaid or (or due and unpaid when settled).
                                                          WHEN odsSchedule.totalCustomerArrears >= 6*odsScheduleRentalProfile.rentalProfileAmount
                                                                                      THEN '6'                                                                                      
                                                          ELSE 'U'
                                           END,

                                            -- #15 | 657 to 668 (+12) | Original Default Balance
                                           -- If the CAIS Account Status is 8 (in default) then this will be set to the Default Amount from the CAIS Override record for this Third Party Billing Address and Schedule combination. 
                                            -- The Default Original Balance is manually entered and should be the Current Balance as at the Default Date. Only full pounds (GBP) will be entered.
                                           -- Otherwise filled with zeros
                                           CASE 
                                                         WHEN odsScheduleCaisDefault.originalCaisDefaultBalance != 0 THEN LPAD(cast(ROUND(odsScheduleCaisDefault.originalCaisDefaultBalance, 0) as text), 12, '0')
                                                          ELSE '000000000000'
                                           END,
                                           
                                           -- #16 | 669 to 676 (+8) | Default Satisfaction Date |                                             

                                           -- If the CAIS Account Status is 8 (in default) and there are no outstanding items due from the customer, 
                                            -- Alfa will set the Default Satisfaction Date to the date of the receipt that settled the account in DDMMCCYY format.
                                             CASE
                                                          WHEN odsScheduleCaisDefault.caisDefaultNoticeDate IS NOT NULL AND odsSchedule.totalCustomerArrears <= 0
                                                                        THEN coalesce(TO_CHAR(
                                                                                      (SELECT MAX(transactionEffectiveDate) FROM odsCashAllocation odsCashAllocation
                                                                                                     WHERE odsCashAllocation.scheduleId = odsSchedule.id
                                                                                                     AND odsCashAllocation.locationType IN ('Invoice Line', 'Schedule suspense', 'Agreement suspense')

                                                                                                     AND odsCashAllocation.transactionStatus = 'Applied'
                                                                                      ), 'DDMMYYYY'), LPAD('0', 8, '0'))
                                                          ELSE LPAD('0', 8, '0')
                                           END,

                                           -- #17 | 677 to 688 (+12) | Credit Limit
                                           -- This field will be set to ‘000000000000’.
                                           LPAD('0', 12, '0'), -- x12 -- 0 field

                                           -- #18 | 689 to 689 (+1) | Flag Settings
                                           CASE
                                             -- This will be set from any CAIS Override record for this Third Party Billing Address and Schedule combination.
                                             WHEN odsScheduleCaisDefault.applicableCaisFlagCode != '-' AND odsScheduleCaisDefault.applicableCaisFlagCode IN ('D', 'P', 'C', 'S', 'G', 'R', 'V', 'A', 'M', 'I', 'Q') THEN odsScheduleCaisDefault.applicableCaisFlagCode
                                             -- If a CAIS override flag other than the above, or no override record exists, then the flag will be set to blank.
                                             ELSE LPAD(' ', 1, ' ') -- x1 -- blank field
                                           END,

                                           -- #19 | 690 (+1) | Transferred to Consolidated Debt Account | This field will always be set to blank.
                                           LPAD(' ', 1, ' '), -- x1 -- blank field

                                           -- #20 | 691 to 693 (+3)| Repayment Period | NEEDS PADDING FOR LEADING 0s
                                           LPAD(cast(odsSchedule.termInMonths as text), 3, '0'),

                                           -- #21 | 694 to 705 (+12) | Payment Amount
                                           LPAD(cast(round(odsScheduleRentalProfile.rentalProfileAmount,0) as text), 12, '0'), 
                                             
                                           -- #22 | 706 (+1)| Payment Frequency Indicator | This will be set from the Rental Frequency of the main Rental Profile as follows
                                                          -- The main rental profile will be considered to be the rental profile with the highest number of rentals in the primary period. 
                                                           -- If there is a tie, the rental profile with the lowest frequency will be chosen (e.g. monthly would be chosen over quarterly).
                                           CASE                                 
                                                          -- Set to ‘M’ (Monthly) if the Rental Frequency is 1. 
                                                          WHEN odsScheduleRentalProfile.rentalFrequency = '1 month' THEN 'M'
                                                          -- Set to ‘Q’ (Quarterly) if the Rental Frequency is 3.
                                                          WHEN odsScheduleRentalProfile.rentalFrequency = '3 months' THEN 'Q'
                                                          -- Set to ‘A’ (Annually) if the Rental Frequency is 12.
                                                          WHEN odsScheduleRentalProfile.rentalFrequency = '12 months' THEN 'A'
                                                          -- Set to ‘P’ (Periodically) for all other values.
                                                          ELSE 'P'
                                           END,                                 
                                                                                                        
                                           -- #23 | 707 to 708 (+2) | Number of cash advances | This field will always be set to ‘00’ as the field is only applicable for current account products (account type 05) 
                                           LPAD('0', 2, '0'), -- x2 -- 0 field
                                             
                                           -- #24 | 709 to 720 (+12) | Value of Cash Advances | This field will always be ‘000000’ as the field is only applicable for current account products (account type 05) 
                                           LPAD('0', 12, '0'), -- x6 -- 0 field

                                           -- #25 | 721 to 732 (+12) | Minimum Balance | This field will always be set to ‘000000000000’ as the field is only applicable for current account products (account type 05) 
                                           LPAD('0', 12, '0'), -- x2 -- 0 field
                                             
                                           -- #26 | 733 (+1) | Minimum Balance Credit Indicator
                                           ' ',     

                                           -- #27 | 734 to 745 (+12) | Maximum Balance | This field will always be set to ‘000000000000’ as the field is only applicable for current account products (account type 05) 
                                           LPAD('0', 12, '0'), -- x2 -- 0 field
                                             
                                           -- #28 | 746 (+1) | Maximum Balance Credit Indicator
                                           ' ',       

                                           -- #29 | 747 to 758 (+12) | Average Balance | This field will always be set to ‘000000000000’ as the field is only applicable for current account products (account type 05) 
                                           LPAD('0', 12, '0'), -- x2 -- 0 field                                             

                                           -- #30 | 759 (+1) | Average Balance Credit Indicator
                                           ' ',       

                                           -- #31 | 760 to 771 (+12) | Credit Turnover | This field will always be set to ‘000000000000’ as the field is only applicable for current account products (account type 05) 
                                           LPAD('0', 12, '0'), -- x2 -- 0 field

                                           -- #32 | 772 (+1) | Credit Turnover Net/Gross Indicator | This field will always be set to blank as the field is only applicable for current account products (account type 05) 
                                           ' ',       

                                           -- #33 | 773 to 784 (+12) | Debit Turnover | This field will always be set to ‘000000000000’ as the field is only applicable for current account products (account type 05) 
                                           LPAD('0', 12, '0'), -- x2 -- 0 field

                                           -- #34 | 785 (+1) | Debit Turnover Net/Gross Indicator | This field will always be set to blank as the field is only applicable for current account products (account type 05) 
                                           ' ', 

                                           -- #35 | 786 to 791 (+6) | Rejected Payments | This field will always be set to ‘000000000000’ as the field is only applicable for current account products (account type 05) 
                                           LPAD('0', 6, '0'), -- x2 -- 0 field  

                                           -- #36 | 792 to 793 (+2) | Days in Excess | This field will always be set to ‘00’ as the field is only applicable for current account products (account type 05) 
                                           LPAD('0', 2, '0'), -- x2 -- 0 field   

                                           -- #37 | 794 to 893 (+100) | Filler
                                           LPAD(' ', 100, ' '), 

                                           -- #38 | 894 to 912 (+19) | Changed Facility Number
                                           LPAD(' ', 19, ' '), 

                                           -- #39 | 913 to 918 (+6) | Bank Sort Code| This field will be set to zeroes.
                                           '000000', 
                                                                                                                                                                                                                                                                                                                                                                                                         
                                           -- #40 | 919 to 926 (+8) | Bank Account Number| This field will be set to zeroes.
                                           '00000000', 

                                           -- #41 | 927 to 960 (+34) | Bank Account IBAN| This field will be set to blank.
                                           LPAD(' ', 34, ' '), 

                                           -- #42 | 961 (+1) | Current Account Reporting Level Indicator | This field will always be set to blank as the field is only applicable for current account products (account type 05) 
                                           ' ' 
                                                                     
              ) AS DataExtract
              
              -- Schedule are the primary source for records in the extract        
                            FROM odsSchedule odsSchedule          
              
              -- Joining to the Agreement                       
                             JOIN odsAgreement odsAgreement ON odsSchedule.agreementId = odsAgreement.id
                             
              -- Join to Agreement Alerts
                              JOIN odsAgreementAlert odsAgreementAlert ON odsAgreementAlert.agreementId = odsAgreement.id

              -- Joining to the Company and associated credit reference agencies
                             JOIN odsCompanyCreditRefAgency odsCompanyCreditRefAgency ON odsAgreement.agrCompanyId = odsCompanyCreditRefAgency.companyId                        
                                                                                                                                  
              -- Joining to ThirdPartyType on exposure customer
                             JOIN odsThirdParty odsThirdParty ON odsAgreement.expCusId = odsThirdParty.id
                             JOIN odsBillingAddress odsBillingAddress ON odsBillingAddress.id = odsAgreement.expCusBillingId
                                   
              -- Need to filter for main rental profile
                             JOIN odsScheduleRentalProfile odsScheduleRentalProfile ON odsScheduleRentalProfile.scheduleId = odsSchedule.id                                                                                                                                                                                                                                                                                                                                                                                          
                                                                                                                   
              -- Joining to agreement currency to filter for only GBP
                             JOIN odsAgreementCurrency odsAgreementCurrency ON odsAgreement.agrCurrencyId = odsAgreementCurrency.id          
                             
              -- Left join to odsScheduleCaisDefault for any overrides                                                                           
                             LEFT OUTER JOIN odsScheduleCaisDefault odsScheduleCaisDefault ON odsScheduleCaisDefault.scheduleId = odsSchedule.id     
                             
              -- Join to latest cash allocation transaction
                             LEFT OUTER JOIN odsCashAllocation odsCashAllocationLatest ON odsCashAllocationLatest.scheduleId = odsSchedule.id       

                             LEFT OUTER JOIN (SELECT AGR.Id, SUM(INV.unpaidAmount) AS "PC_Arrears"
                                                                                                 FROM odsInvoiceLine INV                                                                                                 
                                                                                                 LEFT OUTER JOIN odsInvoiceHeader INVH ON INV.invoiceHeaderId = INVH.Id 
                                                                                                 LEFT OUTER JOIN odsAgreement AGR ON INV.agreementId = AGR.Id
                                                                                                 WHERE INV.invoiceExtractSource = 'Regular'
                                                                                                 AND INV.isPaid = '0' 
                                                                                                 AND INV.isDue = '1'
                                                                                                 GROUP BY AGR.Id) ARREARS ON odsAgreement.Id = ARREARS.Id                                                                                                                                                                                                                                                                    

              -- Schedules set to ‘in default’ via a CAIS Override record will continue to be sent for 6 years from the default date.                                                                                                                             
                             WHERE 
                             (
                                           CASE 
                                           -- Schedules set to ‘in default’ via a CAIS Override record will continue to be sent for 6 years from the default date,
                                                          WHEN odsScheduleCaisDefault.caisDefaultNoticeDate IS NOT NULL 
                                                          -- TODO AND odsScheduleCaisDefault.caisDefaultNoticeDate > 0
                                                                        then -- Subtract 6 years and compare to the system date 
                                                                        (odsScheduleCaisDefault.caisDefaultNoticeDate - INTERVAL '6 years') <= (SELECT systemDateAsDate FROM odsTmpSystemDate)
                                                          ELSE TRUE
                                           END
                                           )                                          

                             
              -- If a schedule has no outstanding balance and zero arrears and is expired, matured, or terminated and there has been no activity for over 2 months, the schedule will be excluded. 
              -- The ‘last activity’ will be the latest payment or a termination being processed.
                           AND (
                                          CASE
                                                          -- If a schedule has no outstanding balance and zero arrears and is expired, matured, or terminated
                                                         WHEN odsSchedule.totalCustomerArrears <= 0 AND odsSchedule.capitalOutstanding <= 0 AND odsSchedule.scheduleStatus IN ('Terminated', 'Matured', 'Expired')
                                                                        THEN 
                                                                        (
                                                                                      -- and there has been no activity for over 2 months.
                                                                                      -- The ‘last activity’ will be the latest payment or a termination being processed
                                                                                      CASE 
                                                                                                     WHEN odsSchedule.terminationNumber = 1                                                                                                                                                                
                                                                                                                   THEN 
                                                                                                                   -- If terminated, get the termination date                                                                                                                 
                                                                                                                                 (SELECT terminationDate FROM odsTerminationQuote 
                                                                                                                                 WHERE terminationType = 'Full' 
                                                                                                                                  AND quoteProcessingStatus = 'Terminated'
                                                                                                                                  AND scheduleId = odsSchedule.id)
                                                                                                                                 -- Subtract 2 months and compare to the system date
                                                                                                                                  - INTERVAL '2 months' <= (SELECT systemDateAsDate FROM odsTmpSystemDate)
                                                                                                                                  
                                                                                                     -- If not terminated use the laste cash allocation transaction date
                                                                                                     ELSE  odsCashAllocationLatest.transactionEffectiveDate
                                                                                                     -- Subtract 2 months and compare to the system date 
                                                                                                                   - INTERVAL '2 months' <= (SELECT systemDateAsDate FROM odsTmpSystemDate)
                                                                                      END
                                                                        )
                                                         ELSE TRUE
                                          END
                                          )
                                          
                                                                                                     
              -- Filter for latest cash allocation transaction
                             AND odsCashAllocationLatest.id IN (select distinct on(scheduleid) id FROM odsCashAllocation order by scheduleid, transactionEffectiveDate desc) 
                             
              -- Filter for only the primary rental profiles
                             AND odsScheduleRentalProfile.id IN (SELECT distinct on(scheduleid) id FROM odsScheduleRentalProfile order by scheduleid, numberOfRentals desc)
                             
              -- Filter for only live and fully terminated schedules where schedule start date is on or before system date                                                                              
                             AND odsSchedule.terminationNumber IN (0, 1)                                         
                             AND odsSchedule.startDate <= (SELECT systemDateAsDate FROM odsTmpSystemDate)
                             
              -- Filter for only GBP
                             AND odsAgreementCurrency.agrCurrencyCode = 'GBP'

              -- Schedules will be included in the CCDS extract if the invoicing customer on the agreement has the appropriate credit agency type/legal entity type/CAIS limited status set.
                             AND odsCompanyCreditRefAgency.creditRefAgencyCode = 'CAIS UK corporate'
                             AND odsThirdParty.thirdPartyType NOT IN ('IND','JOI') 

              -- Schedules will be excluded if the agreement is undisclosed (based on exposure and invoicing customer not matching)
                             AND odsAgreement.expCusId = odsAgreement.invCusId
              
              -- Pracitcal Finance Block schedules will be excluded
                             AND odsAgreementAlert.dacb = false    
                             
UNION ALL
         SELECT CONCAT(
                             -- #T1 | 1 to 20 (+20) | Trailer Identifier | The field will always be filled with 9s.
                             '99999999999999999999',
                             
                             -- #T2 | 21 to 28 (+8) | Total Number of records | The total number of detail records extracted, i.e. the number of consumer records.
                             -- The field will be right justified with leading zeros where appropriate.
                             LPAD(cast(extractRecordCount as text), 8, '0'),
                             
                                           
                             -- #T3 | 29 to 961 (+933) | Filler | The field will always be filled with 0s.
                             LPAD(' ', 933, ' ') 
                             ) AS trailerRecord
                             -- as trailer
                             
                             FROM (
                             
              -- Select a count of the number of records that have been included in the extract                        
              SELECT COUNT(*) AS extractRecordCount
                             
              -- Scheudle are the primary source for records in the extract        
                             FROM odsSchedule odsSchedule          
              
              -- Joining to the Agreement                       
                             JOIN odsAgreement odsAgreement ON odsSchedule.agreementId = odsAgreement.id

              -- Join to Agreement Alerts
                              JOIN odsAgreementAlert odsAgreementAlert ON odsAgreementAlert.agreementId = odsAgreement.id
                             
              -- Joining to the Company and assocuated credit reference agencies
                             JOIN odsCompanyCreditRefAgency odsCompanyCreditRefAgency ON odsAgreement.agrCompanyId = odsCompanyCreditRefAgency.companyId          
                                                                                                                                  
              -- Joining to ThirdPartyType on exposure customer          
                             JOIN odsThirdParty odsThirdParty ON odsAgreement.expCusId = odsThirdParty.id
                             JOIN odsBillingAddress odsBillingAddress ON odsBillingAddress.id = odsAgreement.expCusBillingId

              -- Need to filter for main rental profile
                             JOIN odsScheduleRentalProfile odsScheduleRentalProfile ON odsScheduleRentalProfile.scheduleId = odsSchedule.id                                                                                                                                                                                                                                                                                                                                                                                          
                                                                                                                   
              -- Joining to agreement currency to filter for only GBP
                             JOIN odsAgreementCurrency odsAgreementCurrency ON odsAgreement.agrCurrencyId = odsAgreementCurrency.id                                                     
                             
              -- Left join to odsScheduleCaisDefault for any overrides                                                                           
                             LEFT OUTER JOIN odsScheduleCaisDefault odsScheduleCaisDefault ON odsScheduleCaisDefault.scheduleId = odsSchedule.id     
                             
              -- Join to latest cash allocation transaction
                             LEFT OUTER JOIN odsCashAllocation odsCashAllocationLatest ON odsCashAllocationLatest.scheduleId = odsSchedule.id
                                                          
                                                                                                                                                                                                                                                   

              -- Schedules set to ‘in default’ via a CAIS Override record will continue to be sent for 6 years from the default date.                                                                                                                             
                             WHERE 
                             (
                                           CASE 
                                           -- Schedules set to ‘in default’ via a CAIS Override record will continue to be sent for 6 years from the default date,
                                                          WHEN odsScheduleCaisDefault.caisDefaultNoticeDate IS NOT NULL 
                                                            AND odsScheduleCaisDefault.caisDefaultNoticeDate > to_timestamp(0) 
                                                                        THEN odsScheduleCaisDefault.caisDefaultNoticeDate - INTERVAL '6 years' <= (SELECT systemDateAsDate FROM odsTmpSystemDate)
                                                          ELSE TRUE
                                           END
                                           )                                          

                             
              -- If a schedule has no outstanding balance and zero arrears and is expired, matured, or terminated and there has been no activity for over 2 months, the schedule will be excluded. 
              -- The ‘last activity’ will be the latest payment or a termination being processed.
                           AND (
                                          CASE
                                                          -- If a schedule has no outstanding balance and zero arrears and is expired, matured, or terminated
                                                         WHEN odsSchedule.totalCustomerArrears <= 0 AND odsSchedule.capitalOutstanding <= 0 AND odsSchedule.scheduleStatus IN ('Terminated', 'Matured', 'Expired')
                                                                        THEN 
                                                                        (
                                                                                      -- and there has been no activity for over 2 months.
                                                                                      -- The ‘last activity’ will be the latest payment or a termination being processed
                                                                                      CASE 
                                                                                                     WHEN odsSchedule.terminationNumber = 1                                                                                                                                                                
                                                                                                                   THEN 
                                                                                                                   -- If terminated, get the termination date
                                                                                                                                 (SELECT terminationDate FROM odsTerminationQuote 
                                                                                                                                                WHERE terminationType = 'Full' 
                                                                                                                                                AND quoteProcessingStatus = 'Terminated'
                                                                                                                                                AND scheduleId = odsSchedule.id)
                                                                                                                                  -- Subtract 2 months and compare to the system date
                                                                                                                                  - INTERVAL '2 months' <= (SELECT systemDateAsDate FROM odsTmpSystemDate)
                                                                                                                                  
                                                                                                     -- If not terminated use the latest cash allocation transaction date
                                                                                                     ELSE  odsCashAllocationLatest.transactionEffectiveDate
                                                                                                     -- Subtract 2 months and compare to the system date 
                                                                                                                   - INTERVAL '2 months' <= (SELECT systemDateAsDate FROM odsTmpSystemDate)
                                                                                      END
                                                                        )
                                                         ELSE TRUE
                                          END
                                          )
              -- Filter for latest cash allocation transaction
                             AND odsCashAllocationLatest.id IN (SELECT distinct on (scheduleid) id FROM odsCashAllocation order by scheduleid, transactionEffectiveDate desc)  

              -- Filter for only the primary rental profiles
                             AND odsScheduleRentalProfile.id IN (select distinct on(scheduleid) id FROM odsScheduleRentalProfile order by scheduleid, numberOfRentals desc)
                             
              -- Filter for only live and fully terminated schedules where schedule start date is on or before system date                                                                              
                             AND odsSchedule.terminationNumber IN (0, 1)                                         
                             AND odsSchedule.startDate <= (SELECT systemDateAsDate FROM odsTmpSystemDate)
                             
              -- Filter for only GBP
                             AND odsAgreementCurrency.agrCurrencyCode = 'GBP'

              -- Schedules will be included in the CCDS extract if the invoicing customer on the agreement has the appropriate credit agency type/legal entity type/CAIS limited status set.
                             AND odsCompanyCreditRefAgency.creditRefAgencyCode = 'CAIS UK corporate'
                             AND odsThirdParty.thirdPartyType NOT IN ('IND','JOI') 

              -- Schedules will be excluded if the agreement is undisclosed (based on exposure and invoicing customer not matching)
                             AND odsAgreement.expCusId = odsAgreement.invCusId
              
              -- Pracitcal Finance Block schedules will be excluded
                             AND odsAgreementAlert.dacb = false                             

                             ) 
                             AS extractRecordCount

