-- SFARREARS:   Extract of arrears information to support the automated creation of arrears cases in Salesforce
SELECT AGR.agreementNumber,
    BILL.caseBillingUniqueRef,
    STAT.currentstatustypecode,
    CASE
        WHEN TPYALERT.tex_code = '1' THEN 'Y'
        ELSE ''
    END AS "vulnerableCustomer"
FROM OdsCase CAS
    JOIN OdsCurrentCaseStatus STAT ON STAT.id = CAS.currentStatusId
    JOIN OdsCaseBilling BILL ON BILL.id = CAS.caseBillingId
    JOIN OdsAgreement AGR ON AGR.id = CAS.agreementid
    JOIN OdsThirdParty TPY ON TPY.id = AGR.invCusId
    LEFT JOIN OdsThirdPartyAlert TPYALERT ON TPYALERT.thirdpartyid = TPY.id
WHERE CAS.status = 'Open'
    AND STAT.currentstatustypecode IN ('SAR', 'NCA', 'TRM')