-- AD2:     ALFA User Roles v1.0
-- Title:   ALFA User Roles
-- Summary: Extract of ALFA Users showing the roles they are assigned (to support audits). To acquire a role, users must be granted membership of a group that has been assigned the role.
SELECT 
    ALFAUSER.name                   AS "User name",    
    ALFAUSER.code                   AS "User code",
    DEPARTMENT.code                 AS "Dept Code",
    DEPARTMENT.description          AS "Dept_desc",
    ALFAUSER.lastLoginDate	        AS "Last login date",
    to_char(ALFAUSER.lastLoginTime, '00:00:00')	    
                                    AS "Last login time",
    ALFAUSER.enabledFrom	        AS "Enabled from",	
    ALFAUSER.disabledFrom	        AS "Disabled from",
    ALFAUSER.isAlfaManaged	        AS "ALFA Managed",
    ALFAUSER.telephoneNumber	    AS "Telephone",
    ALFAUSER.emailAddress	        AS "Email",
    USERROLE.roleName               AS "Role"
    
FROM OdsAlfaUser ALFAUSER 
    JOIN OdsDepartment DEPARTMENT 
        ON ALFAUSER.departmentId = DEPARTMENT.id
    LEFT JOIN OdsUserRole USERROLE  
        ON USERROLE.userId = ALFAUSER.id 

ORDER BY ALFAUSER.name, USERROLE.roleName