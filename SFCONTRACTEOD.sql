-- SFCONTRACTEOD:   Salesforce Integration - End of Day Contract Updates
-- Summary:         **INTEGRATION USE ONLY - DO NOT RUN MANUALLY** SALESFORCE CONTRACT INTEGRATION
--                  Provides data points required for upsert of Contract object in Salesforce
--                  Assumes will always run after ods which will start no earlier than midnight
SELECT CASE
        WHEN AGRCO.agrcompanycode IN ('2', '4') THEN 'Evington'
        ELSE 'Arkle'
    END AS "BOOK__C",
    CASE
        WHEN AGRCO.agrcompanycode = '1' THEN 'CORE'
        WHEN AGRCO.agrcompanycode = '3' THEN 'PRIME'
        ELSE 'CONSUMER'
    END AS "PRICING",
    CASE
        WHEN AGRMISC.PCOMPCode = 'CORE' THEN 'CORE'
        WHEN AGRMISC.PCOMPCode = 'PRIM' THEN 'PRIME'
        WHEN AGRMISC.PCOMPCode = 'EVIN' THEN 'CONSUMER'
        WHEN AGRMISC.PCOMPCode = '<None>' AND AGRCO.agrcompanycode = '1' THEN 'CORE'
        WHEN AGRMISC.PCOMPCode = '<None>' AND AGRCO.agrcompanycode = '3' THEN 'PRIME'
        WHEN AGRMISC.PCOMPCode = '<None>' AND AGRCO.agrcompanycode NOT IN ('1','3') THEN 'CONSUMER'
        ELSE ''
    END AS "Pricing_Company__c",
    COMM.commission AS "COMMISSION",
    LEFT(
        concat(
            AGR.agreementnumber,
            ' ',
            SCHED.scheduleDescription
        ),
        80
    ) AS "NAME",
    -- Some allowances for data alignment following the Iceman migration into ALFA
    CASE
        WHEN AGRCO.agrcompanycode = '4'
        AND EXTREF.reference IS NOT NULL
        AND LEFT(EXTREF.reference, 1) != 'E'
        AND BILL.uniqueref NOT IN ('T000010005/2') THEN EXTREF.reference
        ELSE TRIM(BILL.uniqueref)
    END AS "ACTOR_CODE",
    CASE
        WHEN AGRMISC.bro <> '<None>' THEN AGRMISC.bro
        WHEN DLRBIL.dealerBillingUniqueRef = '<None>' THEN Null
        ELSE DLRBIL.dealerBillingUniqueRef
    END AS "BROKER__C",
    CASE
        WHEN AGR.invCusId != AGR.expCusId THEN TRIM(EXPBILL.expCusBillingUniqueRef)
        ELSE NULL
    END AS "SUB_LESSEE__C",
    AGR.agreementnumber AS "CONTRACT_NUMBER__C",
    CASE
        WHEN ALERT.mig = '1' THEN Null
        ELSE AGR.agreementnumber
    END AS "OPPORTUNITY__C",
    AGR.product AS "PRODUCT_TYPE__C ",
    CASE
        WHEN SCHED.activationDate = processing_date THEN 'NEW'
 --       WHEN NOVATED.flag = true
 --       AND NOVATED.startDate = processing_date THEN 'NEW'
        WHEN ALERT.ppo = true
        AND SCHED.startDate = processing_date THEN 'NEW'
        ELSE ''
    END AS "LOADSTATUS",
    CASE
        WHEN AGR.productCode = 'STG' THEN ROUND(
            coalesce(DRAWDOWNTD.drawdown_amount_to_date, 0) - coalesce(CUSTDEP.customer_deposit, 0),
            2
        )
        WHEN AGRMISC.stpl = true
        AND SCHED.totalassetcostfinanced > coalesce(DRAWDOWNTD.drawdown_amount_to_date, 0) THEN ROUND(
            SCHED.totalassetcostfinanced - coalesce(DRAWDOWNTD.drawdown_amount_to_date, 0),
            2
        )
        WHEN AGRMISC.stpl = true THEN 0
        WHEN AGRMISC.oaf IS NOT NULL THEN AGRMISC.oaf
        ELSE SCHED.totalassetcostfinanced
    END AS "AMOUNT_ADVANCED",
    CASE
        WHEN AGR.isCcaRegulated = '1' THEN 'Regulated'
        ELSE 'Unregulated'
    END AS "REGULATION_TYPE__C",
    CASE
        WHEN AGRMISC.ors IS NOT NULL THEN AGRMISC.ors
        ELSE SCHED.startDate
    END AS "STARTDATE",
    SCHED.maturityDate AS "ENDDATE",
    ASSET.assetDescription AS "EQUIPMENT_ASSET_DESCRIPTION__C",
    ASSET.assetClassificationName AS "ASSET_CLASSIFICATION",
    CASE
        WHEN SCHED.termInMonths = 0 THEN 1
        ELSE SCHED.termInMonths
    END AS "CONTRACTTERM",
    UNPINV.unpaid_invoices AS "UNPAID_INVOICES__C",
    ROUND(UNPINV.unpaid_inv_amount, 2) AS "INVOICED_AMOUNT_UNPAID__C",
    '' AS "NOTES__C",
    ROUND(SCHED.nextRentalAmount, 2) AS "REGULAR_PAYMENT__C",
    ROUND(SCHED.firstRentalAmount, 2) AS "INITIAL_PAYMENT__C",
    extract(
        DAY
        FROM SCHED.nextRentalDate
    ) AS "PAYMENT_DATE__C",
    SCHED.numberOfFutureRentals AS "OUTSTANDING_INSTALLMENTS__C",
    SCHED.nextRentalDate AS "NEXT_RENTAL_DATE__C",
    CASE
        WHEN SCHED.nextRentalFrequency = '<None>' THEN ''
        ELSE SCHED.nextRentalFrequency
    END AS "FREQUENCY__C",
    CASE
        WHEN NEXTRENTAL.nextRentalPaymentTypeName = '<None>' THEN ''
        ELSE NEXTRENTAL.nextRentalPaymentTypeName
    END AS "PAYMENT_METHOD__C",
    SCHED.previousRentalDate AS "LAST_RENTAL_DUE_DATE__C",
    ROUND(SCHED.previousRentalAmount, 2) AS "LAST_RENTAL_DUE_AMOUNT__C",
    ROUND(SCHED.grossBalanceRemaining, 2) AS "BALANCE__C",
    ROUND(SCHED.capitalOutstanding, 2) AS "CAPITAL_OS__C",
    ROUND(coalesce(BALLOONRENTAL.balloonrental, 0), 2) AS "BALLOON__C",
    ROUND(SCHED.totalResidualValue, 2) AS "RESIDUAL_VALUE__C",
    CASE
        WHEN AGRMISC.ora IS NOT NULL THEN ROUND(AGRMISC.ora, 2)
        ELSE ROUND(SCHED.annualPercentageRate, 2)
    END AS "APR__C",
    CASE
        WHEN AGRMISC.ory IS NOT NULL THEN ROUND(AGRMISC.ory, 2)
        ELSE ROUND(SCHED.combinedLessorPtoRate, 2)
    END AS "YIELD__C",
    ROUND(SCHED.totalChargesDue, 2) AS "TOTAL_CHARGES__C",
    SCHED.informallyExtends AS "AUTOMATIC_EXTENSION__C",
    LASTRCT.transactionEffectiveDate AS "LAST_RECEIPT_DATE__C",
    ROUND(LASTRCT.totalAmount, 2) AS "LAST_RECEIPT_AMOUNT__C",
    CASE
        WHEN SCHED.totalScheduleArrears > 0 THEN SCHED.inArrearsSinceDate
        ELSE NULL
    END AS "LAST_ARREARS_DATE__C",
    CASE
        WHEN SCHED.totalScheduleArrears > 0 THEN systemdate - SCHED.inArrearsSinceDate
        ELSE 0
    END AS "DAYS_IN_ARREARS__C",
    0 AS "ARREARS_REPAYMENTS__C",
    CASE
        WHEN coalesce(SCHED.totalScheduleArrears, 0) > 0
        AND coalesce(SCHED.nextRentalAmount, 0) > 0 THEN ROUND(
            SCHED.totalScheduleArrears / SCHED.nextRentalAmount,
            0
        )
        ELSE 0
    END AS "NUMBER_OF_INSTALMENTS_IN_ARREARS__C",
    SCHED.totalScheduleArrears AS "TOTAL_ARREARS__C",
    ROUND(coalesce(zerothirty, 0), 2) AS "X30__C",
    ROUND(coalesce(thirtysixty, 0), 2) AS "X060__C",
    ROUND(coalesce(sixtyninety, 0), 2) AS "X090__C",
    ROUND(coalesce(ninetyplus, 0), 2) AS "X0__C",
    ROUND(coalesce(ARRFEE.arrears_fee, 0), 2) AS "ARREARS_FEES__C",
    ROUND(coalesce(PAYMENTS.payments_received, 0), 2) AS "PAYMENTS__C",
    ROUND(INVOICED.invoiced_amount, 2) AS "INVOICED_AMOUNT__C",
    CASE
        WHEN ALERT.ppo = true THEN 'Practical Paid-Out'
        ELSE SCHED.scheduleStatus
    END AS "STATUS",
    CASE
        WHEN PARTIAL.agreementid IS NOT Null THEN 'Partially Terminated'
        ELSE Null
    END AS "SUB_STATUS__C",
    '0123z000000KtmvAAC' AS "RECORDTYPEID"
FROM odsAgreement AGR
    JOIN odsAgreementAlert ALERT ON ALERT.agreementid = AGR.id
    JOIN odsSystemDate ON 1 = 1
    JOIN odsBillingAddress BILL ON BILL.id = AGR.invCusBillingId
    JOIN odsExposureCustomerBilling EXPBILL on EXPBILL.id = AGR.expCusBillingId
    JOIN odsSchedule SCHED ON SCHED.agreementid = AGR.id
    JOIN odsScheduleMain SCHEDM ON SCHEDM.id = SCHED.id
    LEFT JOIN (
        SELECT agreementid
        FROM odsSchedule
        WHERE terminationNumber > 1
        GROUP BY agreementid
    ) PARTIAL ON PARTIAL.agreementid = AGR.id
    JOIN odsAgreementCompany AGRCO ON AGRCO.id = SCHED.agrCompanyId
    JOIN odsDealer DLR ON DLR.id = SCHED.dealerId
    JOIN odsDealerBilling DLRBIL ON DLRBIL.id = SCHED.dealerBillingId
    LEFT JOIN odsNextRentalPaymentType NEXTRENTAL ON NEXTRENTAL.id = SCHED.nextRentalPaymentTypeId
    LEFT JOIN (
        SELECT RECEIVABLE.scheduleid,
            SUM(RECEIVABLE.amount) AS "arrears_fee"
        FROM odsReceivable RECEIVABLE
            JOIN odsReceivableChargeType RCT ON RCT.id = RECEIVABLE.recvchargetypeid
            AND RCT.recvchargetypecode = 213
        GROUP BY RECEIVABLE.scheduleid
    ) ARRFEE ON ARRFEE.scheduleid = SCHED.id
    LEFT JOIN (
        SELECT RECEIVABLE.scheduleid,
            SUM(RECEIVABLE.amount) AS "invoiced_amount"
        FROM odsReceivable RECEIVABLE
        WHERE RECEIVABLE.isInvoiced = true
        GROUP BY RECEIVABLE.scheduleid
    ) INVOICED ON INVOICED.scheduleid = SCHED.id
    LEFT JOIN (
        SELECT RECEIVABLE.scheduleid,
            SUM(RECEIVABLE.amount) AS "payments_received"
        FROM odsReceivable RECEIVABLE
        WHERE RECEIVABLE.isInvoiced = true
            AND RECEIVABLE.isDue = false
        GROUP BY RECEIVABLE.scheduleid
    ) PAYMENTS ON PAYMENTS.scheduleid = SCHED.id
    LEFT JOIN (
        SELECT scheduleid,
            transactionEffectiveDate,
            totalAmount
        FROM odsCashAllocation
        WHERE id IN (
                SELECT MAX(id)
                FROM odsCashAllocation CA
                GROUP BY CA.scheduleid
            )
    ) LASTRCT ON LASTRCT.scheduleid = SCHED.id
    LEFT JOIN (
        SELECT scheduleid,
            assetTypeName,
            assetTypeCode,
            assetClassificationName,
            assetDescription
        FROM odsAsset
        WHERE isMainAsset = '1'
    ) ASSET ON ASSET.scheduleid = SCHED.id
    LEFT JOIN (
        SELECT scheduleid,
            rentalProfileAmount AS balloonrental
        FROM odsScheduleRentalProfile
        WHERE rentalSubType = 'Balloon'
    ) BALLOONRENTAL ON BALLOONRENTAL.scheduleid = SCHED.id
    LEFT JOIN (
        SELECT INV.scheduleid,
            SUM(unpaidAmount) AS "zerothirty"
        FROM odsInvoiceLine INV
            JOIN odsSystemDate on 1 = 1
        WHERE unpaidAmount > 0
            AND isDue = '1'
            AND isPaid = '0'
            AND systemdate - invoiceReferenceDate >= 0
            AND systemdate - invoiceReferenceDate < 31
        GROUP BY scheduleid
    ) ARR30 ON ARR30.scheduleid = SCHED.id
    AND zerothirty > 0
    LEFT JOIN (
        SELECT INV.scheduleid,
            SUM(unpaidAmount) AS "thirtysixty"
        FROM odsInvoiceLine INV
            JOIN odsSystemDate on 1 = 1
        WHERE unpaidAmount > 0
            AND isDue = '1'
            AND isPaid = '0'
            AND systemdate - invoiceReferenceDate > 30
            AND systemdate - invoiceReferenceDate < 61
        GROUP BY scheduleid
    ) ARR60 ON ARR60.scheduleid = SCHED.id
    AND thirtysixty > 0
    LEFT JOIN (
        SELECT INV.scheduleid,
            SUM(unpaidAmount) AS "sixtyninety"
        FROM odsInvoiceLine INV
            JOIN odsSystemDate on 1 = 1
        WHERE unpaidAmount > 0
            AND isDue = '1'
            AND isPaid = '0'
            AND systemdate - invoiceReferenceDate > 60
            AND systemdate - invoiceReferenceDate < 91
        GROUP BY scheduleid
    ) ARR90 ON ARR90.scheduleid = SCHED.id
    AND sixtyninety > 0
    LEFT JOIN (
        SELECT INV.scheduleid,
            SUM(unpaidAmount) AS "ninetyplus"
        FROM odsInvoiceLine INV
            JOIN odsSystemDate on 1 = 1
        WHERE unpaidAmount > 0
            AND isDue = '1'
            AND isPaid = '0'
            AND systemdate - invoiceReferenceDate > 90
        GROUP BY scheduleid
    ) ninetyplus ON ninetyplus.scheduleid = SCHED.id
    AND ninetyplus > 0
    LEFT JOIN (
        SELECT IL.agreementid,
            COUNT(IH.invoiceNumber) AS "unpaid_invoices",
            SUM(invoiceLineAmount) AS "unpaid_inv_amount"
        FROM OdsInvoiceHeader IH
            JOIN odsInvoiceLine IL ON IL.invoiceHeaderId = IH.ID
            AND IL.isPaid = false
            JOIN odsSystemDate ON 1 = 1
        WHERE IH.invoiceDueDate < systemdate
        GROUP BY IL.agreementid
    ) UNPINV ON UNPINV.agreementid = AGR.id
    LEFT JOIN (
        SELECT PAYABLE.scheduleid,
            SUM(PAYABLE.amount) AS "commission"
        FROM OdsPayable PAYABLE
            JOIN odsPayableChargeType PCT ON PCT.id = PAYABLE.payableChargeTypeId
            AND PCT.payableChargeTypeCode = 605
        GROUP BY PAYABLE.scheduleid
    ) COMM ON COMM.scheduleid = SCHED.id
    LEFT JOIN (
        SELECT scheduleId,
            SUM(assetDrawdownAmount) + SUM(customerVatAmountAGR) - SUM(assetDepositAGR) AS "drawdown_amount_to_date"
        FROM odsAsset
            JOIN odsSystemDate ON 1 = 1
        WHERE assetDrawdownDate <= systemdate
        GROUP BY scheduleId
    ) DRAWDOWNTD ON DRAWDOWNTD.scheduleid = SCHED.id
    LEFT JOIN odsAgreementMiscellaneous AGRMISC ON AGRMISC.agreementid = AGR.id
    LEFT JOIN OdsExternalSystemReference EXTREF ON EXTREF.thirdPartyId = AGR.invCusId
    AND EXTREF.systemcode = 'ACR'
    LEFT JOIN (
        SELECT agreementid,
            min(terminationNumber) AS "termno"
        FROM odsSchedule
        GROUP BY agreementid
    ) MIN_TERMNO ON MIN_TERMNO.agreementid = SCHED.agreementid
    AND MIN_TERMNO.termno = SCHED.terminationnumber
    LEFT JOIN (
        SELECT RECEIVABLE.scheduleid,
            SUM(RECEIVABLE.amount) AS "customer_deposit"
        FROM odsReceivable RECEIVABLE
            JOIN odsReceivableChargeType RCT ON RCT.id = RECEIVABLE.recvchargetypeid
            AND RCT.recvchargetypecode = 101
        GROUP BY RECEIVABLE.scheduleid
    ) CUSTDEP ON CUSTDEP.scheduleid = SCHED.id
    LEFT JOIN (
        SELECT RECEIVABLE.scheduleid,
            SUM(RECEIVABLE.amount) AS "customer_subsidy"
        FROM odsReceivable RECEIVABLE
            JOIN odsReceivableChargeType RCT ON RCT.id = RECEIVABLE.recvchargetypeid
            AND RCT.recvchargetypecode = 104
        GROUP BY RECEIVABLE.scheduleid
    ) SUBSIDY ON SUBSIDY.scheduleid = SCHED.id
    LEFT JOIN (
        SELECT RECEIVABLE.scheduleid,
            SUM(RECEIVABLE.amount) AS "amountdeferred"
        FROM odsReceivable RECEIVABLE
            JOIN odsReceivableChargeType RCT ON RCT.id = RECEIVABLE.recvchargetypeid
            AND RCT.recvchargetypecode = 102
        GROUP BY RECEIVABLE.scheduleid
    ) DEFERVAT ON DEFERVAT.scheduleid = SCHED.id
    JOIN (
        SELECT max(etllogid) AS "from_etllogid"
        FROM odsPostingHeader
            JOIN eodsystemdate on 1 = 1
        WHERE postingDate < (
                SELECT max(postingdate)
                FROM odsPostingHeader
            )
    ) ETLLOG ON 1 = 1
    JOIN (
        SELECT systemdate AS "processing_date"
        FROM eodSystemDate
    ) CURRENT ON 1 = 1
    LEFT JOIN (
        SELECT agreementid,
            startdate,
            true AS "flag"
        FROM odsCase CAS
            JOIN odsCaseHistory CASHIST ON CASHIST.caseId = CAS.id
            JOIN odsStartingCaseStatus STAT ON STAT.id = CASHIST.startStatusId
            AND STAT.startStatusTypeCode = 'NOV'
            AND STAT.startStatusStatusCode = '007'
        GROUP BY agreementid,
            startdate
    ) NOVATED ON NOVATED.agreementid = AGR.id
    AND NOVATED.startDate = processing_date
WHERE (
        SCHED.scheduleStatus != 'Proposal'
        AND SCHEDM.etllogid > from_etllogid
        OR (
            ALERT.ppo = true
            AND ALERT.ppo_start = processing_date
        )
        OR SCHED.activationDate = processing_date
    )
    AND MIN_TERMNO.termno = SCHED.terminationNumber