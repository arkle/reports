-- ZDW_DRAWDOWN: Agreement drawdowns extract for Data Warehouse
-- Practical Paid Out
SELECT AGR.agreementNumber AS "agreement_number",
    to_char(SCHED.startDate, 'DD/MM/YYYY') AS "drawdown_date",
    SUM(SCHED.totalAssetCostFinanced) AS "drawdown_amount"
FROM eodSchedule SCHED
    JOIN eodAgreement AGR ON AGR.odsid = SCHED.agreementid
    JOIN eodAgreementAlert AGRALERT ON AGRALERT.agreementid = AGR.odsid
    AND AGRALERT.ppo = true
GROUP BY AGR.agreementnumber,
    SCHED.startDate
UNION ALL
-- Non Staged Payments
SELECT AGR.agreementNumber AS "agreement_number",
    CASE
        WHEN AGRMISC.ors IS NOT NULL THEN to_char(AGRMISC.ors, 'DD/MM/YYYY')
        WHEN AGRALERT.mig = true THEN to_char(SCHED.startDate, 'DD/MM/YYYY')
        ELSE to_char(SCHED.activationDate, 'DD/MM/YYYY')
    END AS "drawdown_date",
    SUM(SCHED.totalAssetCostFinanced) AS "drawdown_amount"
FROM eodSchedule SCHED
    JOIN eodAgreement AGR ON AGR.odsid = SCHED.agreementid
    LEFT JOIN eodAgreementMiscellaneous AGRMISC ON AGRMISC.agreementid = AGR.id
    AND AGR.productCode != 'STG'
    JOIN eodAgreementAlert AGRALERT ON AGRALERT.agreementid = AGR.odsid
    LEFT JOIN (
        SELECT agreementid,
            min(terminationNumber) AS "termno"
        FROM eodSchedule
        GROUP BY agreementid
    ) MIN_TERMNO ON MIN_TERMNO.agreementid = SCHED.agreementid
WHERE (
        SCHED.scheduleStatus != 'Proposal'
        AND MIN_TERMNO.termno = SCHED.terminationNumber
    )
GROUP BY AGR.agreementnumber,
    SCHED.startDate,
    SCHED.activationDate,
    AGRALERT.mig
UNION ALL
-- Staged Payments
SELECT AGR.agreementNumber AS "agreement_number",
    to_char(ASSET.assetDrawdownDate, 'DD/MM/YYYY') AS "drawdown_date",
    CASE
        WHEN ASSET.isMainAsset = true THEN ROUND(
            coalesce(SUM(assetDrawdownAmount), 0) + coalesce(SUM(customerVatAmountAGR), 0) - coalesce(SUM(assetDepositAGR), 0) - coalesce(SUM(CUSTDEP.customer_deposit), 0) - coalesce(SUM(DEFERVAT.amountdeferred), 0) - coalesce(SUM(SUBSIDY.customer_subsidy), 0),
            2
        )
        ELSE ROUND(
            coalesce(SUM(assetDrawdownAmount), 0) + coalesce(SUM(customerVatAmountAGR), 0) - coalesce(SUM(assetDepositAGR), 0),
            2
        )
    END AS "drawdown_amount"
FROM eodSchedule SCHED
    JOIN eodAgreement AGR ON AGR.odsid = SCHED.agreementid
    AND AGR.productCode = 'STG'
    JOIN eodAgreementAlert AGRALERT ON AGRALERT.agreementid = AGR.odsid
    AND AGRALERT.ppo = false
    JOIN eodAsset ASSET ON ASSET.agreementId = AGR.odsid
    LEFT JOIN (
        SELECT RECEIVABLE.scheduleid,
            SUM(RECEIVABLE.amount) AS "customer_deposit"
        FROM eodReceivable RECEIVABLE
            JOIN eodReceivableChargeType RCT ON RCT.odsid = RECEIVABLE.recvchargetypeid
            AND RCT.recvchargetypecode = 101
        GROUP BY RECEIVABLE.scheduleid
    ) CUSTDEP ON CUSTDEP.scheduleid = SCHED.odsid
    LEFT JOIN (
        SELECT RECEIVABLE.scheduleid,
            SUM(RECEIVABLE.amount) AS "customer_subsidy"
        FROM eodReceivable RECEIVABLE
            JOIN eodReceivableChargeType RCT ON RCT.odsid = RECEIVABLE.recvchargetypeid
            AND RCT.recvchargetypecode = 104
        GROUP BY RECEIVABLE.scheduleid
    ) SUBSIDY ON SUBSIDY.scheduleid = SCHED.odsid
    LEFT JOIN (
        SELECT RECEIVABLE.scheduleid,
            SUM(RECEIVABLE.amount) AS "amountdeferred"
        FROM eodReceivable RECEIVABLE
            JOIN eodReceivableChargeType RCT ON RCT.odsid = RECEIVABLE.recvchargetypeid
            AND RCT.recvchargetypecode = 102
        GROUP BY RECEIVABLE.scheduleid
    ) DEFERVAT ON DEFERVAT.scheduleid = SCHED.odsid
    LEFT JOIN (
        SELECT agreementid,
            min(terminationNumber) AS "termno"
        FROM eodSchedule
        GROUP BY agreementid
    ) MIN_TERMNO ON MIN_TERMNO.agreementid = SCHED.agreementid
WHERE (
        SCHED.scheduleStatus != 'Proposal'
        AND MIN_TERMNO.termno = SCHED.terminationNumber
    )
GROUP BY AGR.agreementnumber,
    ASSET.assetDrawdownDate,
    ASSET.isMainAsset