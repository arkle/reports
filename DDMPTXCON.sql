-- DDMPTXCON:   PT-X Integration Support 
-- Title:       Mandate PT-X Integration - Obtain contact ID to be used when adding mandates to PT-X DDM
-- Summary:     Provides the contact ID for the billing customer associated with the direct debit mandate. Also, if there is an existing mandate id this is also returned.
-- Parameter:   reference (String,18)
-- Change of mandate reference logic - e.g. when an agreement is novated and the mandate reference is changed
--      If the current reference is not the same as the previous mandate reference, then set the mandate id to null.
--      This will result in a new mandate being added to ptx and the new mandate number being saved to ALFA
SELECT CASE
        WHEN MANDATE.claimreferencetype != 'Mandate' THEN MANDATE.externalreference
        ELSE MANDATE.reference
    END AS "reference",
    EXTREFBIL.reference AS "contactid",
    EXTREFMAN.ptxmd AS "mandateid",
    CASE
        WHEN EXTREFMAN.ptxmd = '<None>' THEN 'N'
        WHEN MANDATE.status = 'Closed' THEN 'U'
        WHEN MANDATE.claimreferencetype != PREV.claimreferencetype
        AND MANDATE.claimreferencetype LIKE 'External%'
        AND PREV.reference = MANDATE.externalreference THEN 'U'
        WHEN MANDATE.claimreferencetype LIKE 'External%'
        AND PREV.externalreference = MANDATE.externalreference THEN 'U'
        WHEN MANDATE.claimreferencetype != PREV.claimreferencetype
        AND MANDATE.claimreferencetype LIKE 'External%'
        AND PREV.reference != MANDATE.externalreference THEN 'R'
        WHEN MANDATE.claimreferencetype LIKE 'External%'
        AND PREV.externalreference = MANDATE.externalreference THEN 'U'
        WHEN MANDATE.claimreferencetype LIKE 'External%'
        AND PREV.externalreference != MANDATE.externalreference THEN 'R'
        WHEN MANDATE.claimreferencetype != PREV.claimreferencetype
        AND MANDATE.claimreferencetype = 'Mandate'
        AND PREV.externalreference = MANDATE.reference THEN 'U'
        WHEN MANDATE.claimreferencetype = 'Mandate'
        AND PREV.reference = MANDATE.reference THEN 'U'
        WHEN MANDATE.claimreferencetype != PREV.claimreferencetype
        AND MANDATE.claimreferencetype = 'Mandate'
        AND PREV.externalreference != MANDATE.reference THEN 'R'
        WHEN MANDATE.claimreferencetype = 'Mandate'
        AND PREV.reference != MANDATE.reference THEN 'R'
        WHEN PREV.billingaddressid != MANDATE.billingaddressid THEN 'R'
        ELSE 'U'
    END AS "action",
    AGR.agreementnumber AS "agreement",
    SCHED.scheduleNumber AS "schedulenumber"
FROM OdsDirectDebitMandate MANDATE
    JOIN OdsBillingAddressMiscellaneous BILLMISC ON BILLMISC.billingAddressId = MANDATE.billingAddressId
    JOIN OdsAgreement AGR ON AGR.agreementnumber = LEFT(MANDATE.reference, 10)
    JOIN OdsSchedule SCHED ON SCHED.agreementid = AGR.id
    LEFT JOIN OdsExternalSystemReference EXTREFBIL ON EXTREFBIL.billingaddressid = MANDATE.billingAddressId
    AND EXTREFBIL.systemcode = 'PTXCO'
    LEFT JOIN OdsSchExternalSystemReference EXTREFMAN ON EXTREFMAN.scheduleid = SCHED.id
    LEFT JOIN (
        SELECT reference,
            externalreference,
            claimreferencetype,
            billingaddressid
        FROM OdsDirectDebitMandate OdsDirectDebitMandate
        WHERE reference LIKE CONCAT(LEFT(:reference, 10), '%')
        ORDER BY lodgeddate DESC OFFSET 1
        LIMIT 1
    ) PREV ON 1 = 1
WHERE (
        RTRIM(:reference) = RTRIM(MANDATE.reference)
        OR RTRIM(:reference) = RTRIM(MANDATE.externalreference)
    )
ORDER BY MANDATE.id DESC
LIMIT 1