SELECT
        CASE
                WHEN MISC.pcompcode = 'EVIN' THEN 'Evington'       
                WHEN AGRCO.agrcompanycode IN ('2', '4') THEN 'Evington'
                ELSE 'Arkle'
            END as "Agreement_Company",
        EXPCUS.expCusBillingName as "Exposure_Customer",
        AGR.agreementNumber as "Agreement",
        AGR.product as "Product",
        SCHED.activationDate as "Agreement_Date",
        SCHED.totalAssetCostFinanced as "Amount_Advanced",
        (DATE_PART('YEAR', SCHED.maturityDate) - DATE_PART('YEAR', SCHED.startDate)) * 12 +
        (DATE_PART('MONTH', SCHED.maturityDate) - DATE_PART('MONTH', SCHED.startDate))
         as "Duration",
        SCHED.nextRentalAmount as "Next_Rental",
        SCHED.capitalOutstanding as "Capital_OS",
        to_char(SCHED.maturityDate, 'DD/MM/YYYY') as "End_Date",
        SCHED.annualPercentageRate as "APR",
        SCHED.balloonAmount as "Balloon"
FROM odsSchedule SCHED
JOIN odsAgreement AGR ON AGR.id = SCHED.agreementid
JOIN odsExposureCustomerBilling EXPCUS ON EXPCUS.id = SCHED.expCusBillingId
JOIN odsAgreementCompany AGRCO ON AGRCO.id = SCHED.agrCompanyId
JOIN odsAgreementMiscellaneous MISC ON MISC.agreementid = AGR.id
JOIN odsAgreementAlert AGRALERT ON AGR.id = AGRALERT.agreementId
WHERE SCHED.scheduleStatus = 'Live (Primary)'
AND AGR.product <> 'Operating Lease'
AND SCHED.activationDate >= CURRENT_DATE - INTERVAL '7 DAYS'
AND AGRALERT.mig <> '1'
ORDER BY 5 asc;