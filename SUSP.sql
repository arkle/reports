-- SUSP:    Suspense report
-- Summary: Identify all unresolved suspense items and their age in order to prioritise investigation and processing.
-- Suspense items are matched to open cases where the current case status is "Review and Process Suspense" or "Investigate Suspense Item"

SELECT DATA.* FROM

(SELECT 
	
	CASHALLOC.locationType		                        AS "Suspense Type",
    CASHTXN.effectivedate                               AS "Effective_date",
    systemdate - CASHTXN.effectivedate                  AS "Age_days",
    BILLADD.uniqueref                                   AS "Third_party_id",
    BILLADD.name                                        AS "Third_party_name",
    SCHEDMAIN.scheduleref                               AS "Agreement_number",
	SUM(CASHALLOCORIG.totalAmount)   	                    AS "Amount",
    SUM(CASHALLOC.amount_pending)                            AS "Pending",
    ALFAUSERINPUT.name                                  AS "Input_by",
    CASHINOUT.reference                                 AS "Reference"
    CASHINOUT.alfaidentifier                            AS "Receipt Number"

FROM OdsCashAllocationtxn CASHTXN 
    JOIN OdsCashInOut CASHINOUT 
        ON CASHINOUT.id = CASHTXN.cashid
-- To identify "open" suspense look for Cash Allocations with a suspense type that have not been netted out. Suspense items are grouped for this purpose using the scheduleid, originatingtxnid 
    JOIN (SELECT companyid, locationtype, agreementid, originatingtxnid, scheduleid, min(id) AS "cashalloc_id", sum(totalAmount) AS "amount_pending"
             FROM OdsCashAllocation
             WHERE locationType IN ('Billing address suspense','General suspense','Schedule suspense') AND transactionStatus = 'Applied'
             GROUP BY companyid, locationtype, agreementid, originatingtxnid, scheduleid
             HAVING sum(totalAmount) <> 0) CASHALLOC
             ON CASHALLOC.originatingtxnid = CASHTXN.id
-- Join with the originating Cash Allocation (first instance) by joining again on OdsCashAllocation in order to identify the original suspense amount
    JOIN OdsCashAllocation CASHALLOCORIG 
        ON CASHALLOCORIG.id = CASHALLOC.cashalloc_id 
    JOIN OdsCashAllocationCompany CASHCOMP
		ON CASHALLOC.companyid = CASHCOMP.id
    LEFT JOIN OdsScheduleMain SCHEDMAIN 
        ON SCHEDMAIN.id = CASHALLOC.scheduleid
    LEFT JOIN OdsBillingAddress BILLADD 
        ON BILLADD.id = CASHALLOCORIG.billingAddressId 
    JOIN OdsAlfaUser ALFAUSERINPUT
        ON ALFAUSERINPUT.id = CASHTXN.inputByUserId
    JOIN OdsSystemDate ON 1 = 1
    WHERE CASHALLOCORIG.totalAmount <> 0  
GROUP BY CASHALLOC.locationType, CASHTXN.effectivedate, odssystemdate.systemdate, BILLADD.uniqueref, BILLADD.name, SCHEDMAIN.scheduleref, ALFAUSERINPUT.name, CASHINOUT.reference, CASHINOUT.alfaidentifier
ORDER BY CASHALLOC.locationType,CASHTXN.effectivedate) DATA
WHERE DATA."Amount" <> 0