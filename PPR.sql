-- PPR:         Proposal Performance Report
-- Summary:     Details of the key progressions of ORG workflow cases and number of business hours
-- Parameter:   Created_from (Date), Created_to (Date)
SELECT DLR.dealerBillingUniqueRef AS "Broker_Third_Party_Ref",
    DLR.dealerBillingName AS "Broker_Name",
    CASE
        WHEN DLRALERT.prb = '1' THEN 'Y'
        ELSE ''
    END AS "Priority_Broker",
    AGR.agreementnumber AS "Agreement",
    SCHED.totalAssetCostFinanced AS "Amount_financed",
    STAT.currentStatusStatusName AS "ORG_Case_Status",
    CONCAT(
        to_char(CAS.openedDate, 'YYYY-MM-DD'),
        to_char(CAS.openedTime, ' 99:99:99')
    ) AS "Created",
    UWADMIN.actionedByUserName AS "Underwriting_Admin",
    CONCAT(
        to_char(REVIEW.startDate, 'YYYY-MM-DD'),
        to_char(REVIEW.startTime, ' 99:99:99')
    ) AS "Review",
    CASE
        WHEN REVIEW.caseid IS NOT NULL THEN ROUND(
            CAST (
                (
                    SELECT count(*) AS work_interval
                    FROM generate_series (
                            TO_TIMESTAMP(
                                CONCAT(
                                    CAS.openedDate,
                                    to_char(CAS.openedTime, ' 99:99:99')
                                ),
                                'YYYY-MM-DD HH24:MI:SS'
                            ),
                            TO_TIMESTAMP(
                                CONCAT(
                                    REVIEW.startDate,
                                    to_char(REVIEW.startTime, ' 99:99:99')
                                ),
                                'YYYY-MM-DD HH24:MI:SS'
                            ),
                            interval '5 min'
                        ) h
                    WHERE EXTRACT(
                            ISODOW
                            FROM h
                        ) < 6
                        AND h.time >= '09:00'
                        AND h.time <= '16:55'
                ) AS decimal
            ) / 12,
            2
        )
        ELSE 0
    END AS "Create_to_review_Bus_Hrs",
    CASE
        WHEN REVIEW.actionedByUserName IS NOT NULL THEN REVIEW.actionedByUserName
        ELSE NULL
    END AS "Review_picked_up_by",
    CONCAT(
        to_char(MOREINFO.startDate, 'YYYY-MM-DD'),
        to_char(MOREINFO.startTime, ' 99:99:99')
    ) AS "More_Info",
    CASE
        WHEN MOREINFO.caseid IS NOT NULL THEN ROUND(
            CAST (
                (
                    SELECT count(*) AS work_interval
                    FROM generate_series (
                            TO_TIMESTAMP(
                                CONCAT(
                                    CAS.openedDate,
                                    to_char(CAS.openedTime, ' 99:99:99')
                                ),
                                'YYYY-MM-DD HH24:MI:SS'
                            ),
                            TO_TIMESTAMP(
                                CONCAT(
                                    MOREINFO.startDate,
                                    to_char(MOREINFO.startTime, ' 99:99:99')
                                ),
                                'YYYY-MM-DD HH24:MI:SS'
                            ),
                            interval '5 min'
                        ) h
                    WHERE EXTRACT(
                            ISODOW
                            FROM h
                        ) < 6
                        AND h.time >= '09:00'
                        AND h.time <= '16:55'
                ) AS decimal
            ) / 12,
            2
        )
        ELSE 0
    END AS "Create_to_More_Info_Bus_Hrs",
    MOREINFO.actionedByUserName AS "More_Info_Actioned_By",
    CONCAT(
        to_char(APPROVED.startDate, 'YYYY-MM-DD'),
        to_char(APPROVED.startTime, ' 99:99:99')
    ) AS "Approved",
    CASE
        WHEN APPROVED.caseid IS NOT NULL THEN ROUND(
            CAST (
                (
                    SELECT count(*) AS work_interval
                    FROM generate_series (
                            TO_TIMESTAMP(
                                CONCAT(
                                    CAS.openedDate,
                                    to_char(CAS.openedTime, ' 99:99:99')
                                ),
                                'YYYY-MM-DD HH24:MI:SS'
                            ),
                            TO_TIMESTAMP(
                                CONCAT(
                                    APPROVED.startDate,
                                    to_char(APPROVED.startTime, ' 99:99:99')
                                ),
                                'YYYY-MM-DD HH24:MI:SS'
                            ),
                            interval '5 min'
                        ) h
                    WHERE EXTRACT(
                            ISODOW
                            FROM h
                        ) < 6
                        AND h.time >= '09:00'
                        AND h.time <= '16:55'
                ) AS decimal
            ) / 12,
            2
        )
        ELSE 0
    END AS "Create_to_Approved_Bus_Hrs",
    APPROVED.actionedByUserName AS "Approved_Actioned_By",
    CONCAT(
        to_char(DECLINED.startDate, 'YYYY-MM-DD'),
        to_char(DECLINED.startTime, ' 99:99:99')
    ) AS "Declined",
    CASE
        WHEN DECLINED.caseid IS NOT NULL THEN ROUND(
            CAST (
                (
                    SELECT count(*) AS work_interval
                    FROM generate_series (
                            TO_TIMESTAMP(
                                CONCAT(
                                    CAS.openedDate,
                                    to_char(CAS.openedTime, ' 99:99:99')
                                ),
                                'YYYY-MM-DD HH24:MI:SS'
                            ),
                            TO_TIMESTAMP(
                                CONCAT(
                                    DECLINED.startDate,
                                    to_char(DECLINED.startTime, ' 99:99:99')
                                ),
                                'YYYY-MM-DD HH24:MI:SS'
                            ),
                            interval '5 min'
                        ) h
                    WHERE EXTRACT(
                            ISODOW
                            FROM h
                        ) < 6
                        AND h.time >= '09:00'
                        AND h.time <= '16:55'
                ) AS decimal
            ) / 12,
            2
        )
        ELSE 0
    END AS "Create_to_Declined_Bus_Hrs",
    DECLINED.actionedByUserName AS "Declined_Actioned_By",
    CASE
        WHEN MISC.repur = '1' THEN 'Y'
        ELSE ''
    END AS "CR_Repurchase_Undertaking",
    CASE
        WHEN MISC.intp = '1' THEN 'Y'
        ELSE ''
    END AS "CR_Init_Pmt_Clr_Funds",
    CASE
        WHEN MISC.ccgd = '1' THEN 'Y'
        ELSE ''
    END AS "CR_CCG_as_specified",
    CASE
        WHEN MISC.law = '1' THEN 'Y'
        ELSE ''
    END AS "CR_Landlords_Waiver",
    CASE
        WHEN MISC.assi = '1' THEN 'Y'
        ELSE ''
    END AS "CR_Asset_inspection",
    MISC.pgsd AS "CR_PG_description",
    MISC.supd AS "CR_Supplier_description",
    MISC.cond1 AS "CR_Condition_1",
    MISC.cond2 AS "CR_Condition_2",
    MISC.cond3 AS "CR_Condition_3",
    MISC.cond4 AS "CR_Condition_4",
    MISC.cond5 AS "CR_Condition_5",
    CASE
        WHEN MISC.dec3 = '1' THEN 'Y'
        ELSE ''
    END AS "DEC_Bank_Stmts_DNS",
    CASE
        WHEN MISC.dec4 = '1' THEN 'Y'
        ELSE ''
    END AS "DEC_Adverse_Credit",
    CASE
        WHEN MISC.dec5 = '1' THEN 'Y'
        ELSE ''
    END AS "DEC_Sufficiently_Exp",
    CASE
        WHEN MISC.dec6 = '1' THEN 'Y'
        ELSE ''
    END AS "DEC_Outside_Policy",
    CASE
        WHEN MISC.dec7 = '1' THEN 'Y'
        ELSE ''
    END AS "DEC_Insufficient_Info",
    MISC.dec1 AS "DEC_Other_1",
    MISC.dec2 AS "DEC_Other_2",
    CASE
        WHEN MISC.bank = '1' THEN 'Y'
        ELSE ''
    END AS "FUI_3_months_bank_stmt",
    CASE
        WHEN MISC.opn = '1' THEN 'Y'
        ELSE ''
    END AS "FUI_Open_Banking",
    CASE
        WHEN MISC.ovr = '1' THEN 'Y'
        ELSE ''
    END AS "FUI_Overdraft",
    CASE
        WHEN MISC.acc = '1' THEN 'Y'
        ELSE ''
    END AS "FUI_Last_filed_acc",
    CASE
        WHEN MISC.man = '1' THEN 'Y'
        ELSE ''
    END AS "FUI_Latest_mgmt_acc",
    CASE
        WHEN MISC.sal = '1' THEN 'Y'
        ELSE ''
    END AS "FUI_SAL",
    CASE
        WHEN MISC.equip = '1' THEN 'Y'
        ELSE ''
    END AS "FUI_Equip_spec",
    CASE
        WHEN MISC.dpd = '1' THEN 'Y'
        ELSE ''
    END AS "FUI_Director_pd",
    MISC.furi1 AS "FUI_Other_1",
    MISC.furi2 AS "FUI_Other_2",
    MISC.furi3 AS "FUI_Other_3",
    CASE
        WHEN MISC.vatde = '1' THEN 'Y'
        ELSE ''
    END AS "Other_VAT_deferral_app",
    CASE
        WHEN AGRALERT.rls = '1' THEN 'Y'
        ELSE ''
    END AS "RLS"
FROM OdsCase CAS
    JOIN OdsAllocatedToUser ALLOC ON ALLOC.id = CAS.allocatedToUserId
    JOIN OdsCurrentCaseStatus STAT ON STAT.id = CAS.currentStatusId
    JOIN OdsAgreement AGR ON AGR.id = CAS.agreementid
    JOIN OdsDealerBilling DLR ON DLR.id = AGR.dealerBillingId
    LEFT JOIN OdsBillingAddressAlert DLRALERT ON DLRALERT.billingAddressId = DLR.id
    JOIN OdsSchedule SCHED ON SCHED.agreementid = AGR.id
    LEFT JOIN OdsAgreementMiscellaneous MISC ON MISC.agreementId = AGR.id
    LEFT JOIN (
        SELECT CASHIST.caseid,
            CASHIST.startDate,
            CASHIST.startTime,
            ACTIONED.actionedByUserName
        FROM OdsCaseHistory CASHIST
            JOIN OdsActionedByUser ACTIONED ON ACTIONED.id = CASHIST.actionedByUserId
        WHERE CASHIST.id IN (
                SELECT min(H.id)
                FROM OdsCaseHistory H
                    JOIN OdsEndingCaseStatus ENDSTAT ON ENDSTAT.id = H.endStatusId
                WHERE ENDSTAT.endStatusStatusCode = '016'
                    AND ENDSTAT.endStatusTypeCode = 'ORG'
                GROUP BY H.caseid
            )
    ) MOREINFO ON MOREINFO.caseid = CAS.id
    LEFT JOIN (
        SELECT CASHIST.caseid,
            CASHIST.startDate,
            CASHIST.startTime,
            ACTIONED.actionedByUserName
        FROM OdsCaseHistory CASHIST
            JOIN OdsActionedByUser ACTIONED ON ACTIONED.id = CASHIST.actionedByUserId
        WHERE CASHIST.id IN (
                SELECT min(H.id)
                FROM OdsCaseHistory H
                    JOIN OdsEndingCaseStatus ENDSTAT ON ENDSTAT.id = H.endStatusId
                WHERE ENDSTAT.endStatusStatusCode = '020'
                    AND ENDSTAT.endStatusTypeCode = 'ORG'
                GROUP BY H.caseid
            )
    ) APPROVED ON APPROVED.caseid = CAS.id
    LEFT JOIN (
        SELECT CASHIST.caseid,
            CASHIST.startDate,
            CASHIST.startTime,
            ACTIONED.actionedByUserName
        FROM OdsCaseHistory CASHIST
            JOIN OdsActionedByUser ACTIONED ON ACTIONED.id = CASHIST.actionedByUserId
        WHERE CASHIST.id IN (
                SELECT min(H.id)
                FROM OdsCaseHistory H
                    JOIN OdsEndingCaseStatus ENDSTAT ON ENDSTAT.id = H.endStatusId
                WHERE ENDSTAT.endStatusStatusCode = '021'
                    AND ENDSTAT.endStatusTypeCode = 'ORG'
                GROUP BY H.caseid
            )
    ) DECLINED ON DECLINED.caseid = CAS.id
    LEFT JOIN (
        SELECT CASHIST.caseid,
            CASHIST.startDate,
            CASHIST.startTime,
            ACTIONED.actionedByUserName
        FROM OdsCaseHistory CASHIST
            JOIN OdsActionedByUser ACTIONED ON ACTIONED.id = CASHIST.actionedByUserId
        WHERE CASHIST.id IN (
                SELECT min(H.id)
                FROM OdsCaseHistory H
                    JOIN OdsStartingCaseStatus STARTSTAT ON STARTSTAT.id = H.startStatusId
                WHERE STARTSTAT.startStatusStatusCode IN ('008', '055')
                    AND STARTSTAT.startStatusTypeCode = 'ORG'
                GROUP BY H.caseid
            )
    ) REVIEW ON REVIEW.caseid = CAS.id
    LEFT JOIN (
        SELECT CASHIST.caseid,
            CASHIST.startDate,
            CASHIST.startTime,
            ACTIONED.actionedByUserName
        FROM OdsCaseHistory CASHIST
            JOIN OdsActionedByUser ACTIONED ON ACTIONED.id = CASHIST.actionedByUserId
        WHERE CASHIST.id IN (
                SELECT min(H.id)
                FROM OdsCaseHistory H
                    JOIN OdsStartingCaseStatus STARTSTAT ON STARTSTAT.id = H.startStatusId
                WHERE STARTSTAT.startStatusStatusCode IN ('001')
                    AND STARTSTAT.startStatusTypeCode = 'ORG'
                GROUP BY H.caseid
            )
    ) UWADMIN ON UWADMIN.caseid = CAS.id
    LEFT JOIN OdsAgreementAlert AGRALERT ON AGRALERT.agreementId = AGR.id
WHERE STAT.currentStatusTypeCode = 'ORG'
    AND (
        CAS.openedDate >= to_date(:Created_from, 'YYYY-MM-DD')
        AND CAS.openedDate <= to_date(:Created_to, 'YYYY-MM-DD')
    )