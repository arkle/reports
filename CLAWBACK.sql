-- CLAWBACK:    Commission Clawbacks
-- Summary:     Provides a view of commission clawback invoices and their status
-- Parameters:  unpaid (boolean)
SELECT DLRBILL.dealerBillingUniqueRef AS "Broker_ref",
    DLRBILL.dealerBillingName AS "Broker_name",
    BILL.inv3pyBillingUniqueRef AS "Third_party_ref",
    BILL.inv3pyBillingName AS "Third_party_name",
    AGR.agreementnumber AS "Agreement",
    AGR.agreementType AS "Agreement_type",
    AGR.product AS "Product",
    CASE
        WHEN AGR.isCcaRegulated = '1' THEN 'Y'
        ELSE ''
    END AS "Regulated?",
    INVHDR.formattedInvoiceNumber AS "Invoice_Number",
    CASE
        WHEN INVLIN.isDue IS true THEN 'Y'
        ELSE 'N'
    END AS "Due?",
    CASE
        WHEN INVLIN.isPaid IS true THEN 'Y'
        ELSE 'N'
    END AS "Paid?",
    INVHDR.invoiceIssueDate AS "Issue_Date",
    INVHDR.invoiceDueDate AS "Due_Date",
    INVLIN.invoiceLineAmount AS "Amount",
    INVLIN.receivedAmount AS "Received",
    INVLIN.unpaidAmount AS "Unpaid"
FROM OdsInvoiceHeader INVHDR
    JOIN OdsInvoiceLine INVLIN ON INVLIN.invoiceHeaderId = INVHDR.id
    JOIN OdsAgreement AGR ON AGR.id = INVLIN.agreementid
    JOIN (
        SELECT invoiceHeaderId
        FROM OdsInvoiceLine LIN
            JOIN OdsChargeType CT ON CT.id = LIN.chargeTypeId
            AND CT.code = '215'
        GROUP BY invoiceHeaderId
    ) CLAWINV ON CLAWINV.invoiceHeaderId = INVHDR.id
    JOIN OdsInvoicingThirdPartyBilling BILL ON BILL.id = INVHDR.inv3pyBillingId
    LEFT JOIN OdsDealerBilling DLRBILL ON DLRBILL.id = AGR.dealerBillingId
WHERE 1 = CASE
        WHEN :unpaid = true
        AND INVLIN.isPaid = false THEN 1
        WHEN :unpaid = false THEN 1
    END
ORDER BY DLRBILL.dealerBillingUniqueRef,
    BILL.inv3pyBillingUniqueRef,
    INVHDR.invoiceDueDate,
    INVLIN.isPaid