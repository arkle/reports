-- STAGED:      Staged Payment Report
-- Summary:     Provides a view of staged payment agreements
-- Parameters:  include_completed (Boolean)
SELECT SORTAGR.final_agreement AS "Final_Agreement",
    CASE
        WHEN AGR.agreementnumber = SORTAGR.final_agreement THEN ''
        ELSE AGR.agreementnumber
    END AS "Stage_Payment_Agreement",
    CASE
        WHEN ASSET.assetDrawdownDate > systemdate THEN CAST(ASSET.assetDrawdownDate - systemdate AS text)
        ELSE ''
    END AS "Days_until_drawdown",
    ASSET.assetDrawdownDate AS "Drawdown_date",
    ASSET.assetDescription AS "Drawdown_description",
    ASSET.assetDrawdownAmountAGR AS "Drawdown_amount",
    ASSET.assetDrawdownVatAGR AS "Drawdown_VAT_amount",
    SCHED.activationDate AS "Activated",
    SCHED.scheduleStatus AS "Status",
    ExpBill.expCusBillingUniqueRef AS "Third_party_ref",
    ExpBill.expCusBillingName AS "Third_party_name"
FROM OdsAgreement AGR
    JOIN OdsSchedule SCHED ON SCHED.agreementid = AGR.id
    JOIN OdsExposureCustomerBilling ExpBill ON ExpBill.id = AGR.expCusBillingId
    JOIN OdsAgreementMiscellaneous AGRMISC ON AGRMISC.agreementId = AGR.id
    JOIN OdsAsset ASSET ON ASSET.agreementId = AGR.id
    LEFT JOIN OdsAgreement FINAL ON FINAL.agreementnumber = AGRMISC.STP
    LEFT JOIN (
        SELECT CAS.agreementid
        FROM OdsCase CAS
            JOIN OdsCurrentCaseStatus STAT ON STAT.id = CAS.currentStatusId
        WHERE CAS.status = 'Open'
            AND STAT.currentStatusTypeCode = 'ORG'
        GROUP BY CAS.agreementid
    ) ORG ON ORG.agreementid = AGR.id
    LEFT JOIN (
        SELECT id AS "agreementid",
            CASE
                WHEN AGRMISC.STPL = true THEN AGR.agreementnumber
                ELSE AGRMISC.STP
            END AS "final_agreement"
        FROM OdsAgreement AGR
            JOIN OdsAgreementMiscellaneous AGRMISC ON AGRMISC.agreementId = AGR.id
        WHERE AGR.productCode = 'STG'
            OR AGRMISC.STPL = true
    ) SORTAGR ON SORTAGR.agreementid = AGR.id
    JOIN OdsSystemDate ON 1 = 1
WHERE (
        AGR.productCode = 'STG'
        OR AGRMISC.STPL = true
    )
    AND NOT (
        SCHED.scheduleStatus = 'Proposal'
        AND ORG.agreementid IS Null
    )
    AND SCHED.scheduleStatus LIKE CASE
        WHEN :include_completed = true THEN '%'
        ELSE 'Live%'
    END
ORDER BY SORTAGR.final_agreement,
    ASSET.assetDrawdownDate