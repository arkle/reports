-- PORTFOLIO:   Portfolio Report
-- Summary:     Provides details of the portfolio covering all proposals and agreements in all statuses selected via the parameters
-- Parameters:  Live_Primary, Live_Secondary, Practical_DAC_Paid_Out, Terminated, Matured, Proposal_Pending, Proposal_Cancelled
-- IMPORTANT:   Changes made to this report should also be considered for the Finance equivalent
SELECT CASE
        WHEN AGRCO.agrcompanycode IN ('2', '4') THEN 'Evington'
        ELSE 'Arkle'
    END AS "Company",
    CASE
        WHEN AGRCO.agrcompanycode = '1' THEN 'Core'
        WHEN AGRCO.agrcompanycode IN ('2', '4') THEN 'Consumer'
        WHEN AGRCO.agrcompanycode = '3' THEN 'Prime'
    END AS "Pricing",
    --  Broker info
    CASE
        WHEN DLRBILALT.dealerBillingName != '<None>' THEN DLRBILALT.dealerBillingName
        ELSE DLR.dealerName
    END AS "Broker",
    CASE
        WHEN BILSALPER.salesperson > '' THEN BILSALPER.salesperson
        ELSE SALPER.salesperson
    END AS "Broker_Manager",
    --  Customer info
    INVCUS.invCusThirdPartyNumber AS "Invoice_cust_id",
    INVCUS.invCusName AS "Invoice_cust_name",
    EXTREF.reference AS "Actor_Code",
    INVCUS.invCusCaisLegalEntityType AS "Invoice_cust_legal_type",
    INVCUS.invCusCompanyRegNo AS "Company_reg",
    CASE
        WHEN EXPCUS.expCusThirdPartyNumber = INVCUS.invCusThirdPartyNumber THEN ''
        ELSE 'Y'
    END AS "Undisclosed",
    CASE
        WHEN AGRALERT.cais = true THEN 'Y'
        ELSE ''
    END AS "Undisclosed_Alert?",
    EXPCUS.expCusName AS "Exposure_Customer",
    --  Flag if the customer is vulnerable or has an active complaint (based on the Invoicing customer)  
    CASE
        WHEN VULNERABLE.thirdpartyid > 0 THEN 'Y'
        ELSE ''
    END AS "Vulnerable",
    CASE
        WHEN COMPLAINT.thirdpartyid > 0 THEN 'Y'
        ELSE ''
    END AS "Complaint_open",
    --  Agreement information                                           
    AGR.agreementNumber AS "Agreement",
    CASE
        WHEN MISC.agrid = '<None>' THEN ''
        ELSE MISC.agrid
    END AS "Alternative_Agreement",
    CASE
        WHEN AGRALERT.mig = '1' THEN 'Y'
        ELSE ''
    END AS "Migrated",
    AGR.agreementType AS "Agreement_type",
    SCHED.scheduleRef AS "Schedule_ref",
    SCHED.scheduleDescription AS "Schedule_description",
    CASE
        WHEN AGR.isCcaRegulated = '1' THEN 'Y'
        ELSE ''
    END AS "Regulated",
    CASE
        WHEN AGR.hasGuarantee = '1' THEN 'Y'
        ELSE ''
    END AS "Guarantee",
    --  Asset information
    ASSET.assetTypeName AS "Asset_type",
    ASSET.assetDescription AS "Asset_description",
    CASE
        WHEN MISC.oaf IS NOT NULL THEN MISC.oaf
        ELSE SCHED.totalAssetCostFinanced
    END AS "Amount_financed",
    CASE
        WHEN AGR.product = 'STG' THEN DRAWDOWN.drawdown_amount + DRAWDOWN.drawdown_vat - DRAWDOWN.drawdown_deposit
        WHEN MISC.oaf IS NOT NULL THEN MISC.oaf
        ELSE SCHED.totalAssetCostFinanced
    END AS "Draw_down_to_date",
    SCHED.totalResidualValue AS "RV",
    SCHED.balloonAmount AS "Balloon",
    SCHED.balloondate AS "Balloon_Date",
    BALLPMT.balloonPaymentTypeCode AS "Balloon_Payment_Type",
    DOCFEE.documentation_fee AS "Document_fee",
    DOCFEESPLIT.documentation_fee_share AS "Document_fee_share",
    OTPFEE.option_to_purchase_fee AS "OTP_fee",
    --  Agrement details
    AGR.product AS "Product",
    SCHED.termInMonths AS "Term",
    CASE
        WHEN AGRALERT.ppo = true THEN SCHED.startDate
        WHEN MISC.ors IS NOT NULL THEN MISC.ors
        ELSE SCHED.activationDate
    END AS "Activated_date",
    CASE
        WHEN MISC.ors IS NOT NULL THEN MISC.ors
        ELSE SCHED.startDate
    END AS "Start_date",
    SCHED.expectedEndDate AS "Expected_end_date",
    SCHED.maturityDate AS "Maturity_date",
    SCHED.secondaryPeriodExpiryDate AS "Secondary_expiry",
    ACTUSER.activatedByUserName AS "Activated_by",
    CASE
        WHEN AGRALERT.ppo = true THEN 'Practical DAC (Paid_Out)'
        WHEN SCHED.schedulestatus = 'Proposal'
        AND NTU.agreementid IS Not Null THEN 'Proposal (NTU Holding)'
        WHEN SCHED.schedulestatus = 'Proposal'
        AND AGRALERT.ntu = true THEN 'Proposal (NTU)'
        WHEN SCHED.schedulestatus = 'Proposal'
        AND ORG.agreementid IS Null THEN 'Proposal (Cancelled)'
        WHEN SCHED.schedulestatus LIKE 'Live%'
        AND PARTIAL.agreementid IS NOT Null THEN CONCAT(SCHED.schedulestatus, ' (Partially Terminated)')
        ELSE SCHED.scheduleStatus
    END AS "Schedule_Status",
    CASE
        WHEN SCHED.isTerminated = true THEN 'Y'
        ELSE ''
    END AS "Terminated",
    --  Termination details
    SCHED.terminationDate AS "Terminated_date",
    SCHED.terminationNumber AS "Termination_number",
    SCHED.terminationType AS "Termination_type",
    SCHED.terminationReason AS "Termination_reason",
    SCHED.terminationAmount AS "Amount_Terminated",
    SCHED.disposalAmount AS "Asset_disposal_proceeds",
    SCHED.totalWriteOffs AS "Total_Write_Offs",
    --  Rates
    ROUND(
        (
            COMM.commission / SCHED.originalAssetCostFinanced
        ) * 100,
        2
    ) AS "Commission_pct",
    COMM.commission AS "Commission",
    CASE
        WHEN SCHED.scheduleStatus IN ('Terminated', 'Matured') THEN 0
        ELSE COMMOS.oscomm
    END AS "Commission_OS",
    SCHED.currentFundingRate AS "Current_funding_rate",
    CASE
        WHEN MISC.ory IS NOT NULL THEN MISC.ory / 100
        ELSE SCHED.combinedLessorPtoRate / 100
    END AS "Yield",
    --  Current balances
    SCHED.totalPrimaryRentals AS "Total_Primary_Rentals",
    SCHED.totalSecondaryRentals AS "Total_Secondary_Rentals",
    SCHED.totalExposure AS "Total_Exposure",
    SCHED.grossBalanceRemaining AS "Balance_OS",
    CASE
        WHEN SCHED.scheduleStatus IN ('Terminated', 'Matured') THEN 0
        ELSE SCHED.capitalOutstanding
    END AS "Capital_OS_Lessee",
    CASE
        WHEN SCHED.scheduleStatus IN ('Terminated', 'Matured') THEN 0
        ELSE INTEREST_LESSEE.interest_os_lessee * -1
    END AS "Interest_OS_Lessee",
    CASE
        WHEN SCHED.scheduleStatus IN ('Terminated', 'Matured') THEN 0
        ELSE CAPOS_LESSOR.capitalOutstanding * -1
    END AS "Capital_OS_Lessor",
    CASE
        WHEN SCHED.scheduleStatus IN ('Terminated', 'Matured') THEN 0
        ELSE INTEREST_LESSOR.interest_os_lessor * -1
    END AS "Interest_OS_Lessor",
    AGR.agreementSuspense AS "Suspense",
    SCHED.totalReceipts AS "Total_receipts",
    CASE
        WHEN SCHED.scheduleStatus IN ('Terminated', 'Matured') THEN 0
        ELSE SCHED.futureReceivables
    END AS "Future_receivables",
    CASE
        WHEN SCHED.scheduleStatus IN ('Terminated', 'Matured') THEN 0
        ELSE coalesce(URINC.UnearnedIncome, 0.00) - coalesce(DepositUnearned, 0.00) - coalesce(NotDueDeposit, 0.00)
    END AS "Unearned Income",
    SCHED.totalScheduleArrears AS "Schedule_arrears",
    SCHED.inArrearsSinceDate AS "In_Arrears_since",
    zerothirty AS "0-30",
    thirtysixty AS "31-60",
    sixtyninety AS "61-90",
    ninetyplus AS "91P",
    SCHED.monthsInExtension AS "Months_in_extension",
    --  Last Payment for the schedule (for the Invoicing Customer)
    LASTPMT.transactionEffectiveDate AS "Last_customer_payment_date",
    LASTPMT.lastpayment AS "Last_customer_payment_amount",
    --  Next rental info
    CASE
        WHEN SCHED.scheduleStatus IN ('Terminated', 'Matured') THEN 0
        ELSE SCHED.numberOfFutureRentals
    END AS "Future_rentals",
    SCHED.nextRentalDate AS "Next_rental_date",
    sched.nextRentalFrequency AS "Next_rental_freq",
    SCHED.nextRentalAmount AS "Next_rental_amount",
    NEXTRENTAL.nextRentalPaymentTypeName AS "Next_rental_pmt_type",
    --  Insurance status
    ACQUIS.insuranceStatus AS "Insurance_status",
    CASE
        WHEN AGRALERT.rls = true THEN 'Y'
        ELSE ''
    END AS "RLS",
       SEC.securityTypeName as "Security_Type",
       GUA.guarantorName as "Guarantor_Name"
FROM OdsSchedule SCHED
    JOIN OdsAgreement AGR ON AGR.id = SCHED.agreementid
    JOIN OdsBalloonPaymentType BALLPMT ON BALLPMT.id = SCHED.balloonPaymentTypeId
    JOIN OdsInvoicingCustomer INVCUS ON INVCUS.id = SCHED.invCusId
    JOIN OdsExposureCustomer EXPCUS ON EXPCUS.id = SCHED.expCusId
    JOIN OdsDealer DLR ON DLR.id = SCHED.dealerId
    JOIN OdsDealerBilling DLRBIL ON DLRBIL.id = SCHED.dealerBillingId
    JOIN OdsAgreementMiscellaneous MISC ON MISC.agreementid = AGR.id
    LEFT JOIN eodDealerBilling DLRBILALT ON DLRBILALT.dealerBillingUniqueRef = MISC.bro
    JOIN OdsAgreementCompany AGRCO ON AGRCO.id = SCHED.agrCompanyId
    JOIN OdsActivatedByUser ACTUSER ON ACTUSER.id = SCHED.activatedByUserId
    LEFT JOIN OdsScheduleLlisl ACQUIS ON ACQUIS.scheduleId = SCHED.id
    LEFT JOIN OdsNextRentalPaymentType NEXTRENTAL ON NEXTRENTAL.id = SCHED.nextRentalPaymentTypeId -- Identify Agreements with a Practical Paid Out Alert
    LEFT JOIN (
        SELECT thirdpartyid
        FROM OdsThirdPartyAlert
            JOIN OdsSystemDate ON 1 = 1
        WHERE com_code = '1'
            AND (
                com_code_start <= systemdate
                AND com_code_exp >= systemdate
            )
    ) COMPLAINT ON COMPLAINT.thirdpartyid = INVCUS.id -- Identify Third Parties with a Vulnerable Customer Alert
    LEFT JOIN (
        SELECT thirdpartyid
        FROM OdsThirdPartyAlert
            JOIN OdsSystemDate ON 1 = 1
        WHERE tex_code = '1'
            AND (
                tex_code_start <= systemdate
                AND tex_code_exp >= systemdate
            )
    ) VULNERABLE ON VULNERABLE.thirdpartyid = INVCUS.id -- Identify Total Drawdown to date
    LEFT JOIN (
        SELECT scheduleid,
            SUM(assetDrawdownAmountAGR) AS drawdown_amount,
            SUM(assetDrawdownVatAGR) AS drawdown_vat,
            SUM(assetDepositAGR) AS drawdown_deposit
        FROM OdsAsset
            JOIN OdsSystemDate ON 1 = 1
        WHERE assetDrawdownDate <= systemdate
        GROUP BY scheduleid
    ) DRAWDOWN ON DRAWDOWN.scheduleid = SCHED.id -- Identify Asset of most value
    LEFT JOIN (
        SELECT scheduleid,
            assetTypeName,
            assetDescription
        FROM OdsAsset
        WHERE isMainAsset = '1'
    ) ASSET ON ASSET.scheduleid = SCHED.id
    LEFT JOIN (
        SELECT DLRSAL.dealerid,
            SAL.name AS "salesperson"
        FROM OdsSalesPerson SAL
            JOIN (
                SELECT dealerid,
                    MIN(salespersonid) AS "salesperson_id"
                FROM OdsDealerSalesperson
                GROUP BY dealerid
            ) DLRSAL ON SAL.id = DLRSAL.salesperson_id
    ) SALPER ON SALPER.dealerid = DLR.id
    LEFT JOIN (
        SELECT DLRBILSAL.dealerBillingId,
            SAL.name AS "salesperson"
        FROM OdsSalesPerson SAL
            JOIN (
                SELECT dealerBillingId,
                    MIN(salespersonid) AS "salesperson_id"
                FROM OdsDealerBillingSalesperson
                GROUP BY dealerBillingId
            ) DLRBILSAL ON SAL.id = DLRBILSAL.salesperson_id
    ) BILSALPER ON BILSALPER.dealerBillingId = DLRBIL.id -- Identify commission
    LEFT JOIN (
        SELECT PAYABLE.scheduleid,
            SUM(PAYABLE.amount) AS "commission"
        FROM OdsPayable PAYABLE
            JOIN OdsPayableChargeType PCT ON PCT.id = PAYABLE.payableChargeTypeId
            AND PCT.payableChargeTypeCode IN (605, 630)
        GROUP BY PAYABLE.scheduleid
    ) COMM ON COMM.scheduleid = SCHED.id -- Outstanding commission
    LEFT JOIN (
        SELECT INC.scheduleId,
            SUM(INC.expenseIncomeAccrued) AS "oscomm"
        FROM OdsScheduleExpenseIncome INC
            JOIN OdsExpenseIncomeChargeType INCTYP ON INCTYP.id = INC.expenseIncomeChargeTypeId
            AND INCTYP.expIncChargeTypeCode IN (605, 630)
        WHERE INC.isRecognised = false
        GROUP BY INC.scheduleid
    ) COMMOS ON COMMOS.scheduleid = SCHED.id
    LEFT JOIN (
        SELECT RECEIVABLE.scheduleid,
            SUM(RECEIVABLE.amount) AS "documentation_fee"
        FROM OdsReceivable RECEIVABLE
            JOIN OdsReceivableChargeType RCT ON RCT.id = RECEIVABLE.recvchargetypeid
            AND RCT.recvchargetypecode = 103
        GROUP BY RECEIVABLE.scheduleid
    ) DOCFEE ON DOCFEE.scheduleid = SCHED.id
    LEFT JOIN (
        SELECT RECEIVABLE.scheduleid,
            SUM(RECEIVABLE.amount) AS "option_to_purchase_fee"
        FROM OdsReceivable RECEIVABLE
            JOIN OdsReceivableChargeType RCT ON RCT.id = RECEIVABLE.recvchargetypeid
            AND RCT.recvchargetypecode = 13
        GROUP BY RECEIVABLE.scheduleid
    ) OTPFEE ON OTPFEE.scheduleid = SCHED.id
    LEFT JOIN (
        SELECT PAYABLE.scheduleid,
            SUM(PAYABLE.amount) AS "documentation_fee_share"
        FROM OdsPayable PAYABLE
            JOIN OdsPayableChargeType PCT ON PCT.id = PAYABLE.payableChargeTypeId
            AND PCT.payableChargeTypeCode = 614
        GROUP BY PAYABLE.scheduleid
    ) DOCFEESPLIT ON DOCFEESPLIT.scheduleid = SCHED.id -- Arrears - Unpaid and overdue invoices 0-30
    LEFT JOIN (
        SELECT INV.scheduleid,
            SUM(unpaidAmount) AS "zerothirty"
        FROM OdsInvoiceLine INV
            JOIN OdsSystemDate on 1 = 1
        WHERE unpaidAmount > 0
            AND isDue = '1'
            AND isPaid = '0'
            AND systemdate - invoiceReferenceDate >= 0
            AND systemdate - invoiceReferenceDate < 31
        GROUP BY scheduleid
    ) ARR30 ON ARR30.scheduleid = SCHED.id
    AND zerothirty > 0 -- Arrears - Unpaid and overdue invoices 31-60
    LEFT JOIN (
        SELECT INV.scheduleid,
            SUM(unpaidAmount) AS "thirtysixty"
        FROM OdsInvoiceLine INV
            JOIN OdsSystemDate on 1 = 1
        WHERE unpaidAmount > 0
            AND isDue = '1'
            AND isPaid = '0'
            AND systemdate - invoiceReferenceDate > 30
            AND systemdate - invoiceReferenceDate < 61
        GROUP BY scheduleid
    ) ARR60 ON ARR60.scheduleid = SCHED.id
    AND thirtysixty > 0 -- Arrears - Unpaid and overdue invoices 61-90
    LEFT JOIN (
        SELECT INV.scheduleid,
            SUM(unpaidAmount) AS "sixtyninety"
        FROM OdsInvoiceLine INV
            JOIN OdsSystemDate on 1 = 1
        WHERE unpaidAmount > 0
            AND isDue = '1'
            AND isPaid = '0'
            AND systemdate - invoiceReferenceDate > 60
            AND systemdate - invoiceReferenceDate < 91
        GROUP BY scheduleid
    ) ARR90 ON ARR90.scheduleid = SCHED.id
    AND sixtyninety > 0 -- Arrears - Unpaid and overdue invoices 90+
    LEFT JOIN (
        SELECT INV.scheduleid,
            SUM(unpaidAmount) AS "ninetyplus"
        FROM OdsInvoiceLine INV
            JOIN OdsSystemDate on 1 = 1
        WHERE unpaidAmount > 0
            AND isDue = '1'
            AND isPaid = '0'
            AND systemdate - invoiceReferenceDate > 90
        GROUP BY scheduleid
    ) ninetyplus ON ninetyplus.scheduleid = SCHED.id
    AND ninetyplus > 0 -- Last Cash Allocation date and amount - based on the scheduleid and linked to the invoicing customer
    LEFT JOIN (
        SELECT C.scheduleid,
            C.transactionEffectiveDate,
            SUM(C.totalamount) AS lastpayment
        FROM OdsCashAllocation C
            JOIN OdsInvoiceLine INV ON INV.id = C.invoiceLineId
            JOIN OdsInvoicingCustomer INVC ON INVC.id = INV.invCusId
            JOIN (
                SELECT scheduleid,
                    max(transactionEffectiveDate) AS lasttxndate
                FROM OdsCashAllocation
                WHERE transactionStatus != 'Expired'
                GROUP BY scheduleid
            ) LASTTXN ON LASTTXN.scheduleid = C.scheduleid
            AND LASTTXN.lasttxndate = C.transactionEffectiveDate
        GROUP BY C.scheduleid,
            C.transactionEffectiveDate
    ) LASTPMT ON LASTPMT.scheduleid = SCHED.id
    LEFT JOIN (
        SELECT CASHFLOW.scheduleid,
            SUM(CASHFLOW.profit) AS "interest_os_lessee"
        FROM OdsScheduleCashflow CASHFLOW
            JOIN OdsSystemDate ON 1 = 1
        WHERE CASHFLOW.isPrimaryAccountingStandard = false
            AND CASHFLOW.cashFlowEventDate > systemdate
        GROUP BY CASHFLOW.scheduleId
    ) INTEREST_LESSEE ON INTEREST_LESSEE.scheduleid = SCHED.id
    LEFT JOIN (
        SELECT CASHFLOW.scheduleid,
            SUM(CASHFLOW.profit) AS "interest_os_lessor"
        FROM OdsScheduleCashflow CASHFLOW
            JOIN OdsSystemDate ON 1 = 1
        WHERE CASHFLOW.isPrimaryAccountingStandard = true
            AND CASHFLOW.cashFlowEventDate > systemdate
        GROUP BY CASHFLOW.scheduleId
    ) INTEREST_LESSOR ON INTEREST_LESSOR.scheduleid = SCHED.id
    LEFT JOIN (
        SELECT CASHFLOW.scheduleid,
            CASHFLOW.capitaloutstanding
        FROM OdsScheduleCashflow CASHFLOW
        WHERE CASHFLOW.id IN (
                SELECT MAX(id)
                FROM OdsScheduleCashflow
                    JOIN OdsSystemDate ON 1 = 1
                WHERE cashFlowEventDate <= systemdate
                    AND isPrimaryAccountingStandard = true
                GROUP BY scheduleid
            )
    ) CAPOS_LESSOR ON CAPOS_LESSOR.scheduleid = SCHED.id
    LEFT JOIN (
        SELECT thirdpartyid,
            reference
        FROM OdsExternalSystemReference
        WHERE systemcode = 'ACR'
    ) EXTREF ON EXTREF.thirdpartyid = SCHED.invCusId
    LEFT JOIN (
        SELECT CAS.agreementid
        FROM OdsCase CAS
            JOIN OdsCurrentCaseStatus STAT ON STAT.id = CAS.currentStatusId
        WHERE CAS.status = 'Open'
            AND STAT.currentStatusTypeCode = 'NTU'
            AND STAT.currentStatusStatusCode = '004'
        GROUP BY CAS.agreementid
    ) NTU ON NTU.agreementid = AGR.id
    LEFT JOIN (
        SELECT CAS.agreementid
        FROM OdsCase CAS
            JOIN OdsCurrentCaseStatus STAT ON STAT.id = CAS.currentStatusId
        WHERE CAS.status = 'Open'
            AND STAT.currentStatusTypeCode = 'ORG'
        GROUP BY CAS.agreementid
    ) ORG ON ORG.agreementid = AGR.id
    LEFT JOIN (
        SELECT CAS.agreementid
        FROM OdsCase CAS
            JOIN OdsCurrentCaseStatus STAT ON STAT.id = CAS.currentStatusId
        WHERE STAT.currentStatusTypeCode = 'MIG'
        GROUP BY CAS.agreementid
    ) MIG ON MIG.agreementid = AGR.id
    LEFT JOIN (
        SELECT agreementId,
            mig,
            ppo,
            ntu,
            rls,
            cais
        FROM OdsAgreementAlert
    ) AGRALERT ON AGRALERT.agreementId = AGR.id
    LEFT JOIN (
        SELECT scheduleid,
            SUM(A.rentalaccrual - A.depreciation) as UnearnedIncome
        FROM OdsScheduleIncome A
        WHERE isPrimaryAccountingStandard = true
            and isRecognised = false
        GROUP BY scheduleid
    ) URINC ON SCHED.id = URINC.scheduleId
    LEFT JOIN (
        SELECT scheduleid,
            SUM(expenseIncomeAccrued) as DepositUnearned
        FROM OdsScheduleExpenseIncome A
            JOIN OdsExpenseIncomeChargeType B ON A.expenseIncomeChargeTypeId = B.id
        WHERE isRecognised = false
            AND expIncChargeTypeCode in (101, 102, 602)
        GROUP BY scheduleid
    ) URDEP ON SCHED.id = URDEP.scheduleId
    LEFT JOIN (
        SELECT scheduleid,
            SUM(amount) as NotDueDeposit
        FROM OdsReceivable A
            JOIN OdsReceivableChargeType B ON A.recvChargeTypeId = B.id
        WHERE isdue = false
            AND recvChargeTypeCode in (101, 102, 602)
        GROUP BY scheduleid
    ) NDDEP ON SCHED.id = NDDEP.scheduleId
    LEFT JOIN (
        SELECT agreementid
        FROM OdsSchedule
        WHERE terminationNumber > 1
        GROUP BY agreementid
    ) PARTIAL ON PARTIAL.agreementid = SCHED.agreementid
LEFT OUTER JOIN (select agreementId, 
                        guarantorId, 
                        securityTypeName
                 from odsSecurity 
                 where securityTypeName = 'Remarketing Agreement') SEC ON AGR.id = SEC.agreementId
left outer join odsGuarantor GUA on SEC.guarantorId = GUA.id
WHERE SCHED.schedulenumber > 0
    AND (
        SCHED.scheduleStatus = CASE
            WHEN :Live_Primary = true THEN 'Live (Primary)'
            ELSE ''
        END
        OR SCHED.scheduleStatus = CASE
            WHEN :Live_Secondary = true THEN 'Live (Secondary)'
            ELSE ''
        END
        OR SCHED.scheduleStatus = CASE
            WHEN :Terminated = true THEN 'Terminated'
            ELSE ''
        END
        OR SCHED.scheduleStatus = CASE
            WHEN :Matured = true THEN 'Matured'
            ELSE ''
        END
        OR SCHED.scheduleStatus = CASE
            WHEN :Proposal_Pending = true
            AND ORG.agreementid IS NOT Null
            AND AGRALERT.ntu = false
            AND AGRALERT.ppo != true THEN 'Proposal'
            ELSE ''
        END
        OR SCHED.scheduleStatus = CASE
            WHEN :Proposal_Pending = true
            AND AGRALERT.mig = true THEN 'Proposal'
            ELSE ''
        END
        OR SCHED.scheduleStatus = CASE
            WHEN :Proposal_Cancelled = true
            AND AGRALERT.ppo != true
            AND (
                ORG.agreementid IS Null
                OR (
                    ORG.agreementid IS Not Null
                    AND AGRALERT.ntu = true
                )
            ) THEN 'Proposal'
            ELSE ''
        END
        OR AGRALERT.ppo = CASE
            WHEN :Practical_DAC_Paid_Out = true THEN true
            ELSE null
        END
        AND AGRALERT.ppo = CASE
            WHEN :Practical_DAC_Paid_Out = true THEN true
            ELSE null
        END
    ) and agr.agreementnumber = 'A000021494'