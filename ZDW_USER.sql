-- ZDW_USER: ALFA User information
SELECT ALFAUSER.name AS "user_name",
    TRIM(ALFAUSER.code) AS "user code",
    DEPARTMENT.code AS "dept_code",
    DEPARTMENT.description AS "department",
    ALFAUSER.lastLoginDate AS "last_login_date",
    ALFAUSER.enabledFrom AS "enabled_from",
    ALFAUSER.disabledFrom AS "disabled_from",
    ALFAUSER.emailAddress AS "email"
FROM OdsAlfaUser ALFAUSER
    JOIN OdsDepartment DEPARTMENT ON ALFAUSER.departmentId = DEPARTMENT.id
WHERE ALFAUSER.isAlfaManaged = true
ORDER BY DEPARTMENT.code,
    ALFAUSER.name