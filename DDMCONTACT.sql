-- DDMCONTACT:  PT-X Integration Support v1.0
-- Title:       Mandate PT-X Integration - Obtain contact information 
-- Summary:     Provides details of the contact associated with a billing customer and is used to create contacts in PT-X.
--              Where a PT-X contact ID has already been stored in ALFA, this is returned also
-- Parameter:   agreement (String,20), thirdpartynumber (String,10), billingaddressnumber (String, 3) - either the agreement or third party details can be used to retrieve contact information
SELECT AGR.agreementnumber AS "agreementnumber",
    CASE
        WHEN :agreement IS NOT NULL THEN true
        WHEN ACTIVATED.active = true THEN true
        ELSE false
    END AS "active",
    INVCUS.invCusBillingThirdPartyNumber AS "thirdpartynumber",
    INVCUS.invCusBillingBillingNumber AS "billingaddressnumber",
    INVCUS.invCusBillingUniqueRef AS "reference",
    EXTREF.reference AS "ptxcontactid",
    CASE
        WHEN TPY.thirdPartyType IN ('IND', 'JOI') THEN 'PERSON'
        ELSE 'BUSINESS'
    END AS "type",
    LEFT(INVCUS.invCusBillingName, 60) AS "companyname",
    --  If the customer is an individual the name details can be obtained for the individual, and if a company from the billing contact
    --  For the billing contact 2 or 3 part names are accommodated assuming spaces as the delimiter
    CASE
        WHEN TPY.thirdPartyType = 'IND' THEN translate(INVCUS.invCusBillingTitle, '.', '')
        WHEN SPLIT_PART(INVCUS.invCusBillingContactName, ' ', 3) > ''
        AND SPLIT_PART(INVCUS.invCusBillingContactName, ' ', 1) IN (
            'Mr',
            'MR',
            'Mrs',
            'MRS',
            'Mx',
            'MX',
            'Ms',
            'MS',
            'Miss',
            'MISS',
            'Master',
            'MASTER',
            'Maid',
            'MAID',
            'Madam',
            'MADAM',
            'Dr',
            'DR'
        ) THEN SPLIT_PART(INVCUS.invCusBillingContactName, ' ', 1)
        ELSE ''
    END AS "title",
    CASE
        WHEN TPY.thirdPartyType = 'IND' THEN INVCUS.invCusBillingFirstName
        WHEN SPLIT_PART(INVCUS.invCusBillingContactName, ' ', 3) > ''
        AND SPLIT_PART(INVCUS.invCusBillingContactName, ' ', 1) NOT IN (
            'Mr',
            'MR',
            'Mrs',
            'MRS',
            'Mx',
            'MX',
            'Ms',
            'MS',
            'Miss',
            'MISS',
            'Master',
            'MASTER',
            'Maid',
            'MAID',
            'Madam',
            'MADAM',
            'Dr',
            'DR'
        ) THEN CONCAT(
            SPLIT_PART(INVCUS.invCusBillingContactName, ' ', 1),
            ' ',
            SPLIT_PART(INVCUS.invCusBillingContactName, ' ', 2)
        )
        WHEN SPLIT_PART(INVCUS.invCusBillingContactName, ' ', 3) > '' THEN SPLIT_PART(INVCUS.invCusBillingContactName, ' ', 2)
        WHEN SPLIT_PART(INVCUS.invCusBillingContactName, ' ', 2) > '' THEN SPLIT_PART(INVCUS.invCusBillingContactName, ' ', 1)
        ELSE ''
    END AS "firstname",
    CASE
        WHEN TPY.thirdPartyType = 'IND' THEN INVCUS.invCusBillingLastName
        WHEN SPLIT_PART(INVCUS.invCusBillingContactName, ' ', 3) > '' THEN SPLIT_PART(INVCUS.invCusBillingContactName, ' ', 3)
        WHEN SPLIT_PART(INVCUS.invCusBillingContactName, ' ', 2) > '' THEN SPLIT_PART(INVCUS.invCusBillingContactName, ' ', 2)
        ELSE SPLIT_PART(INVCUS.invCusBillingContactName, ' ', 1)
    END AS "lastname",
    INVCUS.invCusBillingEmailAddressEnc AS "email",
    CASE
        WHEN INVCUS.invCusBillingTelephoneEnc = '<None>' THEN INVCUS.invCusBillingMobileTelEnc
        ELSE INVCUS.invCusBillingTelephoneEnc
    END AS "phone",
    true AS "isattachmentused",
    'EMAIL' AS "communicationpref",
    LEFT(INVCUS.invCusBillingFullAddress, 60) AS "fulladdress",
    TRIM(
        SPLIT_PART(LEFT(INVCUS.invCusBillingFullAddress, 60), ',', 1)
    ) AS "address1",
    TRIM(
        SPLIT_PART(LEFT(INVCUS.invCusBillingFullAddress, 60), ',', 2)
    ) AS "address2",
    CASE
        WHEN SPLIT_PART(LEFT(INVCUS.invCusBillingFullAddress, 60), ',', 5) = ''
        AND SPLIT_PART(LEFT(INVCUS.invCusBillingFullAddress, 60), ',', 4) = '' THEN Null
        WHEN SPLIT_PART(LEFT(INVCUS.invCusBillingFullAddress, 60), ',', 5) = '' THEN Null
        ELSE TRIM(
            SPLIT_PART(
                SUBSTR(
                    TRIM(
                        SPLIT_PART(LEFT(INVCUS.invCusBillingFullAddress, 60), ',', 1)
                    ) || ',' || TRIM(
                        SPLIT_PART(LEFT(INVCUS.invCusBillingFullAddress, 60), ',', 2)
                    ) || ',' || TRIM(
                        SPLIT_PART(LEFT(INVCUS.invCusBillingFullAddress, 60), ',', 3)
                    ),
                    1,
                    58
                ),
                ',',
                3
            )
        )
    END AS "address3",
    CASE
        WHEN SPLIT_PART(LEFT(INVCUS.invCusBillingFullAddress, 60), ',', 5) = ''
        AND SPLIT_PART(LEFT(INVCUS.invCusBillingFullAddress, 60), ',', 4) = '' THEN Null
        WHEN SPLIT_PART(LEFT(INVCUS.invCusBillingFullAddress, 60), ',', 5) = '' THEN TRIM(
            SPLIT_PART(LEFT(INVCUS.invCusBillingFullAddress, 60), ',', 3)
        )
        ELSE TRIM(
            SPLIT_PART(LEFT(INVCUS.invCusBillingFullAddress, 60), ',', 4)
        )
    END AS "address4",
    CASE
        WHEN SPLIT_PART(LEFT(INVCUS.invCusBillingFullAddress, 60), ',', 5) = ''
        AND SPLIT_PART(LEFT(INVCUS.invCusBillingFullAddress, 60), ',', 4) = '' THEN TRIM(
            SPLIT_PART(LEFT(INVCUS.invCusBillingFullAddress, 60), ',', 3)
        )
        WHEN SPLIT_PART(LEFT(INVCUS.invCusBillingFullAddress, 60), ',', 5) = '' THEN TRIM(
            SPLIT_PART(LEFT(INVCUS.invCusBillingFullAddress, 60), ',', 4)
        )
        ELSE TRIM(
            SPLIT_PART(LEFT(INVCUS.invCusBillingFullAddress, 60), ',', 5)
        )
    END AS "address5",
    CASE
        WHEN INVCUS.invCusBillingPostalCode IS NULL THEN 'NONE'
        ELSE INVCUS.invCusBillingPostalCode
    END AS "postcode",
    INVCUS.invCusBillingCountryCode AS "countrycode",
    INVCUS.invCusBillingCountry AS "country"
FROM OdsInvoicingCustomerBilling INVCUS
    JOIN OdsAgreement AGR ON INVCUS.id = AGR.invCusBillingId
    JOIN OdsThirdParty TPY ON TPY.id = INVCUS.invCusBillingThirdPartyId
    JOIN (
        SELECT SCHED.agreementid,
            true AS "active"
        FROM OdsSchedule SCHED
            JOIN OdsScheduleRentalProfile PROF ON PROF.scheduleid = SCHED.id
            JOIN OdsRentalProfilePaymentType PMT ON PMT.id = PROF.rentalProfilePaymentTypeId
            AND PMT.rentPayTCode = 'DD'
        WHERE scheduleStatus IN ('Live (Primary)', 'Live (Secondary)','Matured')
        GROUP BY SCHED.agreementid
    ) ACTIVATED ON ACTIVATED.agreementid = AGR.id
    LEFT JOIN OdsExternalSystemReference EXTREF ON EXTREF.billingaddressid = AGR.invCusBillingId
    AND EXTREF.systemcode = 'PTXCO'
WHERE TRIM(AGR.agreementNumber) = TRIM(:agreement)
    OR (
        TRIM(INVCUS.invCusBillingThirdPartyNumber) = TRIM(:thirdpartynumber)
        AND INVCUS.invCusBillingBillingNumber = to_number(:billingaddressnumber, '999')
    )

UNION 

SELECT AGR.agreementnumber AS "agreementnumber",
    CASE
        WHEN :agreement IS NOT NULL THEN true
        WHEN ACTIVATED.active = true THEN true
        ELSE false
    END AS "active",
    INVCUS.invCusBillingThirdPartyNumber AS "thirdpartynumber",
    INVCUS.invCusBillingBillingNumber AS "billingaddressnumber",
    INVCUS.invCusBillingUniqueRef AS "reference",
    EXTREF.reference AS "ptxcontactid",
    CASE
        WHEN TPY.thirdPartyType IN ('IND', 'JOI') THEN 'PERSON'
        ELSE 'BUSINESS'
    END AS "type",
    LEFT(INVCUS.invCusBillingName, 60) AS "companyname",
    --  If the customer is an individual the name details can be obtained for the individual, and if a company from the billing contact
    --  For the billing contact 2 or 3 part names are accommodated assuming spaces as the delimiter
    CASE
        WHEN TPY.thirdPartyType = 'IND' THEN translate(INVCUS.invCusBillingTitle, '.', '')
        WHEN SPLIT_PART(INVCUS.invCusBillingContactName, ' ', 3) > ''
        AND SPLIT_PART(INVCUS.invCusBillingContactName, ' ', 1) IN (
            'Mr',
            'MR',
            'Mrs',
            'MRS',
            'Mx',
            'MX',
            'Ms',
            'MS',
            'Miss',
            'MISS',
            'Master',
            'MASTER',
            'Maid',
            'MAID',
            'Madam',
            'MADAM',
            'Dr',
            'DR'
        ) THEN SPLIT_PART(INVCUS.invCusBillingContactName, ' ', 1)
        ELSE ''
    END AS "title",
    CASE
        WHEN TPY.thirdPartyType = 'IND' THEN INVCUS.invCusBillingFirstName
        WHEN SPLIT_PART(INVCUS.invCusBillingContactName, ' ', 3) > ''
        AND SPLIT_PART(INVCUS.invCusBillingContactName, ' ', 1) NOT IN (
            'Mr',
            'MR',
            'Mrs',
            'MRS',
            'Mx',
            'MX',
            'Ms',
            'MS',
            'Miss',
            'MISS',
            'Master',
            'MASTER',
            'Maid',
            'MAID',
            'Madam',
            'MADAM',
            'Dr',
            'DR'
        ) THEN CONCAT(
            SPLIT_PART(INVCUS.invCusBillingContactName, ' ', 1),
            ' ',
            SPLIT_PART(INVCUS.invCusBillingContactName, ' ', 2)
        )
        WHEN SPLIT_PART(INVCUS.invCusBillingContactName, ' ', 3) > '' THEN SPLIT_PART(INVCUS.invCusBillingContactName, ' ', 2)
        WHEN SPLIT_PART(INVCUS.invCusBillingContactName, ' ', 2) > '' THEN SPLIT_PART(INVCUS.invCusBillingContactName, ' ', 1)
        ELSE ''
    END AS "firstname",
    CASE
        WHEN TPY.thirdPartyType = 'IND' THEN INVCUS.invCusBillingLastName
        WHEN SPLIT_PART(INVCUS.invCusBillingContactName, ' ', 3) > '' THEN SPLIT_PART(INVCUS.invCusBillingContactName, ' ', 3)
        WHEN SPLIT_PART(INVCUS.invCusBillingContactName, ' ', 2) > '' THEN SPLIT_PART(INVCUS.invCusBillingContactName, ' ', 2)
        ELSE SPLIT_PART(INVCUS.invCusBillingContactName, ' ', 1)
    END AS "lastname",
    INVCUS.invCusBillingEmailAddressEnc AS "email",
    CASE
        WHEN INVCUS.invCusBillingTelephoneEnc = '<None>' THEN INVCUS.invCusBillingMobileTelEnc
        ELSE INVCUS.invCusBillingTelephoneEnc
    END AS "phone",
    true AS "isattachmentused",
    'EMAIL' AS "communicationpref",
    LEFT(INVCUS.invCusBillingFullAddress, 60) AS "fulladdress",
    TRIM(
        SPLIT_PART(LEFT(INVCUS.invCusBillingFullAddress, 60), ',', 1)
    ) AS "address1",
    TRIM(
        SPLIT_PART(LEFT(INVCUS.invCusBillingFullAddress, 60), ',', 2)
    ) AS "address2",
    CASE
        WHEN SPLIT_PART(LEFT(INVCUS.invCusBillingFullAddress, 60), ',', 5) = ''
        AND SPLIT_PART(LEFT(INVCUS.invCusBillingFullAddress, 60), ',', 4) = '' THEN Null
        WHEN SPLIT_PART(LEFT(INVCUS.invCusBillingFullAddress, 60), ',', 5) = '' THEN Null
        ELSE TRIM(
            SPLIT_PART(
                SUBSTR(
                    TRIM(
                        SPLIT_PART(LEFT(INVCUS.invCusBillingFullAddress, 60), ',', 1)
                    ) || ',' || TRIM(
                        SPLIT_PART(LEFT(INVCUS.invCusBillingFullAddress, 60), ',', 2)
                    ) || ',' || TRIM(
                        SPLIT_PART(LEFT(INVCUS.invCusBillingFullAddress, 60), ',', 3)
                    ),
                    1,
                    58
                ),
                ',',
                3
            )
        )
    END AS "address3",
    CASE
        WHEN SPLIT_PART(LEFT(INVCUS.invCusBillingFullAddress, 60), ',', 5) = ''
        AND SPLIT_PART(LEFT(INVCUS.invCusBillingFullAddress, 60), ',', 4) = '' THEN Null
        WHEN SPLIT_PART(LEFT(INVCUS.invCusBillingFullAddress, 60), ',', 5) = '' THEN TRIM(
            SPLIT_PART(LEFT(INVCUS.invCusBillingFullAddress, 60), ',', 3)
        )
        ELSE TRIM(
            SPLIT_PART(LEFT(INVCUS.invCusBillingFullAddress, 60), ',', 4)
        )
    END AS "address4",
    CASE
        WHEN SPLIT_PART(LEFT(INVCUS.invCusBillingFullAddress, 60), ',', 5) = ''
        AND SPLIT_PART(LEFT(INVCUS.invCusBillingFullAddress, 60), ',', 4) = '' THEN TRIM(
            SPLIT_PART(LEFT(INVCUS.invCusBillingFullAddress, 60), ',', 3)
        )
        WHEN SPLIT_PART(LEFT(INVCUS.invCusBillingFullAddress, 60), ',', 5) = '' THEN TRIM(
            SPLIT_PART(LEFT(INVCUS.invCusBillingFullAddress, 60), ',', 4)
        )
        ELSE TRIM(
            SPLIT_PART(LEFT(INVCUS.invCusBillingFullAddress, 60), ',', 5)
        )
    END AS "address5",
    CASE
        WHEN INVCUS.invCusBillingPostalCode IS NULL THEN 'NONE'
        ELSE INVCUS.invCusBillingPostalCode
    END AS "postcode",
    INVCUS.invCusBillingCountryCode AS "countrycode",
    INVCUS.invCusBillingCountry AS "country"
FROM OdsInvoicingCustomerBilling INVCUS
    JOIN OdsAgreement AGR ON INVCUS.id = AGR.invCusBillingId
    JOIN OdsThirdParty TPY ON TPY.id = INVCUS.invCusBillingThirdPartyId
    JOIN (
        SELECT SCHED.agreementid,
            true AS "active"
        FROM OdsSchedule SCHED
            JOIN OdsScheduleRentalProfile PROF ON PROF.scheduleid = SCHED.id
            JOIN OdsRentalProfilePaymentType PMT ON PMT.id = PROF.rentalProfilePaymentTypeId
            AND PMT.rentPayTCode = 'BT'
        WHERE scheduleStatus IN ('Terminated')
        GROUP BY SCHED.agreementid
    ) ACTIVATED ON ACTIVATED.agreementid = AGR.id
    LEFT JOIN OdsExternalSystemReference EXTREF ON EXTREF.billingaddressid = AGR.invCusBillingId
    AND EXTREF.systemcode = 'PTXCO'
WHERE TRIM(AGR.agreementNumber) = TRIM(:agreement)
    OR (
        TRIM(INVCUS.invCusBillingThirdPartyNumber) = TRIM(:thirdpartynumber)
        AND INVCUS.invCusBillingBillingNumber = to_number(:billingaddressnumber, '999')
    )
LIMIT 1