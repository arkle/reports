SELECT "DATTA"."Third_Party_Number",
       "DATTA"."Third_Party_Name",
       SUM("DATTA"."Capital_Outstanding") AS "Total_Capital_Outstanding",
      CASE 
          WHEN "Invoicing_Cus_Name" = "Third_Party_Name" THEN 'Invoicing_Customer' 
          WHEN "Exposure_Cus_Name" = "Third_Party_Name" THEN 'Exposure_Customer'
          WHEN "Dealer_Name" = "Third_Party_Number" THEN 'Dealer'
          ELSE 'N/A'
      END AS "Agreement_Relationship"
FROM
(SELECT 
    TPT.thirdpartynumber AS "Third_Party_Number", 
    TPT.name AS "Third_Party_Name",
    INV.invcusName AS "Invoicing_Cus_Name",
    EXP.ExpcusName AS "Exposure_Cus_Name", 
    DLR.dealerName AS "Dealer_Name", 
    SCH.capitaloutstanding AS "Capital_Outstanding"
    FROM OdsThirdParty TPT 
    INNER JOIN OdsThirdpartyalert TPA ON TPT.Id = TPA.thirdpartyId
    INNER JOIN OdsAgreement AGR ON TPT.Id = AGR.invcusId
    LEFT OUTER JOIN OdsinvoicingCustomer INV ON AGR.invcusId = INV.id
    LEFT OUTER JOIN OdsExposureCustomer EXP ON AGR.expcusId = EXP.id
    LEFT OUTER JOIN Odsdealer DLR ON AGR.dealerId = DLR.id
    LEFT OUTER JOIN OdsSchedule SCH ON AGR.Id = SCH.agreementId
    WHERE TPA.DNU = TRUE) AS "DATTA"
    GROUP BY "DATTA"."Third_Party_Number", "DATTA"."Third_Party_Name", "Agreement_Relationship"
    ORDER BY 1
   