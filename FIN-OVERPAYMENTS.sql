WITH inv AS (SELECT OIL.agreementId, OIL.totalLineAmount FROM odsInvoiceLine OIL WHERE OIL.isDue = 'Yes'),
     sched AS (SELECT DISTINCT OS.agreementId, OS.scheduleStatus, SUM(OS.totalReceipts) AS "Total Receipts" FROM odsSchedule OS GROUP BY agreementId, scheduleStatus)
SELECT * 
FROM (
	SELECT DISTINCT OA.agreementNumber AS "Agreement Number", 
                	sched.scheduleStatus AS "Contract Status",
                	"Total Receipts", 
                	SUM(inv.totalLineAmount) AS "Total Invoiced", 
                	"Total Receipts"-SUM(inv.totalLineAmount) AS "Amount Overpaid"
	FROM odsAgreement OA
	LEFT OUTER JOIN inv on OA.id = inv.agreementId
	LEFT OUTER JOIN sched on OA.id = sched.agreementId
	WHERE "Total Receipts" > 0
	GROUP BY OA.agreementNumber, "Total Receipts", scheduleStatus) data
WHERE "Amount Overpaid" > 0
ORDER BY 1 ASC;