-- SFOPPORTUNITY:   Salesforce Integration - Opportunity Information 
-- Summary:         **INTEGRATION USE ONLY - DO NOT RUN MANUALLY** SALESFORCE OPPORTUNITY INTEGRATION
--                  Provides data points required for upsert of Opportunity object in Salesforce
-- Parameter:       agreementnumber (String,20)
SELECT EXPBILL.expCusBillingUniqueRef AS "ACTOR_CODE",
    SCHED.annualPercentageRate AS "APR",
    ROUND(DOCFEE.doc_fee, 2) AS "ADMIN_FEE_EXC_VAT__C",
    ROUND(OTPFEE.otp_fee, 2) AS "END_OF_LEASE_AMOUNT__C",
    ROUND(SCHED.totalAssetCostFinanced, 2) AS "AMOUNT",
    CASE
        WHEN CREDIT.decisionstatus = 'Approved' THEN CREDIT.decisionDate
        ELSE Null
    END AS "APPROVED_DATE__C",
    CASE
        WHEN AGRCO.agrcompanycode = '1' THEN 'CORE'
        WHEN AGRCO.agrcompanycode = '3' THEN 'PRIME'
        ELSE 'CONSUMER'
    END AS "PRICING",
    ASSET.assetClassificationName AS "ASSET_CATEGORY__C",
    ASSET.assetDescription AS "ASSET_DESCRIPTION__C",
    ROUND(SCHED.totalAssetCost, 2) AS "ASSET_VALUE__C",
    CASE
        WHEN AGRCO.agrcompanycode IN ('2', '4') THEN 'Evington'
        ELSE 'Arkle'
    END AS "BOOK__C",
    CASE
        WHEN MISC.PCOMPCode = 'CORE' THEN 'CORE'
        WHEN MISC.PCOMPCode = 'PRIM' THEN 'PRIME'
        WHEN MISC.PCOMPCode = 'EVIN' THEN 'CONSUMER'
        WHEN MISC.PCOMPCode = '<None>' AND AGRCO.agrcompanycode = '1' THEN 'CORE'
        WHEN MISC.PCOMPCode = '<None>' AND AGRCO.agrcompanycode = '3' THEN 'PRIME'
        WHEN MISC.PCOMPCode = '<None>' AND AGRCO.agrcompanycode NOT IN ('1','3') THEN 'CONSUMER'
        ELSE ''
    END AS "Pricing_Company__c",
    CASE
        WHEN DLRBIL.dealerBillingUniqueRef = '<None>' THEN Null
        ELSE DLRBIL.dealerBillingUniqueRef
    END AS "BROKER__C",
    ROUND(COMM.commission, 2) AS "COMMISSION__C",
    CREDIT.approvalexpirydate AS "EXPIRY_DATE__C",
    CASE
        WHEN AGR.invCusId != AGR.expCusId THEN CONCAT(
            LEFT(SCHED.scheduleDescription, 66),
            ' (Undisclosed)'
        )
        ELSE LEFT(SCHED.scheduleDescription, 80)
    END AS "NAME",
    SCHED.combinedLessorPtoRate AS "NET_YIELD__C",
    TRIM(AGR.product) AS "PRODUCT_TYPE__C",
    CASE
        WHEN MISC.rntpddesc = '<None>' THEN Null
        ELSE MISC.rntpddesc
    END AS "PROFILE__C",
    AGR.agreementnumber AS "PROPOSAL_NUMBER__C",
    CASE
        WHEN DLRBIL.dealerBillingUniqueRef <> '<None>' THEN '0123z000000KtbYAAS'
        ELSE '0123z000000KtbdAAC'
    END AS "RECORDTYPEID",
    CASE
        WHEN AGRALERT.mig = '1' THEN 'Paid-Out'
        WHEN SCHED.scheduleStatus IN (
            'Live (Primary)',
            'Live (Secondary)',
            'Terminated',
            'Matured'
        ) THEN 'Paid-Out'
        WHEN AGRALERT.ppo = '1' THEN 'Paid-Out'
        WHEN CASESTAT.status = 'Closed'
        AND SCHED.scheduleStatus = 'Proposal'
        AND MISC.ntu_reasondesc <> '<None>' THEN 'NTU'
        WHEN CASESTAT.status = 'Closed'
        AND CREDIT.decisionStatus = 'Approved'
        AND CREDIT.approvalExpiryDate < systemdate THEN 'NTU'
        WHEN CASESTAT.status = 'Closed'
        AND CREDIT.decisionStatus = 'Approved' THEN 'Approved'
        WHEN CASESTAT.status = 'Closed'
        AND CREDIT.decisionStatus IN ('Not Approved', 'Rejected') THEN 'Declined'
        WHEN CASESTAT.status = 'Closed'
        AND SCHED.scheduleStatus = 'Proposal' THEN 'Cancelled'
        WHEN NTU_HOLDING.agreementid IS NOT NULL THEN 'NTU'
        WHEN CASESTAT.currentStatusStatusCode = '006' THEN 'Cancelled'
        WHEN CASESTAT.currentStatusStatusCode IN (
            '008',
            '009',
            '010',
            '011',
            '012',
            '013',
            '014',
            '015',
            '044',
            '054',
            '055',
            '056',
            '070',
            '075'
        ) THEN 'Awaiting Decision'
        WHEN CASESTAT.currentStatusStatusCode IN ('002', '003', '016', '045', '050') THEN 'More Info Requested'
        WHEN CASESTAT.currentStatusStatusCode IN ('025') THEN 'Documents Received'
        WHEN CASESTAT.currentStatusStatusCode IN ('028', '029', '031', '032', '047', '048', '049') THEN 'Documents Incomplete'
        WHEN CASESTAT.currentStatusStatusCode IN ('027', '033', '034', '035', '036', '038') THEN 'Documents Complete'
        WHEN CASESTAT.currentStatusStatusCode IN ('037', '039', '040', '041', '042', '052', '058') THEN 'Paid-Out'
        WHEN CASESTAT.currentStatusStatusCode IN ('006', '051', '053') THEN 'Cancelled'
        WHEN CASESTAT.currentStatusStatusCode IN ('017', '021', '030', '043', '046') THEN 'Declined'
        WHEN CASESTAT.currentStatusStatusCode IN ('018', '019', '020', '022', '023', '024') THEN 'Approved'
        WHEN CASESTAT.currentStatusStatusCode IN ('76') THEN 'Stage Payment holding'
        WHEN CREDIT.decisionStatus = 'Approved' THEN 'Approved'
        WHEN CREDIT.decisionStatus IN ('Not Approved', 'Rejected') THEN 'Declined'
        ELSE 'Proposal'
    END AS "STAGENAME",
    CASE
        WHEN CASESTAT.status = 'Closed'
        AND SCHED.scheduleStatus = 'Proposal'
        AND MISC.ntu_reasondesc <> '<None>' THEN MISC.ntu_reasondesc
        WHEN NTU_HOLDING.agreementid IS NOT NULL
        AND MISC.ntu_reasondesc <> '<None>' THEN MISC.ntu_reasondesc
        WHEN CASESTAT.status = 'Closed'
        AND SCHED.scheduleStatus = 'Proposal'
        AND CREDIT.approvalExpiryDate < systemdate THEN 'Expired'
        ELSE ''
    END AS "NTU_REASON__C",
    CASE
        WHEN AGRALERT.mig = '1' THEN SCHED.startDate
        WHEN SCHED.scheduleStatus IN (
            'Live (Primary)',
            'Live (Secondary)',
            'Terminated',
            'Matured'
        ) THEN SCHED.activationDate
        WHEN AGRALERT.ppo = '1' THEN SCHED.startDate
        WHEN CASESTAT.currentStatusStatusCode IN (
            '025',
            '028',
            '029',
            '031',
            '032',
            '047',
            '048',
            '049',
            '027',
            '033',
            '034',
            '035',
            '036',
            '038',
            '76'
        ) THEN Null
        WHEN CASESTAT.currentStatusStatusCode IN ('008', '055','070','075') THEN systemDate + integer '90'
        WHEN CASESTAT.currentStatusStatusCode IN ('002', '003', '016', '045') THEN CASESTAT.lastActionDate + integer '90'
        WHEN CASESTAT.currentStatusStatusCode IN ('037') THEN SCHED.activationDate
        WHEN CASESTAT.currentStatusStatusCode IN ('006') THEN CASESTAT.lastActionDate
        WHEN CREDIT.decisionStatus IN ('Not Approved', 'Rejected') THEN CREDIT.decisionDate
        WHEN CREDIT.decisionStatus IN ('Approved') THEN CREDIT.approvalExpiryDate
        WHEN SCHED.scheduleStatus = 'Proposal' THEN systemdate + integer '90'
        ELSE Null
    END AS "CLOSEDATE",
    CASE
        WHEN ROUND(SCHED.termInMonths / 12, 0) = 0 THEN 1
        ELSE ROUND(SCHED.termInMonths / 12, 0)
    END AS "TERM_YEARS__C",
    CASE
        WHEN LIVE.non_props > 0 THEN 'Existing'
        ELSE 'New Business'
    END AS "TYPE",
    CASE 
         WHEN AGR.dealerReference = '<None>' THEN '' ELSE AGR.dealerReference END AS "Dealer_Reference"
FROM OdsAgreement AGR
    LEFT JOIN OdsAgreementAlert AGRALERT ON AGRALERT.agreementid = AGR.id
    JOIN OdsExposureCustomerBilling EXPBILL ON EXPBILL.id = AGR.expCusBillingId
    JOIN OdsSchedule SCHED ON SCHED.agreementid = AGR.id
    JOIN OdsAgreementCompany AGRCO ON AGRCO.id = SCHED.agrCompanyId
    JOIN OdsDealer DLR ON DLR.id = SCHED.dealerId
    JOIN OdsDealerBilling DLRBIL ON DLRBIL.id = SCHED.dealerBillingId
    LEFT JOIN (
        SELECT thirdpartyid,
            reference -- External customer reference - Broker Actor Code
        FROM OdsExternalSystemReference
        WHERE systemcode = 'ACR'
    ) BRKR ON BRKR.thirdpartyid = AGR.dealerId
    LEFT JOIN (
        SELECT scheduleid,
            assetTypeName,
            assetClassificationName,
            assetTypeCode,
            assetDescription -- Identify Asset of most value
        FROM OdsAsset
        WHERE isMainAsset = '1'
    ) ASSET ON ASSET.scheduleid = SCHED.id
    LEFT JOIN (
        SELECT PAYABLE.scheduleid,
            SUM(PAYABLE.amount) AS "commission" -- Identify commission
        FROM OdsPayable PAYABLE
            JOIN OdsPayableChargeType PCT ON PCT.id = PAYABLE.payableChargeTypeId
            AND PCT.payableChargeTypeCode = 605
        GROUP BY PAYABLE.scheduleid
    ) COMM ON COMM.scheduleid = SCHED.id
    LEFT JOIN (
        SELECT RECEIVABLE.scheduleid,
            SUM(RECEIVABLE.amount) AS "doc_fee" -- Identify document fee
        FROM OdsReceivable RECEIVABLE
            JOIN OdsReceivableChargeType RCT ON RCT.id = RECEIVABLE.recvchargetypeid
            AND RCT.recvchargetypecode = 103
        GROUP BY RECEIVABLE.scheduleid
    ) DOCFEE ON DOCFEE.scheduleid = SCHED.id
    LEFT JOIN (
        SELECT RECEIVABLE.scheduleid,
            SUM(RECEIVABLE.amount) AS "otp_fee" -- Identify Option to Purchase fee
        FROM OdsReceivable RECEIVABLE
            JOIN OdsReceivableChargeType RCT ON RCT.id = RECEIVABLE.recvchargetypeid
            AND RCT.recvchargetypecode = 13
        GROUP BY RECEIVABLE.scheduleid
    ) OTPFEE ON OTPFEE.scheduleid = SCHED.id
    LEFT JOIN OdsAgreementCreditDecision CREDIT ON CREDIT.agreementId = AGR.id
    LEFT JOIN (
        SELECT id,
            COUNT(id) AS "non_props"
        FROM OdsSchedule
        WHERE scheduleStatus != 'Proposal'
        GROUP BY id
    ) LIVE ON LIVE.id = SCHED.id
    LEFT JOIN (
        SELECT CAS.agreementid,
            CAS.lastActionDate,
            CAS.status,
            CSTAT.currentStatusStatusCode --  Identify latest New Business Workflow Case
        FROM OdsCase CAS
            JOIN OdsCurrentCaseStatus CSTAT ON CSTAT.id = CAS.currentStatusId
        WHERE CAS.id IN (
                SELECT max(OdsCase.id)
                FROM OdsCase
                    JOIN OdsCurrentCaseStatus ON OdsCurrentCaseStatus.id = OdsCase.currentStatusId
                    AND OdsCurrentCaseStatus.currentStatusTypeCode = 'ORG'
                GROUP BY agreementid
            )
    ) CASESTAT ON CASESTAT.agreementid = AGR.id
    LEFT JOIN (
        SELECT CAS.agreementid
        FROM OdsCase CAS
            JOIN OdsCurrentCaseStatus STAT ON STAT.id = CAS.currentStatusId
        WHERE CAS.status = 'Open'
            AND STAT.currentStatusTypeCode = 'NTU'
            AND STAT.currentStatusStatusCode = '004'
        GROUP BY CAS.agreementid
    ) NTU_HOLDING ON NTU_HOLDING.agreementid = AGR.id
    LEFT JOIN OdsAgreementMiscellaneous MISC ON MISC.agreementid = AGR.id
    JOIN OdsSystemDate ON 1 = 1
WHERE TRIM(AGR.agreementnumber) = TRIM(:agreementnumber)