-- CHARGES: Charges by Invoice and Customer
-- SUMMARY: Provides details of a selected charge for a specified date range
-- PARAMETERS: Charge (Sting), Invoice_from (Date), Invoice_to (Date)
SELECT 
    COMPANY.invCompanyName AS "Invoice_Company",
    INVHDR.invoiceDueDate AS "Due_Date",
    INVBILL.inv3pyBillingUniqueRef AS "Billing_TPY_Ref",
    INVBILL.inv3pyBillingName AS "Billing_TPY_Name",
    AGR.agreementNumber,
    CASE WHEN TPYALERT.TEX_CODE IS true THEN 'Y' ELSE '' END AS "Vulnerable",
    INVHDR.formattedInvoiceNumber AS "Invoice#",
    INVLINE.invoiceLineNumber AS "Invoice_Line#",
    INVLINE.invoiceExtractSource AS "Triggered_By",
    INVLINE.invoiceLineAmount AS "Amount",
    INVLINE.invoiceVATAmount AS "VAT",
    INVLINE.totalLineAmount AS "Total",
    CASE
        WHEN INVLINE.isDue = true THEN 'Y'
        ELSE 'N'
    END AS "Due?",
    CASE
        WHEN INVLINE.isPaid = true THEN 'Y'
        ELSE 'N'
    END AS "Paid?",
    INVLINE.effectiveDateFullyPaid AS "Date_Paid",
    INVLINE.unpaidAmount AS "Unpaid_Amount",
    INVLINE.writtenOffAmount AS "Written_Off",
    CHARGE.direction AS "Direction",
    CHARGE.groupName AS "Charge_Group",
    CHARGE.code AS "Charge_Code",
    CHARGE.name AS "Charge_Name",
    CHARGE.incomeRecogn AS "Income_Recognition_Method",
    CASE
        WHEN AGR.isCcaRegulated = true THEN 'Y'
        ELSE 'N'
    END AS "Regulated?"
FROM OdsInvoiceLine INVLINE
    JOIN OdsInvoiceHeader INVHDR ON INVHDR.id = INVLINE.invoiceHeaderId
    JOIN OdsInvoicingCompany COMPANY ON COMPANY.id = INVHDR.invCompanyId
    JOIN OdsInvoicingThirdPartyBilling INVBILL ON INVBILL.id = INVHDR.inv3pyBillingId
    LEFT JOIN OdsThirdPartyAlert TPYALERT ON TPYALERT.thirdPartyId = INVBILL.id
    JOIN OdsChargeType CHARGE ON CHARGE.id = INVLINE.chargeTypeId
    JOIN OdsChargeReason REASON ON REASON.id = INVLINE.chargeReasonId
    JOIN OdsDealerBilling BRKR ON BRKR.id = INVLINE.dealerBillingId
    JOIN OdsInvoicePaymentType PMTTYPE ON PMTTYPE.id = INVHDR.invPaymentTypeId
    JOIN OdsAgreement AGR on INVLINE.agreementId = AGR.id
WHERE to_char(CHARGE.code, '000') = left(:Charge,4)
    AND (
        INVHDR.invoiceDueDate >= to_date(:Invoice_from, 'YYYY-MM-DD')
        AND INVHDR.invoiceDueDate <= to_date(:Invoice_to, 'YYYY-MM-DD') )
ORDER BY INVBILL.inv3pyBillingUniqueRef, INVHDR.invoiceDueDate