-- LRGPMT:     	Large Cash Payments Report v1.0
-- Title:       Large Cash Payments
-- Summary:     Identify large Cash or Card Payments for AML scrutiny > £5000
-- Set-up to run daily at EOD

SELECT 
	-- CashInOut    
    CSH.settlementDate          AS "Date",    
    CSH.alfaIdentifier          AS "Transaction_id",
    CSH.processingStatus        AS "Status",
    CSH.amount					AS "Amount",
    
    -- Payer 
    BILLADD.thirdPartyNumber 	AS "Billing_Cust_ref",
    BILLADD.name                AS "Billing_Customer",
    CASE WHEN VULNERABLE.thirdpartyid > 0 THEN 'Y' ELSE '' END
                                            AS "Vulnerable",
                                            
    -- Cash Allocation    
    CTX.status           	    AS "Allocation_status",
    CTX.reasoncode              AS "Reason",
    cshAllcPaymentTypeCode      AS "Payment_Type_Code",
    cshAllcPaymentTypeName      AS "Payment_Type",	
    TRANTYPE.transactionName    AS "Transaction_type",
	CASHCO.cshAllcCompanyName   AS "Cash_allocation_company",
	CAL.locationType			AS "Allocation_target",
    CAL.totalAmount             AS "Allocated_amount",
    ALFAUSER.name				AS "Input_By",

	-- Agreement info
    AGR.agreementNumber      	AS "Schedule_Reference",
    AGR.agreementType           AS "Agreement_type",
    SCHED.startDate             AS "Start_date",
    SCHED.maturityDate          AS "Maturity_date",
    SCHED.terminationDate       AS "Termination_date",
    SCHED.totalExposure         AS "Total_Exposure",
    SCHED.grossBalanceRemaining AS "Balance_OS",    
    SCHED.capitalOutstanding    AS "Capital_OS",
    AGR.agreementSuspense       AS "Suspense",
    SCHED.totalReceipts         AS "Total_receipts",
    SCHED.futureReceivables     AS "Future_receivables",    
	SCHED.totalScheduleArrears  AS "Schedule_arrears",
    SCHED.totalCustomerArrears  AS "Customer_arrears",
    SCHED.inArrearsSinceDate    AS "In_Arrears_since",
    zerothirty                  AS "0-30",
    thirtysixty                 AS "31-60",
    sixtyninety                 AS "61-90",
    ninetyplus                  AS "91P",

    --  Next rental info
    SCHED.numberOfFutureRentals             AS "Future_rentals",
    SCHED.nextRentalDate                    AS "Next_rental_date",
    SCHED.nextRentalFrequency               AS "Next_rental_freq",
    SCHED.nextRentalAmount                  AS "Next_rental_amount",
    
    -- Most recent open case
    to_char(CASEINFO.caseidsequencenumber,'999999999') AS "Case_ID",
    CASEINFO.currentStatusTypeName      AS "Case_type",
    CASEINFO.currentstatusstatusname    AS "Case_status",
    CASEINFO.department                 AS "Department",  
    CASEINFO.allocatedtousername        AS "Allocated_to",
    CASEINFO.openeddate                 AS "Case_opened",
    CASEINFO.lastactiondate             AS "Case_last_action"

FROM OdsCashInOut CSH 
    LEFT JOIN OdsCashAllocationTxn CTX
        ON CTX.cashid = CSH.id
    LEFT JOIN OdsTransactionType  TRANTYPE 
        ON TRANTYPE.id = CTX.inputTxnTypeId
	LEFT JOIN OdsCashAllocation CAL
		ON CAL.cashAllocationTransactionId = CTX.id
    LEFT JOIN OdsBillingAddress BILLADD 
        ON BILLADD.id = CAL.billingAddressId
    LEFT JOIN OdsCashAllocationCompany CASHCO 
        ON CASHCO.id = CAL.companyId
	LEFT JOIN OdsSchedule SCHED  
		ON SCHED.id = CAL.scheduleid 
        AND SCHED.jurisdictioncode = 'UK'   -- Avoid picking up *non* schedules
	LEFT JOIN OdsAlfaUser ALFAUSER 
		ON ALFAUSER.id = CTX.inputByUserId
	LEFT JOIN OdsAgreement AGR
		ON CAL.agreementId = AGR.id

-- Identify Third Parties with a Vulnerable Customer Alert
    LEFT JOIN ( SELECT thirdpartyid 
                FROM OdsThirdPartyAlert
				JOIN OdsSystemDate ON 1 = 1 
                WHERE tex_code = '1' AND (tex_code_start <= systemdate AND tex_code_exp >= systemdate) ) VULNERABLE  
                ON VULNERABLE.thirdpartyid = CAL.billingAddressId
-- Arrears - Unpaid and overdue invoices 0-30
    LEFT JOIN ( SELECT INV.scheduleid, SUM(unpaidAmount) AS "zerothirty"
                FROM OdsInvoiceLine INV 
                JOIN OdsSystemDate on 1 = 1
                WHERE unpaidAmount > 0
                    AND isDue = '1'
                    AND isPaid = '0'
                    AND systemdate - invoiceReferenceDate < 31
                GROUP BY scheduleid) ARR30 
                ON ARR30.scheduleid = SCHED.id
                AND zerothirty > 0 
-- Arrears - Unpaid and overdue invoices 31-60
    LEFT JOIN ( SELECT INV.scheduleid, SUM(unpaidAmount) AS "thirtysixty"
                FROM OdsInvoiceLine INV 
                JOIN OdsSystemDate on 1 = 1
                WHERE unpaidAmount > 0
                    AND isDue = '1'
                    AND isPaid = '0'
                    AND systemdate - invoiceReferenceDate > 30
                    AND systemdate - invoiceReferenceDate < 61
                GROUP BY scheduleid) ARR60 
                ON ARR60.scheduleid = SCHED.id
                AND thirtysixty > 0
-- Arrears - Unpaid and overdue invoices 61-90
    LEFT JOIN ( SELECT INV.scheduleid, SUM(unpaidAmount) AS "sixtyninety"
                FROM OdsInvoiceLine INV 
                JOIN OdsSystemDate on 1 = 1
                WHERE unpaidAmount > 0
                    AND isDue = '1'
                    AND isPaid = '0'
                    AND systemdate - invoiceReferenceDate > 60
                    AND systemdate - invoiceReferenceDate < 91
                GROUP BY scheduleid) ARR90 
                ON ARR90.scheduleid = SCHED.id
                AND sixtyninety > 0
-- Arrears - Unpaid and overdue invoices 90+
    LEFT JOIN ( SELECT INV.scheduleid, SUM(unpaidAmount) AS "ninetyplus"
                FROM OdsInvoiceLine INV 
                JOIN OdsSystemDate on 1 = 1
                WHERE unpaidAmount > 0
                    AND isDue = '1'
                    AND isPaid = '0'
                    AND systemdate - invoiceReferenceDate > 90
                GROUP BY scheduleid) ninetyplus
                ON ninetyplus.scheduleid = SCHED.id
                AND ninetyplus > 0 

-- Most recent open case associated with the payer billing 3P
    LEFT JOIN ( SELECT  CAS.case3pyId,
                        CAS.scheduleid,
                        CAS.caseidsequencenumber,  
                        CAS.status,    
                        STAT.currentStatusTypeName,    
                        STAT.currentstatusstatusname,  
                        DEP.description AS department,    
                        ALLOC.allocatedtousername,   
                        CAS.openeddate,               
                        CAS.lastactiondate       
                FROM OdsCase CAS
                    JOIN OdsCurrentCaseStatus STAT 
                        ON STAT.id = CAS.currentstatusid
                    JOIN OdsAllocatedToUser ALLOC 
                        ON ALLOC.id = CAS.allocatedtouserid
                    LEFT JOIN OdsDepartment DEP 
                        ON DEP.id = ALLOC.allocatedToUserDepartmentId
-- 					Most recent case for the 3PY and schedule
                    JOIN ( SELECT CA.case3pyId, CA.scheduleid, MAX(CA.id) AS "caseid"
                           FROM OdsCase CA 
                           WHERE CA.status = 'Open'
                           GROUP BY CA.case3pyId, CA.scheduleid ) RECENTCASE 
                        ON RECENTCASE.caseid = CAS.id ) CASEINFO
        		ON CASEINFO.case3pyId = CAL.billingaddressid
         		AND CASEINFO.scheduleid = CAL.scheduleid           

-- Non Direct Debit Payments
	JOIN OdsCashAllocationPaymentType PYT
	ON CSH.paymentTypeId = PYT.id
	-- Limit to Credit Card and Debit Card payments
	AND PYT.cshAllcPaymentTypeCode IN ('BT','CC','CQ','DB','MT')

	JOIN OdsSystemDate ON 1 = 1 

WHERE CSH.amount > 5000 
	AND CSH.settlementDate   = systemdate	
    AND COALESCE(CAL.locationType,'') IN ('Billing address suspense','Capital prepayment','External cash','General suspense','Invoice Line','Schedule suspense','Unpaid interest and capital','')

ORDER BY CSH.settlementDate, BILLADD.name  