-- PAYREJ:  Proposals report v1.0
-- Title:   Payment Rejections
-- Summary: Provides an overview of all payment rejections in a given period, used to highlight potential technical errors (e.g. technical arrears).

SELECT 

    to_char(CASHINOUT.alfaIdentifier,'999999999') AS "ALFA_Id",
    CAPMTYP.cshAllcPaymentTypeCode          AS "Payment_type_code",
    CAPMTYP.cshAllcPaymentTypeName          AS "Payment_type",
    PAYEE.payerBillingName                  AS "Payer_Billing_Address_Name", 
    SCHEDMAIN.scheduleref                   AS "Schedule",
    CASHTXN.appliedDate                     AS "Date_applied",
    CASHTXN.effectiveDate                   AS "Date_effective", 
    CASHINOUT.settlementDate                AS "Date_settlement",
    CASHCOMP.cshAllcCompanyName             AS "Cash_allocation_company",
--  TRANTYP.transactioncode                 AS "TXN_code",
--  TRANTYP.transactionname                 AS "TXN_name",
--  CASHTXN.reason                          AS "TXN_reason",  
    CASHINOUT.amount                        AS "Amount",  
    CASHINOUT.reference                     AS "Ref",  
    CASHINOUT.externalIdentifier            AS "Ext_ref",  
    CASHINOUT.processingStatus              AS "Status"


FROM OdsCashInOut CASHINOUT 
    JOIN OdsCashAllocationTXN CASHTXN 
        ON CASHTXN.cashid = CASHINOUT.id
    JOIN OdsCashAllocation CASHALLOC 
        ON CASHALLOC.cashAllocationTransactionId = CASHTXN.id
    JOIN OdsScheduleMain SCHEDMAIN 
        ON SCHEDMAIN.id = CASHALLOC.scheduleid
    JOIN OdsCashAllocationPaymentType CAPMTYP  
        ON CAPMTYP.id = CASHINOUT.paymentTypeId
    JOIN OdsTransactionType TRANTYP 
        ON TRANTYP.id = CASHTXN.inputTxnTypeId
    JOIN OdsPayer PAYER  
        ON PAYER.id = CASHINOUT.payerPayeeId
    JOIN OdsCashAllocationCompany CASHCOMP  
        ON CASHCOMP.id = CASHINOUT.cashCompanyId
    JOIN OdsPayerBilling PAYEE  
        ON PAYEE.id = CASHINOUT.payerPayeeBillingId

WHERE CASHINOUT.processingStatus = 'Rejected'
    AND (CASHINOUT.settlementDate >= to_date(:Date_from, 'YYYY-MM-DD') 
        AND CASHINOUT.settlementDate <= to_date(:Date_to, 'YYYY-MM-DD')) 
