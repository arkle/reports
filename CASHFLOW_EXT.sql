-- CASHFLOW_EXT:    Cashflow Report - Extended Version
-- Summary:         Cashflow showing unearned income by day in selected range and dimensions of company, product, regulated
-- Parameter:       Date_from, Date_to
SELECT CASHFLOW.date AS "Date",
    CASHFLOW.instalments AS "Instalments",
    CASHFLOW.agrCompanyName AS "Agreement_Company",
    CASHFLOW.product AS "Product",
    CASE
        WHEN CASHFLOW.isCcaRegulated = '1' THEN 'Y'
        ELSE ''
    END AS "Regulated",
    ABS(CASHFLOW.income) AS "Income",
    --      TOTALVAT.check,             
    to_char(TOTALVAT.vat_amount, '999999.999') AS "VAT"
FROM (
        SELECT cashFlowEventDate AS "date",
            agrCompanyName,
            AGR.product,
            AGR.isCcaRegulated,
            SUM(repayments) AS "instalments",
            SUM(profit) AS "income",
            SUM(expenses) AS "exp"
        FROM OdsScheduleCashFlow CSH
            JOIN OdsSchedule SCHED ON SCHED.id = CSH.scheduleid
            JOIN OdsAgreement AGR ON AGR.id = SCHED.agreementid
            JOIN OdsAgreementCompany AGRCO ON AGRCO.id = SCHED.agrCompanyId
        WHERE isPrimaryAccountingStandard = false
        GROUP BY cashFlowEventDate,
            agrCompanyName,
            AGR.product,
            AGR.isCcaRegulated
        HAVING (
                cashFlowEventDate >= to_date(:Date_from, 'YYYY-MM-DD')
                AND cashFlowEventDate <= to_date(:Date_to, 'YYYY-MM-DD')
            )
    ) CASHFLOW
    LEFT JOIN (
        SELECT VAT.dueDate,
            VAT.agrCompanyName,
            VAT.product,
            VAT.isCcaRegulated,
            SUM(VAT.rcvamount) AS "check",
            SUM(vatamount) AS vat_amount
        FROM (
                SELECT RECV.dueDate,
                    AGRCO.agrCompanyName,
                    AGR.product,
                    AGR.isCcaRegulated,
                    VATRATE.rate,
                    SUM(RECV.amount) AS "rcvamount",
                    SUM(RECV.amount) *(VATRATE.rate / 100) AS "vatamount"
                FROM OdsReceivable RECV
                    JOIN OdsSchedule SCHED ON SCHED.id = RECV.scheduleid
                    JOIN OdsAgreementCompany AGRCO ON AGRCO.id = SCHED.agrCompanyId
                    JOIN OdsAgreement AGR ON AGR.id = SCHED.agreementid
                    JOIN OdsReceivableChargeType CHRGTYP ON CHRGTYP.id = RECV.recvChargeTypeId
                    AND CHRGTYP.recvchargetypecode IN (1, 2, 3, 45) -- Lease Rental, Loan/HP Repayment, Loan/HP Repayment - Interest, Lease Rental - Interest
                    JOIN OdsReceivableVatType VATTYP ON VATTYP.id = RECV.recvVatTypeId
                    JOIN (
                        SELECT B.vatTypeCode,
                            A.rate
                        FROM OdsVatRate A
                            JOIN OdsVatType B ON A.vatTypeId = B.id
                            JOIN (
                                SELECT vatTypeCode,
                                    max(rateDate) AS effectiverateDate
                                FROM OdsVatRate
                                    JOIN OdsVatType ON OdsVatType.id = OdsVatRate.vatTypeId
                                    JOIN OdsSystemDate ON 1 = 1
                                WHERE rateDate <= systemdate
                                GROUP BY vatTypeCode
                            ) C ON C.vatTypeCode = B.vatTypeCode
                            AND C.effectiverateDate = A.rateDate
                    ) VATRATE ON VATRATE.vatTypeCode = VATTYP.recvVatTypeCode
                WHERE RECV.isterminated = false
                GROUP BY RECV.dueDate,
                    AGRCO.agrCompanyName,
                    AGR.product,
                    AGR.isCcaRegulated,
                    VATRATE.rate
            ) VAT
        GROUP BY VAT.dueDate,
            VAT.agrCompanyName,
            VAT.product,
            VAT.isCcaRegulated
    ) TOTALVAT ON TOTALVAT.dueDate = CASHFLOW.date
    AND TOTALVAT.agrCompanyName = CASHFLOW.agrCompanyName
    AND TOTALVAT.product = CASHFLOW.product
    AND TOTALVAT.isCcaRegulated = CASHFLOW.isCcaRegulated
ORDER BY CASHFLOW.date