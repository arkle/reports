-- INTPROPDET:  Integration - Obtain details to load proposals to ALFA
-- Title:       Integration - Obtain details to load proposals to ALFA
-- Summary:     **INTEGRATION USE ONLY - DO NOT RUN MANUALLY**
--              Finds customer, introducer identifiers to use in the save proposal web service
--              Also checks to see if the agreement external reference already exists - indicating agreement should not be added
--              For the Supplier, Broker, and Customer External References - if no third party is found with the external reference then
--              a check is performed to see if there is a match to the primary ALFA unique reference for a third party. If that is the case then that
--              will be returned.
-- Parameters:  BROKER_EXTREF String(20), AGREEMENT_EXTREF String(20)
--              INVOICING_CUSTOMER_EXTREF String(20), EXPOSURE_CUSTOMER_EXTREF String(20)
SELECT CASE
        WHEN INVCUS.thirdpartynumber IS NOT NULL THEN INVCUS.thirdPartyNumber
        WHEN INVCUSEXIST.thirdPartyNumber IS NOT NULL THEN INVCUSEXIST.thirdPartyNumber
        ELSE ''
    END AS "INVCUS_THIRD_PARTY_NUMBER",
    CASE
        WHEN INVCUS.billingNumber IS NOT NULL THEN INVCUS.billingNumber
        WHEN INVCUSEXIST.billingNumber IS NOT NULL THEN INVCUSEXIST.billingNumber
        ELSE NULL
    END AS "INVCUS_BILLING_ADDRESS_NUMBER",
    CASE
        WHEN EXPCUS.thirdpartynumber IS NOT NULL THEN EXPCUS.thirdPartyNumber
        WHEN EXPCUSEXIST.thirdPartyNumber IS NOT NULL THEN EXPCUSEXIST.thirdPartyNumber
        ELSE ''
    END AS "EXPCUS_THIRD_PARTY_NUMBER",
    CASE
        WHEN EXPCUS.billingNumber IS NOT NULL THEN EXPCUS.billingNumber
        WHEN EXPCUSEXIST.billingNumber IS NOT NULL THEN EXPCUSEXIST.billingNumber
        ELSE NULL
    END AS "EXPCUS_BILLING_ADDRESS_NUMBER",
    CASE
        WHEN BRKR.thirdpartynumber IS NOT NULL THEN BRKR.thirdPartyNumber
        WHEN BRKREXIST.thirdPartyNumber IS NOT NULL THEN BRKREXIST.thirdPartyNumber
        ELSE ''
    END AS "BRKR_THIRD_PARTY_NUMBER",
    CASE
        WHEN BRKR.billingNumber IS NOT NULL THEN BRKR.billingNumber
        WHEN BRKREXIST.billingNumber IS NOT NULL THEN BRKREXIST.billingNumber
        ELSE NULL
    END AS "BRKR_BILLING_ADDRESS_NUMBER",
    CASE
        WHEN EXISTING.agreementid IS NOT NULL THEN 'Y'
        ELSE 'N'
    END AS "EXISTING_AGREEMENT"
FROM OdsSystemDate
    LEFT JOIN (
        SELECT agreementid
        FROM OdsAgreementMiscellaneous AGRMISC
            JOIN OdsAgreement AGR ON AGRMISC.agreementid = AGR.id
            AND AGRMISC.agrid = TRIM(:AGREEMENT_EXTREF)
    ) EXISTING ON 1 = 1
    LEFT JOIN (
        SELECT thirdPartyNumber,
            billingNumber
        FROM OdsBillingAddress
            JOIN OdsExternalSystemReference ON systemcode = 'ACR'
            AND OdsBillingAddress.thirdpartyid = OdsExternalSystemReference.thirdpartyid
            AND reference = TRIM(:INVOICING_CUSTOMER_EXTREF)
        LIMIT 1
    ) INVCUS ON 1 = 1
    LEFT JOIN (
        SELECT thirdPartyNumber,
            billingNumber
        FROM OdsBillingAddress
            JOIN OdsExternalSystemReference ON systemcode = 'ACR'
            AND OdsBillingAddress.thirdpartyid = OdsExternalSystemReference.thirdpartyid
            AND reference = TRIM(:EXPOSURE_CUSTOMER_EXTREF)
        LIMIT 1
    ) EXPCUS ON 1 = 1
    LEFT JOIN (
        SELECT thirdPartyNumber,
            billingNumber
        FROM OdsBillingAddress
            JOIN OdsExternalSystemReference ON systemcode = 'ACR'
            AND OdsBillingAddress.thirdpartyid = OdsExternalSystemReference.thirdpartyid
            AND reference = TRIM(:BROKER_EXTREF)
        LIMIT 1
    ) BRKR ON 1 = 1
    LEFT JOIN (
        SELECT thirdPartyNumber,
            billingNumber
        FROM OdsBillingAddress
        WHERE uniqueRef = TRIM(:INVOICING_CUSTOMER_EXTREF)
    ) INVCUSEXIST ON 1 = 1
    LEFT JOIN (
        SELECT thirdPartyNumber,
            billingNumber
        FROM OdsBillingAddress
        WHERE uniqueRef = TRIM(:EXPOSURE_CUSTOMER_EXTREF)
    ) EXPCUSEXIST ON 1 = 1
    LEFT JOIN (
        SELECT thirdPartyNumber,
            billingNumber
        FROM OdsBillingAddress
        WHERE uniqueRef = TRIM(:BROKER_EXTREF)
    ) BRKREXIST ON 1 = 1