-- GLEXTRACT:   GL Extract
-- Parameter:   Posting_Date
SELECT to_char(PS.glJournalNumber, '999999999') AS "journalNumber",
    to_char(PS.glpostingid, '999999999') AS "journalSequenceNumber",
    CP.code AS "company",
    '' AS "company2",
    '' AS "costCentre",
    AC.accountNumber AS "accountNumber",
    'A' AS "source",
    to_char(PS.accountingperiodyear, '9999') AS "year",
    PS.accountingperiodmonth AS "month",
    to_char(PS.inputDate, 'YYYYMMDD') AS "inputDate",
    to_char(PS.postingDate, 'YYYYMMDD') AS "postingDate",
    TRANTYPE.transactionCode AS "transactionCode",
    to_char(round(PS.amountBase, 2), '999999990D99') AS "baseAmount",
    BCUR.code AS "baseAmountCurrency",
    to_char(round(PS.amountLocal, 2), '999999990D99') AS "localAmount",
    LCUR.code AS "localAmountCurrency",
    to_char(round(PS.amountGroup, 2), '999999990D99') AS "groupAmount",
    GCUR.code AS "groupAmountCurrency",
    CASE
        WHEN CODE1.glAnalysisCode1 = '<None>' THEN ''
        ELSE CODE1.glAnalysisCode1
    END AS "analysisCode1",
    CASE
        WHEN CODE2.glAnalysisCode2 = '<None>' THEN ''
        ELSE CODE2.glAnalysisCode2
    END AS "analysisCode2",
    CASE
        WHEN ANSTR.glAnalysisString = '<None>' THEN ''
        ELSE ANSTR.glAnalysisString
    END AS "analysisString",
    concat(
        AGR.agreementnumber,
        ' ',
        to_char(SCHED.scheduleNumber, '000'),
        ' ',
        ENT.description,
        NARR.systemNarrative
    ) AS "narrative",
    '' AS "transactionId",
    PS.accountingPeriodYear,
    PS.accountingPeriodMonth,
    CASE
        WHEN MISC.agrid = '<None>' THEN ''
        ELSE MISC.agrid
    END AS "Alt_Agr"
FROM OdsPosting PS
    JOIN OdsPostingHeader HDR ON HDR.gljournalnumber = PS.glJournalNumber
    JOIN OdsPostingEntityType ENT ON ENT.id = PS.postingEntityTypeId
    JOIN OdsPostingSystemNarrative NARR ON NARR.id = HDR.postingSystemNarrativeId
    JOIN OdsSchedule SCHED ON SCHED.id = PS.scheduleId
    JOIN OdsAgreement AGR ON AGR.id = SCHED.agreementId
    LEFT JOIN OdsAgreementMiscellaneous MISC ON MISC.agreementid = AGR.id
    JOIN OdsAccount AC ON PS.glAccountId = AC.id
    JOIN OdsTransactionType TRANTYPE ON TRANTYPE.id = PS.transactionTypeId
    JOIN OdsCompany CP ON PS.companyId = CP.id
    JOIN OdsCurrency BCUR ON BCUR.id = PS.baseCurrencyId
    JOIN OdsCurrency LCUR ON LCUR.id = PS.baseCurrencyId
    JOIN OdsCurrency GCUR ON GCUR.id = PS.baseCurrencyId
    JOIN OdsCostCentre COST ON COST.id = PS.alfaCostCentreId
    JOIN OdsPostingAnalysisCodeOne CODE1 ON CODE1.id = PS.analysisCode1Id
    JOIN OdsPostingAnalysisCodeTwo CODE2 ON CODE2.id = PS.analysisCode2Id
    JOIN OdsPostingAnalysisString ANSTR ON ANSTR.id = PS.analysisStringId
WHERE PS.postingDate = to_date(:Posting_date, 'YYYY-MM-DD')
ORDER BY PS.glJournalNumber,
    PS.glpostingid