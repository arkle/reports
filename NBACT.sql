-- NBACT:       New Business Activations (Drawdowns)
-- Summary:     Provides a view of drawdowns for the date range specified
SELECT DRAWDOWN.drawdown_date AS "Drawdown_Date",
    AGRCO.agrcompanyname AS "Agreement Company",
    AGR.agreementNumber AS "Agreement",
    DLR.dealerName AS "Broker",
    CASE
        WHEN ALERT.ppo = true THEN 'Y'
        ELSE ''
    END AS "Practical_Paid_Out",
    INVCUS.invcusname AS "Customer",
    AGR.isccaregulated AS "CCA",
    AGR.product AS "Product",
    SCHED.startdate AS "Start",
    CASE
        WHEN AGR.product = 'Staged Payment Loan' THEN DRAWDOWN.drawdown_amount
        WHEN AGRMISC.stpl = true THEN SCHED.totalAssetCostFinanced - coalesce(DRAWDOWNTD.drawdown_amount_to_date, 0)
        ELSE SCHED.totalAssetCostFinanced
    END AS "Drawdown_amount",
    CASE
        WHEN AGRMISC.stpl = true
        OR AGR.product = 'Staged Payment Loan' THEN 'Y'
        ELSE ''
    END AS "StgPmt",
    SCHED.totalresidualvalue AS "RV",
    SCHED.firstrentaldate AS "First_rental",
    SCHED.currentInterestRate AS "Rate",
    SCHED.currentfundingrate AS "Funding_rate",
    SCHED.combinedLessorPtoRate AS "Yield",
    DOCSRCV.docsreceived AS "Documents_received",
    DRAWDOWN.drawdown_date - DOCSRCV.docsreceived AS "Received_to_Drawdown_Days",
    ACTUSER.activatedbyusername AS "Activated_by",
    CASE
        WHEN ALERT.rls = true THEN 'Y'
        ELSE ''
    END AS "RLS",
    ASSET.assetDescription AS "Asset_Description",
    ASSET.assetTypeName AS "Asset_Type_Name",
    CASE WHEN ASSET.vehicleRegistration = '<None>' THEN '' ELSE ASSET.vehicleRegistration END AS "Registration_Number",
    CASE WHEN ASSET.serialNumber = '<None>' THEN '' ELSE ASSET.serialNumber END AS "Chassis_Number",
    CASE
        WHEN ALERT.atr = true THEN 'Y'
        ELSE ''
    END AS "Asset_Tag_Reqd",
    CASE
        WHEN AGRALERT.oop = true THEN 'Y'
        ELSE ''
    END AS "Credit - Policy Exceptions",
    CASE
        WHEN AGRALERT.oop2 = true THEN 'Y'
        ELSE ''
    END AS "Credit - Out of Policy"
FROM OdsSchedule SCHED
    JOIN OdsAgreement AGR ON AGR.id = SCHED.agreementid
    JOIN OdsAsset ASSET ON ASSET.agreementId = AGR.id
    AND ASSET.isMainAsset = true
    JOIN OdsInvoicingCustomer INVCUS ON INVCUS.id = AGR.invcusid
    JOIN OdsDealer DLR ON DLR.id = AGR.dealerId
    JOIN OdsActivatedByUser ACTUSER ON ACTUSER.id = SCHED.activatedbyuserid
    JOIN OdsAgreementCompany AGRCO ON AGRCO.id = AGR.agrcompanyid
    LEFT JOIN OdsAgreementAlert AGRALERT ON AGRALERT.agreementId = AGR.id
    LEFT JOIN (
        SELECT agreementid,
            stpl
        FROM OdsAgreementMiscellaneous
        WHERE stpl = true
    ) AGRMISC ON AGRMISC.agreementid = AGR.id
    LEFT JOIN (
        SELECT agreementnumber,
            MIN(startdate) AS "docsreceived"
        FROM OdsCase
            JOIN OdsAgreement ON OdsAgreement.id = OdsCase.agreementid
            JOIN OdsCaseHistory ON OdsCaseHistory.caseid = OdsCase.id
            JOIN OdsStartingCaseStatus ON OdsStartingCaseStatus.id = OdsCaseHistory.startstatusid
        WHERE OdsCase.status = 'Closed'
            AND OdsStartingCaseStatus.startstatustypecode = 'ORG'
            AND OdsStartingCaseStatus.startstatusstatuscode = '025'
        GROUP BY agreementnumber
    ) DOCSRCV ON DOCSRCV.agreementnumber = AGR.agreementnumber
    LEFT JOIN OdsAgreementAlert ALERT ON ALERT.agreementid = AGR.id
    LEFT JOIN (
        SELECT scheduleId,
            assetDrawdownDate AS "drawdown_date",
            SUM(assetDrawdownAmount) AS "drawdown_amount"
        FROM OdsAsset
        GROUP BY scheduleId,
            assetDrawdownDate
    ) DRAWDOWN ON DRAWDOWN.scheduleid = SCHED.id
    LEFT JOIN (
        SELECT scheduleId,
            SUM(assetDrawdownAmount) AS "drawdown_amount_to_date"
        FROM OdsAsset
            JOIN OdsSystemDate ON 1 = 1
        WHERE assetDrawdownDate <= to_date(:Drawdown_to, 'YYYY-MM-DD')
        GROUP BY scheduleId
    ) DRAWDOWNTD ON DRAWDOWNTD.scheduleid = SCHED.id
WHERE DRAWDOWN.drawdown_date >= to_date(:Drawdown_from, 'YYYY-MM-DD')
    AND DRAWDOWN.drawdown_date <= to_date(:Drawdown_to, 'YYYY-MM-DD')
    AND (
        ALERT.ppo = true
        OR SCHED.scheduleStatus != 'Proposal'
    )
ORDER BY DRAWDOWN.drawdown_date,
    AGRCO.agrcompanyname,
    AGR.agreementNumber