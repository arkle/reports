-- ZDW_THIRDPARTY: Third Party Extract for Data Warehouse
SELECT BILL.uniqueRef AS "third_party_uniqueref",
    EXTREF.reference AS "alt_third_party_uniqueref",
    BILL.name AS "name",
    BILL.postalcode AS "postcode",
    TPY.thirdPartyType AS "type",
    CASE
        WHEN TPY.companyRegNo = '<None>' THEN ''
        ELSE TPY.companyRegNo
    END AS "company",
    '' AS "owner",
    CASE
        WHEN TPY.industryCode = '<None>' THEN 'NS'
        ELSE TPY.industryCode
    END AS "sector",
    CASE
        WHEN ALERT.prb = '1' THEN true
        ELSE false
    END AS "priority"
FROM eodBillingAddress BILL
    LEFT JOIN eodBillingAddressAlert ALERT ON ALERT.billingAddressId = BILL.odsid
    JOIN eodThirdParty TPY ON TPY.odsid = BILL.thirdPartyId
    LEFT JOIN (
        SELECT thirdpartyid,
            reference
        FROM eodExternalSystemReference
        WHERE systemcode = 'ACR'
    ) EXTREF ON EXTREF.thirdpartyid = BILL.thirdPartyId
WHERE BILL.uniqueRef NOT IN ('<None>', '<Unknown>')